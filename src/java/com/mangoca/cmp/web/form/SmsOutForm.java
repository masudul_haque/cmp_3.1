/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;


/**
 *
 * @author Masudul Haque
 */
public class SmsOutForm extends org.apache.struts.validator.ValidatorForm {
    
    private String receiver;
    private String sender;
    private String status;
    private String msg;
    private String reference;

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    
   
}
