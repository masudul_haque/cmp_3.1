/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;


import org.apache.struts.validator.ValidatorForm;

/**
 *
 * @author Masudul Haque
 */
public class RevocationForm extends ValidatorForm {
    
    private String userCIN;
    private String reasons;
    private String revokComment;

    public String getUserCIN() {
        return userCIN;
    }

    public void setUserCIN(String userCIN) {
        this.userCIN = userCIN;
    }
    
    
    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reasons) {
        this.reasons = reasons;
    }

    public String getRevokComment() {
        return revokComment;
    }

    public void setRevokComment(String revokComment) {
        this.revokComment = revokComment;
    }

     
    
}
