/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import com.mangoca.cmp.model.Notification;
import java.util.List;

/**
 *
 * @author Masudul Haque
 */
public class NotificationList  extends org.apache.struts.action.ActionForm{
    private List<Notification> notifications;

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    
    
    
}
