/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import org.apache.struts.validator.ValidatorForm;

/**
 *
 * @author hp
 */
public class EnrolleduserForm extends ValidatorForm {

    //private String certificatesArray[];
    private Integer certId;
    private String certEmail;
    private String certEmailDomain;
    private String certPassword;
    private String verifyEmail;
    private String verifyEmailDomain;
    private String verifyPassword;
    private String commonName;
    private String orgUnit;
    private String organization;
    private String birthDate;
    private String birthMonth;
    private String birthYear;
    private String parmanentAddress;
    private String postalCode;
    private String presentAddress;
    private String countryUnit;
    private String locality;
    private String renewComment;
    private String domainName;
    private String ipAddress;
    private Integer sslId;
    private String csr;    
    private String certType;
    private String serialNo;
    private String certUserName;
    private String hashContent;
    

    public EnrolleduserForm() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(String birthMonth) {
        this.birthMonth = birthMonth;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }
    
    public String getCertEmail() {
        return certEmail;
    }

    public void setCertEmail(String certEmail) {
        this.certEmail = certEmail;
    }

    public String getCertEmailDomain() {
        return certEmailDomain;
    }

    public void setCertEmailDomain(String certEmailDomain) {
        this.certEmailDomain = certEmailDomain;
    }

    public String getCertPassword() {
        return certPassword;
    }

    public void setCertPassword(String certPassword) {
        this.certPassword = certPassword;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getCountryUnit() {
        return countryUnit;
    }

    public void setCountryUnit(String countryUnit) {
        this.countryUnit = countryUnit;
    }

    public String getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(String orgUnit) {
        this.orgUnit = orgUnit;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getParmanentAddress() {
        return parmanentAddress;
    }

    public void setParmanentAddress(String parmanentAddress) {
        this.parmanentAddress = parmanentAddress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getVerifyEmail() {
        return verifyEmail;
    }

    public void setVerifyEmail(String verifyEmail) {
        this.verifyEmail = verifyEmail;
    }

    public String getVerifyEmailDomain() {
        return verifyEmailDomain;
    }

    public void setVerifyEmailDomain(String verifyEmailDomain) {
        this.verifyEmailDomain = verifyEmailDomain;
    }

    public String getVerifyPassword() {
        return verifyPassword;
    }

    public void setVerifyPassword(String verifyPassword) {
        this.verifyPassword = verifyPassword;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }
    
    public String getRenewComment() {
        return renewComment;
    }

    public void setRenewComment(String renewComment) {
        this.renewComment = renewComment;
    }

    public String getCsr() {
        return csr;
    }

    public void setCsr(String csr) {
        this.csr = csr;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Integer getCertId() {
        return certId;
    }

    public void setCertId(Integer certId) {
        this.certId = certId;
    }

    public Integer getSslId() {
        return sslId;
    }

    public void setSslId(Integer sslId) {
        this.sslId = sslId;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getCertUserName() {
        return certUserName;
    }

    public void setCertUserName(String certUserName) {
        this.certUserName = certUserName;
    }

    public java.lang.String getHashContent() {
        return hashContent;
    }

    public void setHashContent(java.lang.String hashContent) {
        this.hashContent = hashContent;
    }
    
    
    
}
