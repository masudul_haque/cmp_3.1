/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import com.mangoca.cmp.model.Mangocert;
import java.util.List;

/**
 *
 * @author Masudul Haque
 */
public class MangocertList  extends org.apache.struts.action.ActionForm{
    private List<Mangocert> mangocerts;

    public List<Mangocert> getMangocerts() {
        return mangocerts;
    }

    public void setMangocerts(List<Mangocert> mangocerts) {
        this.mangocerts = mangocerts;
    }
    
    
}
