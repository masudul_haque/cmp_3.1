/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import org.apache.struts.validator.ValidatorForm;

/**
 *
 * @author Masudul Haque
 */
public class SuspensionForm extends ValidatorForm {
    
     private String reasons;
     private String suspendComment;

    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reasons) {
        this.reasons = reasons;
    }

    public String getSuspendComment() {
        return suspendComment;
    }

    public void setSuspendComment(String suspendComment) {
        this.suspendComment = suspendComment;
    }
     
     
    
}
