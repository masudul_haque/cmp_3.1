/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import com.mangoca.cmp.database.Constants;

/**
 *
 * @author Masudul Haque
 */
public class ForgotForm extends org.apache.struts.validator.ValidatorForm {
    
    private String forgotOption=Constants.FORGOT_USERNAME;
    private String userCIN;
    private String emailAccount;

    public String getEmailAccount() {
        return emailAccount;
    }

    public void setEmailAccount(String emailAccount) {
        this.emailAccount = emailAccount;
    }

    public String getForgotOption() {
        return forgotOption;
    }

    public void setForgotOption(String forgotOption) {
        this.forgotOption = forgotOption;
    }

    public String getUserCIN() {
        return userCIN;
    }

    public void setUserCIN(String userCIN) {
        this.userCIN = userCIN;
    }
   
    
}
