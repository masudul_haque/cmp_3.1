/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import org.apache.struts.validator.ValidatorForm;

/**
 *
 * @author hp
 */
public class UserAccountForm extends ValidatorForm {
  
    private String fullName;
    private String mobileNo;
    private String emailAccount;
    private String userName;
    private String password;
    private String confPassword;
    private String userType;
    private String raSelect;
    private String captchaText;

    
    public String getCaptchaText() {
        return captchaText;
    }

    public void setCaptchaText(String captchaText) {
        this.captchaText = captchaText;
    }

    public String getConfPassword() {
        return confPassword;
    }

    public void setConfPassword(String confPassword) {
        this.confPassword = confPassword;
    }

    public String getEmailAccount() {
        return emailAccount;
    }

    public void setEmailAccount(String emailAccount) {
        this.emailAccount = emailAccount;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRaSelect() {
        return raSelect;
    }

    public void setRaSelect(String raSelect) {
        this.raSelect = raSelect;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
    
    public UserAccountForm() {
        super();
        // TODO Auto-generated constructor stub
    }
    

}
