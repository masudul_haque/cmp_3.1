/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import com.mangoca.cmp.model.Enrolleduser;
import java.util.List;

/**
 *
 * @author Masudul Haque
 */
public class EnrolleduserList extends org.apache.struts.action.ActionForm { 
    
    
   private List<Enrolleduser> enrolledusers;

    public List<Enrolleduser> getEnrolledusers() { 
        return enrolledusers;
    }

    public void setEnrolledusers(List<Enrolleduser> enrolledusers) {
        this.enrolledusers = enrolledusers;
    }
   
   
}
