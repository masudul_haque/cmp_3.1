/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import org.apache.struts.upload.FormFile;

/**
 *
 * @author Masudul Haque
 */
public class DocumentForm extends org.apache.struts.action.ActionForm {
    private String userCIN; 
    private FormFile[] files;
    
    private Short status;
    private String comment;

    public FormFile[] getFiles() {
        return files;
    }

    public void setFiles(FormFile[] files) {
        this.files = files;
    }

    public String getUserCIN() {
        return userCIN;
    }

    public void setUserCIN(String userCIN) {
        this.userCIN = userCIN;
    }
   
    
    
    public DocumentForm() {
        files = new FormFile[5];
    }

    public FormFile getFile(int i) {
        return files[i];
    }
    
    public void setFile(int i, FormFile f) {
        files[i] = f;
    }
    
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    
}
