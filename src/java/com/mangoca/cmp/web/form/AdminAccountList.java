/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import com.mangoca.cmp.model.AdminAccount;
import java.util.List;

/**
 *
 * @author Masudul Haque
 */
public class AdminAccountList  extends org.apache.struts.action.ActionForm{
    private List<AdminAccount> adminAccounts;

    public List<AdminAccount> getAdminAccounts() {
        return adminAccounts;
    }

    public void setAdminAccounts(List<AdminAccount> adminAccounts) {
        this.adminAccounts = adminAccounts;
    }
    
    
}
