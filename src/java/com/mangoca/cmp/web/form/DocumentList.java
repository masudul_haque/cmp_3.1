/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import com.mangoca.cmp.model.Document;
import java.util.List;

/**
 *
 * @author Masudul Haque
 */
public class DocumentList  extends org.apache.struts.action.ActionForm{
    
    private List<Document> documents;

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }
    
    
}
