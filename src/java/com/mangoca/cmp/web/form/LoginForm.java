/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;
import org.apache.struts.validator.ValidatorForm;

/**
 * 
 * @author hp
 */
public class LoginForm extends ValidatorForm {
   
    private String userName;
    private String password;
    private boolean isAdmin=false;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
    
    
    public LoginForm() {
        super(); 
    }

}
