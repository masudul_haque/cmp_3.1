/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import com.mangoca.cmp.model.UserAccount;
import java.util.List;

/**
 *
 * @author Masudul Haque
 */
public class UserAccountList   extends org.apache.struts.action.ActionForm{
    
    private List<UserAccount> userAccounts;

    public List<UserAccount> getUserAccounts() {
        return userAccounts;
    }

    public void setUserAccounts(List<UserAccount> userAccounts) {
        this.userAccounts = userAccounts;
    }
    
    
}
