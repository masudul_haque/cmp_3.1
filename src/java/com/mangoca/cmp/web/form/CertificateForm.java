/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

/**
 *
 * @author Masudul Haque
 */
public class CertificateForm extends org.apache.struts.action.ActionForm {
    private Integer certId;
    private String certEmail;
    private String userCIN;
    private String certEmailDomain;
    private String certPassword;
    private String verifyEmail;
    private String verifyEmailDomain;
    private String verifyPassword;
    private String commonName;
    private String orgUnit;
    private String organization;
    private String postalCode;
    private String countryUnit;
    private String locality;
    private String renewComment;
    private String domainName;
    private String ipAddress;
    private Integer sslId;
    private String csr;
    private Short status;
    private String comment;

    public String getCertEmail() {
        return certEmail;
    }

    public void setCertEmail(String certEmail) {
        this.certEmail = certEmail;
    }

    public String getCertEmailDomain() {
        return certEmailDomain;
    }

    public void setCertEmailDomain(String certEmailDomain) {
        this.certEmailDomain = certEmailDomain;
    }

    public Integer getCertId() {
        return certId;
    }

    public void setCertId(Integer certId) {
        this.certId = certId;
    }

    public String getCertPassword() {
        return certPassword;
    }

    public void setCertPassword(String certPassword) {
        this.certPassword = certPassword;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getCountryUnit() {
        return countryUnit;
    }

    public void setCountryUnit(String countryUnit) {
        this.countryUnit = countryUnit;
    }

    public String getCsr() {
        return csr;
    }

    public void setCsr(String csr) {
        this.csr = csr;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(String orgUnit) {
        this.orgUnit = orgUnit;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getRenewComment() {
        return renewComment;
    }

    public void setRenewComment(String renewComment) {
        this.renewComment = renewComment;
    }

    public Integer getSslId() {
        return sslId;
    }

    public void setSslId(Integer sslId) {
        this.sslId = sslId;
    }

    public String getUserCIN() {
        return userCIN;
    }

    public void setUserCIN(String userCIN) {
        this.userCIN = userCIN;
    }

    public String getVerifyEmail() {
        return verifyEmail;
    }

    public void setVerifyEmail(String verifyEmail) {
        this.verifyEmail = verifyEmail;
    }

    public String getVerifyEmailDomain() {
        return verifyEmailDomain;
    }

    public void setVerifyEmailDomain(String verifyEmailDomain) {
        this.verifyEmailDomain = verifyEmailDomain;
    }

    public String getVerifyPassword() {
        return verifyPassword;
    }

    public void setVerifyPassword(String verifyPassword) {
        this.verifyPassword = verifyPassword;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }
    
    
}
