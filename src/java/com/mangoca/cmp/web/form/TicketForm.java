/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;


/**
 *
 * @author Masudul Haque
 */
public class TicketForm extends org.apache.struts.validator.ValidatorForm {
    private String userCIN;
    private String ticketPhone;
    private String helpTopics;
    private String subject;
    private String message;
    private String priority;
    private Short status;
    private String replyMessage;
    
    public String getHelpTopics() {
        return helpTopics;
    }

    public void setHelpTopics(String helpTopics) {
        this.helpTopics = helpTopics;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }
 
    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTicketPhone() {
        return ticketPhone;
    }

    public void setTicketPhone(String ticketPhone) {
        this.ticketPhone = ticketPhone;
    }

    public String getUserCIN() {
        return userCIN;
    }

    public void setUserCIN(String userCIN) {
        this.userCIN = userCIN;
    }
    
    public String getReplyMessage() {
        return replyMessage;
    }

    public void setReplyMessage(String replyMessage) {
        this.replyMessage = replyMessage;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

  
    
}
