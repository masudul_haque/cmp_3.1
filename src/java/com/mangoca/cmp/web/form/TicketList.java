/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import com.mangoca.cmp.model.Ticket;
import java.util.List;

/**
 *
 * @author Masudul Haque
 */
public class TicketList   extends org.apache.struts.action.ActionForm{
     private List<Ticket> tickets;

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }
     
     
}
