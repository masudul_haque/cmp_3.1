/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import org.apache.struts.validator.ValidatorForm;

/**
 *
 * @author Masudul Haque
 */
public class PinForm extends ValidatorForm {
        
    private String userCIN;
    private String mobileNo;
    private String smsPin;
    private String confSmsPin;

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getUserCIN() {
        return userCIN;
    }

    public void setUserCIN(String userCIN) {
        this.userCIN = userCIN;
    }

    
    
    public String getConfSmsPin() {
        return confSmsPin;
    }

    public void setConfSmsPin(String confSmsPin) {
        this.confSmsPin = confSmsPin;
    }

    public String getSmsPin() {
        return smsPin;
    }

    public void setSmsPin(String smsPin) {
        this.smsPin = smsPin;
    }

    

    
     
    
    
}
