/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import org.apache.struts.upload.FormFile;


/**
 *
 * @author Masudul Haque
 */
public class EmailControlForm extends org.apache.struts.validator.ValidatorForm {
   
    private Short emailType;
    private Integer receiverRole;
    private String subject;
    private String header;
    private String msgBody;
    private String footer;
    
    private String attachmentName;
    private FormFile attachFile;

    public Short getEmailType() {
        return emailType;
    }

    public void setEmailType(Short emailType) {
        this.emailType = emailType;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public void setMsgBody(String msgBody) {
        this.msgBody = msgBody;
    }

    public Integer getReceiverRole() {
        return receiverRole;
    }

    public void setReceiverRole(Integer receiverRole) {
        this.receiverRole = receiverRole;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public FormFile getAttachFile() {
        return attachFile;
    }

    public void setAttachFile(FormFile attachFile) {
        this.attachFile = attachFile;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }
    
    
}
