/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import com.mangoca.cmp.database.Constants;

/**
 *
 * @author Masudul Haque
 */
public class PaymentForm extends org.apache.struts.action.ActionForm {
    
    private String userCIN;
    private String requestType;
    private String bankName;
    private String branchName;
    private String depositorName;
    private Double amount;
    private String paymentDateStr;
    private String receivedDateStr;
    private Double balance;
    private String accountNo;
    private String moneyReceivedNo;
    private String accStatus=Constants.PAYMENT_PAID;
    private String status;
    private String comment;

    public String getUserCIN() {
        return userCIN;
    }

    public void setUserCIN(String userCIN) {
        this.userCIN = userCIN;
    }
    
    
    
    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    
    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getPaymentDateStr() {
        return paymentDateStr;
    }

    public void setPaymentDateStr(String paymentDateStr) {
        this.paymentDateStr = paymentDateStr;
    }

    public String getReceivedDateStr() {
        return receivedDateStr;
    }

    public void setReceivedDateStr(String receivedDateStr) {
        this.receivedDateStr = receivedDateStr;
    }
     
    
    
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDepositorName() {
        return depositorName;
    }

    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

    public String getMoneyReceivedNo() {
        return moneyReceivedNo;
    }

    public void setMoneyReceivedNo(String moneyReceivedNo) {
        this.moneyReceivedNo = moneyReceivedNo;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }
    
}
