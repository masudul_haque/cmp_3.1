/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import org.apache.struts.upload.FormFile;

/**
 *
 * @author Masudul Haque
 */
public class EditDocForm extends org.apache.struts.action.ActionForm {
    
    private Long documentId;
    private String documentName;
    private String documentStatus;
    private String documentType;
    private String raComment;
    private String vaComment;
    
    private FormFile formFile;

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentStatus() {
        return documentStatus;
    }

    public void setDocumentStatus(String documentStatus) {
        this.documentStatus = documentStatus;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getRaComment() {
        return raComment;
    }

    public void setRaComment(String raComment) {
        this.raComment = raComment;
    }

    public String getVaComment() {
        return vaComment;
    }

    public void setVaComment(String vaComment) {
        this.vaComment = vaComment;
    }

    public FormFile getFormFile() {
        return formFile;
    }

    public void setFormFile(FormFile formFile) {
        this.formFile = formFile;
    }
    
    
}
