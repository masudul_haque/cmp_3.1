/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;


/**
 *
 * @author hp
 */
public class AdvanceSearchForm extends org.apache.struts.action.ActionForm {
    
    private String userName=null;
    private String emailAccount=null;
    private String mobileNo=null;
    private String certClass=null;
    private String applicationStart;
    private String applicationEnd;
    
    
    public AdvanceSearchForm() {
        super();
        // TODO Auto-generated constructor stub
    }

   

    public String getApplicationEnd() {
        return applicationEnd;
    }

    public void setApplicationEnd(String applicationEnd) {
        this.applicationEnd = applicationEnd;
    }

    public String getApplicationStart() {
        return applicationStart;
    }

    public void setApplicationStart(String applicationStart) {
        this.applicationStart = applicationStart;
    }

    public String getCertClass() {
        return certClass;
    }

    public void setCertClass(String certClass) {
        this.certClass = certClass;
    }

    public String getEmailAccount() {
        return emailAccount;
    }

    public void setEmailAccount(String emailAccount) {
        this.emailAccount = emailAccount;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    
    
}
