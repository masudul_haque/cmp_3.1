/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.form;

import com.mangoca.cmp.model.Payment;
import java.util.List;

/**
 *
 * @author Masudul Haque
 */
public class PaymentList   extends org.apache.struts.action.ActionForm{
    
    private List<Payment> payments;

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }
    
    
}
