/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.DataHelper;
import com.mangoca.cmp.model.Ticket;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.ITicketService;
import com.mangoca.cmp.service.spring.TicketSpringService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class TicketData extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "next";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
       // request.setAttribute("page", Constants.TICKET);
        HttpSession session=request.getSession();
        int page = DataHelper.getPage(request);
        int size = Ticket.PAGE_SIZE;
        ITicketService service = TicketSpringService.getInstance();
        List collection = null;
        Short status=(Short) session.getAttribute("ticketStatus");
        Integer userRole= (Integer) session.getAttribute("userRole");
                
        int resultSize = service.countTicketByStatus(status);
        
        if(status==Ticket.STATUS_OPEN){
            
            collection=service.getTicketDataByStatus(status, page, size);
        }
        else if(status==Ticket.STATUS_RE_OPEN){
            collection=service.getTicketDataByStatus(status, page, size);
        }
        else if(status==Ticket.STATUS_CLOSED){
            collection=service.getTicketDataByStatus(status, page, size);
        }
        else if(userRole==User.USER_ENROLL){
            User user=  (User) session.getAttribute("user");
            resultSize=service.countTicketByUserName(user.getUserName());
            collection=service.getTicketDataByUsername(user.getUserName(), page, size);
        }
        
        
        request.setAttribute("resultSize", resultSize);
        request.setAttribute("collection", collection);
        return mapping.findForward(SUCCESS);
    }
}
