/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MailHelper;
import com.mangoca.cmp.model.Notification;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.IMangoCertService;
import com.mangoca.cmp.service.INotificationService;
import com.mangoca.cmp.service.ITicketService;
import com.mangoca.cmp.service.spring.MangoCertSpringService;
import com.mangoca.cmp.service.spring.NotificationSpringService;
import com.mangoca.cmp.service.spring.TicketSpringService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class UserMenuAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String USER_HOME="user_home";
    private final static String APPLICATION="application";
    private final static String USER_STATUS="user_status";
    private final static String REVOCATION="revocation";
    private final static String SUSPENSION="suspension";
    private final static String RENEWAL="renewal";
    private final static String BILLING="billing";
    private final static String SETTING="setting";


    public ActionForward userHome(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        IMangoCertService service=MangoCertSpringService.getInstance();
        List mangoCerts=service.findAllMangocerts();
        request.setAttribute("mangoCerts", mangoCerts );
        HttpSession session= request.getSession();
        UserAccount userAccount= (UserAccount) session.getAttribute("userAccount");
        if(userAccount!=null){
        INotificationService notificationService= NotificationSpringService.getInstance();
        List<Notification> notifications= notificationService.findNotificationsByCIN(userAccount.getUserCIN());
        session.setAttribute("notifySize", notifications.size());
        session.setAttribute("notifications", notifications);
          if(session.getAttribute("enrolleduser")!=null){
              MailHelper.loadEmailAttachments(request);
          }
        }
        return mapping.findForward(USER_HOME);
    }

    
    public ActionForward application(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(APPLICATION);
    }
    public ActionForward userStatus(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(USER_STATUS);
    }
    public ActionForward revocation(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(REVOCATION);
    }
    public ActionForward suspension(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(SUSPENSION);
    }
    public ActionForward renewal(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(RENEWAL);
    }
    public ActionForward billing(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(BILLING);
    }
    public ActionForward ticket(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        HttpSession session= request.getSession();
        UserAccount userAccount= (UserAccount) session.getAttribute("userAccount");
       // String userName= (String) session.getAttribute("userName");
//        ApplicationContext context= getWebApplicationContext();
        ITicketService clientTicketService=TicketSpringService.getInstance();
        
        List clientTickets= clientTicketService.findTicketsByUsername(userAccount.getUserName());
        
        session.setAttribute("clientTickets", clientTickets);
        
        ActionForward forward= new ActionForward();
        forward.setPath("/clientticketData.do");
        return forward;
        //return mapping.findForward(TICKET);//forward;
    }
    public ActionForward setting(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(SETTING);
    }
    
    
    public ActionForward sendAttachment(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        UserAccount userAccount = (UserAccount) session.getAttribute("userAccount");
       /* if (userAccount != null) {
            String certClass = request.getParameter("certClass");
            IEmailControlService controlService = EmailControlSpringService.getInstance();
            EmailControl emailControl = null;
            if ("1".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_1_IND_ATTACHMENT);
            } else if ("2".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_2_IND_ATTACHMENT);
            } else if ("3".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_2_ENT_ATTACHMENT);
            } else if ("4".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_3_IND_ATTACHMENT);
            } else if ("5".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_3_ENT_ATTACHMENT);
            } else if ("6".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_3_SERVER_ATTACHMENT);
            }

            String msgBody = MailHelper.certAttachMailBody(userAccount, emailControl.getMsgBody()); 
            boolean isSend=MailEngine.isMailSendWithAttchement(emailControl.getSubject(), userAccount.getEmailAccount(), 
                    msgBody, emailControl.getAttachmentName());
            if(isSend){
                request.setAttribute("success", "cert.attachment.send.success");
            }
        }
        * 
        */
        return mapping.findForward("success");
    }
}
