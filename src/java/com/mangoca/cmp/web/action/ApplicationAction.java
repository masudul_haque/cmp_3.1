/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
//import org.springframework.context.ApplicationContext;
//import org.springframework.web.struts.DispatchActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class ApplicationAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String REVOKE_ACTION = "revoke_action";
    private final static String RENEWAL_ACTION = "renewal_action";
    private final static String SUSPENSION_ACTION = "suspension_action";
    private final static String REVOKE_DETAILS = "revoke_details";
    private final static String RENEWAL_DETAILS = "renewal_details";
    private final static String SUSPENSION_DETAILS = "suspension_details";

    public ActionForward revokeAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        getEnrolledUser(request);
        return mapping.findForward(REVOKE_ACTION);
    }

    public ActionForward renewalAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        getEnrolledUser(request);
        return mapping.findForward(RENEWAL_ACTION);
    }

    public ActionForward suspensionAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        getEnrolledUser(request);
        return mapping.findForward(SUSPENSION_ACTION);
    }

    public ActionForward revokeDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        getEnrolledUser(request);
        return mapping.findForward(REVOKE_DETAILS);
    }

    public ActionForward renewalDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        getEnrolledUser(request);
        return mapping.findForward(RENEWAL_DETAILS);
    }

    public ActionForward suspensionDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        getEnrolledUser(request);
        return mapping.findForward(SUSPENSION_DETAILS);
    }

    private void getEnrolledUser(HttpServletRequest request) throws Exception {

        String enrollId = request.getParameter("enrollId");
        Long id = MyConverter.strToLong(enrollId);
        //ApplicationContext context = getWebApplicationContext();
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();

        Enrolleduser enrolleduser = enrolleduserService.findEnrolleduserById(id);
       // HttpSession session = request.getSession();

        request.setAttribute("enrolleduser", enrolleduser);

    }
}
