/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.Mangocert;
import com.mangoca.cmp.service.IMangoCertService;
import com.mangoca.cmp.service.spring.MangoCertSpringService;
import com.mangoca.cmp.web.form.MangocertForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class MangocertAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";

    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setMangocert(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setMangocert(request);
        return mapping.findForward(VIEW);
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();

        String userName = (String) session.getAttribute("userName");

//        ApplicationContext context= getWebApplicationContext();
        IMangoCertService certService = MangoCertSpringService.getInstance();
        MangocertForm mangocertForm = (MangocertForm) form;
        String idStr = request.getParameter("id");
        int id = MyConverter.strToInt(idStr);
        Mangocert mangocert = null;

        if (id != 0) {
            mangocert = certService.findMangocertById(id);
        } else {
            mangocert = new Mangocert();
        }
        BeanUtils.copyProperties(mangocert, mangocertForm);
        mangocert.setUpdatedDate(new Date());
        mangocert.setUpdatedBy(userName);
        certService.persistMangocert(mangocert);

        request.setAttribute("success", "save.success.Mangocert");
        ActionForward forward = new ActionForward();
        forward.setPath("/MangocertData.do");
        return forward;
    }

    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionForward forward = new ActionForward();
        forward.setPath("/MangocertData.do");
        return forward;
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String idStr = request.getParameter("id");
        Integer id = MyConverter.strToInt(idStr);
        IMangoCertService certService = MangoCertSpringService.getInstance();
        Mangocert mangocert = certService.findMangocertById(id);
        certService.removeMangocert(mangocert);
        
        ActionForward forward = new ActionForward();
        forward.setPath("/MangocertData.do");
        return forward;
    }

    private void setMangocert(HttpServletRequest request) throws Exception {

        String idStr = request.getParameter("id");
        Integer id = MyConverter.strToInt(idStr);
        IMangoCertService certService = MangoCertSpringService.getInstance();

        Mangocert mangocert = certService.findMangocertById(id);

        request.setAttribute("mangocert", mangocert);
    }
}
