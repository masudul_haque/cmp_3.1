/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.SmsOut;
import com.mangoca.cmp.service.ISmsOutService;
import com.mangoca.cmp.service.spring.SmsOutSpringService;
import com.mangoca.cmp.web.form.SmsOutForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
//import org.springframework.web.struts.DispatchActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class SmsOutAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";

    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setSmsOut(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setSmsOut(request);
        return mapping.findForward(VIEW);
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();

        String userName = (String) session.getAttribute("userName");
        ISmsOutService certService = SmsOutSpringService.getInstance();
        SmsOutForm SmsOutForm = (SmsOutForm) form;
        String idStr = request.getParameter("id");
        int id = MyConverter.strToInt(idStr);
        SmsOut smsOut = null;

        if (id != 0) {
            smsOut = certService.findSmsOutById(id);
        } else {
            smsOut = new SmsOut();
        }
        BeanUtils.copyProperties(smsOut, SmsOutForm);
        smsOut.setSender(userName);
        smsOut.setSenttime(new Date().toString());
        certService.persistSmsOut(smsOut);

        request.setAttribute("success", "save.success.SmsOut");
        ActionForward forward = new ActionForward();
        forward.setPath("/SmsOutData.do");
        return forward;
    }

    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionForward forward = new ActionForward();
        forward.setPath("/SmsOutData.do");
        return forward;
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String idStr = request.getParameter("id");
        Integer id = MyConverter.strToInt(idStr);
        ISmsOutService service = SmsOutSpringService.getInstance();
        SmsOut smsOut = service.findSmsOutById(id);
        service.removeSmsOut(smsOut);
        ActionForward forward = new ActionForward();
        forward.setPath("/SmsOutData.do");
        return forward;
    }

    private void setSmsOut(HttpServletRequest request) throws Exception {

        String idStr = request.getParameter("id");
        Integer id = MyConverter.strToInt(idStr);
        ISmsOutService certService = SmsOutSpringService.getInstance();

        SmsOut smsOut = certService.findSmsOutById(id);

        request.setAttribute("smsOut", smsOut);
    }
}
