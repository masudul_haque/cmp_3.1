/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.helper.DataHelper;
import com.mangoca.cmp.model.EmailControl;
import com.mangoca.cmp.service.IEmailControlService;
import com.mangoca.cmp.service.spring.EmailControlSpringService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Masudul Haque
 */
public class EmailControlData extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "next";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        request.setAttribute("page", Constants.MANGO_CERT);
        int page = DataHelper.getPage(request);
        int size = EmailControl.PAGE_SIZE;
//        ApplicationContext context = getWebApplicationContext();
        IEmailControlService service = EmailControlSpringService.getInstance();
        List collection = service.getEmailControlData(page, size);
        int resultSize = (int) service.getResultSize();
        
        request.setAttribute("resultSize", resultSize);
        request.setAttribute("collection", collection);
        return mapping.findForward(SUCCESS);
    }
}
