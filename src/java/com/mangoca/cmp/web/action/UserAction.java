/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MailEngine;
import com.mangoca.cmp.helper.MailHelper;
import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.EmailControl;
import com.mangoca.cmp.model.Ticket;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.IEmailControlService;
import com.mangoca.cmp.service.ITicketService;
import com.mangoca.cmp.service.IUserAccountService;
import com.mangoca.cmp.service.spring.EmailControlSpringService;
import com.mangoca.cmp.service.spring.TicketSpringService;
import com.mangoca.cmp.service.spring.UserAccountSpringService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
//import org.springframework.web.struts.DispatchActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class UserAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String NEW_TICKET = "new_ticket";
    private final static String REPLY_TICKET = "reply_ticket";
    private final static String CERT_CLASS = "cert_class";
    private final static String EDIT_ACCOUNT = "edit_account";
    private final static String DOCUMENTS = "documents";
    private final static String CERTIFICATE = "certificate";
    private final static String SEND_ATTACHMENT = "send_attachment";

    /**
     * This is the Struts action method called on
     * http://.../actionPath?method=myAction1,
     * where "method" is the value specified in <action> element : 
     * ( <action parameter="method" .../> )
     */
    public ActionForward newTicket(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(NEW_TICKET);
    }

    public ActionForward replyTicket(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();

        String ticketIdStr = request.getParameter("ticketId");
        Long ticketId = MyConverter.strToLong(ticketIdStr);

//        ApplicationContext context= getWebApplicationContext();

        ITicketService ticketService = TicketSpringService.getInstance();

        Ticket ticket = ticketService.findTicketById(ticketId);

        session.setAttribute("ticket", ticket);
        return mapping.findForward(REPLY_TICKET);
    }

    /**
     * This is the Struts action method called on
     * http://.../actionPath?method=myAction2,
     * where "method" is the value specified in <action> element : 
     * ( <action parameter="method" .../> )
     */
    public ActionForward certClass(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(CERT_CLASS);
    }

    public ActionForward editAccount(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(EDIT_ACCOUNT);
    }

    public ActionForward documents(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(DOCUMENTS);
    }

    public ActionForward certificate(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(CERTIFICATE);
    }

    public ActionForward sendAttachment(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        UserAccount userAccount = (UserAccount) session.getAttribute("userAccount");
        if (userAccount != null) {
            String certClass = request.getParameter("certClass");
            IEmailControlService controlService = EmailControlSpringService.getInstance();
            EmailControl emailControl = null;
            if ("1".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_1_IND_ATTACHMENT);
            } else if ("2".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_2_IND_ATTACHMENT);
            } else if ("3".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_2_ENT_ATTACHMENT);
            } else if ("4".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_3_IND_ATTACHMENT);
            } else if ("5".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_3_ENT_ATTACHMENT);
            } else if ("6".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_3_SERVER_ATTACHMENT);
            }

            String msgBody = MailHelper.certAttachMailBody(userAccount, emailControl.getMsgBody()); 
            boolean isSend=MailEngine.isMailSendWithAttchement(emailControl.getSubject(), userAccount.getEmailAccount(), 
                    msgBody, emailControl.getAttachmentName(), emailControl.getAttachmentLocation());
            if(isSend){
                userAccount.setDocStatus(UserAccount.MAILED);
                IUserAccountService accountService=UserAccountSpringService.getInstance();
                accountService.persistUserAccount(userAccount);
                request.setAttribute("success", "cert.attachment.send.success");
            }
        }
        ActionForward forward = new ActionForward();
        forward.setPath("/userMenuAction.do?method=userHome");
        return forward;
    }
    
    public ActionForward download(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        UserAccount userAccount = (UserAccount) session.getAttribute("userAccount");
        if (userAccount != null) {
            String certClass = request.getParameter("certClass");
            IEmailControlService controlService = EmailControlSpringService.getInstance();
            EmailControl emailControl = null;
            if ("1".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_1_IND_ATTACHMENT);
            } else if ("2".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_2_IND_ATTACHMENT);
            } else if ("3".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_2_ENT_ATTACHMENT);
            } else if ("4".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_3_IND_ATTACHMENT);
            } else if ("5".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_3_ENT_ATTACHMENT);
            } else if ("6".equals(certClass)) {
                emailControl = controlService.findEmailControlByEmailType(EmailControl.CLASS_3_SERVER_ATTACHMENT);
            }

            if(emailControl!=null){
                userAccount.setDocStatus(UserAccount.DOWNLOADED);
                IUserAccountService accountService=UserAccountSpringService.getInstance();
                accountService.persistUserAccount(userAccount);
                
                response.sendRedirect(emailControl.getAttachment());
                return null;
               // request.setAttribute("success", "cert.attachment.send.success");
            }
        }
        ActionForward forward = new ActionForward();
        forward.setPath("/userMenuAction.do?method=userHome");
        return forward;
    }
}
