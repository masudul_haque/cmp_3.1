/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.DataHelper;
import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.Renewal;
import com.mangoca.cmp.service.IApplicationService;
import com.mangoca.cmp.service.spring.ApplicationSpringService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Masudul Haque
 */
public class RenewalData extends org.apache.struts.action.Action {

    
    /* forward name="success" path="" */
    private static final String SUCCESS = "next";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
       
         int page = DataHelper.getPage(request);
        int size = Renewal.PAGE_SIZE;
        IApplicationService service = ApplicationSpringService.getInstance();
        List collection =null;// service.getDocumentData(page, size); 
        Short status = MyConverter.strToShort(request.getParameter("status"));
        Integer userRole= (Integer) request.getSession().getAttribute("userRole");
         int resultSize =0;
        if (status != null && status!=0) {
            collection = service.findRenewalByAdminStatus(userRole, status);
            resultSize =  MyConverter.strToInt(request.getParameter("size"));//(int) service.countRevocationByStatus(status);
        } else {
            collection = service.findRenewalEnrolledusers(null);
            resultSize =  (int) service.countRenewalByStatus(null);
        }
        
        request.setAttribute("resultSize", resultSize);
        request.setAttribute("collection", collection);
        return mapping.findForward(SUCCESS);
    }
}
