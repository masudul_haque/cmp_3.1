/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.Notification;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.INotificationService;
import com.mangoca.cmp.service.spring.NotificationSpringService;
import com.mangoca.cmp.web.form.NotificationForm;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class NotificationAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
   
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";
    
      public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(ADD);
    } 
    
    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setNotification(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String idStr= request.getParameter("id");
        long id=MyConverter.strToLong(idStr);
        INotificationService service=NotificationSpringService.getInstance();
        
        Notification   notification= service.findNotificationById(id);
        Integer userRole= (Integer) request.getSession().getAttribute("userRole");
        if(User.USER_ACTIVE== userRole || User.USER_ENROLL== userRole){
            notification.setStatus(Notification.STATUS_READ);
            service.persistNotification(notification);
        }
        request.setAttribute("notification", notification);
        return mapping.findForward(VIEW);
    }
    
    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session= request.getSession();
        
        User user= (User) session.getAttribute("user");
        INotificationService service=NotificationSpringService.getInstance();//MangoCertSpringService.getInstance();
        NotificationForm notificationForm=  (NotificationForm) form;
        String idStr= request.getParameter("id");
        long id=MyConverter.strToLong(idStr);
        Notification notification=null;
        
        if(id!=0){
          notification= service.findNotificationById(id);
        }
        else{
            notification= new Notification();
        }
        BeanUtils.copyProperties(notification, notificationForm);
        notification.setSendDate(new Date());
        notification.setSnderId(user.getId());
       // notification.set
        service.persistNotification(notification);
       
        request.setAttribute("success", "save.success.Notification");
        ActionForward forward = new ActionForward();
        forward.setPath("/NotificationData.do");
        return forward;
    }
    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        ActionForward forward = new ActionForward();
        forward.setPath("/NotificationData.do");
        return forward;
    }  
    
    public ActionForward userList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session= request.getSession();
        UserAccount userAccount= (UserAccount) session.getAttribute("userAccount");
        INotificationService service=NotificationSpringService.getInstance();
        List<Notification> notifications=service.findNotificationsByCIN(userAccount.getUserCIN());
        session.setAttribute("notifications", notifications);
        ActionForward forward = new ActionForward();
        forward.setPath("/NotificationData.do");
        return forward;
    }
    
    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        String idStr= request.getParameter("id");
        long id=MyConverter.strToLong(idStr);
        INotificationService service=NotificationSpringService.getInstance();
        
        Notification   notification= service.findNotificationById(id);
        service.removeNotification(notification);
        
        ActionForward forward = new ActionForward();
        forward.setPath("/NotificationData.do");
        return forward;
    }

    private void setNotification(HttpServletRequest request) throws Exception{
       
        String idStr= request.getParameter("id");
        long id=MyConverter.strToLong(idStr);
        INotificationService service=NotificationSpringService.getInstance();
        
        Notification   notification= service.findNotificationById(id);
        
        request.setAttribute("notification", notification);
    }
}
