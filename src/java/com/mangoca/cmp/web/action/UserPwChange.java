/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class UserPwChange extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private static final String LOGIN = "login";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
      /*  UserAccount empuser= (UserAccount) request.getSession().getAttribute("empuser");
        if(empuser!=null){
        
        PasswordForm passwordForm= (PasswordForm) form;
        ApplicationContext context= getWebApplicationContext();
        
        IUserAccountService empUserService= UserAccountSpringService.getInstance(context);
        if(passwordForm.getCurPassword().equals(empuser.getPassword())){
            
            empuser.setPassword(passwordForm.getPassword());
            empUserService.persistEmpuser(empuser);
        }        
        return mapping.findForward(SUCCESS);
        }
        else{
            return mapping.findForward(LOGIN);
        }
       * 
       */
        return null;
    }
}
