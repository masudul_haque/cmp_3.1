/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.helper.EnrolleduserHelper;
import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.Ccaction;
import com.mangoca.cmp.model.Certificate;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Raaction;
import com.mangoca.cmp.model.Suspension;
import com.mangoca.cmp.model.Techaction;
import com.mangoca.cmp.service.IApplicationService;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.spring.ApplicationSpringService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.web.form.SuspensionForm;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class SuspensionAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";
    private static final String LOGIN = "login";

    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setSuspension(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        setSuspension(request);
        return mapping.findForward(VIEW);
    }

    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        request.setAttribute("page", Constants.SUSPENSION);
        ActionForward forward = new ActionForward();       
        IApplicationService applicationService = ApplicationSpringService.getInstance();
        
         Short status= MyConverter.strToShort(request.getParameter("status"));
        List<Enrolleduser> enrolledusers=null;
        
        if(status!=null && status.equals(Suspension.STATUS_NEW)){
         enrolledusers= applicationService.findSuspensionEnrolledusers(status);
        }else{
         enrolledusers= applicationService.findSuspensionEnrolledusers(null);
        }

        request.getSession().setAttribute("enrolledusers", enrolledusers);
        forward.setPath("/SuspensionData.do");
        return forward;//mapping.findForward(USERS);
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
       
        SuspensionForm reqForm = (SuspensionForm) form;
        HttpSession session = request.getSession();
        Enrolleduser enrolleduser = (Enrolleduser) session.getAttribute("enrolleduser");

        if (enrolleduser != null) {

            Certificate certificate = enrolleduser.getCertificate();
            if (certificate != null && certificate.getCertStatus().equals(Certificate.CERT_ACTIVE)) {

                Suspension suspension = enrolleduser.getSuspension();
                if (suspension == null) {
                    suspension = new Suspension();
                }
                BeanUtils.copyProperties(suspension, reqForm);

                suspension.setApplyDate(new Date());
                suspension.setStatus(Suspension.STATUS_NEW);

                Raaction raaction = new Raaction();
                Ccaction ccaction = new Ccaction();
                Techaction techaction = new Techaction();

                
                raaction.setStatus(Raaction.STATUS_NEW);
                ccaction.setStatus(Ccaction.STATUS_NEW);
                techaction.setStatus(Techaction.STATUS_NEW);

                suspension.setRaaction(raaction);
                suspension.setCcaction(ccaction);
                suspension.setTechaction(techaction);
                IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
                enrolleduser.setSuspension(suspension);

                certificate.setCertStatus(Certificate.CERT_APPLY_SUSPEND);

                enrolleduser.setCertificate(certificate);
                enrolleduserService.persistEnrolleduser(enrolleduser);
            }
            session.setAttribute("enrolleduser", enrolleduser);
            request.setAttribute("message", "save.success.Suspension");
            ActionForward forward = new ActionForward();
            forward.setPath("/SuspensionData.do");
            return forward;
        } else {
            return mapping.findForward(LOGIN);
        }

    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {


        return mapping.findForward(SUCCESS);
    }

    private void setSuspension(HttpServletRequest request) throws Exception {
        EnrolleduserHelper.setEnrolleduser(request);
    }
}
