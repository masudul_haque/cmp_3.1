/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.helper.CertFieldHelper;
import com.mangoca.cmp.helper.CertificateHelper;
import com.mangoca.cmp.helper.EnrolleduserHelper;
import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.Ccaction;
import com.mangoca.cmp.model.Certificate;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Mangocert;
import com.mangoca.cmp.model.Raaction;
import com.mangoca.cmp.model.Renewal;
import com.mangoca.cmp.model.Techaction;
import com.mangoca.cmp.service.IApplicationService;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.IMangoCertService;
import com.mangoca.cmp.service.spring.ApplicationSpringService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.service.spring.MangoCertSpringService;
import com.mangoca.cmp.web.form.EnrolleduserForm;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class RenewalAction extends DispatchAction {

    /* forward name="success" path="" */
    private static final String LOGIN = "login";
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";

    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        CertificateHelper.loadCertiticate(request);
        CertificateHelper.loadCertTypes(request);
        CertFieldHelper.loadLocalities(request);
        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setRenewal(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        setRenewal(request);
        return mapping.findForward(VIEW);
    }

    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        request.setAttribute("page", Constants.RENEWAL);
        ActionForward forward = new ActionForward();
//        ApplicationContext context = getWebApplicationContext();
        IApplicationService applicationService = ApplicationSpringService.getInstance();

        Short status = MyConverter.strToShort(request.getParameter("status"));
        List<Enrolleduser> enrolledusers = null;

        if (status != null && status.equals(Renewal.STATUS_NEW)) {
            enrolledusers = applicationService.findRenewalEnrolledusers(status);
        } else {
            enrolledusers = applicationService.findRenewalEnrolledusers(null);
        }

        request.getSession().setAttribute("enrolledusers", enrolledusers);
        forward.setPath("/RenewalData.do");
        return forward;//mapping.findForward(USERS);
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        EnrolleduserForm reqForm = (EnrolleduserForm) form;
        HttpSession session = request.getSession();
        Enrolleduser enrolleduser = (Enrolleduser) session.getAttribute("enrolleduser");

        if (enrolleduser != null) {

            Certificate certificate = enrolleduser.getCertificate();
            if (certificate != null && certificate.getCertStatus().equals(Certificate.CERT_ACTIVE)) {

                Renewal renewal = enrolleduser.getRenewal();
                if (renewal == null) {
                    renewal = new Renewal();
                }
                Certificate renewCert = renewal.getCertificate();
                if (renewCert == null) {
                    renewCert = new Certificate();
                }
                BeanUtils.copyProperties(renewCert, reqForm);
                if (reqForm.getCertId() != 0) {
                    IMangoCertService mangoCertService = MangoCertSpringService.getInstance();
                    Mangocert mangocert = mangoCertService.findMangocertById(reqForm.getCertId());
                    renewCert.setCertClass(mangocert.getCertClass());
                }


                renewal.setApplyDate(new Date());
                renewal.setStatus(Renewal.STATUS_NEW);
                renewal.setComment(reqForm.getRenewComment());
                Raaction raaction = new Raaction();
                Ccaction ccaction = new Ccaction();
                Techaction techaction = new Techaction();

                raaction.setStatus(Raaction.STATUS_NEW);
                ccaction.setStatus(Ccaction.STATUS_NEW);
                techaction.setStatus(Techaction.STATUS_NEW);

                renewal.setRaaction(raaction);
                renewal.setCcaction(ccaction);
                renewal.setTechaction(techaction);
                renewal.setCertificate(renewCert);

                IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
                enrolleduser.setRenewal(renewal);

                enrolleduserService.persistEnrolleduser(enrolleduser);
            }
            session.setAttribute("enrolleduser", enrolleduser);
            request.setAttribute("success", "save.success.Renewal");
            return mapping.getInputForward();
        } else {
            return mapping.findForward(LOGIN);
        }

    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {


        return mapping.findForward(SUCCESS);
    }

    private void setRenewal(HttpServletRequest request) throws Exception {
        EnrolleduserHelper.setEnrolleduser(request);
    }
}
