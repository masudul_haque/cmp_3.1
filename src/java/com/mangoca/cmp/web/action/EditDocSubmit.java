/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.model.Document;
import com.mangoca.cmp.service.IDocumentService;
import com.mangoca.cmp.service.spring.DocumentSpringService;
import com.mangoca.cmp.web.form.EditDocForm;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class EditDocSubmit extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private static final String FAILED = "failed";
    private FileOutputStream outputStream = null;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        EditDocForm editDocForm = (EditDocForm) form;
        Long documentId = editDocForm.getDocumentId();
        if (documentId != null && documentId != 0) {
            IDocumentService documentService = DocumentSpringService.getInstance();

            Document document = documentService.findDocumentById(documentId);
           // BeanUtils.copyProperties(document, editDocForm);

            FormFile formFile = editDocForm.getFormFile();

            if (formFile != null && formFile.getFileSize() != 0) {
                    String pathPrefix ="";
            if(request.getServerName().equals("localhost")){
                pathPrefix=Constants.LOCAL_HOST_LOCATION;
            }
            else{
                pathPrefix=Constants.SERVER_LOCATION;
            }
                long fileName = documentService.getResultSize() + 1;
                String path = pathPrefix + "/" + document.getDocumentId() + ".jpg";
                try {
                    //Provide the real path of document
                    outputStream = new FileOutputStream(new File(path));
                    outputStream.write(formFile.getFileData());
                } catch (IOException ex) {
                    // throw new IOException(ex.fillInStackTrace());
                } finally {
                    if (outputStream != null) {
                        outputStream.close();
                    }
                }                
            document.setUploadDate(new Date());
            }
            document.setDocumentName(editDocForm.getDocumentName());
            document.setDocumentType(editDocForm.getDocumentType());
            document.setDocumentStatus(editDocForm.getDocumentStatus());
            document.setRaComment(editDocForm.getRaComment());
            documentService.persistDocument(document);
            return mapping.findForward(SUCCESS);
        } else {

            request.setAttribute("failed", "Failed");
            return mapping.getInputForward();
        }
    }
}
