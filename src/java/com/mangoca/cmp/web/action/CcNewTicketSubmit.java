/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Ticket;
import com.mangoca.cmp.model.TicketMessage;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.ITicketService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.service.spring.TicketSpringService;
import com.mangoca.cmp.web.form.TicketForm;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
//import org.springframework.context.ApplicationContext;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class CcNewTicketSubmit extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        TicketForm newTicketForm = (TicketForm) form;
        String userCIN = newTicketForm.getUserCIN();
      //  ApplicationContext context = getWebApplicationContext();
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
        Enrolleduser enrolleduser = enrolleduserService.findEnrolluserByUserCIN(userCIN);
        if (enrolleduser == null) {
            request.setAttribute("cinFailed", "Customer CIN does not exist.");
            return mapping.getInputForward();
        } else {
            Ticket clientticket = new Ticket();
            ITicketService clientTicketService = TicketSpringService.getInstance();


            List<TicketMessage> ticketmessages = clientticket.getTicketMessages();
            if (ticketmessages == null) {
                ticketmessages = new ArrayList<TicketMessage>(1);
            }
            TicketMessage ticketmessage = new TicketMessage();

            ticketmessage.setMessage(newTicketForm.getMessage());
            ticketmessage.setMessagedBy(enrolleduser.getUserCIN());
            ticketmessage.setMessageDate(new Date());
            ticketmessages.add(ticketmessage);


            BeanUtils.copyProperties(clientticket, newTicketForm);
            BeanUtils.copyProperties(clientticket, enrolleduser);
            clientticket.setStatus(Ticket.STATUS_OPEN);
            clientticket.setCreationDate(new Date());
            clientTicketService.persistTicket(clientticket);
            return mapping.findForward(SUCCESS);
        }

    }
}
