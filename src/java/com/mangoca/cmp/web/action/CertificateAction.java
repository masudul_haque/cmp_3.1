/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.helper.CertificateHelper;
import com.mangoca.cmp.helper.EnrolleduserHelper;
import com.mangoca.cmp.helper.MailEngine;
import com.mangoca.cmp.helper.MailHelper;
import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.helper.SmsEngine;
import com.mangoca.cmp.model.Certificate;
import com.mangoca.cmp.model.EmailControl;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Techaction;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.ICertificateService;
import com.mangoca.cmp.service.IEmailControlService;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.IUserAccountService;
import com.mangoca.cmp.service.IUserService;
import com.mangoca.cmp.service.spring.CertificateSpringService;
import com.mangoca.cmp.service.spring.EmailControlSpringService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.service.spring.UserAccountSpringService;
import com.mangoca.cmp.service.spring.UserSpringService;
import com.mangoca.cmp.web.form.CertificateForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.jasypt.hibernate3.encryptor.HibernatePBEStringEncryptor;

/**
 *
 * @author Masudul Haque
 */
public class CertificateAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String VIEW = "view";
    private final static String EDIT = "edit";

    public ActionForward techAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        EnrolleduserHelper.setEnrolleduser(request);
        ActionForward forward = new ActionForward();
        forward.setPath("/CertificateAction.do?method=add");
        return forward;
    }

    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        CertificateHelper.loadCertiticate(request);
        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setCertificate(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        setCertificate(request);

        return mapping.findForward(VIEW);
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        String userName = user.getUserName();
        CertificateForm certificateForm = (CertificateForm) form;
        String userCIN = certificateForm.getUserCIN();
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
        Enrolleduser enrolleduser = enrolleduserService.findEnrolluserByUserCIN(userCIN);
        if (enrolleduser == null) {
            request.setAttribute("cinFailed", "Customer CIN does not exist.");
            return mapping.getInputForward();
        } else {
            Techaction techaction = enrolleduser.getTechaction();
            if (techaction == null) {
                techaction = new Techaction();
            }
            techaction.setStatus(certificateForm.getStatus());
            techaction.setComment(certificateForm.getComment());
            techaction.setUpdatedBy(userName);
            techaction.setUpdateDate(new Date());
            if (Techaction.STATUS_COMPLETE.equals(certificateForm.getStatus())) {
                IUserAccountService accountService = UserAccountSpringService.getInstance();
                IUserService userService = UserSpringService.getInstance();
                UserAccount userAccount = accountService.findUserAccountByCIN(enrolleduser.getUserCIN());
                User targetUser = userService.getUserByUserName(enrolleduser.getUserName());

                Certificate certificate = enrolleduser.getCertificate();
                if (userAccount != null && certificate != null) {
                    //BeanUtils.copyProperties(certificate, certificateForm);

                    certificate.setCertStatus(Certificate.CERT_ACTIVE);
                    certificate.setIssuedBy(userName);
                    certificate.setIssueDate(new Date());
                    certificate.setDownloadLink(Constants.CERT_DOWN_LINK);
                    enrolleduser.setCertificate(certificate);
                    String smsBody = "Your digital certificate application process completed. Check your email for download instructions. –from MANGO CA";
                    SmsEngine.sendSms(smsBody, userAccount.getMobileNo());

                    IEmailControlService emailService = EmailControlSpringService.getInstance();

                    EmailControl emailControl = emailService.findEmailControlByEmailType(EmailControl.CERT_ISSUE);

                    HibernatePBEStringEncryptor encryptor = new HibernatePBEStringEncryptor();
                    encryptor.setPassword(Constants.ENCRYPT_PASSWORD);


                    String mailBody = MailHelper.certificateIssueMailBody(userAccount, emailControl.getMsgBody(), encryptor.decrypt(targetUser.getPassword()));
                    boolean isSend = MailEngine.isMailSendToUserAccount(emailControl.getSubject(),
                            userAccount.getEmailAccount(), mailBody);
                    if (isSend) {
                        request.setAttribute("success", "save.success.Certificate");
                    }
                }
            }

            enrolleduser.setTechaction(techaction);
            enrolleduserService.persistEnrolleduser(enrolleduser);
        }
        ActionForward forward = new ActionForward();
        forward.setPath("/adminMenuAction.do?method=adminHome");
        return forward;
    }

    private void setCertificate(HttpServletRequest request) throws Exception {
        String idStr = request.getParameter("certId");
        Long id = MyConverter.strToLong(idStr);
        ICertificateService service=CertificateSpringService.getInstance();
        Certificate certificate=service.findCertificateById(id);
        request.setAttribute("certificate", certificate);
    }
}
