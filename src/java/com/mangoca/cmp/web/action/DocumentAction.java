/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.helper.AdminHelper;
import com.mangoca.cmp.helper.EnrolleduserHelper;
import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.Document;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Raaction;
import com.mangoca.cmp.service.IDocumentService;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.spring.DocumentSpringService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.web.form.DocumentForm;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author Masudul Haque
 */
public class DocumentAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";
    private FileOutputStream outputStream = null;
 
    public ActionForward raAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        EnrolleduserHelper.setEnrolleduser(request);
        
        ActionForward forward = new ActionForward();
        forward.setPath("/DocumentAction.do?method=add"); 
        return forward;
    }
    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setDocument(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        setDocument(request);

        return mapping.findForward(VIEW);
    }

    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionForward forward = new ActionForward();
        forward.setPath("/DocumentData.do");
        return forward;//mapping.findForward(USERS);
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String documentNames[] = {"National ID", "Passport", "Driving Licence", "TIN", "General"};
        DocumentForm raUploadForm = (DocumentForm) form;
        HttpSession session = request.getSession();
        String userName = (String) session.getAttribute("userName");

        String userCIN = raUploadForm.getUserCIN();
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
        IDocumentService documentService = DocumentSpringService.getInstance();
        long fileName = documentService.getResultSize() + 1;
       // Enrolleduser enrolleduser = (Enrolleduser) request.getAttribute("enrolleduser");
       // if(enrolleduser==null){
          Enrolleduser  enrolleduser=enrolleduserService.findEnrolluserByUserCIN(userCIN);
        //}
        if (enrolleduser == null) {
            request.setAttribute("cinFailed", "Customer CIN does not exist.");
            return mapping.getInputForward();
        } else {
            List<Document> documents = enrolleduser.getDocuments();
            if (documents == null) {
                documents = new ArrayList<Document>(5);
            }
            /*  String location= AdminHelper.serverAddress(request);
            location=userCIN+"/";
            //String pathPrefix = System.getProperty("user.home") + "/file/" + folderName;*/
            String pathPrefix ="";
            if(request.getServerName().equals("localhost")){
                pathPrefix=Constants.LOCAL_HOST_LOCATION;
            }
            else{
                pathPrefix=Constants.SERVER_LOCATION;
            }
           
            for (int i = 0; i < documentNames.length; i++) {
                FormFile formFile = raUploadForm.getFile(i);
                if (formFile != null && formFile.getFileSize() != 0) {
                    String path = pathPrefix + "/" + (fileName + i) + ".jpg";
                    String locationStr = AdminHelper.serverAddress(request) + (fileName + i) + ".jpg";
                    //location.append(documentNames[i]).append(".jpg");
                    try {
                        //Provide the real path of document
                        outputStream = new FileOutputStream(new File(path));
                        outputStream.write(formFile.getFileData());
                    } catch (IOException ex) {
                        // throw new IOException(ex.fillInStackTrace());
                    } finally {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    }
                    Document document = new Document();
                    // BeanUtils.copyProperties(document, enrolleduser);
                    document.setCertClass(enrolleduser.getCertClass());
                    document.setUserCIN(enrolleduser.getUserCIN());
                    document.setUserName(enrolleduser.getUserName());
                    document.setDocumentName(documentNames[i]);
                    document.setDocumentLink(locationStr);
                    document.setUploadDate(new Date());
                    document.setUploadedBy(userName);
                    document.setDocumentStatus(Constants.DOC_NEW);
                    document.setDocumentType(Constants.DOC_UPLOADED_FOR_VALIDATION);
                    documents.add(document);
                }
            }
            Raaction raaction = enrolleduser.getRaaction();
            raaction.setStatus(raUploadForm.getStatus());
            raaction.setComment(raUploadForm.getComment());
            raaction.setUpdatedBy(userName);
            raaction.setUpdateDate(new Date());
            enrolleduser.setRaaction(raaction);
            enrolleduser.setDocuments(documents);

            enrolleduserService.persistEnrolleduser(enrolleduser);

            request.setAttribute("success", "save.success.Document");
            ActionForward forward = new ActionForward();
            forward.setPath("/DocumentData.do");
            return forward;
        }
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(SUCCESS);
    }

    private void setDocument(HttpServletRequest request) throws Exception {
        String documentIdStr = request.getParameter("documentId");
        Long documentId = MyConverter.strToLong(documentIdStr);
        //   ApplicationContext context= getWebApplicationContext();
        IDocumentService documentService = DocumentSpringService.getInstance();
        Document document = documentService.findDocumentById(documentId);
        request.setAttribute("document", document);
    }
}
