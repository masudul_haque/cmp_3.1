/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Revocation;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.IApplicationService;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.ITicketService;
import com.mangoca.cmp.service.spring.ApplicationSpringService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.service.spring.TicketSpringService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class AdminMenuAction extends DispatchAction {

    private final static String ADMIN_HOME = "admin_home";
    private final static String USERS = "users";
    private final static String DOCUMENTS = "documents";
    private final static String SETTING = "setting";

    public ActionForward adminHome(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        //ApplicationContext context = getWebApplicationContext();
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
        IApplicationService applicationService = ApplicationSpringService.getInstance();
        HttpSession session = request.getSession();
        Integer userRole =  (Integer) session.getAttribute("userRole");
        List<Object[]> statuses = null;
        List<Object[]> revokeStatuses = null;
        List<Object[]> suspendStatuses = null;
        List<Object[]> renewalStatuses = null;
        if (userRole==User.ADMIN_RA) {
            statuses = enrolleduserService.findUserByRaRole();
            revokeStatuses=applicationService.countRevokeStatusesByRa();
            suspendStatuses=applicationService.countSuspendStatusesByRa();
            renewalStatuses=applicationService.countRenewalStatusesByRa();
        } else if (userRole==User.ADMIN_ACC) {
            statuses = enrolleduserService.findUserByAccRole();
            
        } 
        else if (userRole==User.ADMIN_VA) {
            statuses = enrolleduserService.findUserByVaRole();
        } 
        else if (userRole==User.ADMIN_TECH) {
            statuses = enrolleduserService.findUserByTechRole();
            revokeStatuses=applicationService.countRevokeStatusesByTech();
            suspendStatuses=applicationService.countSuspendStatusesByTech();
            renewalStatuses=applicationService.countRenewalStatusesByTech();
        }
        else if (userRole==User.ADMIN_CC) {
            ITicketService ticketService=TicketSpringService.getInstance();
             statuses = ticketService.findTicketByStatus();
            revokeStatuses=applicationService.countRevokeStatusesByCc();
            suspendStatuses=applicationService.countSuspendStatusesByCc();
            renewalStatuses=applicationService.countRenewalStatusesByCc();
        }
        //Integer newRevoke = applicationService.countRevocationByStatus(Revocation.STATUS_NEW);
        //Integer newRenewal = applicationService.countRenewalByStatus(Renewal.STATUS_NEW);
        //Integer newSuspension = applicationService.countSuspensionByStatus(Suspension.STATUS_NEW);

        //session.setAttribute("newRevoke", newRevoke);
        //session.setAttribute("newRenewal", newRenewal);
        //session.setAttribute("newSuspension", newSuspension);

        session.setAttribute("statuses", statuses);
        
        session.setAttribute("revokeStatuses", revokeStatuses);
        session.setAttribute("suspendStatuses", suspendStatuses);
        session.setAttribute("renewalStatuses", renewalStatuses);

        return mapping.findForward(ADMIN_HOME);
    }

    public ActionForward users(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ActionForward forward = new ActionForward();
        //ApplicationContext context = getWebApplicationContext();
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
        List<Enrolleduser> enrolledusers = enrolleduserService.findAllEnrolledusers();

        request.getSession().setAttribute("enrolledusers", enrolledusers);
        forward.setPath("/enrolleduserData.do");
        return forward;//mapping.findForward(USERS);
    }

    public ActionForward revokeApp(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ActionForward forward = new ActionForward();
        //ApplicationContext context = getWebApplicationContext();
        IApplicationService applicationService = ApplicationSpringService.getInstance();
        
        Short status= MyConverter.strToShort(request.getParameter("status"));
        List<Enrolleduser> enrolledusers=null;
        
        if(status!=null && status.equals(Revocation.STATUS_NEW)){
         enrolledusers= applicationService.findRevocationEnrolledusers(status);
        }else{
         enrolledusers= applicationService.findRevocationEnrolledusers(null);
        }
        

        request.getSession().setAttribute("enrolledusers", enrolledusers);
        forward.setPath("/revokeData.do");
        return forward;//mapping.findForward(USERS);
    }

    public ActionForward renewalApp(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ActionForward forward = new ActionForward();
        //ApplicationContext context = getWebApplicationContext();
        IApplicationService applicationService = ApplicationSpringService.getInstance();
        
        Short status= MyConverter.strToShort(request.getParameter("status"));
        List<Enrolleduser> enrolledusers=null;
        
        if(status!=null && status.equals(Revocation.STATUS_NEW)){
         enrolledusers= applicationService.findRenewalEnrolledusers(status);
        }else{
         enrolledusers= applicationService.findRenewalEnrolledusers(null);
        }

        request.getSession().setAttribute("enrolledusers", enrolledusers);
        forward.setPath("/renewalData.do");
        return forward;//mapping.findForward(USERS);
    }

    public ActionForward suspensionApp(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ActionForward forward = new ActionForward();
        //ApplicationContext context = getWebApplicationContext();        
        IApplicationService applicationService = ApplicationSpringService.getInstance();
        
        Short status= MyConverter.strToShort(request.getParameter("status"));
        List<Enrolleduser> enrolledusers=null;
        
        if(status!=null && status.equals(Revocation.STATUS_NEW)){
         enrolledusers= applicationService.findSuspensionEnrolledusers(status);
        }else{
         enrolledusers= applicationService.findSuspensionEnrolledusers(null);
        }

        request.getSession().setAttribute("enrolledusers", enrolledusers);
        forward.setPath("/suspensionData.do");
        return forward;//mapping.findForward(USERS);
    }

    public ActionForward setting(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(SETTING);
    }
    
}
