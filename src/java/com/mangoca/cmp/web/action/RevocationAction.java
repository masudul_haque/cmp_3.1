/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.EnrolleduserHelper;
import com.mangoca.cmp.model.Ccaction;
import com.mangoca.cmp.model.Certificate;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Raaction;
import com.mangoca.cmp.model.Revocation;
import com.mangoca.cmp.model.Techaction;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.web.form.RevocationForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class RevocationAction extends DispatchAction {

    /* forward name="success" path="" */
    private static final String LOGIN = "login";
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";
   
    
    public ActionForward search(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
         ActionForward forward = new ActionForward();
         forward.setPath("/RevocationData.do");
        return forward;
      //  return mapping.findForward(ADD);
    }
    
    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setRevocation(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        setRevocation(request);
        return mapping.findForward(VIEW);
    }

    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        ActionForward forward = new ActionForward();
        /*IApplicationService applicationService = ApplicationSpringService.getInstance();

        Short status = MyConverter.strToShort(request.getParameter("status"));
        List<Enrolleduser> enrolledusers = null;

        if (status != null && status.equals(Revocation.STATUS_NEW)) {
            enrolledusers = applicationService.findRevocationEnrolledusers(status);
        } else {
            enrolledusers = applicationService.findRevocationEnrolledusers(null);
        }


        request.getSession().setAttribute("enrolledusers", enrolledusers);
         * 
         */
        forward.setPath("/RevocationData.do");
        return forward;//mapping.findForward(USERS);

    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        RevocationForm reqForm = (RevocationForm) form;
        HttpSession session = request.getSession();
        Integer userRole = (Integer) session.getAttribute("userRole");
        Enrolleduser enrolleduser = null;
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();

        if (userRole == User.USER_ENROLL) {
            enrolleduser = (Enrolleduser) session.getAttribute("enrolleduser");
        } else {
            enrolleduser = enrolleduserService.findEnrolluserByUserCIN(reqForm.getUserCIN());
            if (enrolleduser == null) {
                request.setAttribute("cinFailed", "Customer CIN does not exist.");
                return mapping.getInputForward();
            }
        }

        Certificate certificate = enrolleduser.getCertificate();
        if (certificate != null && certificate.getCertStatus().equals(Certificate.CERT_ACTIVE)) {

            Revocation revocation = enrolleduser.getRevocation();
            if (revocation == null) {
                revocation = new Revocation();
            }
            BeanUtils.copyProperties(revocation, reqForm);

            revocation.setApplyDate(new Date());
            revocation.setStatus(Revocation.STATUS_NEW);

            Raaction raaction = new Raaction();
            Ccaction ccaction = new Ccaction();
            Techaction techaction = new Techaction();


            raaction.setStatus(Raaction.STATUS_NEW);
            ccaction.setStatus(Ccaction.STATUS_NEW);
            techaction.setStatus(Techaction.STATUS_NEW);

            revocation.setRaaction(raaction);
            revocation.setCcaction(ccaction);
            revocation.setTechaction(techaction);

            enrolleduser.setRevocation(revocation);

            certificate.setCertStatus(Certificate.CERT_APPLY_REVOKE);

            enrolleduser.setCertificate(certificate);
            enrolleduserService.persistEnrolleduser(enrolleduser);
            session.setAttribute("enrolleduser", enrolleduser);
            request.setAttribute("success", "save.success.Revocation");
            if (userRole == User.USER_ENROLL) {
                return mapping.getInputForward();
            } 
            else {
                ActionForward forward = new ActionForward();
                forward.setPath("/RevocationData.do");
                return forward;
            }
        } 
        else {
            request.setAttribute("failure", "failure.exist.certificate");
            return mapping.getInputForward();
        }
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {


        return mapping.findForward(SUCCESS);
    }

    private void setRevocation(HttpServletRequest request) throws Exception {
        EnrolleduserHelper.setEnrolleduser(request);
    }
}
