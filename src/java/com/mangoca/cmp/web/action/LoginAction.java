/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.model.AdminAccount;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.IAdminAccountService;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.IUserAccountService;
import com.mangoca.cmp.service.IUserService;
import com.mangoca.cmp.service.spring.AdminAccountSpringService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.service.spring.UserAccountSpringService;
import com.mangoca.cmp.service.spring.UserSpringService;
import com.mangoca.cmp.web.form.LoginForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jasypt.hibernate3.encryptor.HibernatePBEStringEncryptor;

/**
 *
 * @author Masudul Haque
 * @version 2.1.0
 */
public class LoginAction extends Action {

    public static final String PIN_VALID = "pin_valid";
    public static final String USER_DASHBOARD = "user_home";
    public static final String ADMIN_DASHBOARD = "admin_home";
    public static final String CC_DASHBOARD = "cc_home";
    public static final String OPERATOR_DASHBOARD = "operator_home";
    private static final String FAILURE = "failure";
    //private EnrollmentOperation enrollOperation= new EnrollmentOperation(); 

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        session.removeAttribute("userRole");
        LoginForm loginForm = (LoginForm) form;
        String userName = loginForm.getUserName();
        String password = loginForm.getPassword();

        HibernatePBEStringEncryptor encryptor = new HibernatePBEStringEncryptor();
        encryptor.setPassword(Constants.ENCRYPT_PASSWORD);

        IUserService userService = UserSpringService.getInstance();
        User user = userService.getUserByUserName(userName);

        if (user != null && password.equals(encryptor.decrypt(user.getPassword()))) {

            ActionForward forward = new ActionForward();
            int userRole = user.getUserRole();
            session.setAttribute("userRole", userRole);
            session.setAttribute("user", user);
            if (userRole == User.USER_INITIAL || userRole== User.USER_EMAIL_VALIDATE) {
                if(userRole==User.USER_INITIAL){                
                request.setAttribute(FAILURE, "login.failure.email.sms");    
                }
                else{                    
                request.setAttribute(FAILURE, "login.failure.sms");
                }
                IUserAccountService userAccountService = UserAccountSpringService.getInstance();
                UserAccount userAccount = userAccountService.getUserAccountByUserName(userName);
                session.setAttribute("userAccount", userAccount);
                forward.setPath("/PinAction.do?method=add");
                forward.setRedirect(true);
                return forward;
                //RequestDispatcher dispatcher=request.getRequestDispatcher("/PinAction.do?method=add");
                
               //response.sendRedirect(request.getContextPath()+"/PinAction.do?method=add");
               // return null;//mapping.findForward(PIN_VALID);
            }
            else if(userRole==User.USER_PIN_VALIDATE){
                request.setAttribute(FAILURE, "login.failure.email");
                return mapping.getInputForward();
            }
            else if (userRole == User.USER_ACTIVE || userRole == User.USER_ENROLL ) {
                IUserAccountService userAccountService = UserAccountSpringService.getInstance();
                UserAccount userAccount = userAccountService.getUserAccountByUserName(userName);
                session.setAttribute("userAccount", userAccount);
                if (userRole == User.USER_ENROLL) {
                     IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
                    Enrolleduser enrolleduser = enrolleduserService.getEnrolluserByUserName(userName);
                    if (enrolleduser != null) {
                        session.setAttribute("enrolleduser", enrolleduser);
                    }
                }
                forward.setPath("/userMenuAction.do?method=userHome");
                return forward;
            } else if (userRole == User.ADMIN_RA || userRole == User.ADMIN_ACC || userRole== User.ADMIN_CC
                    || userRole == User.ADMIN_VA || userRole == User.ADMIN_TECH) {
                setAdminAccount(userName, session);
                forward.setPath("/adminMenuAction.do?method=adminHome");
                return forward;
            } else if (userRole == User.ADMIN_ADMIN) {
                setAdminAccount(userName, session);
                forward.setPath("/AdminAccountAction.do?method=list");
                return forward;
            } /*else if (userRole == User.ADMIN_CC) {
                setAdminAccount(userName, session);
                forward.setPath("/TicketAction.do?method=ccOpen");
                return forward;
            }*/
        }
        session.removeAttribute("user");
        session.removeAttribute("userAccount");
        request.setAttribute(FAILURE, "login.failure.username.password");
        return mapping.getInputForward();
    }

    private void setAdminAccount(String userName, HttpSession session) {
        IAdminAccountService userAccountService = AdminAccountSpringService.getInstance();
        AdminAccount adminAccount = userAccountService.getAdminAccountByUserName(userName);
        session.setAttribute("adminAccount", adminAccount);
    }
}
