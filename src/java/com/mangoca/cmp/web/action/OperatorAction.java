/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.AdminAccount;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.IAdminAccountService;
import com.mangoca.cmp.service.IUserAccountService;
import com.mangoca.cmp.service.spring.AdminAccountSpringService;
import com.mangoca.cmp.service.spring.UserAccountSpringService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
//import org.springframework.context.ApplicationContext;
//import org.springframework.web.struts.DispatchActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class OperatorAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String ADD_ADMIN = "add_admin";
    private final static String ADMIN_EDIT = "admin_edit";
    private final static String ADMIN_DETAILS = "admin_details";
    
    
    private final static String EMPUSER_EDIT = "empuser_edit";
    private final static String EMPUSER_DETAILS = "empuser_details";
    private final static String SUCCESS = "success";

    public ActionForward addAdmin(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        commonAdminTask(request);
        return mapping.findForward(ADD_ADMIN);
    } 
    
    public ActionForward adminEdit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        commonAdminTask(request);
        return mapping.findForward(ADMIN_EDIT);
    }

    public ActionForward adminDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        commonAdminTask(request);
        return mapping.findForward(ADMIN_DETAILS);
    }
   
    
    public ActionForward empuserEdit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        commonEmpuserTask(request);
        return mapping.findForward(EMPUSER_EDIT);
    }

    public ActionForward empuserDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        commonEmpuserTask(request);
        return mapping.findForward(EMPUSER_DETAILS);
    }
    
    private void commonAdminTask(HttpServletRequest request) throws Exception{
       
        String idStr= request.getParameter("id");
        long id=MyConverter.strToInt(idStr);
//        ApplicationContext context= getWebApplicationContext();
        IAdminAccountService userService= AdminAccountSpringService.getInstance();
        AdminAccount admin= userService.findAdminAccountById(id); 
        request.setAttribute("admin", admin);
    }
    
    private void commonEmpuserTask(HttpServletRequest request) throws Exception{
        
        String idStr= request.getParameter("id");
        Long id=MyConverter.strToLong(idStr);
       
     //   ApplicationContext context= getWebApplicationContext();
        IUserAccountService empUserService= UserAccountSpringService.getInstance(); 
        IAdminAccountService userService= AdminAccountSpringService.getInstance();
            List<AdminAccount> raUsers= userService.findAllRaAdminAccount();
         
        request.getSession().setAttribute("raUsers", raUsers); 
        UserAccount empuser= empUserService.findUserAccountById(id);
        request.setAttribute("empuser", empuser);
    }
    
     
    
}
