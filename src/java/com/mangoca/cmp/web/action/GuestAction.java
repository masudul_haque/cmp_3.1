/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.RaSelectHelper;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.IUserAccountService;
import com.mangoca.cmp.service.IUserService;
import com.mangoca.cmp.service.spring.UserAccountSpringService;
import com.mangoca.cmp.service.spring.UserSpringService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class GuestAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String REGISTRATION = "registration";
    private final static String LOGIN = "login";
    private final static String FORGOT = "forgot";
    //  private final static String ACTIVATION = "forgot";

    public ActionForward registration(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        RaSelectHelper.loadRaAdmins(session);        
        request.getSession().setAttribute("avoid", 1);
        return mapping.findForward(REGISTRATION);
    }

    public ActionForward login(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        session.invalidate();
        //request.getSession().setAttribute("userRole", 100);
        return mapping.findForward(LOGIN);
    }

    public ActionForward forgot(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        request.getSession().setAttribute("avoid", 1);
        return mapping.findForward(FORGOT);
    }

    public ActionForward mailActivation(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String validityNo = request.getParameter("validity");
        IUserAccountService userAccountService = UserAccountSpringService.getInstance();
        //BigInteger validity = new BigInteger("" + validityNo);
        UserAccount userAccount = userAccountService.getUserAccountByValidityNo(validityNo);
        if (userAccount != null) {
            userAccount.setIsEmailValid(Boolean.TRUE);
            IUserService userService = UserSpringService.getInstance();
            User user = userService.getUserByUserName(userAccount.getUserName());
            if (userAccount.getIsPinValidate()) {
                user.setUserRole(User.USER_ACTIVE);
                userAccount.setUserRole(User.USER_ACTIVE);
            } else {
                user.setUserRole(User.USER_EMAIL_VALIDATE);
                userAccount.setUserRole(User.USER_EMAIL_VALIDATE);
            }
            userAccountService.persistUserAccount(userAccount);
            userService.persistUser(user);
        }
        return mapping.findForward(LOGIN);
    }
}
