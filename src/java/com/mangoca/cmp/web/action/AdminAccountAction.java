/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.AdminAccount;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.IAdminAccountService;
import com.mangoca.cmp.service.IUserService;
import com.mangoca.cmp.service.spring.AdminAccountSpringService;
import com.mangoca.cmp.service.spring.UserSpringService;
import com.mangoca.cmp.web.form.AdminAccountForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.jasypt.hibernate3.encryptor.HibernatePBEStringEncryptor;
//import org.springframework.context.ApplicationContext;
//import org.springframework.web.struts.DispatchActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class AdminAccountAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";

    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setAdminAccount(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        setAdminAccount(request);
        return mapping.findForward(VIEW);
    }

    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        //request.setAttribute("page", Constants.ADMIN_ACCOUNT);
        ActionForward forward = new ActionForward();
        //ApplicationContext context = getWebApplicationContext();
        //IAdminAccountService userService = AdminAccountSpringService.getInstance();
        //List<AdminAccount> adminAccounts = userService.findAllAdminAccounts();
        
        //request.getSession().setAttribute("list", adminAccounts);
        forward.setPath("/AdminAccountData.do");
        return forward;
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        AdminAccountForm adminForm = (AdminAccountForm) form;
        String idStr = request.getParameter("id");
        long id = MyConverter.strToLong(idStr);
        IAdminAccountService adminAccountService = AdminAccountSpringService.getInstance();
        IUserService userService = UserSpringService.getInstance();
        AdminAccount adminAccount = adminAccountService.findAdminAccountById(id);
        User user = null;
        if (adminAccount == null && id == 0) {
            user = new User();
            HibernatePBEStringEncryptor encryptor = new HibernatePBEStringEncryptor();
            // encryptor.setAlgorithm("MD5");
            encryptor.setPassword("0000");

            user.setPassword(encryptor.encrypt(adminForm.getPassword()));
            adminAccount = new AdminAccount();
            adminAccount.setRegiDate(new Date());
        }
        else{
            user= userService.getUserByUserName(adminForm.getUserName());
            user.setUserName(adminForm.getUserName());
            user.setUserRole(adminForm.getUserRole());
        }
        BeanUtils.copyProperties(adminAccount, adminForm);
        BeanUtils.copyProperties(user, adminForm);
        userService.persistUser(user);
        adminAccountService.persistAdminAccount(adminAccount);
        
        request.setAttribute("success", "save.success.AdminAccount");
        ActionForward forward = new ActionForward();
        forward.setPath("/AdminAccountData.do");
        return forward;

    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {


        return mapping.findForward(SUCCESS);
    }

    private void setAdminAccount(HttpServletRequest request) throws Exception {
         String idStr= request.getParameter("id");
        Long id= MyConverter.strToLong(idStr);
       // ApplicationContext context= getWebApplicationContext();
        IAdminAccountService adminAccountService = AdminAccountSpringService.getInstance();
        AdminAccount otherAccount= adminAccountService.findAdminAccountById(id);
        request.setAttribute("otherAdmin", otherAccount);
    }
}
