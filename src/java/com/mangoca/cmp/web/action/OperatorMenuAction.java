/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.AdminAccount;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.IAdminAccountService;
import com.mangoca.cmp.service.IApplicationService;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.ITicketService;
import com.mangoca.cmp.service.IUserAccountService;
import com.mangoca.cmp.service.spring.AdminAccountSpringService;
import com.mangoca.cmp.service.spring.ApplicationSpringService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.service.spring.TicketSpringService;
import com.mangoca.cmp.service.spring.UserAccountSpringService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
/**
 *
 * @author Masudul Haque
 */
public class OperatorMenuAction extends DispatchAction {

    private final static String SETTING = "setting";
    private final static String ADMIN_STATUS = "admin_status";
    private final static String CC_STATUS = "cc_status";
    

    public ActionForward admins(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ActionForward forward = new ActionForward();
//        ApplicationContext context = getWebApplicationContext();
        IAdminAccountService userService= AdminAccountSpringService.getInstance();
        List<AdminAccount> users = userService.findAllAdminAccounts(); 

        request.getSession().setAttribute("users", users);
        forward.setPath("/userData.do");
        return forward;
    }

    

    public ActionForward empusers(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ActionForward forward = new ActionForward();
//        ApplicationContext context = getWebApplicationContext();
        IUserAccountService empUserService= UserAccountSpringService.getInstance();
        List<UserAccount> empusers= empUserService.findAllUserAccounts();
        request.getSession().setAttribute("empusers", empusers);
        forward.setPath("/UserAccountData.do");
        return forward;
    }

    public ActionForward adminStatus(ActionMapping mapping, ActionForm form,
             HttpServletRequest request, HttpServletResponse response)
            throws Exception{
//        ApplicationContext context = getWebApplicationContext();
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
        IApplicationService applicationService = ApplicationSpringService.getInstance();
        //HttpSession request = request.getSession();
        String userRoleStr = request.getParameter("admin");
        int targetAdminRole= MyConverter.strToInt(userRoleStr);
        List<Object[]> statuses = null;
        List<Object[]> revokeStatuses = null;
        List<Object[]> suspendStatuses = null;
        List<Object[]> renewalStatuses = null;
        if (targetAdminRole ==User.ADMIN_RA) {
            statuses = enrolleduserService.findUserByRaRole();
            revokeStatuses=applicationService.countRevokeStatusesByRa();
            suspendStatuses=applicationService.countSuspendStatusesByRa();
            renewalStatuses=applicationService.countRenewalStatusesByRa();
        } else if (targetAdminRole == User.ADMIN_ACC) {
            statuses = enrolleduserService.findUserByAccRole();
        } else if (targetAdminRole ==User.ADMIN_VA) {
            statuses = enrolleduserService.findUserByVaRole();
        } else if (targetAdminRole ==User.ADMIN_TECH) {
            statuses = enrolleduserService.findUserByTechRole();
            revokeStatuses=applicationService.countRevokeStatusesByTech();
            suspendStatuses=applicationService.countSuspendStatusesByTech();
            renewalStatuses=applicationService.countRenewalStatusesByTech();
        }
        else if(targetAdminRole== User.ADMIN_CC){
            ITicketService ticketService= TicketSpringService.getInstance();
            statuses = ticketService.findTicketByStatus();
            revokeStatuses=applicationService.countRevokeStatusesByCc();
            suspendStatuses=applicationService.countSuspendStatusesByCc();
            renewalStatuses=applicationService.countRenewalStatusesByCc();
        }
        
        /*Integer newRevoke = applicationService.countRevocationByStatus(Revocation.STATUS_NEW);
        Integer newRenewal = applicationService.countRenewalByStatus(Renewal.STATUS_NEW);
        Integer newSuspension = applicationService.countSuspensionByStatus(Suspension.STATUS_NEW);

        session.setAttribute("newRevoke", newRevoke);
        session.setAttribute("newRenewal", newRenewal);
        session.setAttribute("newSuspension", newSuspension);
         */
        request.setAttribute("targetAdminRole", targetAdminRole);
        request.setAttribute("revokeStatuses", revokeStatuses);
        request.setAttribute("suspendStatuses", suspendStatuses);
        request.setAttribute("renewalStatuses", renewalStatuses);
        request.setAttribute("statuses", statuses);
        return mapping.findForward(ADMIN_STATUS);
    }
    
    public ActionForward ccStatus(ActionMapping mapping, ActionForm form,
             HttpServletRequest request, HttpServletResponse response)
            throws Exception{
//        ApplicationContext context = getWebApplicationContext();
        
        ITicketService ticketService= TicketSpringService.getInstance();
        
        
        HttpSession session = request.getSession();
        List<Object[]> statuses = ticketService.findTicketByStatus();
        
        session.setAttribute("statuses", statuses);
        return mapping.findForward(CC_STATUS);
    }
    public ActionForward setting(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(SETTING);
    }
    
    
}
