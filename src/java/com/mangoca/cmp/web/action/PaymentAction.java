/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.helper.EnrolleduserHelper;
import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.Accaction;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Payment;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.IPaymentService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.service.spring.PaymentSpringService;
import com.mangoca.cmp.web.form.PaymentForm;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class PaymentAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String VIEW = "view";
    private final static String EDIT = "edit";
    
   public ActionForward accAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        EnrolleduserHelper.setEnrolleduser(request);
        ActionForward forward = new ActionForward();
        forward.setPath("/PaymentAction.do?method=add"); 
        return forward;
    }
    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(ADD);
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        User user = (User) request.getSession().getAttribute("user");
        String userName=user.getUserName();
        PaymentForm paymentForm = (PaymentForm) form;
        String idStr = request.getParameter("paymentId");
        long id = MyConverter.strToInt(idStr);
        Payment payment = null;

        if (id != 0) {
            IPaymentService paymentService = PaymentSpringService.getInstance();
            payment = paymentService.findPaymentById(id);
            BeanUtils.copyProperties(payment, paymentForm);
            payment.setUpdatedBy(userName);
            payment.setPaymentDate(MyConverter.strToDate(paymentForm.getPaymentDateStr(), Constants.DATE_PICKER_PATTERN));
            payment.setReceivedDate(MyConverter.strToDate(paymentForm.getReceivedDateStr(), Constants.DATE_PICKER_PATTERN));
            payment.setUpdatedDate(new Date());
            paymentService.persistPayment(payment);
            return mapping.findForward(SUCCESS);

        } else {
            String userCIN = paymentForm.getUserCIN();
            IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
            Enrolleduser enrolleduser = enrolleduserService.findEnrolluserByUserCIN(userCIN);
            if (enrolleduser == null) {
                request.setAttribute("cinFailed", "Customer CIN does not exist.");
                return mapping.getInputForward();
            } else {
                List<Payment> payments = enrolleduser.getPayments();
                if (payments == null) {
                    payments = new ArrayList<Payment>(1);
                }
                payment = new Payment();
                BeanUtils.copyProperties(payment, paymentForm);
                payment.setUserCIN(userCIN);
                payment.setCertClass(enrolleduser.getCertClass());
                payment.setUserName(enrolleduser.getUserName());
                payment.setFullName(enrolleduser.getFullName());
                payment.setPaymentDate(MyConverter.strToDate(paymentForm.getPaymentDateStr(), Constants.DATE_PICKER_PATTERN));
                payment.setReceivedDate(MyConverter.strToDate(paymentForm.getReceivedDateStr(), Constants.DATE_PICKER_PATTERN));
                payment.setUpdatedDate(new Date());
                payment.setUpdatedBy(userName);

                payments.add(payment);

                enrolleduser.setPayments(payments);

                Accaction accaction = enrolleduser.getAccaction();
                if (accaction == null) {
                    accaction = new Accaction();
                }
                BeanUtils.copyProperties(accaction, paymentForm);
                accaction.setUpdatedBy(userName);
                accaction.setUpdateDate(new Date());
                enrolleduser.setAccaction(accaction);
                enrolleduserService.persistEnrolleduser(enrolleduser);
            }            
        request.setAttribute("message", "save.success.Payment");
        ActionForward forward = new ActionForward();
        forward.setPath("/PaymentData.do");
        return forward;
        }
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setPayment(request);
        return mapping.findForward(VIEW);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setPayment(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        request.setAttribute("page", Constants.PAYMENT);
        ActionForward forward = new ActionForward();
//        ApplicationContext context = getWebApplicationContext();
        IPaymentService documentService = PaymentSpringService.getInstance();
        List<Payment> payments = documentService.findAllPayments();

        request.getSession().setAttribute("payments", payments);
        forward.setPath("/PaymentData.do");
        return forward;//mapping.findForward(USERS);
    }

    private void setPayment(HttpServletRequest request) throws Exception {
        String idStr = request.getParameter("paymentId");
        Long id = MyConverter.strToLong(idStr);
//        ApplicationContext context = getWebApplicationContext();
        IPaymentService paymentService = PaymentSpringService.getInstance();
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
        Payment payment = paymentService.findPaymentById(id);
        if (payment != null) {
            Enrolleduser enrolleduser = enrolleduserService.findEnrolluserByUserCIN(payment.getUserCIN());
            request.setAttribute("payment", payment);
            request.setAttribute("enrolleduser", enrolleduser);

        }
    }
}
