/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.EnrolleduserHelper;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.model.Vaaction;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.web.form.ValidationForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class ValidationAction extends DispatchAction {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String VIEW = "view";
    private final static String EDIT = "edit";

    public ActionForward vaAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        EnrolleduserHelper.setEnrolleduser(request);
        ActionForward forward = new ActionForward();
        forward.setPath("/ValidationAction.do?method=add");
        return forward;
    }

    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        // CertificateHelper.loadCertiticate(request);
        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        //setCertificate(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        //setCertificate(request);

        return mapping.findForward(VIEW);
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        String userName = user.getUserName();
        ValidationForm actionForm = (ValidationForm) form;

        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
        Enrolleduser enrolleduser = enrolleduserService.findEnrolluserByUserCIN(actionForm.getUserCIN());

        if (enrolleduser == null) {
            request.setAttribute("cinFailed", "Customer CIN does not exist.");
            return mapping.getInputForward();
        } else {

            Vaaction vaaction = enrolleduser.getVaaction();
            if (vaaction == null) {
                vaaction = new Vaaction();
            }
            BeanUtils.copyProperties(vaaction, actionForm);
            vaaction.setUpdatedBy(userName);
            vaaction.setUpdateDate(new Date());
            enrolleduser.setVaaction(vaaction);
            enrolleduserService.persistEnrolleduser(enrolleduser);
            
            request.setAttribute("success", "save.success.Validation");
            ActionForward forward = new ActionForward();
            forward.setPath("/adminMenuAction.do?method=adminHome");
            return forward;
        }

    }
}
