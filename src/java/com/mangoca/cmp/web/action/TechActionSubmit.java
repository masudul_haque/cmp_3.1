/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.model.Certificate;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Techaction;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.web.form.AdminActionForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class TechActionSubmit extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        String userName= (String) session.getAttribute("userName");
        Enrolleduser enrolleduser= (Enrolleduser) session.getAttribute("enrolleduser");
        if(enrolleduser!=null){
//        ApplicationContext context= getWebApplicationContext();
        IEnrolleduserService enrolleduserService= EnrolleduserSpringService.getInstance();
        AdminActionForm actionForm= (AdminActionForm) form;
        Techaction techaction= enrolleduser.getTechaction();
        if(techaction==null){
            techaction= new Techaction();
        }
        BeanUtils.copyProperties(techaction, actionForm);
        
        techaction.setUpdatedBy(userName);
        techaction.setUpdateDate(new Date());
        if(actionForm.getStatus().equals(Constants.TECH_COMPLETED_REQ)){
         Certificate certificate= enrolleduser.getCertificate();
         if(certificate==null){
             certificate= new Certificate();
         }
         certificate.setCertStatus(Certificate.CERT_ACTIVE);
         certificate.setIssuedBy(userName);
         certificate.setIssueDate(new Date());
         certificate.setDownloadLink(Constants.CERT_DOWN_LINK);
         enrolleduser.setCertificate(certificate);
        }
        
        enrolleduser.setTechaction(techaction);
        enrolleduserService.persistEnrolleduser(enrolleduser);
        }
        
        return mapping.findForward(SUCCESS);
    }
}
