/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.AdminAccount;
import com.mangoca.cmp.service.IAdminAccountService;
import com.mangoca.cmp.service.spring.AdminAccountSpringService;
import com.mangoca.cmp.web.form.AdminAccountForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jasypt.hibernate3.encryptor.HibernatePBEStringEncryptor;

/**
 *
 * @author Masudul Haque
 */
public class AddAdminSubmit extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        AdminAccountForm adminForm=  (AdminAccountForm) form;
        String idStr= request.getParameter("id");
        long id= MyConverter.strToInt(idStr);
       // ApplicationContext context= getWebApplicationContext();
        IAdminAccountService userService= AdminAccountSpringService.getInstance();
        AdminAccount user= userService.findAdminAccountById(id);
        if(user==null && id==0){
            user= new AdminAccount();
            user.setRegiDate(new Date());
        }       
        BeanUtils.copyProperties(user, adminForm);
        HibernatePBEStringEncryptor encryptor= new HibernatePBEStringEncryptor();
        // encryptor.setAlgorithm("MD5");
         encryptor.setPassword("0000");
         
      //   user.setPassword( encryptor.encrypt(adminForm.getPassword()));
        userService.persistAdminAccount(user);
        return mapping.findForward(SUCCESS);
    }
}
