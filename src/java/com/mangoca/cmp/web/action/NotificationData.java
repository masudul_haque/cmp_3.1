/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.DataHelper;
import com.mangoca.cmp.model.Notification;
import com.mangoca.cmp.service.INotificationService;
import com.mangoca.cmp.service.spring.NotificationSpringService;
import com.mangoca.cmp.web.form.NotificationList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Masudul Haque
 */
public class NotificationData extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "next";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        List<Notification> notifications= (List<Notification>) request.getSession().getAttribute("notifications");
        if(notifications!=null){
           NotificationList notificationList= (NotificationList) form;
           notificationList.setNotifications(notifications);
        }
        else{
        int page = DataHelper.getPage(request);
        int size = Notification.PAGE_SIZE;
        INotificationService service = NotificationSpringService.getInstance();
        List collection = service.getNotificationData(page, size); 
        int resultSize =  service.getResultSize();
        
        request.setAttribute("resultSize", resultSize);
        request.setAttribute("collection", collection);
        }
        return mapping.findForward(SUCCESS);
    }
}
