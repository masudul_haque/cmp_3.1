/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.model.Ccaction;
import com.mangoca.cmp.model.Certificate;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Raaction;
import com.mangoca.cmp.model.Suspension;
import com.mangoca.cmp.model.Techaction;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.web.form.AdminActionForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Masudul Haque
 */
public class SuspensionAdminSubmit extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
 AdminActionForm actionForm = (AdminActionForm) form;
        HttpSession session = request.getSession();
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
        Enrolleduser enrolleduser = enrolleduserService.findEnrolluserByUserCIN(actionForm.getUserCIN());
        if (enrolleduser != null) {
            User user =  (User) session.getAttribute("user");
            String userName= user.getUserName();
            Suspension suspension = enrolleduser.getSuspension();
            if (suspension == null) {
                suspension = new Suspension();
            }

            Integer userRole = (Integer) session.getAttribute("userRole");
            if (userRole.equals(User.ADMIN_RA)) {
                Raaction raaction = suspension.getRaaction();
                if (raaction == null) {
                    raaction = new Raaction();
                }
                BeanUtils.copyProperties(raaction, actionForm);

                raaction.setUpdatedBy(userName);
                raaction.setUpdateDate(new Date());
                suspension.setRaaction(raaction);
            } else if (userRole.equals(User.ADMIN_CC)) {
                Ccaction ccaction = suspension.getCcaction();
                if (ccaction == null) {
                    ccaction = new Ccaction();
                }
                BeanUtils.copyProperties(ccaction, actionForm);

                ccaction.setUpdatedBy(userName);
                ccaction.setUpdateDate(new Date());
                suspension.setCcaction(ccaction);
            } else if (userRole.equals(User.ADMIN_TECH)) {
                Techaction techaction = suspension.getTechaction();
                if (techaction == null) {
                    techaction = new Techaction();
                }
                BeanUtils.copyProperties(techaction, actionForm);
                if (Suspension.STATUS_COMPLETE.equals(actionForm.getStatus())) {
                    Certificate certificate = enrolleduser.getCertificate();
                    certificate.setCertStatus(Certificate.CERT_SUSPENDED);
                    suspension.setStatus(Suspension.STATUS_COMPLETE);

                    enrolleduser.setCertificate(certificate);
                } else if (Suspension.STATUS_PENDING.equals(actionForm.getStatus())) {
                    suspension.setStatus(Suspension.STATUS_PENDING);
                }
                techaction.setUpdatedBy(userName);
                techaction.setUpdateDate(new Date());
                suspension.setTechaction(techaction);
            }
            enrolleduser.setSuspension(suspension);
            enrolleduserService.persistEnrolleduser(enrolleduser);
            request.setAttribute("success", "success.admin.Suspension.action");
        } else {
            request.setAttribute("failure", "failure.admin.Suspension.action");
        }

        ActionForward forward = new ActionForward();
        forward.setPath("/adminMenuAction.do?method=adminHome");
        return forward;
    }
}
