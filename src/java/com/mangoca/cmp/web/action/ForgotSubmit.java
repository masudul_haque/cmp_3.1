/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.helper.MailEngine;
import com.mangoca.cmp.helper.MailHelper;
import com.mangoca.cmp.model.EmailControl;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.IEmailControlService;
import com.mangoca.cmp.service.IUserAccountService;
import com.mangoca.cmp.service.IUserService;
import com.mangoca.cmp.service.spring.EmailControlSpringService;
import com.mangoca.cmp.service.spring.UserAccountSpringService;
import com.mangoca.cmp.service.spring.UserSpringService;
import com.mangoca.cmp.web.form.ForgotForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jasypt.hibernate3.encryptor.HibernatePBEStringEncryptor;
//import org.springframework.context.ApplicationContext;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class ForgotSubmit extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        // ApplicationContext context = getWebApplicationContext();
        IUserAccountService empUserService = UserAccountSpringService.getInstance();
        ForgotForm forgotForm = (ForgotForm) form;
        UserAccount userAccount = empUserService.findUserAccountByUserCinAndEmail(forgotForm.getUserCIN(), forgotForm.getEmailAccount());

//        UserAccount empuser = empUserService.findEmpuserByUserCinAndEmail(forgotForm.getUserCIN(), forgotForm.getEmailAccount());

        if (userAccount != null) {
            IEmailControlService controlService = EmailControlSpringService.getInstance();
            if (Constants.FORGOT_PASSWORD.equals(forgotForm.getForgotOption())) {
                HibernatePBEStringEncryptor encryptor = new HibernatePBEStringEncryptor();
                // encryptor.setAlgorithm("MD5");
                encryptor.setPassword(Constants.ENCRYPT_PASSWORD);
                IUserService userService = UserSpringService.getInstance();
                User user = userService.getUserByUserName(userAccount.getUserName());
                String password = encryptor.decrypt(user.getPassword());
                EmailControl emailControl = controlService.findEmailControlByEmailType(EmailControl.EMAIL_FORGOT_PASS);

                String msgBody = MailHelper.forgotPassMailBody(userAccount, emailControl.getMsgBody(), password);
                MailEngine.isMailSendToUserAccount(emailControl.getSubject(), userAccount.getEmailAccount(), msgBody);
            } else if (Constants.FORGOT_USERNAME.equals(forgotForm.getForgotOption())) {
                EmailControl emailControl = controlService.findEmailControlByEmailType(EmailControl.EMAIL_FORGOT_USERNAME);

                String msgBody = MailHelper.forgotUsernameMailBody(userAccount, emailControl.getMsgBody());
                MailEngine.isMailSendToUserAccount(emailControl.getSubject(), userAccount.getEmailAccount(), msgBody);
             }

            request.setAttribute(SUCCESS, "save.success.forgot.userName");
            return mapping.findForward(SUCCESS);
        } else {
            request.setAttribute("submitFailed", "CIN or email is incorrect.");
            return mapping.getInputForward();
        }

    }
}
