/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.model.Ccaction;
import com.mangoca.cmp.model.Certificate;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Raaction;
import com.mangoca.cmp.model.Revocation;
import com.mangoca.cmp.model.Techaction;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.web.form.AdminActionForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class RevokeAdminSubmit extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        AdminActionForm actionForm = (AdminActionForm) form;
        HttpSession session = request.getSession();
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
        Enrolleduser enrolleduser = enrolleduserService.findEnrolluserByUserCIN(actionForm.getUserCIN());
        if (enrolleduser != null) {
            User user =  (User) session.getAttribute("user");
            String userName= user.getUserName();
            Revocation revocation = enrolleduser.getRevocation();
            if (revocation == null) {
                revocation = new Revocation();
            }

            Integer userRole = (Integer) session.getAttribute("userRole");
            if (userRole.equals(User.ADMIN_RA)) {
                Raaction raaction = revocation.getRaaction();
                if (raaction == null) {
                    raaction = new Raaction();
                }
                BeanUtils.copyProperties(raaction, actionForm);

                raaction.setUpdatedBy(userName);
                raaction.setUpdateDate(new Date());
                revocation.setRaaction(raaction);
            } else if (userRole.equals(User.ADMIN_CC)) {
                Ccaction ccaction = revocation.getCcaction();
                if (ccaction == null) {
                    ccaction = new Ccaction();
                }
                BeanUtils.copyProperties(ccaction, actionForm);

                ccaction.setUpdatedBy(userName);
                ccaction.setUpdateDate(new Date());
                revocation.setCcaction(ccaction);
            } else if (userRole.equals(User.ADMIN_TECH)) {
                Techaction techaction = revocation.getTechaction();
                if (techaction == null) {
                    techaction = new Techaction();
                }
                BeanUtils.copyProperties(techaction, actionForm);
                if (Revocation.STATUS_COMPLETE.equals(actionForm.getStatus())) {
                    Certificate certificate = enrolleduser.getCertificate();
                    certificate.setCertStatus(Certificate.CERT_REVOKED);
                    revocation.setStatus(Revocation.STATUS_COMPLETE);

                    enrolleduser.setCertificate(certificate);
                } else if (Revocation.STATUS_PENDING.equals(actionForm.getStatus())) {
                    revocation.setStatus(Revocation.STATUS_PENDING);
                }
                techaction.setUpdatedBy(userName);
                techaction.setUpdateDate(new Date());
                revocation.setTechaction(techaction);
            }
            enrolleduser.setRevocation(revocation);
            enrolleduserService.persistEnrolleduser(enrolleduser);
            request.setAttribute("success", "success.admin.Revocation.action");
        } else {
            request.setAttribute("failure", "failure.admin.Revocation.action");
        }

        ActionForward forward = new ActionForward();
        forward.setPath("/adminMenuAction.do?method=adminHome");
        return forward;

    }
}
