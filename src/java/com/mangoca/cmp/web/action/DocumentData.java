/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.DataHelper;
import com.mangoca.cmp.model.Document;
import com.mangoca.cmp.service.IDocumentService;
import com.mangoca.cmp.service.spring.DocumentSpringService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Masudul Haque
 */
public class DocumentData extends org.apache.struts.action.Action {

    
    /* forward name="success" path="" */
    private static final String SUCCESS = "next";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
      //  request.setAttribute("page", Constants.DOCUMENT);
        
        int page = DataHelper.getPage(request);
        int size = Document.PAGE_SIZE;
        IDocumentService service = DocumentSpringService.getInstance();
        List collection = service.getDocumentData(page, size); 
        int resultSize =  (int) service.getResultSize(); 
        
        request.setAttribute("resultSize", resultSize);
        request.setAttribute("collection", collection);
        return mapping.findForward(SUCCESS);
    }
}
