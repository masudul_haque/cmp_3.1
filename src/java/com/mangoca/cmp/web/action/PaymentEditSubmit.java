/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.Payment;
import com.mangoca.cmp.service.IPaymentService;
import com.mangoca.cmp.service.spring.PaymentSpringService;
import com.mangoca.cmp.web.form.PaymentForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class PaymentEditSubmit extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        String paymentIdStr= request.getParameter("paymentId");
        String userName= (String) request.getSession().getAttribute("userName");
        Long paymentId=MyConverter.strToLong(paymentIdStr);
        if(paymentId!=0){
//            ApplicationContext context=getWebApplicationContext();
            IPaymentService paymentService= PaymentSpringService.getInstance();
            Payment payment= paymentService.findPaymentById(paymentId);
            PaymentForm paymentForm= (PaymentForm) form;
            BeanUtils.copyProperties(payment, paymentForm);
            payment.setUpdatedBy(userName);
            
            payment.setPaymentDate(MyConverter.strToDate(paymentForm.getPaymentDateStr(), Constants.DATE_PICKER_PATTERN));
            payment.setReceivedDate(MyConverter.strToDate(paymentForm.getReceivedDateStr(), Constants.DATE_PICKER_PATTERN));
            payment.setUpdatedDate(new Date());
            paymentService.persistPayment(payment);
        }
        
        return mapping.findForward(SUCCESS);
    }
}
