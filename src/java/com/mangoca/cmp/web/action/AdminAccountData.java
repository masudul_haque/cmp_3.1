/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.helper.DataHelper;
import com.mangoca.cmp.model.AdminAccount;
import com.mangoca.cmp.service.IAdminAccountService;
import com.mangoca.cmp.service.spring.AdminAccountSpringService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Masudul Haque
 */
public class AdminAccountData extends org.apache.struts.action.Action {

    
    /* forward name="success" path="" */
    private static final String SUCCESS = "next";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        request.setAttribute("page", Constants.ADMIN_ACCOUNT);
       
        int page = DataHelper.getPage(request);
        int size = AdminAccount.PAGE_SIZE;
        IAdminAccountService service = AdminAccountSpringService.getInstance();
        List collection = service.getAdminAccountData(page, size); 
        int resultSize = (int) service.getResultSize(); 
        
        request.setAttribute("resultSize", resultSize);
        request.setAttribute("collection", collection);
        return mapping.findForward(SUCCESS);
}
}
