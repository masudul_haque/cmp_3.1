/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Ticket;
import com.mangoca.cmp.model.TicketMessage;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.ITicketService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.service.spring.TicketSpringService;
import com.mangoca.cmp.web.form.TicketForm;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class TicketAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";

    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setTicket(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        setTicket(request);
        return mapping.findForward(VIEW);
    }

    public ActionForward userList(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionForward forward = new ActionForward();
        forward.setPath("/TicketData.do");
        return forward;
    }

    public ActionForward ccOpen(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        //request.setAttribute("page", Constants.TICKET);
        HttpSession session = request.getSession();
        session.setAttribute("ticketStatus", Ticket.STATUS_OPEN);

        ActionForward forward = new ActionForward();
        forward.setPath("/TicketData.do");
        return forward;
    }

    public ActionForward ccReOpen(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        Integer userRole = (Integer) session.getAttribute("userRole");
        if (userRole != User.USER_ENROLL) {
            session.setAttribute("ticketStatus", Ticket.STATUS_RE_OPEN);
        }
        ActionForward forward = new ActionForward();
        forward.setPath("/TicketData.do");
        return forward;
    }

    public ActionForward ccClosed(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        session.setAttribute("ticketStatus", Ticket.STATUS_CLOSED);

        ActionForward forward = new ActionForward();
        forward.setPath("/TicketData.do");
        return forward;
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        TicketForm ticketForm = (TicketForm) form;
        HttpSession session = request.getSession();
        Enrolleduser enrolleduser = (Enrolleduser) session.getAttribute("enrolleduser");
        Integer userRole= (Integer) session.getAttribute("userRole");
        if (enrolleduser == null) {
            String userCIN = ticketForm.getUserCIN();
            IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
            enrolleduser = enrolleduserService.findEnrolluserByUserCIN(userCIN);
            if (enrolleduser == null) {
                request.setAttribute("cinFailed", "Customer CIN does not exist.");
                return mapping.getInputForward();
            }
        }
        ITicketService ticketService = TicketSpringService.getInstance();
        Ticket ticket = new Ticket();

        List<TicketMessage> ticketMessages = new ArrayList<TicketMessage>(1);

        TicketMessage ticketMessage = new TicketMessage();

        ticketMessage.setMessage(ticketForm.getMessage());
        ticketMessage.setMessagedBy(enrolleduser.getUserCIN());
        ticketMessage.setMessageDate(new Date());
        ticketMessages.add(ticketMessage);

        BeanUtils.copyProperties(ticket, ticketForm);
        BeanUtils.copyProperties(ticket, enrolleduser);
        ticket.setCreationDate(new Date());
        ticket.setStatus(Ticket.STATUS_OPEN);

        ticket.setTicketMessages(ticketMessages);
        ticketService.persistTicket(ticket);
        if(User.USER_ENROLL != userRole){
        session.setAttribute("ticketStatus", Ticket.STATUS_OPEN);
        }
        request.setAttribute("success", "save.success.Ticket");
        ActionForward forward = new ActionForward();
        forward.setPath("/TicketData.do");
        return forward;

    }

    public ActionForward reply(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        String userName = (String) session.getAttribute("userName");
        Enrolleduser enrolleduser = (Enrolleduser) session.getAttribute("enrolleduser");
        Ticket ticket = (Ticket) session.getAttribute("ticket");
        Integer userRole = (Integer) session.getAttribute("userRole");
        if (enrolleduser != null && ticket != null) {

            TicketForm ticketForm = (TicketForm) form;

            ITicketService service = TicketSpringService.getInstance();


            List<TicketMessage> ticketMessages = ticket.getTicketMessages();

            if (ticketMessages != null) {


                TicketMessage ticketMessage = ticketMessages.get(ticketMessages.size() - 1);

                ticketMessage.setReplyMessage(ticketForm.getReplyMessage());
                ticketMessage.setRepliedBy(userName);
                ticketMessage.setReplyDate(new Date());

               // BeanUtils.copyProperties(ticket, ccTicketForm);
               // BeanUtils.copyProperties(ticket, enrolleduser);
                if (userRole == User.USER_ENROLL) {
                    ticket.setStatus(Ticket.STATUS_RE_OPEN);
                }

                if (Ticket.STATUS_CLOSED==ticketForm.getStatus()) {
                    ticket.setClosedDate(new Date());
                }
                ticket.setTicketMessages(ticketMessages);
                service.persistTicket(ticket);
            }
        }
        session.setAttribute("ticketStatus", Ticket.STATUS_CLOSED);
        request.setAttribute("success", "save.success.Ticket");
        ActionForward forward = new ActionForward();
        forward.setPath("/TicketData.do");
        return forward;
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {


        return mapping.findForward(SUCCESS);
    }

    private void setTicket(HttpServletRequest request) throws Exception {
        String ticketIdStr = request.getParameter("ticketId");
        Long ticketId = MyConverter.strToLong(ticketIdStr);
        ITicketService ticketService = TicketSpringService.getInstance();
        Ticket ticket = ticketService.findTicketById(ticketId);
        request.getSession().setAttribute("ticket", ticket);
    }
}
