/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.CertFieldHelper;
import com.mangoca.cmp.helper.CertificateHelper;
import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.helper.SubmitHelper;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.IUserAccountService;
import com.mangoca.cmp.service.IUserService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.service.spring.UserAccountSpringService;
import com.mangoca.cmp.service.spring.UserSpringService;
import com.mangoca.cmp.web.form.EnrolleduserForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class EnrolleduserAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";
    private final static String LOGIN = "login";

    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Enrolleduser enrolleduser= (Enrolleduser) request.getSession().getAttribute("enrolleduser");
        if(enrolleduser==null){
        CertificateHelper.loadCertiticate(request);
        CertificateHelper.loadCertTypes(request);        
        CertFieldHelper.loadLocalities(request);
        }
        else{
           request.setAttribute("instruction", "message.enrolleduser.header"); 
        }
        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {        
        CertificateHelper.loadCertiticate(request);
        setEnrolleduser(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setEnrolleduser(request);

        return mapping.findForward(VIEW);
    }

    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionForward forward = new ActionForward();
        request.getSession().removeAttribute("enrolledusers");
        forward.setPath("/EnrolleduserData.do");
        return forward;
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        EnrolleduserForm enrollForm = (EnrolleduserForm) form;
        
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
        String enrollIdStr= request.getParameter("enrollId");
        
        Long enrollId= MyConverter.strToLong(enrollIdStr);
        Enrolleduser enrolleduser=null;
        if(enrollId!=0){
            enrolleduser= enrolleduserService.findEnrolleduserById(enrollId);
            EnrolleduserForm enrollmentForm = (EnrolleduserForm) form;
            enrolleduser = SubmitHelper.afterSubmit(enrolleduser, enrollmentForm);
            enrolleduserService.persistEnrolleduser(enrolleduser);
            
            request.setAttribute("success", "save.success.Enrolleduser");
            return mapping.findForward(SUCCESS);
        }
        else{            
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("user");
            UserAccount userAccount = (UserAccount) session.getAttribute("userAccount");
            enrolleduser = enrolleduserService.getEnrolluserByUserName(userAccount.getUserName());
            IUserAccountService empuserService = UserAccountSpringService.getInstance();
            IUserService userService = UserSpringService.getInstance();
            if (userAccount != null && enrolleduser == null) {
                enrolleduser = new Enrolleduser();
                BeanUtils.copyProperties(enrolleduser, userAccount);
                enrolleduser = SubmitHelper.afterSubmit(enrolleduser, enrollForm);
                enrolleduser.setStatus(Enrolleduser.ACTIVE);
                enrolleduser.setEnrolDate(new java.util.Date());
                user.setUserRole(User.USER_ENROLL);
                userAccount.setUserRole(User.USER_ENROLL);
                userService.persistUser(user);
                empuserService.persistUserAccount(userAccount);
                enrolleduserService.persistEnrolleduser(enrolleduser);
                session.setAttribute("enrolleduser", enrolleduser);
            }            
            request.setAttribute("success", "save.success.Enrolleduser");
            return mapping.getInputForward();
        }
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {


        return mapping.findForward(SUCCESS);
    }

    private void setEnrolleduser(HttpServletRequest request) throws Exception {
        String idStr = request.getParameter("enrollId");
        Long id = MyConverter.strToLong(idStr);
        IEnrolleduserService service = EnrolleduserSpringService.getInstance();
        Enrolleduser enrolleduser = service.findEnrolleduserById(id);
        request.setAttribute("enrolleduser", enrolleduser);
    }
}
