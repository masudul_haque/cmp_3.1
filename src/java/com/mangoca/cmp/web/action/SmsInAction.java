/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.SmsIn;
import com.mangoca.cmp.service.ISmsInService;
import com.mangoca.cmp.service.spring.SmsInSpringService;
import com.mangoca.cmp.web.form.SmsInForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
//import org.springframework.web.struts.DispatchActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class SmsInAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";

    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setSmsIn(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setSmsIn(request);
        return mapping.findForward(VIEW);
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();

        String userName = (String) session.getAttribute("userName");

//        ApplicationContext context= getWebApplicationContext();
        ISmsInService certService = SmsInSpringService.getInstance();
        SmsInForm SmsInForm = (SmsInForm) form;
        String idStr = request.getParameter("id");
        int id = MyConverter.strToInt(idStr);
        SmsIn smsIn = null;

        if (id != 0) {
            smsIn = certService.findSmsInById(id);
        } else {
            smsIn = new SmsIn();
        }
        BeanUtils.copyProperties(smsIn, SmsInForm);
        // smsIn.setReceivedtime(new Date());
        smsIn.setSenttime(new Date());
        certService.persistSmsIn(smsIn);

        request.setAttribute("success", "save.success.SmsIn");
        ActionForward forward = new ActionForward();
        forward.setPath("/SmsInData.do");
        return forward;
    }

    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionForward forward = new ActionForward();
        forward.setPath("/SmsInData.do");
        return forward;
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String idStr = request.getParameter("id");
        Integer id = MyConverter.strToInt(idStr);
        ISmsInService service = SmsInSpringService.getInstance();

        SmsIn smsIn = service.findSmsInById(id);
        service.removeSmsIn(smsIn);
        
        ActionForward forward = new ActionForward();
        forward.setPath("/SmsInData.do");
        return forward;
    }

    private void setSmsIn(HttpServletRequest request) throws Exception {

        String idStr = request.getParameter("id");
        Integer id = MyConverter.strToInt(idStr);
//        ApplicationContext context= getWebApplicationContext();
        ISmsInService certService = SmsInSpringService.getInstance();

        SmsIn smsIn = certService.findSmsInById(id);

        request.setAttribute("smsIn", smsIn);
    }
}
