/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.tiles.TilesRequestProcessor;

/**
 *
 * @author Masudul Haque
 */
public class MyRequestProcessor extends TilesRequestProcessor {

    public MyRequestProcessor() {
    }
    
    

    @Override
    protected boolean processPreprocess(HttpServletRequest request,
                        HttpServletResponse response) {
        HttpSession session = request.getSession(false);
        //If user is trying to access login page
        // then don't check
        if (request.getServletPath().equals("/guestAction.do")
                || request.getServletPath().equals("/loginAction.do")|| session.getAttribute("avoid")!=null) {
            return true;
        }
        //Check if userName attribute is there is session.
        //If so, it means user has allready logged in
        if (session != null
                && session.getAttribute("userRole") != null && session.getAttribute("user") !=null) {

            return true;

        } else {
            try {
                response.sendRedirect("index.jsp");
                //If no redirect user to login Page
               // request.getRequestDispatcher("/index.jsp").forward(request, response);
            } catch (Exception ex) {
            }
        }


        return false;
    }
}