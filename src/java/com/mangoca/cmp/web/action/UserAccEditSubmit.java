/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.IUserAccountService;
import com.mangoca.cmp.service.spring.UserAccountSpringService;
import com.mangoca.cmp.web.form.UserAccEditForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class UserAccEditSubmit extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session= request.getSession();
        UserAccount userAccount= (UserAccount) session.getAttribute("userAccount");
        
//        ApplicationContext context= getWebApplicationContext();
        if(userAccount!=null){
         UserAccEditForm userAccForm=  (UserAccEditForm) form;
         BeanUtils.copyProperties(userAccount, userAccForm);
         IUserAccountService empUserService= UserAccountSpringService.getInstance();
         empUserService.persistUserAccount(userAccount);
         session.setAttribute("userAccount", userAccount);
         request.setAttribute("success", "save.success.edit.user.account");
        ActionForward forward= new ActionForward();
        forward.setPath("/userMenuAction.do?method=setting");
        return forward;
        }
        return mapping.getInputForward();
    }
}
