package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.database.MailConstant;
import com.mangoca.cmp.helper.MailEngine;
import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.helper.RaSelectHelper;
import com.mangoca.cmp.helper.UserHelper;
import com.mangoca.cmp.model.SmsOut;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.ISmsOutService;
import com.mangoca.cmp.service.IUserAccountService;
import com.mangoca.cmp.service.IUserService;
import com.mangoca.cmp.service.spring.SmsOutSpringService;
import com.mangoca.cmp.service.spring.UserAccountSpringService;
import com.mangoca.cmp.service.spring.UserSpringService;
import com.mangoca.cmp.web.form.UserAccountForm;
import java.math.BigInteger;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.jasypt.hibernate3.encryptor.HibernatePBEStringEncryptor;

/**
 *
 * @author Masudul Haque
 */
public class AdminUserAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";

    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        RaSelectHelper.loadRaAdmins(request.getSession());
        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RaSelectHelper.loadRaAdmins(request.getSession());
        setUserAccount(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        setUserAccount(request);
        return mapping.findForward(VIEW);
    }

    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        //request.setAttribute("page", Constants.USER_ACCOUNT);
        ActionForward forward = new ActionForward();
        forward.setPath("/UserAccountData.do");
        return forward;
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        UserAccountForm regForm = (UserAccountForm) form;
//        ApplicationContext context = getWebApplicationContext();
        IUserService userService = UserSpringService.getInstance();
        IUserAccountService userAccountService = UserAccountSpringService.getInstance();
        ISmsOutService smsOutService = SmsOutSpringService.getInstance();
        String captcha = (String) request.getSession().getAttribute("captcha");
        String captchaText = regForm.getCaptchaText();
        if (captchaText.equals(captcha)) {
            String userName = regForm.getUserName();
            User user = userService.getUserByUserName(userName);
            if (user == null) {
                int pin = UserHelper.generatePin();
                long validityNo = System.currentTimeMillis();
                BigInteger validity = new BigInteger("" + validityNo);
                user = new User();
                UserAccount userAccount = new UserAccount();
                BeanUtils.copyProperties(user, regForm);
                BeanUtils.copyProperties(userAccount, regForm);
                String userCin = userAccountService.generateCIN(regForm.getRaSelect());
                HibernatePBEStringEncryptor encryptor = new HibernatePBEStringEncryptor();
                // encryptor.setAlgorithm("MD5");
                encryptor.setPassword(Constants.ENCRYPT_PASSWORD);
                String encryptPassword = encryptor.encrypt(regForm.getPassword());
                user.setPassword(encryptPassword);
                user.setUserRole(User.USER_INITIAL);
                userAccount.setUserCIN(userCin);
                userAccount.setValidityNo(""+validity);
                userAccount.setRegiDate(new Date());
                userAccount.setIsEmailValid(Boolean.FALSE);
                userAccount.setSmsPin("" + pin);
                userAccount.setIsPinValidate(Boolean.FALSE);
                userService.persistUser(user);

                SmsOut smsOut = new SmsOut();
                smsOut.setMsg("Enter this Pin: " + pin);
                smsOut.setReceiver(regForm.getMobileNo());
                smsOut.setSender(Constants.SENDER_NO);
                smsOut.setStatus(SmsOut.STATUS_SEND);
                userAccountService.persistUserAccount(userAccount);
                smsOutService.persistSmsOut(smsOut);

                String mailBody = MailConstant.ACTIVATION_MAIL_TEXT + "\n\n" + "https://portal.mangoca.com/cmp/guestAction.do?method=mailActivation&validity=" + validityNo;
                boolean isSend = MailEngine.isMailSendToUserAccount(MailConstant.ACTIVATION_MAIL_SUBJECT, regForm.getEmailAccount(),
                        mailBody);
                if (isSend) {
                    request.setAttribute("isSend", isSend);
                } else {
                    request.setAttribute("validity", validity);
                }
                request.setAttribute("userAccount", userAccount);

                request.setAttribute("success", "save.success.registration");
                ActionForward forward = new ActionForward();
                forward.setPath("/PinAction.do?method=add");
                return forward;
            } else {
                request.setAttribute("userExist", "Username is not available.");
                return mapping.getInputForward();
            }
        } else {

            request.setAttribute("captchaFailed", "Type correct image text");
            return mapping.getInputForward();
        }

    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {


        return mapping.findForward(SUCCESS);
    }

    private void setUserAccount(HttpServletRequest request) throws Exception {
        String idStr = request.getParameter("id");
        Long id = MyConverter.strToLong(idStr);
        //ApplicationContext context = getWebApplicationContext();
        IUserAccountService adminAccountService = UserAccountSpringService.getInstance();
        UserAccount userAccount = adminAccountService.findUserAccountById(id);
        request.setAttribute("userAccount", userAccount);
    }
}
