/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Ticket;
import com.mangoca.cmp.model.TicketMessage;
import com.mangoca.cmp.service.ITicketService;
import com.mangoca.cmp.service.spring.TicketSpringService;
import com.mangoca.cmp.web.form.TicketForm;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
//import org.springframework.context.ApplicationContext;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class NewTicketSubmit extends Action {

    private static final String SUCCESS = "success";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        Enrolleduser enrolleduser= (Enrolleduser) request.getSession().getAttribute("enrolleduser");
        
        if(enrolleduser!=null){
        
        TicketForm newTicketForm= (TicketForm) form;
//        ApplicationContext context= getWebApplicationContext();
        
        ITicketService clientTicketService = TicketSpringService.getInstance();
        
        Ticket clientticket= new Ticket();
        
        List<TicketMessage> ticketMessages= clientticket.getTicketMessages();
        if(ticketMessages==null){
            ticketMessages= new ArrayList<TicketMessage>(1);
        }
        TicketMessage ticketMessage= new TicketMessage();
        
        ticketMessage.setMessage(newTicketForm.getMessage());
        ticketMessage.setMessagedBy(enrolleduser.getUserCIN());
        ticketMessage.setMessageDate(new Date());
        ticketMessages.add(ticketMessage);
        
        BeanUtils.copyProperties(clientticket, newTicketForm);
        BeanUtils.copyProperties(clientticket, enrolleduser);
        clientticket.setCreationDate(new Date());
        clientticket.setStatus(Ticket.STATUS_OPEN);
        
        clientticket.setTicketMessages(ticketMessages);
        clientTicketService.persistTicket(clientticket);
        }
        return mapping.findForward(SUCCESS);
    }
}
