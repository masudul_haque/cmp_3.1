/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.web.form.AdvanceSearchForm;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class AdminAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SEARCH_RESULT = "search_result";
    private final static String CIN_RESULT = "cin_search";
    private final static String ADVANCE_SEARCH = "advance_search";
    private final static String ACTION = "action";
    private final static String RA_ACTION = "ra_action";
    private final static String VA_ACTION = "va_action";
    private final static String ACC_ACTION = "acc_action";
    private final static String TECH_ACTION = "tech_action";
    private final static String DETAILS = "details";
    private final static String USER_EDIT = "user_edit";
    private final static String PW_CHANGE="pw_change";

    
    public ActionForward goReqResult(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ActionForward forward= new ActionForward(); 
        String reqStatus= request.getParameter("reqStatus");
        short status= MyConverter.strToShort(reqStatus); 
        
        HttpSession session= request.getSession();
        int userRole= (Integer) session.getAttribute("userRole");
        
        IEnrolleduserService enrolleduserService= EnrolleduserSpringService.getInstance();
        List<Enrolleduser> enrolledusers=null;
        if(userRole==User.ADMIN_RA){
             enrolledusers= enrolleduserService.findEnrolledusersByRaStatus(status);
        }
        else if(userRole==User.ADMIN_ACC){
             enrolledusers= enrolleduserService.findEnrolledusersByAccStatus(status);
        }
        else if(userRole==User.ADMIN_VA){
             enrolledusers= enrolleduserService.findEnrolledusersByVaStatus(status);
        }
        else if(userRole==User.ADMIN_TECH){
             enrolledusers= enrolleduserService.findEnrolledusersByTechStatus(status);
        }
        session.setAttribute("enrolledusers", enrolledusers);
        forward.setPath("/EnrolleduserData.do");         
        return forward;
    }

    
    public ActionForward goSearchResult(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        AdvanceSearchForm searchForm= (AdvanceSearchForm) form;
        //ApplicationContext context= getWebApplicationContext();
        
        IEnrolleduserService enrolleduserService= EnrolleduserSpringService.getInstance();
        List<Enrolleduser> enrolledusers= enrolleduserService.findEnrolledusersByAdvanceSearch(
                                          searchForm.getUserName().trim(), searchForm.getEmailAccount().trim(),
                                          searchForm.getMobileNo().trim(), searchForm.getCertClass().trim(), null, null);
        
        ActionForward forward= new ActionForward(); 
        
        HttpSession session= request.getSession();
        session.setAttribute("enrolledusers", enrolledusers);
        forward.setPath("/enrolleduserData.do");         
        return forward;//mapping.findForward(SUCCESS);
        //return mapping.findForward(SEARCH_RESULT);
    }
    
    public ActionForward goAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setEnrolleduser(request);
        request.setAttribute("userAction", true);
        return mapping.findForward(ACTION);
    }
    
    public ActionForward vaAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setEnrolleduser(request);
       // request.setAttribute("userAction", true);
        return mapping.findForward(VA_ACTION);
    }
    
    public ActionForward accAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setEnrolleduser(request);
        request.setAttribute("userAction", true);
        return mapping.findForward(ACC_ACTION);
    }
    
    public ActionForward techAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setEnrolleduser(request);
        request.setAttribute("userAction", true);
        return mapping.findForward(TECH_ACTION);
    }
    public ActionForward goDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setEnrolleduser(request);
        request.setAttribute("userAction", true);
        return mapping.findForward(DETAILS);
    }
    public ActionForward goEdit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        request.setAttribute("userAction", true);
        return mapping.findForward(USER_EDIT);
    }
    
    public ActionForward cinSearch(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String userCIN = request.getParameter("userCIN");
       
        //ApplicationContext context= getWebApplicationContext();
        IEnrolleduserService enrolleduserService= EnrolleduserSpringService.getInstance();
        
        Enrolleduser enrolleduser= enrolleduserService.findEnrolluserByUserCIN(userCIN);
        HttpSession session= request.getSession();
        
        session.setAttribute("enrolleduser", enrolleduser);
        request.setAttribute("userAction", true);
        return mapping.findForward(CIN_RESULT);
    }
    
    
    public ActionForward advanceSearch(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(ADVANCE_SEARCH);
    }
    
    public ActionForward pwChange(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(PW_CHANGE);
    }
    
    
    private void setEnrolleduser(HttpServletRequest request) throws Exception{
        String enrollId= request.getParameter("enrollId");
        Long id=MyConverter.strToLong(enrollId);
        IEnrolleduserService enrolleduserService= EnrolleduserSpringService.getInstance();        
        Enrolleduser enrolleduser= enrolleduserService.findEnrolleduserById(id);       
        request.setAttribute("enrolleduser", enrolleduser);
        
    }
}
