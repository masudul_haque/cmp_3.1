/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.helper.AdminHelper;
import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.EmailControl;
import com.mangoca.cmp.service.IEmailControlService;
import com.mangoca.cmp.service.spring.EmailControlSpringService;
import com.mangoca.cmp.web.form.EmailControlForm;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author Masudul Haque
 */
public class EmailControlAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";
    private FileOutputStream outputStream = null;

    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setEmailControl(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setEmailControl(request);
        return mapping.findForward(VIEW);
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();

        String userName = (String) session.getAttribute("userName");

//        ApplicationContext context= getWebApplicationContext();
        IEmailControlService certService = EmailControlSpringService.getInstance();
        EmailControlForm emailControlForm = (EmailControlForm) form;
        String idStr = request.getParameter("id");
        int id = MyConverter.strToInt(idStr);
        EmailControl emailControl = null;

        if (id != 0) {
            emailControl = certService.findEmailControlById(id);
        } else {
            emailControl = new EmailControl();
        }
        FormFile attachFile = emailControlForm.getAttachFile();
        if (attachFile != null && attachFile.getFileSize() != 0) {
            String pathPrefix = "";
            if (request.getServerName().equals("localhost")) {
                pathPrefix = Constants.LOCAL_HOST_EMAIL_ATTACH_LOCATION;
            } else {
                pathPrefix = Constants.SERVER_EMAIL_ATTACH_LOCATION;
            }
            String path = pathPrefix + attachFile.getFileName();
            String locationStr = AdminHelper.serverAttachAddress(request) + attachFile.getFileName();
            try {
                //Provide the real path of document
                outputStream = new FileOutputStream(new File(path));
                outputStream.write(attachFile.getFileData());
            } catch (IOException ex) {
                // throw new IOException(ex.fillInStackTrace());
            } finally {
                if (outputStream != null) {
                    outputStream.close();
                }
            }
            emailControl.setAttachment(locationStr);
            emailControl.setAttachmentLocation(path);
        }
        BeanUtils.copyProperties(emailControl, emailControlForm);


        emailControl.setUpdatedDate(new Date());
        emailControl.setUpdatedBy(userName);
        certService.persistEmailControl(emailControl);

        request.setAttribute("success", "save.success.EmailControl");
        ActionForward forward = new ActionForward();
        forward.setPath("/EmailControlData.do");
        return forward;
    }

    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionForward forward = new ActionForward();
        forward.setPath("/EmailControlData.do");
        return forward;
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(SUCCESS);
    }

    private void setEmailControl(HttpServletRequest request) throws Exception {

        String idStr = request.getParameter("id");
        Integer id = MyConverter.strToInt(idStr);
//        ApplicationContext context= getWebApplicationContext();
        IEmailControlService certService = EmailControlSpringService.getInstance();

        EmailControl emailControl = certService.findEmailControlById(id);

        request.setAttribute("emailControl", emailControl);
    }
}
