/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.SubmitHelper;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.web.form.DocumentForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class RaActionSubmit extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    //private FileOutputStream outputStream = null;//, outputStream2 = null;

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
//        ApplicationContext context= getWebApplicationContext();
        IEnrolleduserService enrolleduserService= EnrolleduserSpringService.getInstance();
        
        DocumentForm raUploadForm = (DocumentForm) form;
        Enrolleduser enrolleduser= (Enrolleduser) request.getSession().getAttribute("enrolleduser");
        SubmitHelper.afterUpload(enrolleduser, raUploadForm, request);
        enrolleduserService.persistEnrolleduser(enrolleduser);
        return mapping.findForward(SUCCESS);
    }
}
