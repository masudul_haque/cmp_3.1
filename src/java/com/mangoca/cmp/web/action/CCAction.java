/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Ticket;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.ITicketService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.service.spring.TicketSpringService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
//import org.springframework.web.struts.DispatchActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class CCAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String TICKET_ACTION = "ticket_action";
    private final static String NEW_TICKET = "new_ticket";

    
    public ActionForward ticketAction(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session= request.getSession();
        
        String ticketIdStr=request.getParameter("ticketId");
        Long ticketId=MyConverter.strToLong(ticketIdStr);
        
//        ApplicationContext context= getWebApplicationContext();
        
        ITicketService service=TicketSpringService.getInstance();
        IEnrolleduserService enrolleduserService=EnrolleduserSpringService.getInstance();
        
        Ticket ticket= service.findTicketById(ticketId);
        Enrolleduser enrolleduser= enrolleduserService.getEnrolluserByUserName(ticket.getUserName());
        
        session.setAttribute("enrolleduser", enrolleduser);
        session.setAttribute("ticket", ticket);
        
        return mapping.findForward(TICKET_ACTION);
    }

    
    public ActionForward newTicket(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(NEW_TICKET);
    }
    
    public ActionForward userTickets(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        HttpSession session= request.getSession();
        Enrolleduser enrolleduser= (Enrolleduser) session.getAttribute("enrolleduser");
//        ApplicationContext context= getWebApplicationContext();
        ITicketService clientTicketService=TicketSpringService.getInstance();
        
        List clientTickets= clientTicketService.findTicketsByUsername(enrolleduser.getUserName());
        
        session.setAttribute("clientTickets", clientTickets);

        ActionForward forward = new ActionForward();
        forward.setPath("/ccTicketData.do");
        request.setAttribute("userAction", true);
        return forward;
    }
}
