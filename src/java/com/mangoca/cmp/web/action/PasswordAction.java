/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.IUserService;
import com.mangoca.cmp.service.spring.UserSpringService;
import com.mangoca.cmp.web.form.PasswordForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.jasypt.hibernate3.encryptor.HibernatePBEStringEncryptor;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class PasswordAction extends DispatchAction {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private static final String LOGIN = "login";
    private static final String ADD = "add";

    
   
    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(ADD);
    }
   
    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        User user = (User) request.getSession().getAttribute("user");
        if (user != null) {

            PasswordForm passwordForm = (PasswordForm) form;
            IUserService userService = UserSpringService.getInstance();
            
             HibernatePBEStringEncryptor encryptor = new HibernatePBEStringEncryptor();
             encryptor.setPassword(Constants.ENCRYPT_PASSWORD);
            // System.out.println(encryptor.decrypt(user.getPassword()));
            if (passwordForm.getCurPassword().equals(encryptor.decrypt(user.getPassword()))) {

                user.setPassword(encryptor.encrypt(passwordForm.getPassword()));
                userService.persistUser(user);
                
                request.setAttribute("success", "save.success.password.change");
                return mapping.findForward(SUCCESS);
            }             
        }
        request.setAttribute("failure", "Your current password is incorrect.");
        return mapping.getInputForward();
    }
}
