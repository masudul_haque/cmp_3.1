/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.helper.MailEngine;
import com.mangoca.cmp.helper.MailHelper;
import com.mangoca.cmp.model.EmailControl;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.IEmailControlService;
import com.mangoca.cmp.service.IUserAccountService;
import com.mangoca.cmp.service.IUserService;
import com.mangoca.cmp.service.spring.EmailControlSpringService;
import com.mangoca.cmp.service.spring.UserAccountSpringService;
import com.mangoca.cmp.service.spring.UserSpringService;
import com.mangoca.cmp.web.form.PinForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.jasypt.hibernate3.encryptor.HibernatePBEStringEncryptor;

/**
 *
 * @author Masudul Haque
 */
public class PinAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";

    public ActionForward login(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionForward forward = new ActionForward();
        forward.setPath("/PinAction.do?method=add");
        return forward;
    }

    public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(ADD);
    }

    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {


        return mapping.findForward(VIEW);
    }

    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PinForm pinForm = (PinForm) form;
        String mobileNo = pinForm.getMobileNo();
        String userCIN = pinForm.getUserCIN();
        UserAccount userAccount = null;
        IUserAccountService service = UserAccountSpringService.getInstance();
        if (userCIN != null && mobileNo != null) {
            userAccount = service.findUserAccountByMobileNo(userCIN, mobileNo);
        }
        if (userAccount != null
                && pinForm.getSmsPin().equals(userAccount.getSmsPin())
                && pinForm.getConfSmsPin().equals(userAccount.getSmsPin())) {
            IUserService userService = UserSpringService.getInstance();
            User user = userService.getUserByUserName(userAccount.getUserName());
            userAccount.setIsPinValidate(Boolean.TRUE);
            if (userAccount.getIsEmailValid()) {
                user.setUserRole(User.USER_ACTIVE);
                userAccount.setUserRole(User.USER_ACTIVE);
            } else {
                user.setUserRole(User.USER_PIN_VALIDATE);
                userAccount.setUserRole(User.USER_PIN_VALIDATE);
            }

            HibernatePBEStringEncryptor encryptor = new HibernatePBEStringEncryptor();
            // encryptor.setAlgorithm("MD5");
            encryptor.setPassword(Constants.ENCRYPT_PASSWORD);
            IEmailControlService emailService = EmailControlSpringService.getInstance();

            EmailControl emailControl = emailService.findEmailControlByEmailType(EmailControl.EMAIL_ACTIVATION);

            String mailBody = MailHelper.activationMailBody(userAccount, emailControl.getMsgBody(), encryptor.decrypt(user.getPassword()));//MailConstant.ACTIVATION_MAIL_TEXT+"\n\n"+"https://portal.mangoca.com/cmp/guestAction.do?method=mailActivation&validity="+validityNo;
            boolean isSend = MailEngine.isMailSendToUserAccount(emailControl.getSubject(), userAccount.getEmailAccount(),
                    mailBody);
            service.persistUserAccount(userAccount);
            userService.persistUser(user);

            if (isSend) {
                request.setAttribute("success", "save.success.add.pin");
                request.setAttribute("instruction", "instruction.pin.success");
            }
            //ActionForward forward= new ActionForward();
            //forward.setPath("/loginAction.do");
            //return forward;
            return mapping.findForward(SUCCESS);

        } else {

            // request.getSession().setAttribute("userRole", 100);
            // request.setAttribute("userAccount", userAccount);
            request.setAttribute("failure", "failure.pin.vertification");
            //ActionForward forward= new ActionForward();
            //forward.setPath("/PinAction.do?method=add");
            //return forward;
            return mapping.getInputForward();
        }
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(SUCCESS);
    }
}
