/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.MyConverter;
import com.mangoca.cmp.model.MangoSSLCert;
import com.mangoca.cmp.service.IMangoSSLCertService;
import com.mangoca.cmp.service.spring.MangoSSLCertSpringService;
import com.mangoca.cmp.web.form.MangoSSLCertForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author Masudul Haque
 */
public class MangoSSLCertAction extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";
   
    private final static String ADD = "add";
    private final static String EDIT = "edit";
    private final static String VIEW = "view";
    
      public ActionForward add(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        return mapping.findForward(ADD);
    } 
    
    public ActionForward edit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setMangoSSLCert(request);
        return mapping.findForward(EDIT);
    }

    public ActionForward view(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        setMangoSSLCert(request);
        return mapping.findForward(VIEW);
    }
    
    public ActionForward save(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session= request.getSession();
        
        String userName= (String) session.getAttribute("userName");
        
        IMangoSSLCertService certService=MangoSSLCertSpringService.getInstance();
        MangoSSLCertForm mangocertForm=  (MangoSSLCertForm) form;
        String idStr= request.getParameter("id");
        int id=MyConverter.strToInt(idStr);
        MangoSSLCert mangocert=null;
        
        if(id!=0){
          mangocert= certService.findMangoSSLCertById(id);
        }
        else{
            mangocert= new MangoSSLCert();
        }
        BeanUtils.copyProperties(mangocert, mangocertForm);
        mangocert.setUpdatedDate(new Date());
        mangocert.setUpdatedBy(userName);
        certService.persistMangoSSLCert(mangocert);
        
        request.setAttribute("success", "save.success.MangoSSLCert");
        ActionForward forward = new ActionForward();
        forward.setPath("/MangoSSLCertData.do");
        return forward;
    }
    public ActionForward list(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        ActionForward forward = new ActionForward();
        forward.setPath("/MangoSSLCertData.do");
        return forward;
    }
    
    public ActionForward delete(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        String idStr= request.getParameter("id");
        Integer id=MyConverter.strToInt(idStr);
        IMangoSSLCertService service=MangoSSLCertSpringService.getInstance();
        
        MangoSSLCert mangoSSL= service.findMangoSSLCertById(id);
        service.removeMangoSSLCert(mangoSSL);
        ActionForward forward = new ActionForward();
        forward.setPath("/MangoSSLCertData.do");
        return forward;
    }

    private void setMangoSSLCert(HttpServletRequest request) throws Exception{
       
        String idStr= request.getParameter("id");
        Integer id=MyConverter.strToInt(idStr);
//        ApplicationContext context= getWebApplicationContext();
        IMangoSSLCertService service=MangoSSLCertSpringService.getInstance();
        
        MangoSSLCert mangoSSL= service.findMangoSSLCertById(id);
        
        request.setAttribute("mangoSSL", mangoSSL);
    }
}
