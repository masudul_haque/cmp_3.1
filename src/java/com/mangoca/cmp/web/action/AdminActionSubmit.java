/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.model.Accaction;
import com.mangoca.cmp.model.Certificate;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Raaction;
import com.mangoca.cmp.model.Techaction;
import com.mangoca.cmp.model.Vaaction;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.web.form.AdminActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
//import org.springframework.context.ApplicationContext;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class AdminActionSubmit extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        AdminActionForm actionForm = (AdminActionForm) form;
       // ApplicationContext context = getWebApplicationContext();
        HttpSession session = request.getSession();
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
        Enrolleduser enrolleduser = (Enrolleduser) session.getAttribute("enrolleduser");
        if (enrolleduser != null) {
            String userRole = (String) session.getAttribute("userRole");
            if (userRole.equals(Constants.ADMIN_RA)) {
                Raaction raaction = enrolleduser.getRaaction();
                if (raaction == null) {
                    raaction = new Raaction();
                }
                BeanUtils.copyProperties(raaction, actionForm);
                
                enrolleduser.setRaaction(raaction);
            } else if (userRole.equals(Constants.ADMIN_ACC)) {
                Accaction accaction = enrolleduser.getAccaction();
                if (accaction == null) {
                    accaction = new Accaction();
                }
                BeanUtils.copyProperties(accaction, actionForm);
                enrolleduser.setAccaction(accaction);
            } else if (userRole.equals(Constants.ADMIN_VA)) {
                Vaaction vaaction = enrolleduser.getVaaction();
                if (vaaction == null) {
                    vaaction = new Vaaction();
                }
                BeanUtils.copyProperties(vaaction, actionForm);
                enrolleduser.setVaaction(vaaction);
            } else if (userRole.equals(Constants.ADMIN_TECH)) {
                Techaction techaction = enrolleduser.getTechaction();
                if (techaction == null) {
                    techaction = new Techaction();
                }
                BeanUtils.copyProperties(techaction, actionForm);
                
                Certificate certificate= enrolleduser.getCertificate();
                if(certificate==null){
                    certificate= new Certificate();
                }
                certificate.setCertStatus(Certificate.CERT_ACTIVE);
                
                enrolleduser.setTechaction(techaction);
                enrolleduser.setCertificate(certificate);
            }
            enrolleduserService.persistEnrolleduser(enrolleduser);
            session.setAttribute("enrolleduser", enrolleduser);
        }

        return mapping.findForward(SUCCESS);
    }
}
