/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.service.ITicketService;
import com.mangoca.cmp.service.spring.TicketSpringService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
//import org.springframework.web.struts.DispatchActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class CcMenuAction extends DispatchAction {

    
    public ActionForward ccOpen(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
       // ApplicationContext context = getWebApplicationContext();
        ITicketService clientTicketService = TicketSpringService.getInstance();

        List clientTickets = clientTicketService.findTicketsByStatus(Constants.TICKET_OPEN);

        session.setAttribute("clientTickets", clientTickets);

        ActionForward forward = new ActionForward();
        forward.setPath("/ccTicketData.do");
        return forward;
    }

    public ActionForward ccReOpen(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
//        ApplicationContext context = getWebApplicationContext();
        ITicketService clientTicketService = TicketSpringService.getInstance();

        List clientTickets = clientTicketService.findTicketsByStatus(Constants.TICKET_RE_OPEN);

        session.setAttribute("clientTickets", clientTickets);

        ActionForward forward = new ActionForward();
        forward.setPath("/ccTicketData.do");
        return forward;
    }

    public ActionForward ccClosed(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
//        ApplicationContext context = getWebApplicationContext();
        ITicketService clientTicketService = TicketSpringService.getInstance();

        List clientTickets = clientTicketService.findTicketsByStatus(Constants.TICKET_CLOSED);

        session.setAttribute("clientTickets", clientTickets);

        ActionForward forward = new ActionForward();
        forward.setPath("/ccTicketData.do");
        return forward;
    }

}
