/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.action;

import com.mangoca.cmp.helper.UserHelper;
import com.mangoca.cmp.model.Ccaction;
import com.mangoca.cmp.model.Certificate;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Raaction;
import com.mangoca.cmp.model.Renewal;
import com.mangoca.cmp.model.Techaction;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.IUserAccountService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import com.mangoca.cmp.service.spring.UserAccountSpringService;
import com.mangoca.cmp.web.form.AdminActionForm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
//import org.springframework.context.ApplicationContext;
//import org.springframework.web.struts.ActionSupport;

/**
 *
 * @author Masudul Haque
 */
public class RenewAdminSubmit extends Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        AdminActionForm actionForm = (AdminActionForm) form;
        HttpSession session = request.getSession();
        IEnrolleduserService enrolleduserService = EnrolleduserSpringService.getInstance();
        Enrolleduser enrolleduser = enrolleduserService.findEnrolluserByUserCIN(actionForm.getUserCIN());
        if (enrolleduser != null) {
            User user = (User) session.getAttribute("user");
            String userName = user.getUserName();
            Renewal renewal = enrolleduser.getRenewal();
            if (renewal == null) {
                renewal = new Renewal();
            }

            Integer userRole = (Integer) session.getAttribute("userRole");
            if (userRole.equals(User.ADMIN_RA)) {
                Raaction raaction = renewal.getRaaction();
                if (raaction == null) {
                    raaction = new Raaction();
                }
                BeanUtils.copyProperties(raaction, actionForm);

                raaction.setUpdatedBy(userName);
                raaction.setUpdateDate(new Date());
                renewal.setRaaction(raaction);
            } else if (userRole.equals(User.ADMIN_CC)) {
                Ccaction ccaction = renewal.getCcaction();
                if (ccaction == null) {
                    ccaction = new Ccaction();
                }
                BeanUtils.copyProperties(ccaction, actionForm);

                ccaction.setUpdatedBy(userName);
                ccaction.setUpdateDate(new Date());
                renewal.setCcaction(ccaction);
            } else if (userRole.equals(User.ADMIN_TECH)) {
                Techaction techaction = renewal.getTechaction();
                if (techaction == null) {
                    techaction = new Techaction();
                }
                BeanUtils.copyProperties(techaction, actionForm);
                if (Renewal.STATUS_COMPLETE.equals(actionForm.getStatus())) {
                    //certificate.setCertStatus(Certificate.CERT_REVOKED);
                    //renewal.setStatus(Renewal.STATUS_COMPLETE);

                    //enrolleduser.setCertificate(certificate);
                    IUserAccountService userAccountService=UserAccountSpringService.getInstance();
                    UserAccount userAccount= userAccountService.getUserAccountByUserName(enrolleduser.getUserName());
                    
                    
                    String userCin = userAccount.getUserCIN();//userAccountService.generateCIN(userAccount.getRaSelect());
                    String nexCIN= UserHelper.generateRenewalCIN(userCin);
                    userAccount.setUserCIN(nexCIN);
                    
                    Enrolleduser newEnrolleduser= new Enrolleduser();
                    BeanUtils.copyProperties(newEnrolleduser, enrolleduser);
                    
                    Certificate certificate = new Certificate();
                    Certificate renewalCert= renewal.getCertificate();
                    BeanUtils.copyProperties(certificate, renewalCert);
                    certificate.setId(null);
                    certificate.setCertStatus(Certificate.CERT_ACTIVE);
                    certificate.setIssuedBy(userName);
                    certificate.setIssueDate(new Date());
                    newEnrolleduser.setCertClass(renewalCert.getCertClass());
                    newEnrolleduser.setCertificate(certificate);
                    newEnrolleduser.setRenewal(null);
                    newEnrolleduser.setStatus(Enrolleduser.ACTIVE);
                    newEnrolleduser.setEnrollId(null);
                    newEnrolleduser.setUserCIN(nexCIN);
                    enrolleduser.setStatus(Enrolleduser.IN_ACTIVE);
                    
                    enrolleduserService.persistEnrolleduser(newEnrolleduser);
                    enrolleduserService.persistEnrolleduser(enrolleduser);
                    userAccountService.persistUserAccount(userAccount);
                    
                    request.setAttribute("success", "success.admin.Renewal.action");
                    ActionForward forward = new ActionForward();
                    forward.setPath("/adminMenuAction.do?method=adminHome");
                    return forward;

                } else if (Renewal.STATUS_PENDING.equals(actionForm.getStatus())) {
                    renewal.setStatus(Renewal.STATUS_PENDING);
                }
                techaction.setUpdatedBy(userName);
                techaction.setUpdateDate(new Date());
                renewal.setTechaction(techaction);
            }
            enrolleduser.setRenewal(renewal);
            enrolleduserService.persistEnrolleduser(enrolleduser);
        } else {
            request.setAttribute("failure", "failure.admin.Renewal.action");
        }

        ActionForward forward = new ActionForward();
        forward.setPath("/adminMenuAction.do?method=adminHome");
        return forward;


    }
}