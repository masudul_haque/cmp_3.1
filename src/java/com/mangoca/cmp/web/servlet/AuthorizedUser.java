/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.web.servlet;

import com.mangoca.cmp.model.User;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Masudul Haque
 */
public class AuthorizedUser implements Filter {

    private FilterConfig filterConfig = null;
    String urls;

    @Override
    public void init(FilterConfig config) throws ServletException {

        this.filterConfig = filterConfig;
        urls = config.getInitParameter("avoid");


    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

        boolean authorized = false;
        if (req instanceof HttpServletRequest) {
            HttpSession session = ((HttpServletRequest) req).getSession(false);
            if (session != null) {
                User user = (User) session.getAttribute("user");
                if (user != null || urls.indexOf("guest") != -1) {
                    authorized = true;
                }
            }
        }

        if (!authorized) {
            HttpServletResponse response = (HttpServletResponse) res;
            HttpServletRequest request= (HttpServletRequest) req;
            HttpSession session = request.getSession(false);
            if (null == session) {
                response.sendRedirect("index.jsp");
            }
        }

        chain.doFilter(req, res);
    }

    @Override
    public void destroy() {
    }
}
