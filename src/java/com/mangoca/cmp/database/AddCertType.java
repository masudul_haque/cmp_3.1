/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.database;

import com.mangoca.cmp.model.CertType;

/**
 *
 * @author Masudul Haque
 */
public class AddCertType {
    public static void main(String[] args) {
        addData("Soft Token (User Generated)");
        addData("Soft Token (as .p12 file)");
        addData("USB Hard Token");
        addData("Smart Card Token");
    }
     public static void addData(String name){
          CertType type= new CertType();
          type.setCertTypeKey(name);
          DatabaseOperation.addCertTypeData(type);
      }
}
