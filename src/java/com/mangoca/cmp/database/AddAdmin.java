package com.mangoca.cmp.database;

import com.mangoca.cmp.model.AdminAccount;
import com.mangoca.cmp.model.User;
import org.jasypt.hibernate3.encryptor.HibernatePBEStringEncryptor;

public class AddAdmin {

    /**
     * @param args
     */
    public static void main(String[] args) {

        addAdminData("100", "Admin", "Admin",
                "admin", "admin", "admin@mango.com.bd",
                "01730068800", User.ADMIN_ADMIN, null, null);
        addAdminData("100", "Tabassum", "Wardina",
                "mango", "0", "mango@mango.com.bd",
                "01730068800", User.ADMIN_RA, "Mango RA", "11");
        addAdminData("100", "Mohammad", "Selim",
                "ra", "0", "selim@mango.com.bd",
                "01730068800", User.ADMIN_RA, "RA Chittagong", "12");
        addAdminData("100", "Kazi Al", "Mamun",
                "acc", "0", "mamun@mango.com.bd",
                "01730068800", User.ADMIN_ACC, "", "");
        addAdminData("100", "Rezaul", "Karim",
                "va", "0", "reza@mango.com.bd",
                "01730068800", User.ADMIN_VA, "", "");
        addAdminData("100", "Ariful", "Alam",
                "tech", "0", "arif@mango.com.bd",
                "01730068800", User.ADMIN_TECH, "", "");
        addAdminData("100", "Ariful", "Alam",
                "cc", "0", "cc@mango.com.bd",
                "01730068800", User.ADMIN_CC, "", "");

    }

    public static void addAdminData(
            String employeeId, String firstName, String lastName,
            String userName, String password, String emailAccount,
            String mobileNo, int userRole, String raName, String raSymbol) {

        User u = new User();
        AdminAccount user = new AdminAccount();
        user.setEmployeeId(employeeId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setUserName(userName);

        HibernatePBEStringEncryptor encryptor = new HibernatePBEStringEncryptor();

        encryptor.setPassword("0000");
        String encryptPassword = encryptor.encrypt(password);
        u.setPassword(encryptPassword);
        user.setEmailAccount(emailAccount);
        user.setMobileNo(mobileNo);
        u.setUserRole(userRole);
        user.setUserRole(userRole);
        u.setUserName(userName);
        //user.set
        user.setRaName(raName);
        user.setRaSymbol(raSymbol);
        DatabaseOperation.addUserData(u);
        DatabaseOperation.addNewData(user);



    }
}
