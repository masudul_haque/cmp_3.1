package com.mangoca.cmp.database;

public class MailConstant {

	public static final String MAIL_HOST = "localhost";
	public static final String MAIL_MESSAGE_FROM = "noreply@mangoca.com";
	public static final String ACTIVATION_MAIL_SUBJECT = "Mango CA application enrollment activation link";
	public static final String FORGOT_USERNAME_SUBJECT = "Mango CA Forgot Username";
	public static final String FORGOT_PASSWORD_SUBJECT = "Mango CA Forgot Password";
	public static final String CERT_ISSUE_SUBJECT = "Your digital certificate application process completed";
	
	public static final String ACTIVATION_MAIL_TEXT="Welcome to Mango CA website. " +
			"Following is your email activation link.\nClick to following link to activate your email account.\n\n";

	public static final String FORGOT_USERNAME_TEXT="You have requested to recover your Mango CA username.\n";
	public static final String FORGOT_PASSWORD_TEXT="You have requested to recover your Mango CA password.\n";
	public static final String MAIL_SIGNATURE="\n\nThank You\n"+
	                                           "Mango Teleservices Ltd.";
}
