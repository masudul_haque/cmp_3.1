/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.database;

import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.Raaction;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 *
 * @author Masudul Haque
 */
public class PersistTest {
    public static void main(String[] args) {
          try{
        ApplicationContext context= new FileSystemXmlApplicationContext("/web/WEB-INF/applicationContext.xml");
        IEnrolleduserService userService= EnrolleduserSpringService.getInstance();
      // Enrolleduser enrolleduser= new Enrolleduser();
        Enrolleduser enrolleduser= userService.findEnrolleduserById(2L);
        Raaction raAction= enrolleduser.getRaaction();
        if(raAction==null){
            raAction=new Raaction();
        }
    //    raAction.setStatus("Pending");
        enrolleduser.setRaaction(raAction);
       userService.persistEnrolleduser(enrolleduser);
       }
       catch(Exception exp){} 
    }
}
