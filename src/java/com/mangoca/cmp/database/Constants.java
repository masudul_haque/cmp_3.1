package com.mangoca.cmp.database;

public class Constants {

    public static final String FORGOT_USERNAME = "Username";
    public static final String FORGOT_PASSWORD = "Password";
    public static final String SERVER_LOCATION = "/var/lib/tomcat6/webapps/cmp/file/";
    public static final String LOCAL_HOST_LOCATION = "F:/Netbeans Project/Professional/cmp/web/file/";
    public static final String LOCAL_HOST_EMAIL_ATTACH_LOCATION = "F:/Netbeans Project/Professional/cmp/web/attachment/";
    public static final String SERVER_EMAIL_ATTACH_LOCATION= "/var/lib/tomcat6/webapps/cmp/attachment/";
    public static final String GLOBAL_LOGIN_FORWARD = "login";
    public static final int ROW_PER_PAGE = 5;
    public static final String HOST_NAME = "http://mtslwas.mangoca.com/cmp";
    public static final String CERT_DOWN_LINK = "https://secure.mangoca.com/enrol/browser.jsp";
    public static final String EJBCA_ADMIN_HOME = "https://192.168.2.116:8443/ejbca/adminweb";
    public static final String DATE_PICKER_PATTERN = "dd/MM/yyyy";
    /*******************CIN RA********************************/
    public static final String ENCRYPT_PASSWORD = "0000";
    public static final String RA_DIGIT = "11";
    public static final String RA_SYMBOL = "A";
    public static final String SENDER_NO = "12334345";
    /*************ENROLL Account Status*****************/
    public static final String ACC_NEW_REQ = "New";
    public static final String ACC_RE_NEW_REQ = "Renew";
    public static final String ACC_RE_KEY_REQ = "Rekey";
    public static final String ACC_IN_PROCESS_USER = "Pending";
    public static final String ACC_COMPLETED_USER = "Completed";
    /*************ENROLL RA Status*****************/
    public static final String RA_NEW_REQ = "New";
    public static final String RA_INCOMPLETE_REQ = "Incomplete";
    public static final String RA_PENDING_REQ = "Pending";
    public static final String RA_COMPLETED_REQ = "Completed";
    /***************Validation Status*******************/
    public static final String VA_NEW_REQ = "New";
    public static final String VA_PENDING_REQ = "Pending";
    public static final String VA_COMPLETED_REQ = "Completed";
    /*****************Technical Status****************************/
    public static final String TECH_NEW_REQ = "New";
    public static final String TECH_PENDING_REQ = "Pending";
    public static final String TECH_COMPLETED_REQ = "Completed";
    /******************CERT Status********************/
    public static final String CERT_DOWNLOADABLE = "Downloadable";
    public static final String CERT_NEW = "New";
    public static final String CERT_IN_PROCESS = "Pending";
    public static final String CERT_APPLY_REVOKE = "Apply for Revocaion";
    public static final String CERT_APPLY_RENEW = "Apply for Renew";
    public static final String CERT_APPLY_SUSPEND = "Apply for Suspension";
    public static final String CERT_REVOKED = "Revoked";
    public static final String CERT_SUSPENDED = "Suspended";
    public static final String CERT_ACTIVE = "Active";
    /*******************Ticket Status*******************/
    public static final String TICKET_OPEN = "Open";
    public static final String TICKET_RE_OPEN = "Reopen";
    public static final String TICKET_CLOSED = "Closed";
    public static final Integer NO_OF_STATUS = 5;
    public static final Integer CLASS_ONE_PRICE = 200;
    public static final Integer CLASS_TWO_INDIV_CERT_PRICE = 300;
    public static final Integer CLASS_TWO_ENTER_CERT_PRICE = 500;
    public static final Integer CLASS_THREE_INDIV_CERT_PRICE = 1000;
    public static final Integer CLASS_THREE_ENTER_CERT_PRICE = 2000;
    public static final String MIN_TIME_OF_DAY = " 00:00:00";
    public static final String MAX_TIME_OF_DAY = " 23:59:59";
    public static final String STANDARD_DATE_FORMATE = "MM/dd/yyyy HH:mm:ss";
    public static final String DATABASE_DATE_FORMATE = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_OF_BIRTH = "dd MMM yyyy";
    /*************User Role************/
    public static final String USER_INITIAL = "Initial";
    public static final String USER_ACTIVE = "Active";
    public static final String USER_ENROLL = "Enrolled";
    public static final String ADMIN_ACC = "Account Admin";
    public static final String ADMIN_RA = "RA Admin";
    public static final String ADMIN_CC = "CC Admin";
    public static final String ADMIN_VA = "VA Admin";
    public static final String ADMIN_TECH = "Technical";
    public static final String ADMIN_ADMIN = "Super Admin";
    public static final String ADMIN_LOCKED = "Locked";
    /*************************************/
    public static final String BANK_PAYMENT_MODE = "Bank";
    public static final String CASH_PAYMENT_MODE = "Cash";
    /*************CERT Class******************/
    public static final String CERT_CLASS_1 = "Class 1- Individual";
    public static final String CERT_CLASS_2 = "Class 2- Individual";
    public static final String CERT_CLASS_3 = "Class 2- Enterprise";
    public static final String CERT_CLASS_4 = "Class 3- Individual";
    public static final String CERT_CLASS_5 = "Class 3- Enterprise";
    public static final String CERT_CLASS_6 = "Class 3- Server & Device";
    /************Duration of CERT****************/
    public static final int CERT_DURATION = 365;
    public static final int CERT_KEY_LENGTH = 2048;
    public static final String CERT_VERSION = "V3";
    /*************CERT Type********************/
    public static final String CERT_INDIVIDUAL = "Individual";
    public static final String CERT_ENTERPRISE = "Enterprise";
    public static final String CERT_SERVER_DEVICE = "Server and Device";
    /*************Revocation Status*******************/
    public static final String REVOCATION_NEW = "New";
    public static final String REVOCATION_PENDING = "Pending";
    public static final String REVOCATION_COMPLETED = "Completed";
    /*************Suspension Status*******************/
    public static final String SUSPENSION_NEW = "New";
    public static final String SUSPENSION_PENDING = "Pending";
    public static final String SUSPENSION_COMPLETED = "Completed";
    /******************Renewal Status*******************/
    public static final String RENEWAL_NEW = "New";
    public static final String RENEWAL_PENDING = "Pending";
    public static final String RENEWAL_COMPLETED = "Completed";
    /******************Revocation Status*******************/
    public static final String REKEY_NEW = "New";
    public static final String REKEY_IN_PROCESS = "Eligible to Rekey";
    public static final String REKEY_COMPLETED = "Completed";
    /******************CC ticket Status********************/
    public static final String CC_TICKET_OPEN = "Open";
    public static final String CC_TICKET_RE_OPEN = "Reopen";
    public static final String CC_TICKET_CLOSED = "Closed";
    /******************DOC Type**********************/
    public static final String DOC_TYPE_NID = "NID";
    public static final String DOC_TYPE_PASSPORT = "Passport";
    public static final String DOC_TYPE_DRIVING_LICENCE = "Driving Licence";
    public static final String DOC_TYPE_TIN = "TIN";
    public static final String DOC_TYPE_SLIP = "Payment Slip";
    public static final String DOC_TYPE_TRANSACTION_DOC = "Transaction DOC";
    public static final String DOC_TYPE_OTHER = "Other";
    /******************DOC Uploaded For***************/
    public static final String DOC_UPLOADED_FOR_VALIDATION = "Validation";
    public static final String DOC_UPLOADED_FOR_ACCOUNT = "Account";
    /******************DOC Status******************/
    public static final String DOC_NEW = "New";
    public static final String DOC_VALIDATE = "Validated";
    public static final String DOC_REJECTED = "Rejected";
    public static final String DOC_IN_PROCESS = "Pending";
    /******************DOC Status******************/
    public static final String PAYMENT_PAID = "Paid";
    public static final String PAYMENT_DUE = "Due";
    public static final String PAYMENT_CANCEL = "Canceled";
    /******************DOC Path********/
    public static final String DOC_PATH = "F:/server/jboss-4.2.2.GA/server/default/deploy/jboss-web.deployer/ROOT.war/data/";
    //public static final String DOC_PATH=System.getProperty("user.home")+"/";
    //public static final String DOC_PATH="F:/PHP backup/wamp/www/data/";
    public static final String DOC_DISPLAY_PATH = "http://localhost:8080/data/";
    /******************RA status*******************/
    public static final String RA_ACTIVE = "Active";
    public static final String RA_LOCKED = "Locked";
    public static final String RA_PENDING = "Pending";
    public static final String RA_REVOKE_NEW = "New";
    public static final String RA_REVOKE_PENDING = "Pending";
    public static final String RA_REVOKE_COMPLETE = "Completed";
    public static final String CC_REVOKE_NEW = "New";
    public static final String CC_REVOKE_PENDING = "Pending";
    public static final String CC_REVOKE_COMPLETE = "Completed";
    public static final String TECH_REVOKE_NEW = "New";
    public static final String TECH_REVOKE_PENDING = "Pending";
    public static final String TECH_REVOKE_COMPLETE = "Completed";
    public static final String RA_RENEW_NEW = "New";
    public static final String RA_RENEW_PENDING = "Pending";
    public static final String RA_RENEW_COMPLETE = "Completed";
    public static final String CC_RENEW_NEW = "New";
    public static final String CC_RENEW_PENDING = "Pending";
    public static final String CC_RENEW_COMPLETE = "Completed";
    public static final String TECH_RENEW_NEW = "New";
    public static final String TECH_RENEW_PENDING = "Pending";
    public static final String TECH_RENEW_COMPLETE = "Completed";
    public static final String RA_SUSPENSION_NEW = "New";
    public static final String RA_SUSPENSION_PENDING = "Pending";
    public static final String RA_SUSPENSION_COMPLETE = "Completed";
    public static final String CC_SUSPENSION_NEW = "New";
    public static final String CC_SUSPENSION_PENDING = "Pending";
    public static final String CC_SUSPENSION_COMPLETE = "Completed";
    public static final String TECH_SUSPENSION_NEW = "New";
    public static final String TECH_SUSPENSION_PENDING = "Pending";
    public static final String TECH_SUSPENSION_COMPLETE = "Completed";
    public static final int ADMIN_ACCOUNT = 1;
    public static final int USER_ACCOUNT = 2;
    public static final int MANGO_CERT = 3;
    public static final int ENROLLED_USER = 4;
    public static final int DOCUMENT = 5;
    public static final int PAYMENT = 6;
    public static final int RENEWAL = 7;
    public static final int REVOCATION = 8;
    public static final int SUSPENSION = 9;
    public static final int TICKET = 10;
}
