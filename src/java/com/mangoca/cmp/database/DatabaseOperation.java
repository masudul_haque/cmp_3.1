package com.mangoca.cmp.database;

import com.mangoca.cmp.model.AdminAccount;
import com.mangoca.cmp.model.CertType;
import com.mangoca.cmp.model.Locality;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.IAdminAccountService;
import com.mangoca.cmp.service.ICertFieldService;
import com.mangoca.cmp.service.ICertTariffService;
import com.mangoca.cmp.service.IUserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;


public class DatabaseOperation {
	
   public static void addNewData(final AdminAccount data){
       /* Session session=HibernateSession.getSession();

        session.beginTransaction();
        session.merge(data);
        session.getTransaction().commit();

        session.close();
        * 
        */
       try{
        ApplicationContext context= new FileSystemXmlApplicationContext("/web/WEB-INF/applicationContext.xml");
        
        
        IAdminAccountService userService= (IAdminAccountService) context.getBean("AdminAccountService");
        userService.persistAdminAccount(data);
       }
       catch(Exception exp){} 
       
}

    static void addUserData(final User u) {
        try{
        ApplicationContext context= new FileSystemXmlApplicationContext("/web/WEB-INF/applicationContext.xml");
       IUserService userService= (IUserService) context.getBean("UserService");
        userService.persistUser(u);
       }
       catch(Exception exp){} 
    }
       
    static void addLocalityData(final Locality l) {
        try{
        ApplicationContext context= new FileSystemXmlApplicationContext("/web/WEB-INF/applicationContext.xml");
       ICertFieldService userService=  (ICertFieldService) context.getBean("CertFieldService");
        userService.persistLocality(l);
       }
       catch(Exception exp){} 
    }

    static void addCertTypeData(CertType type) {
        try{
        ApplicationContext context= new FileSystemXmlApplicationContext("/web/WEB-INF/applicationContext.xml");
       ICertTariffService userService=   (ICertTariffService) context.getBean("CertTariffService");
        userService.persistCertType(type);
       }
       catch(Exception exp){}  
    }
        
}
