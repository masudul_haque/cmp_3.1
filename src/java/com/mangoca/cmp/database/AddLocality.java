/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.database;

import com.mangoca.cmp.model.Locality;

/**
 *
 * @author Masudul Haque
 */
public class AddLocality {
      public static void main(String[] args) {
          addData("Dhaka");
          addData("Chittagong");
          addData("Barisal");
          addData("Rajshahi");
          addData("Khulna");
          addData("Shylet");
          addData("Rangpur");
      }
      
      public static void addData(String name){
          Locality locality= new Locality();
          locality.setName(name);
          DatabaseOperation.addLocalityData(locality);
      }
}
