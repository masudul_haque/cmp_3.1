/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.database;

import com.mangoca.cmp.helper.EncryptionHelper;
import com.mangoca.cmp.model.EmailControl;
import com.mangoca.cmp.service.IEmailControlService;
import java.text.MessageFormat;
import org.jasypt.digest.StandardStringDigester;
import org.jasypt.util.password.ConfigurablePasswordEncryptor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 *
 * @author Masudul Haque
 */
public class ReadResource {

    static String message = "Hello {0},"
            + "\nI am from {1}";

    public static void main(String[] args) throws Exception {
        //ResourceBundle rb = ResourceBundle.getBundle("email", Locale.US);
     /*   ApplicationContext context= new FileSystemXmlApplicationContext("/web/WEB-INF/applicationContext.xml");
       
         IEmailControlService service= (IEmailControlService) context.getBean("EmailControlService");//EmailControlSpringService.getInstance();
        
         EmailControl emailControl= service.findEmailControlById(2);
        
         String val=MessageFormat.format(emailControl.getMsgBody(), "masud");
         System.out.println(val);
         * 
         */
        //StandardStringDigester digester = new StandardStringDigester();
        //digester.setAlgorithm("SHA-1");

        // optionally set the algorithm
        //digester.setIterations(20);
        //  System.out.println(digester.digest("1234445645654"));
        // System.out.println(digester.matches("123444564565", "13jHqlfbFDUO5oJVsvP0pCZD364Gspb3DXLTCg=="));
        //ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
        //passwordEncryptor.setAlgorithm("SHA-1");
        //passwordEncryptor.setPlainDigest(true);
        //String encryptedPassword = passwordEncryptor.encryptPassword("0000");
       // String decryptedPassword= passwordEncryptor.
        //System.out.println("Encrpted Password" + encryptedPassword);
        
        String nid="5342543253245345";
        String enc=EncryptionHelper.encrypt(nid);
        System.out.println(enc);
        System.out.println("Return Plain :"+EncryptionHelper.decrypt(enc));
    }
}
