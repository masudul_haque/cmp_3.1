package com.mangoca.cmp.database;

import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AnnotationConfiguration conf = new AnnotationConfiguration();
		conf.configure();
                
		SchemaExport schemaExpo= new SchemaExport(conf);
		
		schemaExpo.execute(true, true, false, true); 
    } 

}
