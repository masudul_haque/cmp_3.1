/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "revocation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Revocation.findAll", query = "SELECT r FROM Revocation r"),
    @NamedQuery(name = "Revocation.findById", query = "SELECT r FROM Revocation r WHERE r.id = :id"),
    @NamedQuery(name = "Revocation.findByApplyDate", query = "SELECT r FROM Revocation r WHERE r.applyDate = :applyDate"),
    @NamedQuery(name = "Revocation.findByReasons", query = "SELECT r FROM Revocation r WHERE r.reasons = :reasons"),
    @NamedQuery(name = "Revocation.findByRevokedBy", query = "SELECT r FROM Revocation r WHERE r.revokedBy = :revokedBy"),
    @NamedQuery(name = "Revocation.findByRevokedDate", query = "SELECT r FROM Revocation r WHERE r.revokedDate = :revokedDate"),
    @NamedQuery(name = "Revocation.findByStatus", query = "SELECT r FROM Revocation r WHERE r.status = :status"),
    @NamedQuery(name = "Revocation.countByStatus", query = "SELECT count(r) FROM Revocation r WHERE r.status = :status"),
    @NamedQuery(name = "Revocation.findByRaStatus", query="SELECT e FROM Enrolleduser e WHERE e.revocation IS NOT NULL and e.revocation.raaction.status = :status"),
    @NamedQuery(name = "Revocation.findByCcStatus", query="SELECT e FROM Enrolleduser e WHERE e.revocation IS NOT NULL and e.revocation.ccaction.status = :status"),
    @NamedQuery(name = "Revocation.findByTechStatus", query="SELECT e FROM Enrolleduser e WHERE e.revocation IS NOT NULL and e.revocation.techaction.status = :status")
   })
public class Revocation implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int PAGE_SIZE=10;
    public static final Short STATUS_NEW=1;
    public static final Short STATUS_PENDING=2;
    public static final Short STATUS_COMPLETE=3;
    public static final Short STATUS_INCOMPLETE=4;
   
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "applyDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date applyDate;
    @Column(name = "reasons")
    private String reasons;
    @Column(name = "revokComment")
    private String revokComment;
    @Column(name = "revokedBy")
    private String revokedBy;
    @Column(name = "revokedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date revokedDate;
    @Column(name = "status")
    private Short status;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Techaction techaction;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Ccaction ccaction;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Raaction raaction;

    public Revocation() {
    }

    public Revocation(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reasons) {
        this.reasons = reasons;
    }

    public String getRevokComment() {
        return revokComment;
    }

    public void setRevokComment(String revokComment) {
        this.revokComment = revokComment;
    }

    public String getRevokedBy() {
        return revokedBy;
    }

    public void setRevokedBy(String revokedBy) {
        this.revokedBy = revokedBy;
    }

    public Date getRevokedDate() {
        return revokedDate;
    }

    public void setRevokedDate(Date revokedDate) {
        this.revokedDate = revokedDate;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    

    public Techaction getTechaction() {
        return techaction;
    }

    public void setTechaction(Techaction techaction) {
        this.techaction = techaction;
    }

    public Ccaction getCcaction() {
        return ccaction;
    }

    public void setCcaction(Ccaction ccaction) {
        this.ccaction = ccaction;
    }

    

    public Raaction getRaaction() {
        return raaction;
    }

    public void setRaaction(Raaction raaction) {
        this.raaction = raaction;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Revocation)) {
            return false;
        }
        Revocation other = (Revocation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.Revocation[ id=" + id + " ]";
    }
    
}
