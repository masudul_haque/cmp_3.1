/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "renewal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Renewal.findAll", query = "SELECT r FROM Renewal r"),
    @NamedQuery(name = "Renewal.findById", query = "SELECT r FROM Renewal r WHERE r.id = :id"),
    @NamedQuery(name = "Renewal.findByAccountStatus", query = "SELECT r FROM Renewal r WHERE r.accountStatus = :accountStatus"),
    @NamedQuery(name = "Renewal.findByApplyDate", query = "SELECT r FROM Renewal r WHERE r.applyDate = :applyDate"),
    @NamedQuery(name = "Renewal.findByReasons", query = "SELECT r FROM Renewal r WHERE r.reasons = :reasons"),
    @NamedQuery(name = "Renewal.findByRenewalDate", query = "SELECT r FROM Renewal r WHERE r.renewalDate = :renewalDate"),
    @NamedQuery(name = "Renewal.findByRenewedBy", query = "SELECT r FROM Renewal r WHERE r.renewedBy = :renewedBy"),
    @NamedQuery(name = "Renewal.findByStatus", query = "SELECT r FROM Renewal r WHERE r.status = :status"),
    @NamedQuery(name = "Renewal.countByStatus", query = "SELECT count(r) FROM Renewal r WHERE r.status = :status"),
    @NamedQuery(name = "Renewal.findByRaStatus", query="SELECT e FROM Enrolleduser e WHERE e.renewal IS NOT NULL and e.renewal.raaction.status = :status"),
    @NamedQuery(name = "Renewal.findByCcStatus", query="SELECT e FROM Enrolleduser e WHERE e.renewal IS NOT NULL and e.renewal.ccaction.status = :status"),
    @NamedQuery(name = "Renewal.findByTechStatus", query="SELECT e FROM Enrolleduser e WHERE e.renewal IS NOT NULL and e.renewal.techaction.status = :status")})
public class Renewal implements Serializable {
    private static final long serialVersionUID = 1L;
     public static final int PAGE_SIZE=10;
    public static final Short STATUS_NEW=1;
    public static final Short STATUS_PENDING=2;
    public static final Short STATUS_COMPLETE=3;
    public static final Short STATUS_INCOMPLETE=4;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "accountStatus")
    private String accountStatus;
    @Column(name = "applyDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date applyDate;
    @Column(name = "reasons")
    private String reasons;
    @Column(name = "comment")
    private String comment;
    @Column(name = "renewalAmount")
    private Integer renewalAmount;
    @Column(name = "renewalDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date renewalDate;
    @Column(name = "renewedBy")
    private String renewedBy;
    @Column(name = "status")
    private Short status;
    
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Techaction techaction;
    
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Raaction raaction;
    
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Ccaction ccaction;
    
    @OneToOne(cascade= CascadeType.ALL, fetch= FetchType.LAZY)
    private Certificate certificate;
    
    //TODO account action may need to include.
    public Renewal() {
    }

    public Renewal(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public Date getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }


    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reasons) {
        this.reasons = reasons;
    }


    public Integer getRenewalAmount() {
        return renewalAmount;
    }

    public void setRenewalAmount(Integer renewalAmount) {
        this.renewalAmount = renewalAmount;
    }

    public Date getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(Date renewalDate) {
        this.renewalDate = renewalDate;
    }

    public String getRenewedBy() {
        return renewedBy;
    }

    public void setRenewedBy(String renewedBy) {
        this.renewedBy = renewedBy;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }


    public Techaction getTechaction() {
        return techaction;
    }

    public void setTechaction(Techaction techaction) {
        this.techaction = techaction;
    }

    public Raaction getRaaction() {
        return raaction;
    }

    public void setRaaction(Raaction raaction) {
        this.raaction = raaction;
    }

    public Ccaction getCcaction() {
        return ccaction;
    }

    public void setCcaction(Ccaction ccaction) {
        this.ccaction = ccaction;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Renewal)) {
            return false;
        }
        Renewal other = (Renewal) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.Renewal[ id=" + id + " ]";
    }
    
}
