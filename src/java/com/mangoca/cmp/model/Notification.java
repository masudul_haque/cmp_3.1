/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name="notificatioin")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Notification.findAll", query = "SELECT n FROM Notification n"),
    @NamedQuery(name = "Notification.findById", query = "SELECT n FROM Notification n WHERE n.id = :id"),
    @NamedQuery(name = "Notification.findByCINAndStatus", query = "SELECT n FROM Notification n WHERE n.userCIN = :userCIN and n.status= :status")})
public class Notification  implements Serializable{   
    
    private static final long serialVersionUID = 1L;
    public static final int PAGE_SIZE=10;
    public static final Short STATUS_SENT=1;
    public static final Short STATUS_READ=2;
    public static final Short STATUS_CLOSED=3;
    public static final Short STATUS_FAILED=4;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name="userCIN")
    private String userCIN;
    @Column(name="sender_id")
    private Long snderId;
    @Column(name="sender_role")
    private int role;
    
    @Column(name="subject")
    private String subject;
    
    @Column(name="msg_body")
    private String msgBody;
    
    private Short status;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date sendDate;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date readDate;

    public String getUserCIN() {
        return userCIN;
    }

    public void setUserCIN(String userCIN) {
        this.userCIN = userCIN;
    }

    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public void setMsgBody(String msgBody) {
        this.msgBody = msgBody;
    }

    public Date getReadDate() {
        return readDate;
    }

    public void setReadDate(Date readDate) {
        this.readDate = readDate;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Long getSnderId() {
        return snderId;
    }

    public void setSnderId(Long snderId) {
        this.snderId = snderId;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
    
    
    
}
