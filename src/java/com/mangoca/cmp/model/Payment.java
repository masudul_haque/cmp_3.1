/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "payment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Payment.findAll", query = "SELECT p FROM Payment p"),
    @NamedQuery(name = "Payment.findById", query = "SELECT p FROM Payment p WHERE p.id = :id"),
    @NamedQuery(name = "Payment.findByAccStatus", query = "SELECT p FROM Payment p WHERE p.accStatus = :accStatus"),
    @NamedQuery(name = "Payment.findByAccountNo", query = "SELECT p FROM Payment p WHERE p.accountNo = :accountNo"),
    @NamedQuery(name = "Payment.findByAmount", query = "SELECT p FROM Payment p WHERE p.amount = :amount"),
    @NamedQuery(name = "Payment.findByBalance", query = "SELECT p FROM Payment p WHERE p.balance = :balance"),
    @NamedQuery(name = "Payment.findByBankDocLocation", query = "SELECT p FROM Payment p WHERE p.bankDocLocation = :bankDocLocation"),
    @NamedQuery(name = "Payment.findByBankName", query = "SELECT p FROM Payment p WHERE p.bankName = :bankName"),
    @NamedQuery(name = "Payment.findByBranchName", query = "SELECT p FROM Payment p WHERE p.branchName = :branchName"),
    @NamedQuery(name = "Payment.findByCertificateClass", query = "SELECT p FROM Payment p WHERE p.certificateClass = :certificateClass"),
    @NamedQuery(name = "Payment.findByComment", query = "SELECT p FROM Payment p WHERE p.comment = :comment"),
    @NamedQuery(name = "Payment.findByDepositorName", query = "SELECT p FROM Payment p WHERE p.depositorName = :depositorName"),
    @NamedQuery(name = "Payment.findByMoneyReceivedNo", query = "SELECT p FROM Payment p WHERE p.moneyReceivedNo = :moneyReceivedNo"),
    @NamedQuery(name = "Payment.findByPaymentDate", query = "SELECT p FROM Payment p WHERE p.paymentDate = :paymentDate"),
    @NamedQuery(name = "Payment.findByPaymentDocLocation", query = "SELECT p FROM Payment p WHERE p.paymentDocLocation = :paymentDocLocation"),
    @NamedQuery(name = "Payment.findByReceivedDate", query = "SELECT p FROM Payment p WHERE p.receivedDate = :receivedDate"),
    @NamedQuery(name = "Payment.findByRequestType", query = "SELECT p FROM Payment p WHERE p.requestType = :requestType"),
    @NamedQuery(name = "Payment.findByStatus", query = "SELECT p FROM Payment p WHERE p.status = :status"),
    @NamedQuery(name = "Payment.findByTransactionId", query = "SELECT p FROM Payment p WHERE p.transactionId = :transactionId"),
    @NamedQuery(name = "Payment.findByUpdatedBy", query = "SELECT p FROM Payment p WHERE p.updatedBy = :updatedBy"),
    @NamedQuery(name = "Payment.findByUpdatedDate", query = "SELECT p FROM Payment p WHERE p.updatedDate = :updatedDate")})
public class Payment implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int PAGE_SIZE=10;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "accStatus")
    private String accStatus;
    @Column(name = "userCIN")
    private String userCIN;
    @Column(name = "userName")
    private String userName;
    @Column(name = "fullName")
    private String fullName;
    @Column(name = "certClass")
    private String certClass;
    @Column(name = "accountNo")
    private String accountNo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private Double amount;
    @Column(name = "balance")
    private Double balance;
    @Column(name = "bankDocLocation")
    private String bankDocLocation;
    @Column(name = "bankName")
    private String bankName;
    @Column(name = "branchName")
    private String branchName;
    @Column(name = "certificateClass")
    private String certificateClass;
    @Column(name = "comment")
    private String comment;
    @Column(name = "depositorName")
    private String depositorName;
    @Column(name = "moneyReceivedNo")
    private String moneyReceivedNo;
    @Column(name = "paymentDate")
    @Temporal(TemporalType.DATE)
    private Date paymentDate;
    @Column(name = "paymentDocLocation")
    private String paymentDocLocation;
    @Column(name = "receivedDate")
    @Temporal(TemporalType.DATE)
    private Date receivedDate;
    @Column(name = "requestType")
    private String requestType;
    @Column(name = "status")
    private String status;
    @Column(name = "transactionId")
    private String transactionId;
    @Column(name = "updatedBy")
    private String updatedBy;
    @Column(name = "updatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

    public Payment() {
    }

    public Payment(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getBankDocLocation() {
        return bankDocLocation;
    }

    public void setBankDocLocation(String bankDocLocation) {
        this.bankDocLocation = bankDocLocation;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getCertificateClass() {
        return certificateClass;
    }

    public void setCertificateClass(String certificateClass) {
        this.certificateClass = certificateClass;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDepositorName() {
        return depositorName;
    }

    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

    public String getMoneyReceivedNo() {
        return moneyReceivedNo;
    }

    public void setMoneyReceivedNo(String moneyReceivedNo) {
        this.moneyReceivedNo = moneyReceivedNo;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentDocLocation() {
        return paymentDocLocation;
    }

    public void setPaymentDocLocation(String paymentDocLocation) {
        this.paymentDocLocation = paymentDocLocation;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCertClass() {
        return certClass;
    }

    public void setCertClass(String certClass) {
        this.certClass = certClass;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserCIN() {
        return userCIN;
    }

    public void setUserCIN(String userCIN) {
        this.userCIN = userCIN;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Payment)) {
            return false;
        }
        Payment other = (Payment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.Payment[ id=" + id + " ]";
    }
    
}
