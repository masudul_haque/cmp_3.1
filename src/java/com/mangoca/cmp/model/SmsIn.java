/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name="ozekimessagein")
@XmlRootElement
public class SmsIn  implements Serializable{
    
    private static final long serialVersionUID = 1L;
    public static final int PAGE_SIZE=10;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name="sender",length=30)
    private String sender;
    @Column(name="receiver",length=30)
    private String receiver;
    @Column(name="msg", length=160)
    private String msg;
    @Column(name="senttime")
    @Temporal(TemporalType.DATE)
    private Date senttime;
    @Column(name="receivedtime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date receivedtime;
     
    @Column(name="operator",length=100)
    private String operator;
    
    @Column(name="msgtype",length=160)
    private String msgtype;
     
    @Column(name="reference", length=100)
    private String reference;
    @Column(name="Reply_Status")
    private Boolean replyStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

  
    public String getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Date getReceivedtime() {
        return receivedtime;
    }

    public void setReceivedtime(Date receivedtime) {
        this.receivedtime = receivedtime;
    }

    

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Date getSenttime() {
        return senttime;
    }

    public void setSenttime(Date senttime) {
        this.senttime = senttime;
    }

    

    public Boolean getReplyStatus() {
        return replyStatus;
    }

    public void setReplyStatus(Boolean replyStatus) {
        this.replyStatus = replyStatus;
    }


    
    
}
