/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.TypeDef;
import org.jasypt.hibernate3.type.EncryptedStringType;

/**
 *
 * @author Masudul Haque
 */
@TypeDef(name = "encryptedString",
typeClass = EncryptedStringType.class,
parameters = {
    @Parameter(name = "encryptorRegisteredName", value = "myHibernateStringEncryptor")
})
@Entity
@Table(name = "admin_account")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdminAccount.findAll", query = "SELECT u FROM AdminAccount u"),
    @NamedQuery(name = "AdminAccount.findById", query = "SELECT u FROM AdminAccount u WHERE u.id = :id"),
    @NamedQuery(name = "AdminAccount.findByComment", query = "SELECT u FROM AdminAccount u WHERE u.comment = :comment"),
    @NamedQuery(name = "AdminAccount.findByEmailAccount", query = "SELECT u FROM AdminAccount u WHERE u.emailAccount = :emailAccount"),
    @NamedQuery(name = "AdminAccount.findByEmployeeId", query = "SELECT u FROM AdminAccount u WHERE u.employeeId = :employeeId"),
    @NamedQuery(name = "AdminAccount.findByFirstName", query = "SELECT u FROM AdminAccount u WHERE u.firstName = :firstName"),
    @NamedQuery(name = "AdminAccount.findByUserName", query = "SELECT u FROM AdminAccount u WHERE u.userName = :userName"),
    @NamedQuery(name = "AdminAccount.findByUserRole", query = "SELECT u FROM AdminAccount u WHERE u.userRole = :userRole"),
    @NamedQuery(name = "AdminAccount.findByLastLogin", query = "SELECT u FROM AdminAccount u WHERE u.lastLogin = :lastLogin"),
    @NamedQuery(name = "AdminAccount.findByLastName", query = "SELECT u FROM AdminAccount u WHERE u.lastName = :lastName"),
    @NamedQuery(name = "AdminAccount.findByMobileNo", query = "SELECT u FROM AdminAccount u WHERE u.mobileNo = :mobileNo"),
    @NamedQuery(name = "AdminAccount.findByRegiDate", query = "SELECT u FROM AdminAccount u WHERE u.regiDate = :regiDate"),
    @NamedQuery(name = "AdminAccount.findByAdminAccountName", query = "SELECT u FROM AdminAccount u WHERE u.userName = :userName"),
    @NamedQuery(name = "AdminAccount.findByAdminAccountRole", query = "SELECT u FROM AdminAccount u WHERE u.userRole = :userRole")})
public class AdminAccount implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int PAGE_SIZE=10;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "comment")
    private String comment;
    @Column(name = "emailAccount")
    private String emailAccount;
    @Column(name = "employeeId")
    private String employeeId;
    @Column(name = "firstName")
    private String firstName;
    @Column(name = "lastLogin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;
    @Column(name = "lastName")
    private String lastName;
    @Column(name = "mobileNo")
    private String mobileNo;

    @Column(name = "regiDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date regiDate;
    @Column(name="userName",unique=true,length=30)
    private String userName;
    
    @Column(name = "userRole")
    private int userRole; 
    @Column(name = "raName")
    private String raName;
    @Column(name = "raSymbol")
    private String raSymbol;

    public AdminAccount() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

   

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEmailAccount() {
        return emailAccount;
    }

    public void setEmailAccount(String emailAccount) {
        this.emailAccount = emailAccount;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }



    public Date getRegiDate() {
        return regiDate;
    }

    public void setRegiDate(Date regiDate) {
        this.regiDate = regiDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

   

    public String getRaName() {
        return raName;
    }

    public void setRaName(String raName) {
        this.raName = raName;
    }

    public String getRaSymbol() {
        return raSymbol;
    }

    public void setRaSymbol(String raSymbol) {
        this.raSymbol = raSymbol;
    }
    
       public int getUserRole() {
        return userRole;
    }

    public void setUserRole(int userRole) {
        this.userRole = userRole;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminAccount)) {
            return false;
        }
        AdminAccount other = (AdminAccount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.AdminAccount[ id=" + id + " ]";
    }
    
}
