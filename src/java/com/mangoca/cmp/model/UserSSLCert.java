/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
/**
 *
 * @author Masudul Haque
 */

@Entity
@Table(name="user_ssl_cert")
public class UserSSLCert   implements Serializable{ 
    
    private static final long serialVersionUID = 1L;

    public static final Short STATUS_NEW=1;
    public static final Short STATUS_PENDING=2;
    public static final Short STATUS_COMPLETE=3;
    public static final Short STATUS_INCOMPLETE=4;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name="domainName",length=100)
    private String domainName;
    @Column(name="ip_address",length=100)
    private String ipAddress;
    @Column(name="admin_email",length=100)
    private String adminEmail;
    @Column(name="server_type",length=100)
    private String serverType;
    
    @Lob
    private String csr;
    
    private Short status;
    private String description;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date reqDate;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date updateDate;

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public String getCsr() {
        return csr;
    }

    public void setCsr(String csr) {
        this.csr = csr;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Date getReqDate() {
        return reqDate;
    }

    public void setReqDate(Date reqDate) {
        this.reqDate = reqDate;
    }

    public String getServerType() {
        return serverType;
    }

    public void setServerType(String serverType) {
        this.serverType = serverType;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    
    
    
}
