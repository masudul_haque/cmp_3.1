/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "certificate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Certificate.findAll", query = "SELECT c FROM Certificate c"),
    @NamedQuery(name = "Certificate.findByAlgName", query = "SELECT c FROM Certificate c WHERE c.algName = :algName"),
    @NamedQuery(name = "Certificate.findByCertEmail", query = "SELECT c FROM Certificate c WHERE c.certEmail = :certEmail"),
    @NamedQuery(name = "Certificate.findByCertStatus", query = "SELECT c FROM Certificate c WHERE c.certStatus = :certStatus"),
    @NamedQuery(name = "Certificate.findByCertType", query = "SELECT c FROM Certificate c WHERE c.certType = :certType"),
    @NamedQuery(name = "Certificate.findByCertVersion", query = "SELECT c FROM Certificate c WHERE c.certVersion = :certVersion"),
    @NamedQuery(name = "Certificate.findByCommonName", query = "SELECT c FROM Certificate c WHERE c.commonName = :commonName"),
    @NamedQuery(name = "Certificate.findByCountryUnit", query = "SELECT c FROM Certificate c WHERE c.countryUnit = :countryUnit"),
    @NamedQuery(name = "Certificate.findByDownloadLink", query = "SELECT c FROM Certificate c WHERE c.downloadLink = :downloadLink"),
    @NamedQuery(name = "Certificate.findByExpiredDate", query = "SELECT c FROM Certificate c WHERE c.expiredDate = :expiredDate"),
    @NamedQuery(name = "Certificate.findByIssueDate", query = "SELECT c FROM Certificate c WHERE c.issueDate = :issueDate"),
    @NamedQuery(name = "Certificate.findByIssuedBy", query = "SELECT c FROM Certificate c WHERE c.issuedBy = :issuedBy"),
    @NamedQuery(name = "Certificate.findByKeyLength", query = "SELECT c FROM Certificate c WHERE c.keyLength = :keyLength"),
    @NamedQuery(name = "Certificate.findByLocality", query = "SELECT c FROM Certificate c WHERE c.locality = :locality"),
    @NamedQuery(name = "Certificate.findByOrgUnit", query = "SELECT c FROM Certificate c WHERE c.orgUnit = :orgUnit"),
    @NamedQuery(name = "Certificate.findByOrganization", query = "SELECT c FROM Certificate c WHERE c.organization = :organization"),
     @NamedQuery(name = "Certificate.findByPublicKey", query = "SELECT c FROM Certificate c WHERE c.publicKey = :publicKey"),
    @NamedQuery(name = "Certificate.findByRenewDate", query = "SELECT c FROM Certificate c WHERE c.renewDate = :renewDate"),
    @NamedQuery(name = "Certificate.findBySerialNo", query = "SELECT c FROM Certificate c WHERE c.serialNo = :serialNo")})
public class Certificate implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final Short CERT_NEW = 1;
    public static final Short CERT_IN_PROCESS = 2;
    public static final Short CERT_ACTIVE = 3;
    public static final Short CERT_APPLY_REVOKE = 4;
    public static final Short CERT_APPLY_RENEW = 5;
    public static final Short CERT_APPLY_SUSPEND = 6;
    public static final Short CERT_REVOKED = 7;
    public static final Short CERT_SUSPENDED = 8;
    public static final Short CERT_DOWNLOADABLE = 9;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "algName")
    private String algName;
    @Column(name = "certClass")
    private String certClass;
    @Column(name = "certEmail")
    private String certEmail;
    @Column(name = "certEmailDomain")
    private String certEmailDomain;
    @Column(name = "certUserName")
    private String certUserName;
    @Column(name = "certPassword")
    private String certPassword;
    @Column(name = "certStatus")
    private Short certStatus;
    @Column(name = "certType")
    private String certType;
    @Column(name = "certVersion")
    private String certVersion;
    @Column(name = "commonName",length=64)
    private String commonName;
    @Column(name = "countryUnit",length=2)
    private String countryUnit;
    @Column(name = "postalCode")
    private String postalCode;
    @Column(name = "downloadLink")
    private String downloadLink;
    @Column(name = "expiredDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiredDate;
    @Column(name = "issueDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDate;
    @Column(name = "issuedBy")
    private String issuedBy;
    @Column(name = "keyLength")
    private Integer keyLength;
    @Column(name = "locality",length=60)
    private String locality;
    @Column(name = "organization_unit",length=64)
    private String orgUnit;
    @Column(name = "organization",length=64)
    private String organization;
    @Column(name = "publicKey")
    private String publicKey;
    @Column(name = "renewDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date renewDate;
    @Column(name = "serialNo")
    private String serialNo;
    @Column(name = "hashContent",length=30)
    private String hashContent;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Techaction techaction;

    public Certificate() {
    }

   

    public String getAlgName() {
        return algName;
    }

    public void setAlgName(String algName) {
        this.algName = algName;
    }

    public String getCertEmail() {
        return certEmail;
    }

    public void setCertEmail(String certEmail) {
        this.certEmail = certEmail;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertVersion() {
        return certVersion;
    }

    public void setCertVersion(String certVersion) {
        this.certVersion = certVersion;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getCountryUnit() {
        return countryUnit;
    }

    public void setCountryUnit(String countryUnit) {
        this.countryUnit = countryUnit;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    public Integer getKeyLength() {
        return keyLength;
    }

    public void setKeyLength(Integer keyLength) {
        this.keyLength = keyLength;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(String orgUnit) {
        this.orgUnit = orgUnit;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public Date getRenewDate() {
        return renewDate;
    }

    public void setRenewDate(Date renewDate) {
        this.renewDate = renewDate;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Techaction getTechaction() {
        return techaction;
    }

    public void setTechaction(Techaction techaction) {
        this.techaction = techaction;
    }

    public String getCertEmailDomain() {
        return certEmailDomain;
    }

    public void setCertEmailDomain(String certEmailDomain) {
        this.certEmailDomain = certEmailDomain;
    }

    public String getCertPassword() {
        return certPassword;
    }

    public void setCertPassword(String certPassword) {
        this.certPassword = certPassword;
    }

    public Short getCertStatus() {
        return certStatus;
    }

    public void setCertStatus(Short certStatus) {
        this.certStatus = certStatus;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCertClass() {
        return certClass;
    }

    public void setCertClass(String certClass) {
        this.certClass = certClass;
    }

    public String getCertUserName() {
        return certUserName;
    }

    public void setCertUserName(String certUserName) {
        this.certUserName = certUserName;
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public java.lang.String getHashContent() {
        return hashContent;
    }

    public void setHashContent(java.lang.String hashContent) {
        this.hashContent = hashContent;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Certificate)) {
            return false;
        }
        Certificate other = (Certificate) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.Certificate[ id=" + id + " ]";
    }
}
