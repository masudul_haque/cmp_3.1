/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "accountlog")
@NamedQueries({
    @NamedQuery(name = "Accountlog.findAll", query = "SELECT a FROM Accountlog a"),
    @NamedQuery(name = "Accountlog.findByLogId", query = "SELECT a FROM Accountlog a WHERE a.logId = :logId"),
    @NamedQuery(name = "Accountlog.findByActionName", query = "SELECT a FROM Accountlog a WHERE a.actionName = :actionName"),
    @NamedQuery(name = "Accountlog.findByBrowserInfo", query = "SELECT a FROM Accountlog a WHERE a.browserInfo = :browserInfo"),
    @NamedQuery(name = "Accountlog.findByComment", query = "SELECT a FROM Accountlog a WHERE a.comment = :comment"),
    @NamedQuery(name = "Accountlog.findByDescription", query = "SELECT a FROM Accountlog a WHERE a.description = :description"),
    @NamedQuery(name = "Accountlog.findByLogDate", query = "SELECT a FROM Accountlog a WHERE a.logDate = :logDate"),
    @NamedQuery(name = "Accountlog.findByLogDocumentLocation", query = "SELECT a FROM Accountlog a WHERE a.logDocumentLocation = :logDocumentLocation"),
    @NamedQuery(name = "Accountlog.findByTrackId", query = "SELECT a FROM Accountlog a WHERE a.trackId = :trackId"),
    @NamedQuery(name = "Accountlog.findByUserIpAddress", query = "SELECT a FROM Accountlog a WHERE a.userIpAddress = :userIpAddress"),
    @NamedQuery(name = "Accountlog.findByUserName", query = "SELECT a FROM Accountlog a WHERE a.userName = :userName"),
    @NamedQuery(name = "Accountlog.findByUserRole", query = "SELECT a FROM Accountlog a WHERE a.userRole = :userRole")})
public class Accountlog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "logId")
    private Long logId;
    @Column(name = "actionName")
    private String actionName;
    @Column(name = "browserInfo")
    private String browserInfo;
    @Column(name = "comment")
    private String comment;
    @Column(name = "description")
    private String description;
    @Column(name = "logDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date logDate;
    @Column(name = "logDocumentLocation")
    private String logDocumentLocation;
    @Column(name = "trackId")
    private String trackId;
    @Column(name = "userIpAddress")
    private String userIpAddress;
    @Column(name = "userName")
    private String userName;
    @Column(name = "userRole")
    private String userRole;

    public Accountlog() {
    }

    public Accountlog(Long logId) {
        this.logId = logId;
    }

    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getBrowserInfo() {
        return browserInfo;
    }

    public void setBrowserInfo(String browserInfo) {
        this.browserInfo = browserInfo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public String getLogDocumentLocation() {
        return logDocumentLocation;
    }

    public void setLogDocumentLocation(String logDocumentLocation) {
        this.logDocumentLocation = logDocumentLocation;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getUserIpAddress() {
        return userIpAddress;
    }

    public void setUserIpAddress(String userIpAddress) {
        this.userIpAddress = userIpAddress;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (logId != null ? logId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Accountlog)) {
            return false;
        }
        Accountlog other = (Accountlog) object;
        if ((this.logId == null && other.logId != null) || (this.logId != null && !this.logId.equals(other.logId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.Accountlog[ logId=" + logId + " ]";
    }
    
}
