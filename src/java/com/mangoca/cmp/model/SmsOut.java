/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name="ozekimessageout")
@XmlRootElement
public class SmsOut  implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String STATUS_SEND="send";
    public static final String STATUS_PENDING="pending";
    public static final String STATUS_TRANSMITTED="transmitted";
    public static final int PAGE_SIZE=10;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name="sender",length=30)
    private String sender;
    @Column(name="receiver",length=30)
    private String receiver;
    @Column(name="msg", length=160)
    private String msg;
    @Column(name="senttime", length=100)
    private String senttime;
    @Column(name="receivedtime",length=100)
    private String receivedtime;
    @Column(name="reference", length=100)
    private String reference;
    @Column(name="status",length=20)
    private String status;
    @Column(name="msgtype",length=160)
    private String msgtype;
    @Column(name="operator",length=100)
    private String operator;
    @Column(name="MsgPostDateTime")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date msgPostDateTime;
    @Column(name="MsgPostFor",length=50)
    private String msgPostFor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getMsgPostDateTime() {
        return msgPostDateTime;
    }

    public void setMsgPostDateTime(Date msgPostDateTime) {
        this.msgPostDateTime = msgPostDateTime;
    }

    public String getMsgPostFor() {
        return msgPostFor;
    }

    public void setMsgPostFor(String msgPostFor) {
        this.msgPostFor = msgPostFor;
    }

    public String getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getReceivedtime() {
        return receivedtime;
    }

    public void setReceivedtime(String receivedtime) {
        this.receivedtime = receivedtime;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenttime() {
        return senttime;
    }

    public void setSenttime(String senttime) {
        this.senttime = senttime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
    
    
}
