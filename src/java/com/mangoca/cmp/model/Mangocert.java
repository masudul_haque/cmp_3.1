/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "mangocert")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mangocert.findAll", query = "SELECT m FROM Mangocert m"),
    @NamedQuery(name = "Mangocert.findById", query = "SELECT m FROM Mangocert m WHERE m.id = :id"),
    @NamedQuery(name = "Mangocert.findByCertClass", query = "SELECT m FROM Mangocert m WHERE m.certClass = :certClass"),
    @NamedQuery(name = "Mangocert.findByCertType", query = "SELECT m FROM Mangocert m WHERE m.certType = :certType"),
    @NamedQuery(name = "Mangocert.findByDescription", query = "SELECT m FROM Mangocert m WHERE m.description = :description"),
    @NamedQuery(name = "Mangocert.findByPrice", query = "SELECT m FROM Mangocert m WHERE m.price = :price"),
    @NamedQuery(name = "Mangocert.findByUpdatedBy", query = "SELECT m FROM Mangocert m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "Mangocert.findByUpdatedDate", query = "SELECT m FROM Mangocert m WHERE m.updatedDate = :updatedDate"),
    @NamedQuery(name = "Mangocert.findByValidFrom", query = "SELECT m FROM Mangocert m WHERE m.validFrom = :validFrom"),
    @NamedQuery(name = "Mangocert.findByValidTo", query = "SELECT m FROM Mangocert m WHERE m.validTo = :validTo"),
    @NamedQuery(name = "Mangocert.findByVat", query = "SELECT m FROM Mangocert m WHERE m.vat = :vat")})
public class Mangocert implements Serializable {
    
    public static final int PAGE_SIZE=5;
    public static final String CERT_SERVER="Server";
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "certClass")
    private String certClass;
    @Column(name = "certType")
    private String certType;
    @Lob
    @Column(name = "comment")
    private String comment;
    @Column(name = "description")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;
    @Column(name = "updatedBy")
    private String updatedBy;
    @Column(name = "updatedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "validFrom")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validFrom;
    @Column(name = "validTo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validTo;
    @Column(name = "vat")
    private Double vat;

    public Mangocert() {
    }

    public Mangocert(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCertClass() {
        return certClass;
    }

    public void setCertClass(String certClass) {
        this.certClass = certClass;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public Double getVat() {
        return vat;
    }

    public void setVat(Double vat) {
        this.vat = vat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mangocert)) {
            return false;
        }
        Mangocert other = (Mangocert) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.Mangocert[ id=" + id + " ]";
    }
    
}
