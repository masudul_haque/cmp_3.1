/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "user_account")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserAccount.findAll", query = "SELECT e FROM UserAccount e"),
    @NamedQuery(name = "UserAccount.findById", query = "SELECT e FROM UserAccount e WHERE e.id = :id"),
    @NamedQuery(name = "UserAccount.findByEmailAccount", query = "SELECT e FROM UserAccount e WHERE e.emailAccount = :emailAccount"),
    @NamedQuery(name = "UserAccount.findByUserRole", query = "SELECT u FROM UserAccount u WHERE u.userRole = :userRole"),
    @NamedQuery(name = "UserAccount.findByFullName", query = "SELECT e FROM UserAccount e WHERE e.fullName = :fullName"),
    @NamedQuery(name = "UserAccount.findByIsEmailValid", query = "SELECT e FROM UserAccount e WHERE e.isEmailValid = :isEmailValid"),
    @NamedQuery(name = "UserAccount.findByMobileNo", query = "SELECT e FROM UserAccount e WHERE e.mobileNo = :mobileNo and  e.isPinValidate = :isPinValidate"),
    @NamedQuery(name = "UserAccount.findByCINAndMobileNo", query = "SELECT e FROM UserAccount e WHERE e.userCIN=:userCIN and e.mobileNo = :mobileNo and  e.isPinValidate = :isPinValidate"),
    @NamedQuery(name = "UserAccount.findByRaSelect", query = "SELECT e FROM UserAccount e WHERE e.raSelect = :raSelect"),
    @NamedQuery(name = "UserAccount.findByRegiDate", query = "SELECT e FROM UserAccount e WHERE e.regiDate = :regiDate"),
    @NamedQuery(name = "UserAccount.findByUserCIN", query = "SELECT e FROM UserAccount e WHERE e.userCIN = :userCIN"),
    @NamedQuery(name = "UserAccount.findByUserCINAndEmail", query = "SELECT e FROM UserAccount e WHERE e.userCIN = :userCIN and e.emailAccount= :emailAccount"),
    @NamedQuery(name = "UserAccount.findByUserName", query = "SELECT e FROM UserAccount e WHERE e.userName = :userName"),
   @NamedQuery(name = "UserAccount.findByUserType", query = "SELECT e FROM UserAccount e WHERE e.userType = :userType"),
    @NamedQuery(name = "UserAccount.findByValidityNo", query = "SELECT e FROM UserAccount e WHERE e.validityNo = :validityNo and  e.isEmailValid = :isEmailValid")})
public class UserAccount implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int PAGE_SIZE=10; 
    public static final Short MAILED=1;
    public static final Short DOWNLOADED=2;
    public static final Short NONE=3;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Lob
    @Column(name = "comment")
    private String comment;
    @Column(name = "emailAccount",length=100)
    private String emailAccount;
    @Column(name = "userName",unique=true)
    private String userName;
    @Column(name = "userRole")
    private int userRole; 
    @Column(name = "fullName")
    private String fullName;
    @Column(name = "isEmailValid")
    private Boolean isEmailValid;
    @Column(name = "mobileNo")
    private String mobileNo;
    @Column(name = "raSelect")
    private String raSelect;
    @Column(name = "regiDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date regiDate;
    @Column(name = "userCIN")
    private String userCIN;
    @Column(name = "userType")
    private String userType;
    @Column(name = "validityNo")
    private String validityNo;
    
    @Column(name = "smsPin")
    private String smsPin;
    @Column(name = "isPinValidate")
    private Boolean isPinValidate;
    
    @Column(name = "doc_status")
    private Short docStatus;

    public UserAccount() {
    }

    public UserAccount(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEmailAccount() {
        return emailAccount;
    }

    public void setEmailAccount(String emailAccount) {
        this.emailAccount = emailAccount;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Boolean getIsEmailValid() {
        return isEmailValid;
    }

    public void setIsEmailValid(Boolean isEmailValid) {
        this.isEmailValid = isEmailValid;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

 

    public String getRaSelect() {
        return raSelect;
    }

    public void setRaSelect(String raSelect) {
        this.raSelect = raSelect;
    }

    public Date getRegiDate() {
        return regiDate;
    }

    public void setRegiDate(Date regiDate) {
        this.regiDate = regiDate;
    }

    
    

    public String getUserCIN() {
        return userCIN;
    }

    public void setUserCIN(String userCIN) {
        this.userCIN = userCIN;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getValidityNo() {
        return validityNo;
    }

    public void setValidityNo(String validityNo) {
        this.validityNo = validityNo;
    }

    
       
    public int getUserRole() {
        return userRole;
    }

    public void setUserRole(int userRole) {
        this.userRole = userRole;
    }

    public Boolean getIsPinValidate() {
        return isPinValidate;
    }

    public void setIsPinValidate(Boolean isPinValidate) {
        this.isPinValidate = isPinValidate;
    }

    public String getSmsPin() {
        return smsPin;
    }

    public void setSmsPin(String smsPin) {
        this.smsPin = smsPin;
    }

    public Short getDocStatus() {
        return docStatus;
    }

    public void setDocStatus(Short docStatus) {
        this.docStatus = docStatus;
    }

   
    
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserAccount)) {
            return false;
        }
        UserAccount other = (UserAccount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "com.mangoca.cmp.model.UserAccount[ id=" + id + " ]";
    }
    
}
