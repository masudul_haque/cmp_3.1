/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TODO certClss and emailAccount need to add.
 * @author Masudul Haque
 */
@Entity
@Table(name = "enrolleduser")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Enrolleduser.findAll", query = "SELECT e FROM Enrolleduser e"),
    @NamedQuery(name = "Enrolleduser.findByEnrollId", query = "SELECT e FROM Enrolleduser e WHERE e.enrollId = :enrollId"),
    @NamedQuery(name = "Enrolleduser.findByEnrolDate", query = "SELECT e FROM Enrolleduser e WHERE e.enrolDate = :enrolDate"),
    @NamedQuery(name = "Enrolleduser.findByFullName", query = "SELECT e FROM Enrolleduser e WHERE e.fullName = :fullName"),
    @NamedQuery(name = "Enrolleduser.findByRaSelect", query = "SELECT e FROM Enrolleduser e WHERE e.raSelect = :raSelect"),
    @NamedQuery(name = "Enrolleduser.findByUserCIN", query = "SELECT e FROM Enrolleduser e WHERE e.userCIN = :userCIN and e.status = :status"),
    @NamedQuery(name = "Enrolleduser.findByUserName", query = "SELECT e FROM Enrolleduser e WHERE e.userName = :userName and e.status = :status"),
    @NamedQuery(name = "Enrolleduser.findByRaStatus", query = "SELECT e FROM Enrolleduser e WHERE e.raaction.status = :status"),
    @NamedQuery(name = "Enrolleduser.findByAccStatus", query = "SELECT e FROM Enrolleduser e WHERE e.accaction.status = :status"),
    @NamedQuery(name = "Enrolleduser.findByVaStatus", query = "SELECT e FROM Enrolleduser e WHERE e.vaaction.status = :status"),
    @NamedQuery(name = "Enrolleduser.findByTechStatus", query = "SELECT e FROM Enrolleduser e WHERE e.techaction.status = :status"),
    
    
    @NamedQuery(name = "Enrolleduser.countRevocationByStatus", query = "SELECT count(e) FROM Enrolleduser e WHERE e.revocation IS NOT NULL and e.revocation.status = :status"),
    @NamedQuery(name = "Enrolleduser.countRenewalByStatus", query = "SELECT count(e) FROM Enrolleduser e WHERE e.renewal IS NOT NULL and e.renewal.status = :status"),
    @NamedQuery(name = "Enrolleduser.countSuspensionByStatus", query = "SELECT count(e) FROM Enrolleduser e WHERE e.suspension IS NOT NULL and e.suspension.status = :status"),
    @NamedQuery(name = "Enrolleduser.findByUserType", query = "SELECT e FROM Enrolleduser e WHERE e.userType = :userType")})
public class Enrolleduser implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int PAGE_SIZE=10;
    public static final Short ACTIVE=1;
    public static final Short IN_ACTIVE=2; 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "enrollId")
    private Long enrollId;
    @Column(name = "comment")
    private String comment;
    @Column(name = "enrolDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enrolDate;
    @Column(name = "fullName")
    private String fullName;
    @Column(name = "certClass")
    private String certClass;
    @Column(name = "emailAccount")
    private String emailAccount;
    @Column(name = "mobileNo")
    private String mobileNo;
    @Column(name = "raSelect")
    private String raSelect;
    @Column(name = "userCIN")
    private String userCIN;
    @Column(name = "userName")
    private String userName;
    @Column(name = "userType")
    private String userType;
    @Column(name = "status")
    private Short status;
    
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Renewal renewal;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Raaction raaction;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Userbasic userbasic;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Accaction accaction;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Revocation revocation;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Vaaction vaaction;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Suspension suspension;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Certificate certificate;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Ccaction ccaction;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Adminaction adminaction;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Verification verification;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Techaction techaction;
    
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private UserSSLCert userSSLCert;
    
    @ManyToMany(cascade= CascadeType.ALL,fetch= FetchType.LAZY)
    private List<Document> documents;
    
    @ManyToMany(cascade= CascadeType.ALL,fetch= FetchType.LAZY)
    private List<Payment> payments;
    
    public Enrolleduser() {
    }

    public Enrolleduser(Long enrollId) {
        this.enrollId = enrollId;
    }

    public Long getEnrollId() {
        return enrollId;
    }

    public void setEnrollId(Long enrollId) {
        this.enrollId = enrollId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getEnrolDate() {
        return enrolDate;
    }

    public void setEnrolDate(Date enrolDate) {
        this.enrolDate = enrolDate;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCertClass() {
        return certClass;
    }

    public void setCertClass(String certClass) {
        this.certClass = certClass;
    }

    public String getEmailAccount() {
        return emailAccount;
    }

    public void setEmailAccount(String emailAccount) {
        this.emailAccount = emailAccount;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    

    public String getRaSelect() {
        return raSelect;
    }

    public void setRaSelect(String raSelect) {
        this.raSelect = raSelect;
    }

    public String getUserCIN() {
        return userCIN;
    }

    public void setUserCIN(String userCIN) {
        this.userCIN = userCIN;
    }

   

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }
   
    
    public Renewal getRenewal() {
        return renewal;
    }

    public void setRenewal(Renewal renewal) {
        this.renewal = renewal;
    }

    public Raaction getRaaction() {
        return raaction;
    }

    public void setRaaction(Raaction raaction) {
        this.raaction = raaction;
    }

    public Userbasic getUserbasic() {
        return userbasic;
    }

    public void setUserbasic(Userbasic userbasic) {
        this.userbasic = userbasic;
    }

    public Accaction getAccaction() {
        return accaction;
    }

    public void setAccaction(Accaction accaction) {
        this.accaction = accaction;
    }

    public Revocation getRevocation() {
        return revocation;
    }

    public void setRevocation(Revocation revocation) {
        this.revocation = revocation;
    }

    public Vaaction getVaaction() {
        return vaaction;
    }

    public void setVaaction(Vaaction vaaction) {
        this.vaaction = vaaction;
    }

    public Suspension getSuspension() {
        return suspension;
    }

    public void setSuspension(Suspension suspension) {
        this.suspension = suspension;
    }

    public Adminaction getAdminaction() {
        return adminaction;
    }

    public void setAdminaction(Adminaction adminaction) {
        this.adminaction = adminaction;
    }

    public Ccaction getCcaction() {
        return ccaction;
    }

    public void setCcaction(Ccaction ccaction) {
        this.ccaction = ccaction;
    }

    public Techaction getTechaction() {
        return techaction;
    }

    public void setTechaction(Techaction techaction) {
        this.techaction = techaction;
    }

    public Verification getVerification() {
        return verification;
    }

    public void setVerification(Verification verification) {
        this.verification = verification;
    }

    public UserSSLCert getUserSSLCert() {
        return userSSLCert;
    }

    public void setUserSSLCert(UserSSLCert userSSLCert) {
        this.userSSLCert = userSSLCert;
    }
    
    
    
    
    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }
    
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (enrollId != null ? enrollId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Enrolleduser)) {
            return false;
        }
        Enrolleduser other = (Enrolleduser) object;
        if ((this.enrollId == null && other.enrollId != null) || (this.enrollId != null && !this.enrollId.equals(other.enrollId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.Enrolleduser[ enrollId=" + enrollId + " ]";
    }
    
}
