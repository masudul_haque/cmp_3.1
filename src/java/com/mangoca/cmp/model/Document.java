/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "document")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Document.findAll", query = "SELECT d FROM Document d"),
    @NamedQuery(name = "Document.findByDocumentId", query = "SELECT d FROM Document d WHERE d.documentId = :documentId"),
    @NamedQuery(name = "Document.findByDocumentLink", query = "SELECT d FROM Document d WHERE d.documentLink = :documentLink"),
    @NamedQuery(name = "Document.findByDocumentName", query = "SELECT d FROM Document d WHERE d.documentName = :documentName"),
    @NamedQuery(name = "Document.findByDocumentStatus", query = "SELECT d FROM Document d WHERE d.documentStatus = :documentStatus"),
    @NamedQuery(name = "Document.findByDocumentType", query = "SELECT d FROM Document d WHERE d.documentType = :documentType"),
    @NamedQuery(name = "Document.findByRaComment", query = "SELECT d FROM Document d WHERE d.raComment = :raComment"),
    @NamedQuery(name = "Document.findByUserCIN", query = "SELECT d FROM Document d WHERE d.userCIN = :userCIN"),
    @NamedQuery(name = "Document.findByUploadDate", query = "SELECT d FROM Document d WHERE d.uploadDate = :uploadDate"),
    @NamedQuery(name = "Document.findByUploadedBy", query = "SELECT d FROM Document d WHERE d.uploadedBy = :uploadedBy"),
    @NamedQuery(name = "Document.findByUploadedFor", query = "SELECT d FROM Document d WHERE d.uploadedFor = :uploadedFor"),
    @NamedQuery(name = "Document.findByUserName", query = "SELECT d FROM Document d WHERE d.userName = :userName"),
    @NamedQuery(name = "Document.findByVaComment", query = "SELECT d FROM Document d WHERE d.vaComment = :vaComment"),
    @NamedQuery(name = "Document.findByVerificationDate", query = "SELECT d FROM Document d WHERE d.verificationDate = :verificationDate")})
public class Document implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int PAGE_SIZE=10;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "documentId")
    private Long documentId;
    @Column(name = "documentLink")
    private String documentLink;
    @Column(name = "documentName")
    private String documentName;
    @Column(name = "documentStatus")
    private String documentStatus;
    @Column(name = "documentType")
    private String documentType;
    @Column(name = "raComment")
    private String raComment;
    @Column(name = "userCIN")
    private String userCIN;
    @Column(name = "uploadDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date uploadDate;
    @Column(name = "uploadedBy")
    private String uploadedBy;
    @Column(name = "uploadedFor")
    private String uploadedFor;
    @Column(name = "userName")
    private String userName;
    @Column(name = "fullName")
    private String fullName;
    @Column(name = "certClass")
    private String certClass;
    @Column(name = "vaComment")
    private String vaComment;
    @Column(name = "verificationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date verificationDate;
    
    
    public Document() {
    }

    public Document(Long documentId) {
        this.documentId = documentId;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public String getDocumentLink() {
        return documentLink;
    }

    public void setDocumentLink(String documentLink) {
        this.documentLink = documentLink;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentStatus() {
        return documentStatus;
    }

    public void setDocumentStatus(String documentStatus) {
        this.documentStatus = documentStatus;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getRaComment() {
        return raComment;
    }

    public void setRaComment(String raComment) {
        this.raComment = raComment;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(String uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    public String getUploadedFor() {
        return uploadedFor;
    }

    public void setUploadedFor(String uploadedFor) {
        this.uploadedFor = uploadedFor;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getVaComment() {
        return vaComment;
    }

    public void setVaComment(String vaComment) {
        this.vaComment = vaComment;
    }

    public Date getVerificationDate() {
        return verificationDate;
    }

    public void setVerificationDate(Date verificationDate) {
        this.verificationDate = verificationDate;
    }

    public String getCertClass() {
        return certClass;
    }

    public void setCertClass(String certClass) {
        this.certClass = certClass;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserCIN() {
        return userCIN;
    }

    public void setUserCIN(String userCIN) {
        this.userCIN = userCIN;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (documentId != null ? documentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Document)) {
            return false;
        }
        Document other = (Document) object;
        if ((this.documentId == null && other.documentId != null) || (this.documentId != null && !this.documentId.equals(other.documentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.Document[ documentId=" + documentId + " ]";
    }
    
}
