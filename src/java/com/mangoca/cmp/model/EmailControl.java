/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "email_control")
@NamedQueries({
    @NamedQuery(name = "EmailControl.findByEmailType", query = "SELECT e FROM EmailControl e  WHERE e.emailType = :emailType"),
    @NamedQuery(name = "EmailControl.findAllClassesEmailControls", query = "SELECT e FROM EmailControl e  WHERE e.attachment IS NOT NULL and e.attachmentName IS NOT NULL")
})  
public class EmailControl implements Serializable{
    private static final long serialVersionUID = 1L;
    
    public static final Short EMAIL_ACTIVATION=1;
    public static final Short EMAIL_FORGOT_PASS=2;
    public static final Short EMAIL_FORGOT_USERNAME=3;
    public static final Short ENROLLMENT=4;
    public static final Short CERT_ISSUE=5;
    public static final Short CLASS_1_IND_ATTACHMENT=6;
    public static final Short CLASS_2_IND_ATTACHMENT=7;
    public static final Short CLASS_2_ENT_ATTACHMENT=8;
    public static final Short CLASS_3_IND_ATTACHMENT=9;
    public static final Short CLASS_3_ENT_ATTACHMENT=10;
    public static final Short CLASS_3_SERVER_ATTACHMENT=11;
    
    public static final int PAGE_SIZE=10;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    private Short emailType;
    private Integer receiverRole;
    private String subject;
    private String header;
    @Lob
    private String msgBody;
    private String footer;
    private String attachment;
    private String attachmentName;
    private String attachmentLocation;
    private String updatedBy;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date updatedDate;

    public Short getEmailType() {
        return emailType;
    }

    public void setEmailType(Short emailType) {
        this.emailType = emailType;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public void setMsgBody(String msgBody) {
        this.msgBody = msgBody;
    }

    public Integer getReceiverRole() {
        return receiverRole;
    }

    public void setReceiverRole(Integer receiverRole) {
        this.receiverRole = receiverRole;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public String getAttachmentLocation() {
        return attachmentLocation;
    }

    public void setAttachmentLocation(String attachmentLocation) {
        this.attachmentLocation = attachmentLocation;
    }

   
    
}    
