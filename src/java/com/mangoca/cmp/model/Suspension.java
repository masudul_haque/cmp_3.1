/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "suspension")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Suspension.findAll", query = "SELECT s FROM Suspension s"),
    @NamedQuery(name = "Suspension.findById", query = "SELECT s FROM Suspension s WHERE s.id = :id"),
    @NamedQuery(name = "Suspension.findByApplyDate", query = "SELECT s FROM Suspension s WHERE s.applyDate = :applyDate"),
    @NamedQuery(name = "Suspension.findByReasons", query = "SELECT s FROM Suspension s WHERE s.reasons = :reasons"),
    @NamedQuery(name = "Suspension.findByStatus", query = "SELECT s FROM Suspension s WHERE s.status = :status"),
    @NamedQuery(name = "Suspension.findBySuspendBy", query = "SELECT s FROM Suspension s WHERE s.suspendBy = :suspendBy"),
    @NamedQuery(name = "Suspension.findBySuspendDate", query = "SELECT s FROM Suspension s WHERE s.suspendDate = :suspendDate"),
    @NamedQuery(name = "Suspension.countByStatus", query = "SELECT count(s) FROM Suspension s WHERE s.status = :status"),
    @NamedQuery(name = "Suspension.findByRaStatus", query="SELECT e FROM Enrolleduser e WHERE e.suspension IS NOT NULL and e.suspension.raaction.status = :status"),
    @NamedQuery(name = "Suspension.findByCcStatus", query="SELECT e FROM Enrolleduser e WHERE e.suspension IS NOT NULL and e.suspension.ccaction.status = :status"),
    @NamedQuery(name = "Suspension.findByTechStatus", query="SELECT e FROM Enrolleduser e WHERE e.suspension IS NOT NULL and e.suspension.techaction.status = :status")
   
})
public class Suspension implements Serializable {
    private static final long serialVersionUID = 1L; 
    
    public static final int PAGE_SIGE=10;
    public static final Short STATUS_NEW=1;
    public static final Short STATUS_PENDING=2;
    public static final Short STATUS_COMPLETE=3;
    public static final Short STATUS_INCOMPLETE=4;
    public static final int PAGE_SIZE=10;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "applyDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date applyDate;
    @Column(name = "reasons")
    private String reasons;
    @Column(name = "status")
    private Short status;
    @Column(name = "suspendBy")
    private String suspendBy;
    @Column(name = "comment")
    private String comment;
    @Column(name = "suspendDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date suspendDate;
    
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Techaction techaction;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Ccaction ccaction;
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private Raaction raaction;

    public Suspension() {
    }

    public Suspension(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reasons) {
        this.reasons = reasons;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    

    public String getSuspendBy() {
        return suspendBy;
    }

    public void setSuspendBy(String suspendBy) {
        this.suspendBy = suspendBy;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    

    public Date getSuspendDate() {
        return suspendDate;
    }

    public void setSuspendDate(Date suspendDate) {
        this.suspendDate = suspendDate;
    }

    public Techaction getTechaction() {
        return techaction;
    }

    public void setTechaction(Techaction techaction) {
        this.techaction = techaction;
    }

    public Ccaction getCcaction() {
        return ccaction;
    }

    public void setCcaction(Ccaction ccaction) {
        this.ccaction = ccaction;
    }

    public Raaction getRaaction() {
        return raaction;
    }

    public void setRaaction(Raaction raaction) {
        this.raaction = raaction;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Suspension)) {
            return false;
        }
        Suspension other = (Suspension) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.Suspension[ id=" + id + " ]";
    }
    
}
