/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "user")
public class User {

    public static final int USER_EMAIL_VALIDATE = 11;
    public static final int USER_PIN_VALIDATE = 12;
    public static final int USER_INITIAL = 1;
    public static final int USER_ACTIVE = 2;
    public static final int USER_ENROLL = 3;
    public static final int ADMIN_RA = 4;
    public static final int ADMIN_ACC = 5;
    public static final int ADMIN_CC = 6;
    public static final int ADMIN_VA = 7;
    public static final int ADMIN_TECH = 8;
    public static final int ADMIN_ADMIN = 9;
    public static final int ADMIN_LOCKED = 10;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "password")
    private String password;
    @Column(name = "userName")
    private String userName;
    @Column(name = "userRole")
    private int userRole;    
    @Column(name = "salt")
    private String  salt;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastLogin;
    
    @ManyToMany
    private List<LoginHistoryLog> loginHistoryLogs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public List<LoginHistoryLog> getLoginHistoryLogs() {
        return loginHistoryLogs;
    }

    public void setLoginHistoryLogs(List<LoginHistoryLog> loginHistoryLogs) {
        this.loginHistoryLogs = loginHistoryLogs;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserRole() {
        return userRole;
    }

    public void setUserRole(int userRole) {
        this.userRole = userRole;
    }
    
    
}
