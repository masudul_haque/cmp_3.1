/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "ticket")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ticket.findAll", query = "SELECT c FROM Ticket c"),
    @NamedQuery(name = "Ticket.findByTicketId", query = "SELECT c FROM Ticket c WHERE c.ticketId = :ticketId"),
    @NamedQuery(name = "Ticket.findByClosedBy", query = "SELECT c FROM Ticket c WHERE c.closedBy = :closedBy"),
    @NamedQuery(name = "Ticket.findByClosedDate", query = "SELECT c FROM Ticket c WHERE c.closedDate = :closedDate"),
    @NamedQuery(name = "Ticket.findByCreationDate", query = "SELECT c FROM Ticket c WHERE c.creationDate = :creationDate"),
    @NamedQuery(name = "Ticket.findByEmailAccount", query = "SELECT c FROM Ticket c WHERE c.emailAccount = :emailAccount"),
    @NamedQuery(name = "Ticket.findByFullName", query = "SELECT c FROM Ticket c WHERE c.fullName = :fullName"),
    @NamedQuery(name = "Ticket.findByHelpTopics", query = "SELECT c FROM Ticket c WHERE c.helpTopics = :helpTopics"),
    @NamedQuery(name = "Ticket.findByInternalMessage", query = "SELECT c FROM Ticket c WHERE c.internalMessage = :internalMessage"),
    @NamedQuery(name = "Ticket.findBySource", query = "SELECT c FROM Ticket c WHERE c.source = :source"),
    @NamedQuery(name = "Ticket.findBySubject", query = "SELECT c FROM Ticket c WHERE c.subject = :subject"),
    @NamedQuery(name = "Ticket.findByTicketPhone", query = "SELECT c FROM Ticket c WHERE c.ticketPhone = :ticketPhone"),
    @NamedQuery(name = "Ticket.findByPriority", query = "SELECT c FROM Ticket c WHERE c.priority = :priority"),
    @NamedQuery(name = "Ticket.findByTicketStatus", query = "SELECT c FROM Ticket c WHERE c.status = :status order by c.ticketId desc"),
    @NamedQuery(name = "Ticket.countTicketByStatus", query = "SELECT count(c) FROM Ticket c WHERE c.status = :status"),
    @NamedQuery(name = "Ticket.findByUserName", query = "SELECT c FROM Ticket c WHERE c.userName = :userName"),
    @NamedQuery(name = "Ticket.findByUserCIN", query = "SELECT c FROM Ticket c WHERE c.userCIN = :userCIN")})
public class Ticket implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public static final Short STATUS_OPEN=1;
    public static final Short STATUS_RE_OPEN=2;
    public static final Short STATUS_CLOSED=3;
    public static final Short STATUS_DELETE_BY_USER=4;
    public static final Short STATUS_DELETE_BY_ADMIN=5;
    public static final int PAGE_SIZE=10;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ticketId")
    private Long ticketId;
    @Column(name = "closedBy")
    private String closedBy;
    @Column(name = "closedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date closedDate;
    @Column(name = "creationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Column(name = "emailAccount")
    private String emailAccount;
    @Column(name = "fullName")
    private String fullName;
    @Column(name = "helpTopics")
    private String helpTopics;
    @Column(name = "internalMessage")
    private String internalMessage;
    @Column(name = "source")
    private String source;
    @Column(name = "subject")
    private String subject;
    @Column(name = "ticketPhone")
    private String ticketPhone;
    @Column(name = "priority")
    private String  priority;
    @Column(name = "status",nullable=false)
    private Short status;
    @Column(name = "userName")
    private String userName;
    @Column(name = "userCIN")
    private String userCIN;
    
    @ManyToMany(cascade= CascadeType.ALL,fetch= FetchType.LAZY)
    private List<TicketMessage> ticketMessages;

    public Ticket() {
    }

    public Ticket(Long ticketId) {
        this.ticketId = ticketId;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public String getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(String closedBy) {
        this.closedBy = closedBy;
    }

    public Date getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(Date closedDate) {
        this.closedDate = closedDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getEmailAccount() {
        return emailAccount;
    }

    public void setEmailAccount(String emailAccount) {
        this.emailAccount = emailAccount;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getHelpTopics() {
        return helpTopics;
    }

    public void setHelpTopics(String helpTopics) {
        this.helpTopics = helpTopics;
    }

    public String getInternalMessage() {
        return internalMessage;
    }

    public void setInternalMessage(String internalMessage) {
        this.internalMessage = internalMessage;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTicketPhone() {
        return ticketPhone;
    }

    public void setTicketPhone(String ticketPhone) {
        this.ticketPhone = ticketPhone;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserCIN() {
        return userCIN;
    }

    public void setUserCIN(String userCIN) {
        this.userCIN = userCIN;
    }

    public List<TicketMessage> getTicketMessages() {
        return ticketMessages;
    }

    public void setTicketMessages(List<TicketMessage> ticketMessages) {
        this.ticketMessages = ticketMessages;
    }

    
    
    
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ticketId != null ? ticketId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Ticket)) {
            return false;
        }
        Ticket other = (Ticket) object;
        if ((this.ticketId == null && other.ticketId != null) || (this.ticketId != null && !this.ticketId.equals(other.ticketId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.Ticket[ ticketId=" + ticketId + " ]";
    }
    
}
