/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "verification")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Verification.findAll", query = "SELECT v FROM Verification v"),
    @NamedQuery(name = "Verification.findById", query = "SELECT v FROM Verification v WHERE v.id = :id"),
    @NamedQuery(name = "Verification.findByDrivingLicence", query = "SELECT v FROM Verification v WHERE v.drivingLicence = :drivingLicence"),
    @NamedQuery(name = "Verification.findByNationalIdNo", query = "SELECT v FROM Verification v WHERE v.nationalIdNo = :nationalIdNo"),
    @NamedQuery(name = "Verification.findByPassportNo", query = "SELECT v FROM Verification v WHERE v.passportNo = :passportNo"),
    @NamedQuery(name = "Verification.findByTinNo", query = "SELECT v FROM Verification v WHERE v.tinNo = :tinNo")})
public class Verification implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "drivingLicence")
    private String drivingLicence;
    @Column(name = "nationalIdNo")
    private String nationalIdNo;
    @Column(name = "passportNo")
    private String passportNo;
    @Column(name = "tinNo")
    private String tinNo;

    public Verification() {
    }

    public Verification(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDrivingLicence() {
        return drivingLicence;
    }

    public void setDrivingLicence(String drivingLicence) {
        this.drivingLicence = drivingLicence;
    }

    public String getNationalIdNo() {
        return nationalIdNo;
    }

    public void setNationalIdNo(String nationalIdNo) {
        this.nationalIdNo = nationalIdNo;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getTinNo() {
        return tinNo;
    }

    public void setTinNo(String tinNo) {
        this.tinNo = tinNo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Verification)) {
            return false;
        }
        Verification other = (Verification) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.Verification[ id=" + id + " ]";
    }
    
}
