/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "userbasic")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Userbasic.findAll", query = "SELECT u FROM Userbasic u"),
    @NamedQuery(name = "Userbasic.findById", query = "SELECT u FROM Userbasic u WHERE u.id = :id"),
    @NamedQuery(name = "Userbasic.findByDateOfBirth", query = "SELECT u FROM Userbasic u WHERE u.dateOfBirth = :dateOfBirth"),
    @NamedQuery(name = "Userbasic.findByDistrict", query = "SELECT u FROM Userbasic u WHERE u.district = :district"),
    @NamedQuery(name = "Userbasic.findByFatherName", query = "SELECT u FROM Userbasic u WHERE u.fatherName = :fatherName"),
    @NamedQuery(name = "Userbasic.findByMotherName", query = "SELECT u FROM Userbasic u WHERE u.motherName = :motherName"),
    @NamedQuery(name = "Userbasic.findByParmanentAddress", query = "SELECT u FROM Userbasic u WHERE u.parmanentAddress = :parmanentAddress"),
    @NamedQuery(name = "Userbasic.findByPresentAddress", query = "SELECT u FROM Userbasic u WHERE u.presentAddress = :presentAddress")})
public class Userbasic implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "dateOfBirth")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfBirth;
    @Column(name = "district")
    private String district;
    @Column(name = "fatherName")
    private String fatherName;
    @Column(name = "motherName")
    private String motherName;
    @Column(name = "parmanentAddress")
    private String parmanentAddress;

    @Column(name = "presentAddress")
    private String presentAddress;

    public Userbasic() {
    }

    public Userbasic(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getParmanentAddress() {
        return parmanentAddress;
    }

    public void setParmanentAddress(String parmanentAddress) {
        this.parmanentAddress = parmanentAddress;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Userbasic)) {
            return false;
        }
        Userbasic other = (Userbasic) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.Userbasic[ id=" + id + " ]";
    }
    
}
