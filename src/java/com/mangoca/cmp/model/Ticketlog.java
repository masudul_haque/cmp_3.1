/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Masudul Haque
 */
@Entity
@Table(name = "ticketlog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ticketlog.findAll", query = "SELECT t FROM Ticketlog t"),
    @NamedQuery(name = "Ticketlog.findByLogId", query = "SELECT t FROM Ticketlog t WHERE t.logId = :logId"),
    @NamedQuery(name = "Ticketlog.findByActionName", query = "SELECT t FROM Ticketlog t WHERE t.actionName = :actionName"),
    @NamedQuery(name = "Ticketlog.findByBrowserInfo", query = "SELECT t FROM Ticketlog t WHERE t.browserInfo = :browserInfo"),
    @NamedQuery(name = "Ticketlog.findByComment", query = "SELECT t FROM Ticketlog t WHERE t.comment = :comment"),
    @NamedQuery(name = "Ticketlog.findByDescription", query = "SELECT t FROM Ticketlog t WHERE t.description = :description"),
    @NamedQuery(name = "Ticketlog.findByLogDate", query = "SELECT t FROM Ticketlog t WHERE t.logDate = :logDate"),
    @NamedQuery(name = "Ticketlog.findByLogDocumentLocation", query = "SELECT t FROM Ticketlog t WHERE t.logDocumentLocation = :logDocumentLocation"),
    @NamedQuery(name = "Ticketlog.findByTrackId", query = "SELECT t FROM Ticketlog t WHERE t.trackId = :trackId"),
    @NamedQuery(name = "Ticketlog.findByUserIpAddress", query = "SELECT t FROM Ticketlog t WHERE t.userIpAddress = :userIpAddress"),
    @NamedQuery(name = "Ticketlog.findByUserName", query = "SELECT t FROM Ticketlog t WHERE t.userName = :userName"),
    @NamedQuery(name = "Ticketlog.findByUserRole", query = "SELECT t FROM Ticketlog t WHERE t.userRole = :userRole")})
public class Ticketlog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "logId")
    private Long logId;
    @Column(name = "actionName")
    private String actionName;
    @Column(name = "browserInfo")
    private String browserInfo;
    @Column(name = "comment")
    private String comment;
    @Column(name = "description")
    private String description;
    @Column(name = "logDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date logDate;
    @Column(name = "logDocumentLocation")
    private String logDocumentLocation;
    @Column(name = "trackId")
    private String trackId;
    @Column(name = "userIpAddress")
    private String userIpAddress;
    @Column(name = "userName")
    private String userName;
    @Column(name = "userRole")
    private String userRole;

    public Ticketlog() {
    }

    public Ticketlog(Long logId) {
        this.logId = logId;
    }

    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getBrowserInfo() {
        return browserInfo;
    }

    public void setBrowserInfo(String browserInfo) {
        this.browserInfo = browserInfo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public String getLogDocumentLocation() {
        return logDocumentLocation;
    }

    public void setLogDocumentLocation(String logDocumentLocation) {
        this.logDocumentLocation = logDocumentLocation;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getUserIpAddress() {
        return userIpAddress;
    }

    public void setUserIpAddress(String userIpAddress) {
        this.userIpAddress = userIpAddress;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (logId != null ? logId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ticketlog)) {
            return false;
        }
        Ticketlog other = (Ticketlog) object;
        if ((this.logId == null && other.logId != null) || (this.logId != null && !this.logId.equals(other.logId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mangoca.cmp.model.Ticketlog[ logId=" + logId + " ]";
    }
    
}
