package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.Bank;
import java.util.List;

/**
 * The DAO interface for the Bank entity.
 */
public interface IBankDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Bank findBankById(int id);
	/**
	 * Return all persistent instances of the <code>Bank</code> entity.
	 */
	public List<Bank> findAllBanks();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistBank(Bank bank);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeBank(Bank bank);
}