package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.MangoSSLCert;
import java.util.List;

/**
 * The DAO interface for the MangoSSLCert entity.
 */
public interface IMangoSSLCertDao {

    /**
     * Return the persistent entities returned from a named query.
     */
    @SuppressWarnings("unchecked")
    public List findByNamedQuery(String queryName);

    /**
     * Return the persistent entities returned from a named query with named parameters.
     */
    @SuppressWarnings("unchecked")
    public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public MangoSSLCert findMangoSSLCertById(int id);

    /**
     * Return all persistent instances of the <code>MangoSSLCert</code> entity.
     */
    public List<MangoSSLCert> findAllMangoSSLCerts();

    /**
     * Make the given instance managed and persistent.
     */
    public void persistMangoSSLCert(MangoSSLCert mangoSSLCert);

    /**
     * Remove the given persistent instance.
     */
    public void removeMangoSSLCert(MangoSSLCert mangoSSLCert);

    public List getMangoSSLCertData(int page, int size);

    public long getResultSize();
}