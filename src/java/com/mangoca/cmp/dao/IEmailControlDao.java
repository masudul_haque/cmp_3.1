package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.EmailControl;
import java.util.List;

/**
 * The DAO interface for the EmailControl entity.
 */
public interface IEmailControlDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public EmailControl findEmailControlById(java.lang.Integer id);
	/**
	 * Return all persistent instances of the <code>EmailControl</code> entity.
	 */
	public List<EmailControl> findAllEmailControls();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistEmailControl(EmailControl emailControl);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeEmailControl(EmailControl emailControl);

    public List getEmailControlData(int page, int size);

    public long getResultSize();
}