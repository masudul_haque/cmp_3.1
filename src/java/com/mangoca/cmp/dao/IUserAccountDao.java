package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.UserAccount;
import java.util.List;

/**
 * The DAO interface for the UserAccount entity.
 */
public interface IUserAccountDao {

    /**
     * Return the persistent entities returned from a named query.
     */
    @SuppressWarnings("unchecked")
    public List findByNamedQuery(String queryName);

    /**
     * Return the persistent entities returned from a named query with named
     * parameters.
     */
    @SuppressWarnings("unchecked")
    public List findByNamedQuery(String queryName, String[] paramNames,
            Object[] paramValues);

    /**
     * Find an entity by its id (primary key).
     * 
     * @return The found entity instance or null if the entity does not exist.
     */
    public UserAccount findUserAccountById(Long id);

    /**
     * Return all persistent instances of the <code>UserAccount</code> entity.
     */
    public List<UserAccount> findAllUserAccounts();

    /**
     * Make the given instance managed and persistent.
     */
    public void persistUserAccount(UserAccount empuser);

    /**
     * Remove the given persistent instance.
     */
    public void removeUserAccount(UserAccount empuser);

    public UserAccount getUserAccountByUserName(String userName);

    public UserAccount getAuthorizedEmpuser(String userName, String password);

    public Long getLastUserId();

    public UserAccount getUserAccountByValidityNo(Long validityNo);
    public List getUserAccountData(final int page, final int size);
    public long getResultSize();
}