package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.LoginHistoryLog;
import java.util.List;

/**
 * The DAO interface for the LoginHistoryLog entity.
 */
public interface ILoginHistoryDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public LoginHistoryLog findLoginHistoryLogById(java.lang.Integer id);
	/**
	 * Return all persistent instances of the <code>LoginHistoryLog</code> entity.
	 */
	public List<LoginHistoryLog> findAllLoginHistoryLogs();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistLoginHistoryLog(LoginHistoryLog loginHistoryLog);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeLoginHistoryLog(LoginHistoryLog loginHistoryLog);
}