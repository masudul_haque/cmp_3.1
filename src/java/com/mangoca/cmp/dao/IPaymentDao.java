package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.Payment;
import java.util.List;

/**
 * The DAO interface for the Payment entity.
 */
public interface IPaymentDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Payment findPaymentById(Long id);
	/**
	 * Return all persistent instances of the <code>Payment</code> entity.
	 */
	public List<Payment> findAllPayments();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistPayment(Payment payment);
	/**
	 * Remove the given persistent instance.
	 */
	public void removePayment(Payment payment);

    public List getPaymentData(int page, int size);

    public long getResultSize();
}