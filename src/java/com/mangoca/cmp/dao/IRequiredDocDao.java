package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.RequiredDoc;
import java.util.List;

/**
 * The DAO interface for the RequiredDoc entity.
 */
public interface IRequiredDocDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public RequiredDoc findRequiredDocById(int id);
	/**
	 * Return all persistent instances of the <code>RequiredDoc</code> entity.
	 */
	public List<RequiredDoc> findAllRequiredDocs();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistRequiredDoc(RequiredDoc requiredDoc);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeRequiredDoc(RequiredDoc requiredDoc);
}