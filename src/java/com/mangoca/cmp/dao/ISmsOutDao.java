package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.SmsOut;
import java.util.List;

/**
 * The DAO interface for the SmsOut entity.
 */
public interface ISmsOutDao {

    /**
     * Return the persistent entities returned from a named query.
     */
    @SuppressWarnings("unchecked")
    public List findByNamedQuery(String queryName);

    /**
     * Return the persistent entities returned from a named query with named parameters.
     */
    @SuppressWarnings("unchecked")
    public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public SmsOut findSmsOutById(int id);

    /**
     * Return all persistent instances of the <code>SmsOut</code> entity.
     */
    public List<SmsOut> findAllSmsOuts();

    /**
     * Make the given instance managed and persistent.
     */
    public void persistSmsOut(SmsOut smsOut);

    /**
     * Remove the given persistent instance.
     */
    public void removeSmsOut(SmsOut smsOut);

    public List getSmsOutData(int page, int size);

    public long getResultSize();
}