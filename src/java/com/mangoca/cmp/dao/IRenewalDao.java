package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.Renewal;
import java.util.List;

/**
 * The DAO interface for the Renewal entity.
 */
public interface IRenewalDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Renewal findRenewalById(java.lang.Long id);
	/**
	 * Return all persistent instances of the <code>Renewal</code> entity.
	 */
	public List<Renewal> findAllRenewals();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistRenewal(Renewal renewal);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeRenewal(Renewal renewal);
}