package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.IUserDao;
import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.model.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the AdminAccount entity.
 */
public class UserJPADao extends JpaDaoSupport implements IUserDao {

    public UserJPADao() {
        super();
    }

    /**
     * Return the persistent entities returned from a named query.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List findByNamedQuery(String queryName) {
        return getJpaTemplate().findByNamedQuery(queryName);
    }

    /**
     * Return the persistent entities returned from a named query with named parameters.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
        if (paramNames.length != paramValues.length) {
            throw new IllegalArgumentException();
        }
        Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
        for (int i = 0; i < paramNames.length; ++i) {
            map.put(paramNames[i], paramValues[i]);
        }
        return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
    }

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    @Override
    public User findUserById(Long id) {
        return (User) getJpaTemplate().find(User.class, id);
    }

    /**
     * Return all persistent instances of the <code>AdminAccount</code> entity.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<User> findAllUsers() {
        return getJpaTemplate().find("select user from " + User.class.getSimpleName() + " user");
    }

    /**
     * Make the given instance managed and persistent.
     */
    @Override
    public void persistUser(User user) {
        JpaTemplate template = getJpaTemplate();
        template.persist(template.merge(user));
    }

    /**
     * Remove the given persistent instance.
     */
    @Override
    public void removeUser(User user) {
        JpaTemplate template = getJpaTemplate();
        /*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
        The merge method returns a managed copy of the given detached entity.*/
        template.remove(template.merge(user));
    }

    @Override
    public List<String> findAllRaName() {
        List<String> raNames = null;//new ArrayList<String>();
        String hqlQuery = "select raName from User "
                + "where userRole='" + Constants.ADMIN_RA + "'";
        JpaTemplate ht = getJpaTemplate();
       // raNames = ht.find(hqlQuery);
        if (raNames == null) {
            return new ArrayList();
        }
        return raNames;
    }

    @Override
    public String getRaSymbolByName(String raName) {
        String raSymbol = "00";//new ArrayList<String>();
        String hqlQuery = "select user.raSymbol from User user "
                + "where user.userRole='" + Constants.ADMIN_RA + "' and user.raName='" + raName + "'";
        JpaTemplate ht = getJpaTemplate();
        List list = ht.find(hqlQuery);
        if (!list.isEmpty()) {
            raSymbol = list.get(0).toString();
        }
        return raSymbol;
    }

    @Override
    public User getUserByUserName(String userName) {
        User user = null;
        String hqlQuery = "select user from User user "
                + "where user.userName='" + userName + "'";
        JpaTemplate ht = getJpaTemplate();
        List<User> userList = ht.find(hqlQuery);
        if (!userList.isEmpty()) {
            user = userList.get(0);
        }
        return user;
    }

    @Override
    public List findUsersByUserRole() {
        /*JpaTemplate template= getJpaTemplate();
        return template.executeFind(
                new JpaCallback() {

            @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                CriteriaBuilder cb=em.getCriteriaBuilder();
                return null;
            }
        }
                );*/
        String query="select user.userRole, count(user) from User user group by(user.userRole)";
        JpaTemplate template= getJpaTemplate();
        return template.find(query);
    }
}