package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.IAppReasonDao;
import com.mangoca.cmp.model.RevokeReason;
import com.mangoca.cmp.model.SuspensionReason;
import java.util.List;
import java.util.Map;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the entities: RevokeReason, SuspensionReason.
 */
public class AppReasonJPADao extends JpaDaoSupport implements IAppReasonDao {
	public AppReasonJPADao() {
		super();
	}
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName) {
		return getJpaTemplate().findByNamedQuery(queryName);
	}
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			throw new IllegalArgumentException();
		}
		Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
		for (int i = 0; i < paramNames.length; ++i) {
			map.put(paramNames[i], paramValues[i]);
		}
		return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
	}

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public RevokeReason findRevokeReasonById(int id) {
		return (RevokeReason)getJpaTemplate().find(RevokeReason.class, new Integer(id));
	}
	/**
	 * Return all persistent instances of the <code>RevokeReason</code> entity.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List<RevokeReason> findAllRevokeReasons() {
	 	return getJpaTemplate().find("select revokeReason from " + RevokeReason.class.getSimpleName() + " revokeReason");
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistRevokeReason(RevokeReason revokeReason) {
		JpaTemplate template = getJpaTemplate();
		template.persist(template.merge(revokeReason));
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeRevokeReason(RevokeReason revokeReason) {
		JpaTemplate template = getJpaTemplate();
		/*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
		 The merge method returns a managed copy of the given detached entity.*/
		template.remove(template.merge(revokeReason));
	}

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public SuspensionReason findSuspensionReasonById(int id) {
		return (SuspensionReason)getJpaTemplate().find(SuspensionReason.class, new Integer(id));
	}
	/**
	 * Return all persistent instances of the <code>SuspensionReason</code> entity.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List<SuspensionReason> findAllSuspensionReasons() {
	 	return getJpaTemplate().find("select suspensionReason from " + SuspensionReason.class.getSimpleName() + " suspensionReason");
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistSuspensionReason(SuspensionReason suspensionReason) {
		JpaTemplate template = getJpaTemplate();
		template.persist(template.merge(suspensionReason));
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeSuspensionReason(SuspensionReason suspensionReason) {
		JpaTemplate template = getJpaTemplate();
		/*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
		 The merge method returns a managed copy of the given detached entity.*/
		template.remove(template.merge(suspensionReason));
	}
}