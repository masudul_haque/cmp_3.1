package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.IPaymentDao;
import com.mangoca.cmp.model.Payment;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the Payment entity.
 */
public class PaymentJPADao extends JpaDaoSupport implements IPaymentDao {
	public PaymentJPADao() {
		super();
	}
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName) {
		return getJpaTemplate().findByNamedQuery(queryName);
	}
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			throw new IllegalArgumentException();
		}
		Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
		for (int i = 0; i < paramNames.length; ++i) {
			map.put(paramNames[i], paramValues[i]);
		}
		return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Payment findPaymentById(Long id) {
		return (Payment)getJpaTemplate().find(Payment.class, id);
	}
	/**
	 * Return all persistent instances of the <code>Payment</code> entity.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List<Payment> findAllPayments() {
	 	return getJpaTemplate().find("select payment from " + Payment.class.getSimpleName() + " payment");
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistPayment(Payment payment) {
		JpaTemplate template = getJpaTemplate();
		template.persist(template.merge(payment));
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removePayment(Payment payment) {
		JpaTemplate template = getJpaTemplate();
		/*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
		 The merge method returns a managed copy of the given detached entity.*/
		template.remove(template.merge(payment));
	}

    @Override
    public List getPaymentData(final int page,final int pageSize) {
        return  getJpaTemplate().executeFind(new JpaCallback() {
           @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                       String queryStr="select payment from " + Payment.class.getSimpleName() + " payment";
                        Query query = em.createQuery(queryStr);
                        query.setFirstResult(page*pageSize);
                        query.setMaxResults(pageSize);
                        List results = query.getResultList();
                        return results;
            }
        });
    }

    @Override
    public long getResultSize() { 
        return  (Long)getJpaTemplate().execute(new JpaCallback() {

            @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                String queryStr="select count(mangocert) from " + Payment.class.getSimpleName() + " mangocert";
                        Query query = em.createQuery(queryStr);
                return query.getSingleResult();        
            }
        });
    }
}