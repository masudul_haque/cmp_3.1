package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.IDocumentDao;
import com.mangoca.cmp.model.Document;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the Document entity.
 */
public class DocumentJPADao extends JpaDaoSupport implements IDocumentDao {
	public DocumentJPADao() {
		super();
	}
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName) {
		return getJpaTemplate().findByNamedQuery(queryName);
	}
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			throw new IllegalArgumentException();
		}
		Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
		for (int i = 0; i < paramNames.length; ++i) {
			map.put(paramNames[i], paramValues[i]);
		}
		return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Document findDocumentById(Long documentId) {
		return (Document)getJpaTemplate().find(Document.class, documentId);
	}
	/**
	 * Return all persistent instances of the <code>Document</code> entity.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List<Document> findAllDocuments() {
	 	return getJpaTemplate().find("select document from " + Document.class.getSimpleName() + " document");
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistDocument(Document document) {
		JpaTemplate template = getJpaTemplate();
		template.persist(template.merge(document));
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeDocument(Document document) {
		JpaTemplate template = getJpaTemplate();
		/*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
		 The merge method returns a managed copy of the given detached entity.*/
		template.remove(template.merge(document));
	}

    @Override
    public long getResultSize() {
        return  (Long)getJpaTemplate().execute(new JpaCallback() {

            @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                String queryStr="select count(document) from " + Document.class.getSimpleName() + " document";
                        Query query = em.createQuery(queryStr);
                return query.getSingleResult();        
            }
        });
    }

    @Override
    public List getDocumentData(final int page,final int pageSize) {
        return  getJpaTemplate().executeFind(new JpaCallback() {
           @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                       String queryStr="select document from " + Document.class.getSimpleName() + " document";
                        Query query = em.createQuery(queryStr);
                        query.setFirstResult(page*pageSize);
                        query.setMaxResults(pageSize);
                        List results = query.getResultList();
                        return results;
            }
        });
    }
}