package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.IAdminAccountDao;
import com.mangoca.cmp.model.AdminAccount;
import com.mangoca.cmp.model.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the AdminAccount entity.
 */
public class AdminAccountJPADao extends JpaDaoSupport implements IAdminAccountDao {

    public AdminAccountJPADao() {
        super();
    }

    /**
     * Return the persistent entities returned from a named query.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List findByNamedQuery(String queryName) {
        return getJpaTemplate().findByNamedQuery(queryName);
    }

    /**
     * Return the persistent entities returned from a named query with named parameters.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
        if (paramNames.length != paramValues.length) {
            throw new IllegalArgumentException();
        }
        Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
        for (int i = 0; i < paramNames.length; ++i) {
            map.put(paramNames[i], paramValues[i]);
        }
        return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
    }

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    @Override
    public AdminAccount findAdminAccountById(Long id) {
        return (AdminAccount) getJpaTemplate().find(AdminAccount.class, id);
    }

    /**
     * Return all persistent instances of the <code>AdminAccount</code> entity.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<AdminAccount> findAllAdminAccounts() {
        return getJpaTemplate().find("select user from " + AdminAccount.class.getSimpleName() + " user");
    }

    /**
     * Make the given instance managed and persistent.
     */
    @Override
    public void persistAdminAccount(AdminAccount user) {
        JpaTemplate template = getJpaTemplate();
        template.persist(template.merge(user));
    }

    /**
     * Remove the given persistent instance.
     */
    @Override
    public void removeAdminAccount(AdminAccount user) {
        JpaTemplate template = getJpaTemplate();
        /*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
        The merge method returns a managed copy of the given detached entity.*/
        template.remove(template.merge(user));
    }
   
    @Override
    public List<String> findAllRaName() {
        List<String> raNames = null;//new ArrayList<String>();
        String hqlQuery = "select raName from AdminAccount "
                + "where userRole=" + User.ADMIN_RA + "";
        JpaTemplate ht = getJpaTemplate();
        raNames = ht.find(hqlQuery);
        if (raNames == null) {
            return new ArrayList();
        }
        return raNames;
    }

    @Override
    public String getRaSymbolByName(String raName) {
        String raSymbol = "00";//new ArrayList<String>();
        String hqlQuery = "select user.raSymbol from AdminAccount user "
                + "where user.userRole=" + User.ADMIN_RA + " and user.raName='" + raName + "'";
        JpaTemplate ht = getJpaTemplate();
        List list = ht.find(hqlQuery);
        if (!list.isEmpty()) {
            raSymbol = list.get(0).toString();
        }
        return raSymbol;
    }

    @Override
    public AdminAccount getAdminAccountByUserName(String userName) {
        AdminAccount user = null;
        String hqlQuery = "select user from AdminAccount user "
                + "where user.userName='" + userName + "'";
        JpaTemplate ht = getJpaTemplate();
        List<AdminAccount> userList = ht.find(hqlQuery);
        if (!userList.isEmpty()) {
            user = userList.get(0);
        }
        return user;
    }

    @Override
    public List getAdminAccountData(final int page, final int size) {
         return  getJpaTemplate().executeFind(new JpaCallback() {
           @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                       String queryStr="select adminAccount from " + AdminAccount.class.getSimpleName() + " adminAccount";
                        Query query = em.createQuery(queryStr);
                        query.setFirstResult(page*size);
                        query.setMaxResults(size);
                        List results = query.getResultList();
                        return results;
            }
        });
    
    }

    @Override
    public long getResultSize() {
        return  (Long)getJpaTemplate().execute(new JpaCallback() {

            @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                String queryStr="select count(adminAccount) from " + AdminAccount.class.getSimpleName() + " adminAccount";
                        Query query = em.createQuery(queryStr);
                return query.getSingleResult();        
            }
        });
    }


    
}