package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.ICertTariffDao;
import com.mangoca.cmp.model.CertType;
import com.mangoca.cmp.model.WebSSL;
import java.util.List;
import java.util.Map;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the entities: CertType, WebSSL.
 */
public class CertTariffJPADao extends JpaDaoSupport implements ICertTariffDao {
	public CertTariffJPADao() {
		super();
	}
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName) {
		return getJpaTemplate().findByNamedQuery(queryName);
	}
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			throw new IllegalArgumentException();
		}
		Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
		for (int i = 0; i < paramNames.length; ++i) {
			map.put(paramNames[i], paramValues[i]);
		}
		return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
	}

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public CertType findCertTypeById(java.lang.Integer id) {
		return (CertType)getJpaTemplate().find(CertType.class, id);
	}
	/**
	 * Return all persistent instances of the <code>CertType</code> entity.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List<CertType> findAllCertTypes() {
	 	return getJpaTemplate().find("select certType from " + CertType.class.getSimpleName() + " certType");
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistCertType(CertType certType) {
		JpaTemplate template = getJpaTemplate();
		template.persist(template.merge(certType));
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeCertType(CertType certType) {
		JpaTemplate template = getJpaTemplate();
		/*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
		 The merge method returns a managed copy of the given detached entity.*/
		template.remove(template.merge(certType));
	}

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public WebSSL findWebSSLById(java.lang.Integer id) {
		return (WebSSL)getJpaTemplate().find(WebSSL.class, id);
	}
	/**
	 * Return all persistent instances of the <code>WebSSL</code> entity.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List<WebSSL> findAllWebSSLs() {
	 	return getJpaTemplate().find("select webSSL from " + WebSSL.class.getSimpleName() + " webSSL");
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistWebSSL(WebSSL webSSL) {
		JpaTemplate template = getJpaTemplate();
		template.persist(template.merge(webSSL));
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeWebSSL(WebSSL webSSL) {
		JpaTemplate template = getJpaTemplate();
		/*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
		 The merge method returns a managed copy of the given detached entity.*/
		template.remove(template.merge(webSSL));
	}

    @Override
    public List<String> findAllCertTypeNames() {
     	return getJpaTemplate().find("select certType.certTypeKey from " + CertType.class.getSimpleName() + " certType");
      }
}