package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.IEnrolleduserDao;
import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.model.Enrolleduser;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the Enrolleduser entity.
 */
public class EnrolleduserJPADao extends JpaDaoSupport implements IEnrolleduserDao {

    public EnrolleduserJPADao() {
        super();
    }

    /**
     * Return the persistent entities returned from a named query.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List findByNamedQuery(String queryName) {
        return getJpaTemplate().findByNamedQuery(queryName);
    }

    /**
     * Return the persistent entities returned from a named query with named parameters.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
        if (paramNames.length != paramValues.length) {
            throw new IllegalArgumentException();
        }
        Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
        for (int i = 0; i < paramNames.length; ++i) {
            map.put(paramNames[i], paramValues[i]);
        }
        return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
    }

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    @Override
    public Enrolleduser findEnrolleduserById(Long enrollId) {
        return (Enrolleduser) getJpaTemplate().find(Enrolleduser.class, enrollId);
    }

    /**
     * Return all persistent instances of the <code>Enrolleduser</code> entity.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Enrolleduser> findAllEnrolledusers() {
        return getJpaTemplate().find("select enrolleduser from " + Enrolleduser.class.getSimpleName() + " enrolleduser");
    }

    /**
     * Make the given instance managed and persistent.
     */
    @Override
    public void persistEnrolleduser(Enrolleduser enrolleduser) {
        JpaTemplate template = getJpaTemplate();
        template.persist(template.merge(enrolleduser));
        //template.persist(enrolleduser);
    }

    /**
     * Remove the given persistent instance.
     */
    @Override
    public void removeEnrolleduser(Enrolleduser enrolleduser) {
        JpaTemplate template = getJpaTemplate();
        /*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
        The merge method returns a managed copy of the given detached entity.*/
        template.remove(template.merge(enrolleduser));
    }

    @Override
    public Enrolleduser getEnrolluserByUserName(String userName) {
        Enrolleduser enrolledUser = null;
        String query = "select enrollUser from Enrolleduser enrollUser where enrollUser.userName='" + userName + "'";

        JpaTemplate ht = getJpaTemplate();

        List<Enrolleduser> enrollList = ht.find(query);
        if (!enrollList.isEmpty()) {
            enrolledUser = enrollList.get(0);
        }
        return enrolledUser;
    }

    @Override
    public Long getNoOfEnrolledUser() {
        JpaTemplate template = getJpaTemplate();
        String query = "select count(enrollUser) from Enrolleduser enrollUser "
                + "where enrollUser.raaction.status='" + Constants.ADMIN_RA + "'";
        return (Long) template.find(query).get(0);
    }

    @Override
    public List findUserByRaRole() {
        JpaTemplate template = getJpaTemplate();
        String query = "select enrollUser.raaction.status, count(enrollUser) from Enrolleduser enrollUser "
                + "group by(enrollUser.raaction.status)";
        return template.find(query);
    }

    @Override
    public List findUserByAccRole() {
        JpaTemplate template = getJpaTemplate();
        String query = "select enrollUser.accaction.status, count(enrollUser) from Enrolleduser enrollUser "
                + "group by(enrollUser.accaction.status)";
        return template.find(query);
    }

    @Override
    public List findUserByVaRole() {
        JpaTemplate template = getJpaTemplate();
        String query = "select enrollUser.vaaction.status, count(enrollUser) from Enrolleduser enrollUser "
                + "group by(enrollUser.vaaction.status)";
        return template.find(query);
    }

    @Override
    public List findUserByTechRole() {
        JpaTemplate template = getJpaTemplate();
        String query = "select enrollUser.techaction.status, count(enrollUser) from Enrolleduser enrollUser "
                + "group by(enrollUser.techaction.status)";
        return template.find(query);
    }

    @Override
    public List<Enrolleduser> findEnrolledusersByAdvanceSearch(
            String userName, String emailAccount, String mobileNo,
            String certClass, Date startDate, Date endDate) {
        StringBuffer query = new StringBuffer();
        query.append("SELECT e ");
        query.append("FROM Enrolleduser e ");
        query.append("WHERE e.enrollId IS NOT NULL ");
       
        List list= new ArrayList();
        //Map<String, Object> map = new java.util.HashMap<String, Object>();
        //Object values[]={userName};
        if (userName != null && userName.length()!=0) {
            query.append(" AND e.userName = '"+userName+"'");
            //list.add(userName);
        }
        
        if (emailAccount != null && emailAccount.length()!=0) {
            query.append(" AND e.emailAccount = '"+emailAccount+"'");
         //   map.put("userName", userName);
        }
        
        if (mobileNo != null && mobileNo.length()!=0) {
            query.append(" AND e.mobileNo = '"+mobileNo+"'");
            //map.put("userName", userName);
        } 
        if (certClass != null && certClass.length()!=0) {            
            query.append(" AND e.certClass = '"+certClass+"'");
            //map.put("certClass", certClass);
        }
        if(startDate!=null && endDate!=null){
            query.append("AND e.enrollDate BETWEEN "+startDate+" AND "+endDate);
      //      map.put("startDate", startDate);
        //    map.put("endDate", endDate);
        }
      
        JpaTemplate template = getJpaTemplate();
       
        return (List<Enrolleduser>) template.find(query.toString()); 
    }
    /*
    @Override
    public List<Enrolleduser> findEnrolledusersByAdvanceSearch(
            String userName, String emailAccount, String mobileNo,
            String certClass, Date startDate, Date endDate) {
        StringBuffer query = new StringBuffer();
        query.append("SELECT e ");
        query.append("FROM Enrolleduser e LEFT JOIN e.certificate p ");
        query.append("WHERE e.enrollId IS NOT NULL ");
        
        Map<String, Object> map = new java.util.HashMap<String, Object>();
        if (userName != null) {
            query.append("AND e.userName = :userName ");
            map.put("userName", userName);
        }
        if (emailAccount != null) {
            query.append("AND e.emailAccount = :emailAccount ");
            map.put("userName", userName);
        }
        if (mobileNo != null) {
            query.append("AND e.mobileNo = :mobileNo ");
            map.put("userName", userName);
        }
        if (certClass != null) {            
            query.append("AND p.certClass = :certClass ");
            map.put("certClass", certClass);
        }
        if(startDate!=null && endDate!=null){
            query.append("e.enrollDate BETWEEN :startDate AND :endDate ");
            map.put("startDate", startDate);
            map.put("endDate", endDate);
        }
       
        JpaTemplate template = getJpaTemplate();
       
        return (List<Enrolleduser>) template.findByNamedQueryAndNamedParams(query.toString(), map);
    }

    */

    @Override
    public long getResultSize() {
         return  (Long)getJpaTemplate().execute(new JpaCallback() {

            @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                String queryStr="select count(mangocert) from " + Enrolleduser.class.getSimpleName() + " mangocert";
                        Query query = em.createQuery(queryStr);
                return query.getSingleResult();        
            }
        });
    }

    @Override
    public List getEnrolleduserData(final int page, final int pageSize) {
        return  getJpaTemplate().executeFind(new JpaCallback() {
           @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                       String queryStr="select payment from " + Enrolleduser.class.getSimpleName() + " payment";
                        Query query = em.createQuery(queryStr);
                        query.setFirstResult(page*pageSize);
                        query.setMaxResults(pageSize);
                        List results = query.getResultList();
                        return results;
            }
        });
    }
    
}