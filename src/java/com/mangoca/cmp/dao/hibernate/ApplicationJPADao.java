/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.IApplicationDao;
import com.mangoca.cmp.model.Enrolleduser;
import java.util.List;
import java.util.Map;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 *
 * @author Masudul Haque
 */
public class ApplicationJPADao extends JpaDaoSupport implements IApplicationDao{
    
    @SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			throw new IllegalArgumentException();
		}
		Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
		for (int i = 0; i < paramNames.length; ++i) {
			map.put(paramNames[i], paramValues[i]);
		}
		return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
	}

    @Override
    public Integer countByNamedQuery(String queryName, String paramName, Object paramValue) {
              Integer count=0;
		Map<String, Object> map = new java.util.HashMap<String, Object>(1);
		
			map.put(paramName, paramValue);
		List list= getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
                if(!list.isEmpty()){
                    count=Integer.parseInt(list.get(0).toString());
                }
           return count;     
    }

    @Override
    public List<Enrolleduser> findRevocationEnrolledusers( Short status) {
         StringBuffer queryBuffer= new StringBuffer();
        Map<String, Object> map = new java.util.HashMap<String, Object>();
        if(status!=null && status !=0){
            queryBuffer.append(" and e.revocation.status = :status");
            map.put("status", status);
        }
        String queryStr="SELECT e FROM Enrolleduser e WHERE e.revocation IS NOT NULL"+queryBuffer;
		
	return getJpaTemplate().findByNamedParams(queryStr, map);
    }

    @Override
    public List<Enrolleduser> findRenewalEnrolledusers(final Short status) {
      
         StringBuffer queryBuffer= new StringBuffer();
        Map<String, Object> map = new java.util.HashMap<String, Object>();
        if(status!=null && status !=0){
            queryBuffer.append(" and e.renewal.status = :status");
            map.put("status", status);
        }
        String queryStr="SELECT e FROM Enrolleduser e WHERE e.renewal IS NOT NULL"+queryBuffer;
		
	return getJpaTemplate().findByNamedParams(queryStr, map);
    }

    @Override
    public List<Enrolleduser> findSuspensionEnrolledusers( Short status) {
          StringBuffer queryBuffer= new StringBuffer();
        Map<String, Object> map = new java.util.HashMap<String, Object>();
        if(status!=null && status !=0){
            queryBuffer.append(" and e.suspension.status = :status");
            map.put("status", status);
        }
        String queryStr="SELECT e FROM Enrolleduser e WHERE e.suspension IS NOT NULL"+queryBuffer;
		
	return getJpaTemplate().findByNamedParams(queryStr, map);
    }

    @Override
    public List<Object[]> countRevokeStatusesByRa() {
        
        JpaTemplate template = getJpaTemplate();
        String query = "select e.revocation.raaction.status, count(e) from Enrolleduser e "
                + "group by(e.revocation.raaction.status)";
        return template.find(query);
    }

    @Override
    public List<Object[]> countRevokeStatusesByCc() {
        JpaTemplate template = getJpaTemplate();
        String query = "select e.revocation.ccaction.status, count(e) from Enrolleduser e "
                + "group by(e.revocation.ccaction.status)";
        return template.find(query);
    }

    @Override
    public List<Object[]> countRevokeStatusesByTech() {
        JpaTemplate template = getJpaTemplate();
        String query = "select e.revocation.techaction.status, count(e) from Enrolleduser e "
                + "group by(e.revocation.techaction.status)";
        return template.find(query);
    }

    @Override
    public List<Object[]> countSuspendStatusesByRa() {
        JpaTemplate template = getJpaTemplate();
        String query = "select e.suspension.raaction.status, count(e) from Enrolleduser e "
                + "group by(e.suspension.raaction.status)";
        return template.find(query);
    }

    @Override
    public List<Object[]> countSuspendStatusesByCc() {
        JpaTemplate template = getJpaTemplate();
        String query = "select e.suspension.ccaction.status, count(e) from Enrolleduser e "
                + "group by(e.suspension.ccaction.status)";
        return template.find(query);
    }

    @Override
    public List<Object[]> countSuspendStatusesByTech() {
        JpaTemplate template = getJpaTemplate();
        String query = "select e.suspension.techaction.status, count(e) from Enrolleduser e "
                + "group by(e.suspension.techaction.status)";
        return template.find(query);
    }

    @Override
    public List<Object[]> countRenewalStatusesByRa() {
        JpaTemplate template = getJpaTemplate();
        String query = "select e.renewal.raaction.status, count(e) from Enrolleduser e "
                + "group by(e.renewal.raaction.status)";
        return template.find(query);
    }

    @Override
    public List<Object[]> countRenewalStatusesByCc() {
        JpaTemplate template = getJpaTemplate();
        String query = "select e.renewal.ccaction.status, count(e) from Enrolleduser e "
                + "group by(e.renewal.ccaction.status)";
        return template.find(query);
    }

    @Override
    public List<Object[]> countRenewalStatusesByTech() {
        JpaTemplate template = getJpaTemplate();
        String query = "select e.renewal.techaction.status, count(e) from Enrolleduser e "
                + "group by(e.renewal.techaction.status)";
        return template.find(query);}

    @Override
    public List<Object[]> countRenewalStatusesByAcc() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
