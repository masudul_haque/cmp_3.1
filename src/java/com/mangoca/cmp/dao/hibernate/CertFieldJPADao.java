package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.ICertFieldDao;
import com.mangoca.cmp.model.Country;
import com.mangoca.cmp.model.Locality;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the entities: Country, Locality.
 */
public class CertFieldJPADao extends JpaDaoSupport implements ICertFieldDao {

    public CertFieldJPADao() {
        super();
    }

    /**
     * Return the persistent entities returned from a named query.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List findByNamedQuery(String queryName) {
        return getJpaTemplate().findByNamedQuery(queryName);
    }

    /**
     * Return the persistent entities returned from a named query with named
     * parameters.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
        if (paramNames.length != paramValues.length) {
            throw new IllegalArgumentException();
        }
        Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
        for (int i = 0; i < paramNames.length; ++i) {
            map.put(paramNames[i], paramValues[i]);
        }
        return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
    }

    /**
     * Find an entity by its id (primary key).
     *
     * @return The found entity instance or null if the entity does not exist.
     */
    @Override
    public Country findCountryById(java.lang.Integer id) {
        return (Country) getJpaTemplate().find(Country.class, id);
    }

    /**
     * Return all persistent instances of the
     * <code>Country</code> entity.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Country> findAllCountries() {
        return getJpaTemplate().find("select country from " + Country.class.getSimpleName() + " country");
    }

    /**
     * Make the given instance managed and persistent.
     */
    @Override
    public void persistCountry(Country country) {
        JpaTemplate template = getJpaTemplate();
        template.persist(template.merge(country));
    }

    /**
     * Remove the given persistent instance.
     */
    @Override
    public void removeCountry(Country country) {
        JpaTemplate template = getJpaTemplate();
        /*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
         The merge method returns a managed copy of the given detached entity.*/
        template.remove(template.merge(country));
    }

    /**
     * Find an entity by its id (primary key).
     *
     * @return The found entity instance or null if the entity does not exist.
     */
    @Override
    public Locality findLocalityById(java.lang.Integer id) {
        return (Locality) getJpaTemplate().find(Locality.class, id);
    }

    /**
     * Return all persistent instances of the
     * <code>Locality</code> entity.
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Locality> findAllLocalities() {
        return getJpaTemplate().find("select locality from " + Locality.class.getSimpleName() + " locality");
    }

    /**
     * Make the given instance managed and persistent.
     */
    @Override
    public void persistLocality(Locality locality) {
        JpaTemplate template = getJpaTemplate();
        template.persist(template.merge(locality));
    }

    /**
     * Remove the given persistent instance.
     */
    @Override
    public void removeLocality(Locality locality) {
        JpaTemplate template = getJpaTemplate();
        /*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
         The merge method returns a managed copy of the given detached entity.*/
        template.remove(template.merge(locality));
    }

    @Override
    public List<String> findAllLocalityNames() {
        return getJpaTemplate().executeFind(new JpaCallback() {
            @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                List<String> list = new ArrayList<String>();
                String queryStr = "select l.name from " + Locality.class.getSimpleName() + " l";
                Query query = em.createQuery(queryStr);
                return query.getResultList();
            }
        });
    }
}