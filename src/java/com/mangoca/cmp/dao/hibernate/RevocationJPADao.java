package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.IRevocationDao;
import com.mangoca.cmp.model.Revocation;
import java.util.List;
import java.util.Map;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the Revocation entity.
 */
public class RevocationJPADao extends JpaDaoSupport implements IRevocationDao {
	public RevocationJPADao() {
		super();
	}
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName) {
		return getJpaTemplate().findByNamedQuery(queryName);
	}
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			throw new IllegalArgumentException();
		}
		Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
		for (int i = 0; i < paramNames.length; ++i) {
			map.put(paramNames[i], paramValues[i]);
		}
		return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Revocation findRevocationById(java.lang.Long id) {
		return (Revocation)getJpaTemplate().find(Revocation.class, id);
	}
	/**
	 * Return all persistent instances of the <code>Revocation</code> entity.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List<Revocation> findAllRevocations() {
	 	return getJpaTemplate().find("select revocation from " + Revocation.class.getSimpleName() + " revocation");
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistRevocation(Revocation revocation) {
		JpaTemplate template = getJpaTemplate();
		template.persist(template.merge(revocation));
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeRevocation(Revocation revocation) {
		JpaTemplate template = getJpaTemplate();
		/*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
		 The merge method returns a managed copy of the given detached entity.*/
		template.remove(template.merge(revocation));
	}
}