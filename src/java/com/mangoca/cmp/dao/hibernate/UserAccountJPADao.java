package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.IUserAccountDao;
import com.mangoca.cmp.model.UserAccount;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the UserAccount entity.
 */
public class UserAccountJPADao extends JpaDaoSupport implements IUserAccountDao {
	public UserAccountJPADao() {
		super();
	}
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName) {
		return getJpaTemplate().findByNamedQuery(queryName);
	}
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			throw new IllegalArgumentException();
		}
		Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
		for (int i = 0; i < paramNames.length; ++i) {
			map.put(paramNames[i], paramValues[i]);
		}
		return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public UserAccount findUserAccountById(Long id) {
		return (UserAccount)getJpaTemplate().find(UserAccount.class, id);
	}
	/**
	 * Return all persistent instances of the <code>UserAccount</code> entity.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List<UserAccount> findAllUserAccounts() {
	 	return getJpaTemplate().find("select empuser from " + UserAccount.class.getSimpleName() + " empuser");
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistUserAccount(UserAccount empuser) {
		JpaTemplate template = getJpaTemplate();
		template.persist(template.merge(empuser));
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeUserAccount(UserAccount empuser) {
		JpaTemplate template = getJpaTemplate();
		/*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
		 The merge method returns a managed copy of the given detached entity.*/
		template.remove(template.merge(empuser));
	}
	@Override
	public UserAccount getUserAccountByUserName(String userName) {
		UserAccount empuser= null;
		JpaTemplate jt= getJpaTemplate();
	    String query="select empuser from UserAccount empuser where empuser.userName='"+userName+"'";
		List list= jt.find(query);
	    if(!list.isEmpty()){
	    	empuser=(UserAccount) list.get(0);
	    }
		return empuser;
	}
	@Override
	public UserAccount getAuthorizedEmpuser(String userName, String password) {
		  UserAccount empuser=null;  
	      JpaTemplate ht= getJpaTemplate();
	      String query="select empuser from UserAccount empuser "
	              + "where empuser.userName='"+userName+"' and empuser.password='"+password+"'";
	      
	      List<UserAccount> empList=(List<UserAccount>)ht.find(query);
	      if(!empList.isEmpty()){
	      empuser=empList.get(0);
	      }
	      return empuser;
	}
	@Override
	public Long getLastUserId() {
                Long lastId=null;
		String query="select MAX(user.id) from User user ";
		JpaTemplate jt= getJpaTemplate();
                List list=jt.find(query);
                if(list !=null && !list.isEmpty()){
                    lastId=(Long) list.get(0);
                    if(lastId==null){
                        lastId=0L;
                    }
                }
               return lastId;
	}
	@Override
	public UserAccount getUserAccountByValidityNo(Long validityNo) {
		 UserAccount empuser=null;  
	      JpaTemplate ht= getJpaTemplate();
	      String query="select empuser from UserAccount empuser where empuser.validityNo="+validityNo;
	      
	      List<UserAccount> empList=(List<UserAccount>)ht.find(query);
	      if(!empList.isEmpty()){
	      empuser=empList.get(0);
	      }
	      return empuser;
	}

    @Override
    public List getUserAccountData(final int page, final int size) {
         return  getJpaTemplate().executeFind(new JpaCallback() {
           @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                       String queryStr="select userAccount from " + UserAccount.class.getSimpleName() + " userAccount";
                        Query query = em.createQuery(queryStr);
                        query.setFirstResult(page*size);
                        query.setMaxResults(size);
                        List results = query.getResultList();
                        return results;
            }
        });
    
    }

    @Override
    public long getResultSize() {
        return  (Long)getJpaTemplate().execute(new JpaCallback() {

            @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                String queryStr="select count(userAccount) from " + UserAccount.class.getSimpleName() + " userAccount";
                        Query query = em.createQuery(queryStr);
                return query.getSingleResult();        
            }
        });
    }


}