package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.ICertificateDao;
import com.mangoca.cmp.model.Certificate;
import java.util.List;
import java.util.Map;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the Certificate entity.
 */
public class CertificateJPADao extends JpaDaoSupport implements ICertificateDao {
	public CertificateJPADao() {
		super();
	}
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName) {
		return getJpaTemplate().findByNamedQuery(queryName);
	}
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			throw new IllegalArgumentException();
		}
		Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
		for (int i = 0; i < paramNames.length; ++i) {
			map.put(paramNames[i], paramValues[i]);
		}
		return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Certificate findCertificateById(java.lang.Long id) {
		return (Certificate)getJpaTemplate().find(Certificate.class, id);
	}
	/**
	 * Return all persistent instances of the <code>Certificate</code> entity.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List<Certificate> findAllCertificates() {
	 	return getJpaTemplate().find("select certificate from " + Certificate.class.getSimpleName() + " certificate");
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistCertificate(Certificate certificate) {
		JpaTemplate template = getJpaTemplate();
		template.persist(template.merge(certificate));
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeCertificate(Certificate certificate) {
		JpaTemplate template = getJpaTemplate();
		/*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
		 The merge method returns a managed copy of the given detached entity.*/
		template.remove(template.merge(certificate));
	}
}