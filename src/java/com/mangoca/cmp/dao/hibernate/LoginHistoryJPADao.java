package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.ILoginHistoryDao;
import com.mangoca.cmp.model.LoginHistoryLog;
import java.util.List;
import java.util.Map;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the LoginHistoryLog entity.
 */
public class LoginHistoryJPADao extends JpaDaoSupport implements ILoginHistoryDao {
	public LoginHistoryJPADao() {
		super();
	}
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName) {
		return getJpaTemplate().findByNamedQuery(queryName);
	}
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			throw new IllegalArgumentException();
		}
		Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
		for (int i = 0; i < paramNames.length; ++i) {
			map.put(paramNames[i], paramValues[i]);
		}
		return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public LoginHistoryLog findLoginHistoryLogById(java.lang.Integer id) {
		return (LoginHistoryLog)getJpaTemplate().find(LoginHistoryLog.class, id);
	}
	/**
	 * Return all persistent instances of the <code>LoginHistoryLog</code> entity.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List<LoginHistoryLog> findAllLoginHistoryLogs() {
	 	return getJpaTemplate().find("select loginHistoryLog from " + LoginHistoryLog.class.getSimpleName() + " loginHistoryLog");
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistLoginHistoryLog(LoginHistoryLog loginHistoryLog) {
		JpaTemplate template = getJpaTemplate();
		template.persist(template.merge(loginHistoryLog));
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeLoginHistoryLog(LoginHistoryLog loginHistoryLog) {
		JpaTemplate template = getJpaTemplate();
		/*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
		 The merge method returns a managed copy of the given detached entity.*/
		template.remove(template.merge(loginHistoryLog));
	}
}