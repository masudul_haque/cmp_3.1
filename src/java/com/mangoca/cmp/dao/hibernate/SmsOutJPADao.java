package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.ISmsOutDao;
import com.mangoca.cmp.model.SmsOut;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the SmsOut entity.
 */
public class SmsOutJPADao extends JpaDaoSupport implements ISmsOutDao {
	public SmsOutJPADao() {
		super();
	}
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName) {
		return getJpaTemplate().findByNamedQuery(queryName);
	}
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			throw new IllegalArgumentException();
		}
		Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
		for (int i = 0; i < paramNames.length; ++i) {
			map.put(paramNames[i], paramValues[i]);
		}
		return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public SmsOut findSmsOutById(int id) {
		return (SmsOut)getJpaTemplate().find(SmsOut.class, new Integer(id));
	}
	/**
	 * Return all persistent instances of the <code>SmsOut</code> entity.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List<SmsOut> findAllSmsOuts() {
	 	return getJpaTemplate().find("select smsOut from " + SmsOut.class.getSimpleName() + " smsOut");
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistSmsOut(SmsOut smsOut) {
		JpaTemplate template = getJpaTemplate();
		template.persist(template.merge(smsOut));
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeSmsOut(SmsOut smsOut) {
		JpaTemplate template = getJpaTemplate();
		/*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
		 The merge method returns a managed copy of the given detached entity.*/
		template.remove(template.merge(smsOut));
	}

    @Override
    public List getSmsOutData(final int page, final int pageSize) {
        return  getJpaTemplate().executeFind(new JpaCallback() {
           @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                       String queryStr="select mangocert from " + SmsOut.class.getSimpleName() + " mangocert";
                        Query query = em.createQuery(queryStr);
                        query.setFirstResult(page*pageSize);
                        query.setMaxResults(pageSize);
                        List results = query.getResultList();
                        return results;
            }
        });
    }

    @Override
    public long getResultSize() {
         return  (Long)getJpaTemplate().execute(new JpaCallback() {

            @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                String queryStr="select count(mangocert) from " + SmsOut.class.getSimpleName() + " mangocert";
                        Query query = em.createQuery(queryStr);
                return query.getSingleResult();        
            }
        });
    }
}