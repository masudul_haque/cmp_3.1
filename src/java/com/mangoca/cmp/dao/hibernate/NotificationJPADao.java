package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.INotificationDao;
import com.mangoca.cmp.model.Notification;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the Notification entity.
 */
public class NotificationJPADao extends JpaDaoSupport implements INotificationDao { 
	public NotificationJPADao() {
		super();
	}
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName) {
		return getJpaTemplate().findByNamedQuery(queryName);
	}
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			throw new IllegalArgumentException();
		}
		Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
		for (int i = 0; i < paramNames.length; ++i) {
			map.put(paramNames[i], paramValues[i]);
		}
		return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Notification findNotificationById(Long id) {
		return (Notification)getJpaTemplate().find(Notification.class, id);
	}
	/**
	 * Return all persistent instances of the <code>Notification</code> entity.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List<Notification> findAllNotifications() {
	 	return getJpaTemplate().find("select notificatioin from " + Notification.class.getSimpleName() + " notificatioin");
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistNotification(Notification notificatioin) {
		JpaTemplate template = getJpaTemplate();
		template.persist(template.merge(notificatioin));
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeNotification(Notification notificatioin) {
		JpaTemplate template = getJpaTemplate();
		/*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
		 The merge method returns a managed copy of the given detached entity.*/
		template.remove(template.merge(notificatioin));
	}

    @Override
    public List getNotificationData(final int page, final int size) {
         return  getJpaTemplate().executeFind(new JpaCallback() {
           @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                       String queryStr="select notification from " + Notification.class.getSimpleName() + " notification";
                        Query query = em.createQuery(queryStr);
                        query.setFirstResult(page*size);
                        query.setMaxResults(size);
                        List results = query.getResultList();
                        return results;
            }
        });
    }

    @Override
    public long getResultSize() {
          return  (Long)getJpaTemplate().execute(new JpaCallback() {
            @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                String queryStr="select count(notification) from " + Notification.class.getSimpleName() + " notification";
                                Query query = em.createQuery(queryStr);
                return query.getSingleResult();        
            }
        });
    }


  
}