package com.mangoca.cmp.dao.hibernate;

import com.mangoca.cmp.dao.ITicketDao;
import com.mangoca.cmp.model.Ticket;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.orm.jpa.JpaTemplate;
import org.springframework.orm.jpa.support.JpaDaoSupport;

/**
 * The DAO class for the Ticket entity.
 */
public class TicketJPADao extends JpaDaoSupport implements ITicketDao {
	public TicketJPADao() {
		super();
	}
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName) {
		return getJpaTemplate().findByNamedQuery(queryName);
	}
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			throw new IllegalArgumentException();
		}
		Map<String, Object> map = new java.util.HashMap<String, Object>(paramNames.length);
		for (int i = 0; i < paramNames.length; ++i) {
			map.put(paramNames[i], paramValues[i]);
		}
		return getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Ticket findTicketById(Long ticketId) {
		return (Ticket)getJpaTemplate().find(Ticket.class, ticketId);
	}
	/**
	 * Return all persistent instances of the <code>Ticket</code> entity.
	 */
	@SuppressWarnings("unchecked")
    @Override
	public List<Ticket> findAllTickets() {
	 	return getJpaTemplate().find("select ticket from " + Ticket.class.getSimpleName() + " ticket");
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistTicket(Ticket ticket) {
		JpaTemplate template = getJpaTemplate();
		template.persist(template.merge(ticket));
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeTicket(Ticket clientticket) {
		JpaTemplate template = getJpaTemplate();
		/*In JPA, objects detach automatically when they are serialized or when a persistence context ends.
		 The merge method returns a managed copy of the given detached entity.*/
		template.remove(template.merge(clientticket));
	}
        
    @Override
    public Integer countByNamedQuery(String queryName, String paramName, Object paramValue){
         Integer count=0;
		Map<String, Object> map = new java.util.HashMap<String, Object>(1);
		
			map.put(paramName, paramValue);
                        
		List list= getJpaTemplate().findByNamedQueryAndNamedParams(queryName, map);
                if(!list.isEmpty()){
                    count=Integer.parseInt(list.get(0).toString());
                }
           return count;   
    }
    @Override
    public List findTicketByStatus() {
        JpaTemplate template = getJpaTemplate();
        String query = "select ticket.status, count(ticket) from Ticket ticket "
                + "group by(ticket.status)";
        return template.find(query);
    }

    @Override
    public List<Ticket> getTicketDataByStatus(final Short status, final int page, final int size) {
             return  getJpaTemplate().executeFind(new JpaCallback() {
           @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                       String queryStr="select ticket from " + Ticket.class.getSimpleName() + " ticket where ticket.status=:status";
                        Query query = em.createQuery(queryStr);
                        query.setParameter("status", status);
                        query.setFirstResult(page*size);
                        query.setMaxResults(size+1);
                        List results = query.getResultList();
                        return results;
            }
        });
    
    }

    @Override
    public List<Ticket> getTicketDataByUserName(final String userName, final int page, final int pageSize) {
             return  getJpaTemplate().executeFind(new JpaCallback() {
           @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                       String queryStr="select ticket from " + Ticket.class.getSimpleName() + " ticket where ticket.userName=:userName";
                        Query query = em.createQuery(queryStr);
                        query.setParameter("userName", userName);
                        query.setFirstResult(page*pageSize);
                        query.setMaxResults(pageSize);
                        List results = query.getResultList();
                        return results;
            }
        });
    
    }

    @Override
    public long countTicketByUserName(final String userName) {
        return  (Long)getJpaTemplate().execute(new JpaCallback() {

            @Override
            public Object doInJpa(EntityManager em) throws PersistenceException {
                String queryStr="select count(ticket) from " + Ticket.class.getSimpleName() + " ticket where ticket.userName=:userName";

                Query query = em.createQuery(queryStr);
                query.setParameter("userName", userName);
                return query.getSingleResult();        
            }
        });
    }
    
}