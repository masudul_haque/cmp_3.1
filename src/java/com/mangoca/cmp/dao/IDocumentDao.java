package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.Document;
import java.util.List;

/**
 * The DAO interface for the Document entity.
 */
public interface IDocumentDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Document findDocumentById(Long documentId);
	/**
	 * Return all persistent instances of the <code>Document</code> entity.
	 */
	public List<Document> findAllDocuments();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistDocument(Document document);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeDocument(Document document);
        
         public long getResultSize();

    public List getDocumentData(int page, int size);
}