package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.Revocation;
import java.util.List;

/**
 * The DAO interface for the Revocation entity.
 */
public interface IRevocationDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Revocation findRevocationById(java.lang.Long id);
	/**
	 * Return all persistent instances of the <code>Revocation</code> entity.
	 */
	public List<Revocation> findAllRevocations();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistRevocation(Revocation revocation);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeRevocation(Revocation revocation);
}