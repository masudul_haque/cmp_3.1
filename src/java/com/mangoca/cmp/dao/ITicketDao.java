package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.Ticket;
import java.util.List;

/**
 * The DAO interface for the Ticket entity.
 */
public interface ITicketDao {

    /**
     * Return the persistent entities returned from a named query.
     */
    @SuppressWarnings("unchecked")
    public List findByNamedQuery(String queryName);

    /**
     * Return the persistent entities returned from a named query with named parameters.
     */
    @SuppressWarnings("unchecked")
    public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public Ticket findTicketById(Long ticketId);

    /**
     * Return all persistent instances of the <code>Ticket</code> entity.
     */
    public List<Ticket> findAllTickets();

    /**
     * Make the given instance managed and persistent.
     */
    public void persistTicket(Ticket clientticket);

    /**
     * Remove the given persistent instance.
     */
    public void removeTicket(Ticket clientticket);

    public Integer countByNamedQuery(String queryName, String paramName, Object paramValue);

    public List findTicketByStatus();

    public List<Ticket> getTicketDataByStatus(Short status, int page, int pageSize);

    public List<Ticket> getTicketDataByUserName(String userName, int page, int pageSize);

    public long countTicketByUserName(String userName);
}