package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.Country;
import com.mangoca.cmp.model.Locality;
import java.util.List;

/**
 * The DAO interface for the entities: Country, Locality.
 */
public interface ICertFieldDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Country findCountryById(java.lang.Integer id);
	/**
	 * Return all persistent instances of the <code>Country</code> entity.
	 */
	public List<Country> findAllCountries();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistCountry(Country country);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeCountry(Country country);

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Locality findLocalityById(java.lang.Integer id);
	/**
	 * Return all persistent instances of the <code>Locality</code> entity.
	 */
	public List<Locality> findAllLocalities();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistLocality(Locality locality);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeLocality(Locality locality);

    public List<String> findAllLocalityNames();
}