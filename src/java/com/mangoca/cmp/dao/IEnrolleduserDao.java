package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.Enrolleduser;
import java.util.Date;
import java.util.List;

/**
 * The DAO interface for the Enrolleduser entity.
 */
public interface IEnrolleduserDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Enrolleduser findEnrolleduserById(Long enrollId);
	/**
	 * Return all persistent instances of the <code>Enrolleduser</code> entity.
	 */
	public List<Enrolleduser> findAllEnrolledusers();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistEnrolleduser(Enrolleduser enrolleduser);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeEnrolleduser(Enrolleduser enrolleduser);

        public Enrolleduser getEnrolluserByUserName(String userName);
        
        public Long  getNoOfEnrolledUser();
        
        public List findUserByRaRole();
        public List findUserByAccRole();
        public List findUserByVaRole();
        public List findUserByTechRole();
        
        
	public List<Enrolleduser> findEnrolledusersByAdvanceSearch(
                String userName, String emailAccount, String mobileNo,
                String certClass, Date startDate, Date endDate);

    public long getResultSize();

    public List getEnrolleduserData(int page, int size);
}