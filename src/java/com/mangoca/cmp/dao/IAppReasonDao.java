package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.RevokeReason;
import com.mangoca.cmp.model.SuspensionReason;
import java.util.List;

/**
 * The DAO interface for the entities: RevokeReason, SuspensionReason.
 */
public interface IAppReasonDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public RevokeReason findRevokeReasonById(int id);
	/**
	 * Return all persistent instances of the <code>RevokeReason</code> entity.
	 */
	public List<RevokeReason> findAllRevokeReasons();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistRevokeReason(RevokeReason revokeReason);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeRevokeReason(RevokeReason revokeReason);

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public SuspensionReason findSuspensionReasonById(int id);
	/**
	 * Return all persistent instances of the <code>SuspensionReason</code> entity.
	 */
	public List<SuspensionReason> findAllSuspensionReasons();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistSuspensionReason(SuspensionReason suspensionReason);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeSuspensionReason(SuspensionReason suspensionReason);
}