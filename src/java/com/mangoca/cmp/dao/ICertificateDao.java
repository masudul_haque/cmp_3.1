package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.Certificate;
import java.util.List;

/**
 * The DAO interface for the Certificate entity.
 */
public interface ICertificateDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Certificate findCertificateById(java.lang.Long id);
	/**
	 * Return all persistent instances of the <code>Certificate</code> entity.
	 */
	public List<Certificate> findAllCertificates();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistCertificate(Certificate certificate);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeCertificate(Certificate certificate);
}