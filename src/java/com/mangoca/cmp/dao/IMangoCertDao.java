package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.Mangocert;
import java.util.List;

/**
 * The DAO interface for the Mangocert entity.
 */
public interface IMangoCertDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Mangocert findMangocertById(int id);
	/**
	 * Return all persistent instances of the <code>Mangocert</code> entity.
	 */
	public List<Mangocert> findAllMangocerts();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistMangocert(Mangocert mangocert);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeMangocert(Mangocert mangocert);
         public List getMangocertData(int page, int pageSize);
         
         
         public long getResultSize();
}