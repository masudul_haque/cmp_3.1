package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.CertType;
import com.mangoca.cmp.model.WebSSL;
import java.util.List;

/**
 * The DAO interface for the entities: CertType, WebSSL.
 */
public interface ICertTariffDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public CertType findCertTypeById(java.lang.Integer id);
	/**
	 * Return all persistent instances of the <code>CertType</code> entity.
	 */
	public List<CertType> findAllCertTypes();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistCertType(CertType certType);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeCertType(CertType certType);

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public WebSSL findWebSSLById(java.lang.Integer id);
	/**
	 * Return all persistent instances of the <code>WebSSL</code> entity.
	 */
	public List<WebSSL> findAllWebSSLs();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistWebSSL(WebSSL webSSL);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeWebSSL(WebSSL webSSL);

        public List<String> findAllCertTypeNames();
}