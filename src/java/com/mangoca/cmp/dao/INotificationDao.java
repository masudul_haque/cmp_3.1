package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.Notification;
import java.util.List;

/**
 * The DAO interface for the Notification entity.
 */
public interface INotificationDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Notification findNotificationById(Long id);
	/**
	 * Return all persistent instances of the <code>Notification</code> entity.
	 */
	public List<Notification> findAllNotifications();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistNotification(Notification notificatioin);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeNotification(Notification notificatioin);

    public List getNotificationData(int page, int size);

    public long getResultSize();
}