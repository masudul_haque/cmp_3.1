package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.Suspension;
import java.util.List;

/**
 * The DAO interface for the Suspension entity.
 */
public interface ISuspensionDao {
	/**
	 * Return the persistent entities returned from a named query.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName);
	/**
	 * Return the persistent entities returned from a named query with named parameters.
	 */
	@SuppressWarnings("unchecked")
	public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Suspension findSuspensionById(java.lang.Long id);
	/**
	 * Return all persistent instances of the <code>Suspension</code> entity.
	 */
	public List<Suspension> findAllSuspensions();
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistSuspension(Suspension suspension);
	/**
	 * Remove the given persistent instance.
	 */
	public void removeSuspension(Suspension suspension);
}