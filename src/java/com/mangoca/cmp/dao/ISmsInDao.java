package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.SmsIn;
import java.util.List;

/**
 * The DAO interface for the SmsIn entity.
 */
public interface ISmsInDao {

    /**
     * Return the persistent entities returned from a named query.
     */
    @SuppressWarnings("unchecked")
    public List findByNamedQuery(String queryName);

    /**
     * Return the persistent entities returned from a named query with named parameters.
     */
    @SuppressWarnings("unchecked")
    public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public SmsIn findSmsInById(int id);

    /**
     * Return all persistent instances of the <code>SmsIn</code> entity.
     */
    public List<SmsIn> findAllSmsIns();

    /**
     * Make the given instance managed and persistent.
     */
    public void persistSmsIn(SmsIn smsIn);

    /**
     * Remove the given persistent instance.
     */
    public void removeSmsIn(SmsIn smsIn);

    public List getSmsInData(int page, int size);

    public long getResultSize();
}