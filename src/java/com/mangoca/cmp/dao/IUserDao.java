package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.User;
import java.util.List;


/**
 * The DAO interface for the AdminAccount entity.
 */
public interface IUserDao {

    /**
     * Return the persistent entities returned from a named query.
     */
    @SuppressWarnings("unchecked")
    public List findByNamedQuery(String queryName);

    /**
     * Return the persistent entities returned from a named query with named parameters.
     */
    @SuppressWarnings("unchecked")
    public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public User findUserById(Long id);

    /**
     * Return all persistent instances of the <code>AdminAccount</code> entity.
     */
    public List<User> findAllUsers();

    /**
     * Make the given instance managed and persistent.
     */
    public void persistUser(User user);

    /**
     * Remove the given persistent instance.
     */
    public void removeUser(User user);

    public List<String> findAllRaName();

    public String getRaSymbolByName(String raName);

    public User getUserByUserName(String userName);

    public List findUsersByUserRole();
}