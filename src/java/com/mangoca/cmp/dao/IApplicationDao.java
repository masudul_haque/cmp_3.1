/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.dao;

import com.mangoca.cmp.model.Enrolleduser;
import java.util.List;

/**
 *
 * @author Masudul Haque
 */
public interface IApplicationDao {

    public Integer countByNamedQuery(String queryName, String paramName, Object paramValue);
    //@SuppressWarnings("unchecked")
    public List findByNamedQuery(String queryName, String[] paramNames, Object[] paramValues);
	
    public List<Enrolleduser> findRevocationEnrolledusers(Short status);

    public List<Enrolleduser> findRenewalEnrolledusers(Short status);

    public List<Enrolleduser> findSuspensionEnrolledusers(Short status);

    //Revocation
    public List<Object[]> countRevokeStatusesByRa();    
    public List<Object[]> countRevokeStatusesByCc();
    public List<Object[]> countRevokeStatusesByTech();
    //Suspension
    public List<Object[]> countSuspendStatusesByRa();
    public List<Object[]> countSuspendStatusesByCc();
    public List<Object[]> countSuspendStatusesByTech();
    //Renewal
    public List<Object[]> countRenewalStatusesByRa();
    public List<Object[]> countRenewalStatusesByCc();
    public List<Object[]> countRenewalStatusesByTech();
    public List<Object[]> countRenewalStatusesByAcc();
}
