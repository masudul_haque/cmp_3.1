package com.mangoca.cmp.service;

import com.mangoca.cmp.model.UserAccount;
import java.util.List;

/**
 * The service interface for the UserAccount entity.
 */
public interface IUserAccountService {

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public UserAccount findUserAccountById(Long id) throws Exception;

    /**
     * Return all persistent instances of the <code>UserAccount</code> entity.
     */
    public List<UserAccount> findAllUserAccounts() throws Exception;

    /**
     * Make the given instance managed and persistent.
     */
    public void persistUserAccount(UserAccount empuser) throws Exception;

    /**
     * Remove the given persistent instance.
     */
    public void removeUserAccount(UserAccount empuser) throws Exception;

    /**
     * Service method for named queries
     */
    public UserAccount getUserAccountByUserName(String userName);

    public UserAccount getAuthorizedEmpuser(String userName, String password);

    public UserAccount getUserAccountByValidityNo(String validityNo) throws Exception;

    public String generateCIN(String raName);

    public UserAccount findUserAccountByUserCinAndEmail(String userCIN, String emailAccount) throws Exception;
    
    
    public List getUserAccountData(final int page, final int size);
    public long getResultSize();

    public UserAccount findUserAccountByMobileNo(String userCIN,String mobileNo);

    public UserAccount findUserAccountByCIN(String userCIN);
}