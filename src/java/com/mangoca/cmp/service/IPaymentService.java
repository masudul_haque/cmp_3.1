package com.mangoca.cmp.service;

import com.mangoca.cmp.model.Payment;
import java.util.List;

/**
 * The service interface for the Payment entity.
 */
public interface IPaymentService {
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Payment findPaymentById(Long id) throws Exception;
	/**
	 * Return all persistent instances of the <code>Payment</code> entity.
	 */
	public List<Payment> findAllPayments() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistPayment(Payment payment) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removePayment(Payment payment) throws Exception;

    public List getPaymentData(int page, int size);

    public long getResultSize();
	/**
	 * Service method for named queries
	 */
}