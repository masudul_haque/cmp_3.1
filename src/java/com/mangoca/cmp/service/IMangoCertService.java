package com.mangoca.cmp.service;

import com.mangoca.cmp.model.Mangocert;
import java.util.List;

/**
 * The service interface for the Mangocert entity.
 */
public interface IMangoCertService {
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Mangocert findMangocertById(int id);
	/**
	 * Return all persistent instances of the <code>Mangocert</code> entity.
	 */
	public List<Mangocert> findAllMangocerts() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistMangocert(Mangocert mangocert) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeMangocert(Mangocert mangocert) throws Exception;
	/**
	 * Service method for named queries
	 */
         public List getMangocertData(int page, int pageSize);
         
         public long getResultSize();
        
}