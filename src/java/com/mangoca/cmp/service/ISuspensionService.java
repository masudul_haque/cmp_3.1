package com.mangoca.cmp.service;

import com.mangoca.cmp.model.Suspension;
import java.util.List;

/**
 * The service interface for the Suspension entity.
 */
public interface ISuspensionService {
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Suspension findSuspensionById(java.lang.Long id) throws Exception;
	/**
	 * Return all persistent instances of the <code>Suspension</code> entity.
	 */
	public List<Suspension> findAllSuspensions() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistSuspension(Suspension suspension) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeSuspension(Suspension suspension) throws Exception;
	/**
	 * Service method for named queries
	 */
	
}