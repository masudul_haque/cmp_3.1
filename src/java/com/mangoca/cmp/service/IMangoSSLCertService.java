package com.mangoca.cmp.service;

import com.mangoca.cmp.model.MangoSSLCert;
import java.util.List;

/**
 * The service interface for the MangoSSLCert entity.
 */
public interface IMangoSSLCertService {

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public MangoSSLCert findMangoSSLCertById(int id);

    /**
     * Return all persistent instances of the <code>MangoSSLCert</code> entity.
     */
    public List<MangoSSLCert> findAllMangoSSLCerts() throws Exception;

    /**
     * Make the given instance managed and persistent.
     */
    public void persistMangoSSLCert(MangoSSLCert mangoSSLCert) throws Exception;

    /**
     * Remove the given persistent instance.
     */
    public void removeMangoSSLCert(MangoSSLCert mangoSSLCert) throws Exception;

    /**
     * Service method for named queries
     */
    public List getMangoSSLCertData(int page, int size);

    public int getResultSize();
}