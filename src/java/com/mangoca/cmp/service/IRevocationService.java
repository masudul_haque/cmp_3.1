package com.mangoca.cmp.service;

import com.mangoca.cmp.model.Revocation;
import java.util.List;

/**
 * The service interface for the Revocation entity.
 */
public interface IRevocationService {
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Revocation findRevocationById(java.lang.Long id) throws Exception;
	/**
	 * Return all persistent instances of the <code>Revocation</code> entity.
	 */
	public List<Revocation> findAllRevocations() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistRevocation(Revocation revocation) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeRevocation(Revocation revocation) throws Exception;
	/**
	 * Service method for named queries
	 */
	
}