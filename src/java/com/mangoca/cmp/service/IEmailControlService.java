package com.mangoca.cmp.service;

import com.mangoca.cmp.model.EmailControl;
import java.util.List;

/**
 * The service interface for the EmailControl entity.
 */
public interface IEmailControlService {
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public EmailControl findEmailControlById(java.lang.Integer id) throws Exception;
	/**
	 * Return all persistent instances of the <code>EmailControl</code> entity.
	 */
	public List<EmailControl> findAllEmailControls() throws Exception;
        
        /**
	 * Return all persistent instances of the <code>EmailControl</code> entity.
	 */
	public List<EmailControl> findAllClassesEmailControls() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistEmailControl(EmailControl emailControl) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeEmailControl(EmailControl emailControl) throws Exception;
        
        public EmailControl findEmailControlByEmailType(Short emailType) throws Exception;

    public List getEmailControlData(int page, int size);

    public long getResultSize();
	/**
	 * Service method for named queries
	 */
}