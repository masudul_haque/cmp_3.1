package com.mangoca.cmp.service;

import com.mangoca.cmp.model.Enrolleduser;
import java.util.Date;
import java.util.List;

/**
 * The service interface for the Enrolleduser entity.
 */
public interface IEnrolleduserService {

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public Enrolleduser findEnrolleduserById(Long enrollId) throws Exception;

    /**
     * Return all persistent instances of the <code>Enrolleduser</code> entity.
     */
    public List<Enrolleduser> findAllEnrolledusers() throws Exception;

    /**
     * Return all persistent instances of the <code>Enrolleduser</code> entity.
     */
    public List<Enrolleduser> findEnrolledusersByRaStatus(Short status) throws Exception;

    public List<Enrolleduser> findEnrolledusersByAccStatus(Short status) throws Exception;

    public List<Enrolleduser> findEnrolledusersByVaStatus(Short status) throws Exception;

    public List<Enrolleduser> findEnrolledusersByTechStatus(Short status) throws Exception;

    public List<Enrolleduser> findEnrolledusersByAdvanceSearch(
            String userName, String emailAccount, String mobileNo,
            String certClass, Date startDate, Date endDate);

    /**
     * Make the given instance managed and persistent.
     */
    public void persistEnrolleduser(Enrolleduser enrolleduser) throws Exception;

    /**
     * Remove the given persistent instance.
     */
    public void removeEnrolleduser(Enrolleduser enrolleduser) throws Exception;

    /**
     * Service method for named queries
     */
    public Enrolleduser getEnrolluserByUserName(String userName);

    public Enrolleduser findEnrolluserByUserCIN(String userCIN);

    public Long getNoOfEnrolledUser();

    public List findUserByRaRole();

    public List findUserByAccRole();

    public List findUserByVaRole();

    public List findUserByTechRole();

    public List getEnrolleduserData(int page, int size);

    public long getResultSize();
}