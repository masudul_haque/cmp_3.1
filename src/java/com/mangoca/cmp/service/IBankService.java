package com.mangoca.cmp.service;

import com.mangoca.cmp.model.Bank;
import java.util.List;

/**
 * The service interface for the Bank entity.
 */
public interface IBankService {
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Bank findBankById(int id) throws Exception;
	/**
	 * Return all persistent instances of the <code>Bank</code> entity.
	 */
	public List<Bank> findAllBanks() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistBank(Bank bank) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeBank(Bank bank) throws Exception;
	/**
	 * Service method for named queries
	 */
}