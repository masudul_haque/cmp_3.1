/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.service;

/**
 *
 * @author Masudul Haque
 */
public interface IEmailService {

    public void sendEmail(String toAddress, String subject, String message);
    public void sendMimeEmail(String toAddress, String subject, String message);
    public void sendEmail(String toAddress, String ccAddress, String subject, String message);

    public void sendEmail(String toAddress[], String ccAddress[], String subject, String message);
    
}
