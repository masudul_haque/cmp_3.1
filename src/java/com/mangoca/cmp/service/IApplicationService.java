/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.service;

import com.mangoca.cmp.model.Enrolleduser;
import java.util.List;

/**
 *
 * @author Masudul Haque
 */
public interface IApplicationService {

    public Integer countRevocationByStatus(Short status);

    public Integer countRenewalByStatus(Short status);

    public Integer countSuspensionByStatus(Short status);

    public List<Enrolleduser> findRevocationEnrolledusers(Short status);

    public List<Enrolleduser> findRenewalEnrolledusers(Short status);

    public List<Enrolleduser> findSuspensionEnrolledusers(Short status);
    //Revocation
    public List<Object[]> countRevokeStatusesByRa();
    public List<Object[]> countRevokeStatusesByCc();
    public List<Object[]> countRevokeStatusesByTech();
    
    
    public List findRevocationByAdminStatus(int userRole,Short status);
    //Suspension
    public List<Object[]> countSuspendStatusesByRa();
    public List<Object[]> countSuspendStatusesByCc();
    public List<Object[]> countSuspendStatusesByTech();
    
    public List findSuspensionByAdminStatus(Integer userRole, Short status);
    //Renewal
    public List<Object[]> countRenewalStatusesByRa();
    public List<Object[]> countRenewalStatusesByCc();
    public List<Object[]> countRenewalStatusesByTech();
    public List<Object[]> countRenewalStatusesByAcc();
   
    public List findRenewalByAdminStatus(Integer userRole, Short status);

}
