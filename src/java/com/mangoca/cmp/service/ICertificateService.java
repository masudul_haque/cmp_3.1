package com.mangoca.cmp.service;

import com.mangoca.cmp.model.Certificate;
import java.util.List;

/**
 * The service interface for the Certificate entity.
 */
public interface ICertificateService {
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Certificate findCertificateById(java.lang.Long id) throws Exception;
	/**
	 * Return all persistent instances of the <code>Certificate</code> entity.
	 */
	public List<Certificate> findAllCertificates() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistCertificate(Certificate certificate) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeCertificate(Certificate certificate) throws Exception;
	/**
	 * Service method for named queries
	 */
	
}