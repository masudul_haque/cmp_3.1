package com.mangoca.cmp.service;

import com.mangoca.cmp.model.Ticket;
import java.util.List;

/**
 * The service interface for the Ticket entity.
 */
public interface ITicketService {

    
    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public Ticket findTicketById(Long ticketId) throws Exception;

    /**
     * Return all persistent instances of the <code>Ticket</code> entity.
     */
    public List<Ticket> findAllTickets() throws Exception;

    public List<Ticket> findTicketsByUsername(String userName) throws Exception;

    public List<Ticket> findTicketsByStatus(String ticketStatus) throws Exception;

    /**
     * Make the given instance managed and persistent.
     */
    public void persistTicket(Ticket clientticket) throws Exception;

    /**
     * Remove the given persistent instance.
     */
    public void removeTicket(Ticket clientticket) throws Exception;

    /**
     * Service method for named queries
     */
    public Integer countTicketByStatus(Short status);
    public int countTicketByUserName(String userName);

    public List findTicketByStatus();
    public List<Ticket> getTicketDataByUsername(String userName,int page, int pageSize);
    
    public List<Ticket> getTicketDataByStatus(Short status, int page, int pageSize);
    
}