package com.mangoca.cmp.service;

import com.mangoca.cmp.model.Notification;
import java.util.List;

/**
 * The service interface for the Notification entity.
 */
public interface INotificationService {

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public Notification findNotificationById(Long id) throws Exception;

    /**
     * Return all persistent instances of the <code>Notification</code> entity.
     */
    public List<Notification> findAllNotifications() throws Exception;
    
    
    public List<Notification> findNotificationsByCIN(String userCIN) throws Exception;

    /**
     * Make the given instance managed and persistent.
     */
    public void persistNotification(Notification notificatioin) throws Exception;

    /**
     * Remove the given persistent instance.
     */
    public void removeNotification(Notification notificatioin) throws Exception;

    public List getNotificationData(int page, int size);

    public int getResultSize();
    
    
    public int countNotificationByCIN(String userCIN);
   
}