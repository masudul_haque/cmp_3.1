package com.mangoca.cmp.service;

import com.mangoca.cmp.model.Country;
import com.mangoca.cmp.model.Locality;
import java.util.List;

/**
 * The service interface for the entities: Country, Locality.
 */
public interface ICertFieldService {

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Country findCountryById(java.lang.Integer id) throws Exception;
	/**
	 * Return all persistent instances of the <code>Country</code> entity.
	 */
	public List<Country> findAllCountries() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistCountry(Country country) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeCountry(Country country) throws Exception;

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Locality findLocalityById(java.lang.Integer id) throws Exception;
	/**
	 * Return all persistent instances of the <code>Locality</code> entity.
	 */
	public List<Locality> findAllLocalities() throws Exception;
        
	public List<String> findAllLocalityNames() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistLocality(Locality locality) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeLocality(Locality locality) throws Exception;
	/**
	 * Service method for named queries
	 */
}