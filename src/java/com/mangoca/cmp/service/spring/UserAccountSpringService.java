package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IAdminAccountDao;
import com.mangoca.cmp.dao.IUserAccountDao;
import com.mangoca.cmp.helper.UserHelper;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.IUserAccountService;
import java.util.List;

/**
 * The service class for the UserAccount entity.
 */
public class UserAccountSpringService implements IUserAccountService {

    /**
     * The dao instance injected by Spring.
     */
    private IUserAccountDao dao;
    private IAdminAccountDao adminAccountDao;
    /**
     * The service Spring bean id, used in the applicationContext.xml file.
     */
    private static final String SERVICE_BEAN_ID = "UserAccountService";

    public UserAccountSpringService() {
        super();
    }

    /**
     * Returns the singleton <code>IUserAccountService</code> instance.
     */
    public static IUserAccountService getInstance() {
        return (IUserAccountService) SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
    }

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    @Override
    public UserAccount findUserAccountById(Long id) throws Exception {
        try {
            return getDao().findUserAccountById(id);
        } catch (RuntimeException e) {
            throw new Exception("findEmpuserById failed with the id " + id + ": " + e.getMessage());
        }
    }

    /**
     * Return all persistent instances of the <code>UserAccount</code> entity.
     */
    @Override
    public List<UserAccount> findAllUserAccounts() throws Exception {
        try {
            return getDao().findAllUserAccounts();
        } catch (RuntimeException e) {
            throw new Exception("findAllEmpusers failed: " + e.getMessage());
        }
    }

    /**
     * Make the given instance managed and persistent. 
     */
    @Override
    public void persistUserAccount(UserAccount empuser) throws Exception {
        try {
            getDao().persistUserAccount(empuser);
        } catch (RuntimeException e) {
            throw new Exception("persistEmpuser failed: " + e.getMessage());
        }
    }

    /**
     * Remove the given persistent instance.
     */
    @Override
    public void removeUserAccount(UserAccount empuser) throws Exception {
        try {
            getDao().removeUserAccount(empuser);
        } catch (RuntimeException e) {
            throw new Exception("removeEmpuser failed: " + e.getMessage());
        }
    }

    /**
     * Service method for named queries
     */
    /**
     * Called by Spring using the injection rules specified in 
     * the Spring beans file "applicationContext.xml".
     */
    public void setDao(IUserAccountDao dao) {
        this.dao = dao;
    }

    public IUserAccountDao getDao() {
        return this.dao;
    }


    @Override
    public UserAccount getUserAccountByUserName(String userName) {
        // TODO Auto-generated method stub
        return dao.getUserAccountByUserName(userName);
    }

    @Override
    public UserAccount getAuthorizedEmpuser(String userName, String password) {
        // TODO Auto-generated method stub
        return dao.getAuthorizedEmpuser(userName, password);
    }

    @Override
    public UserAccount getUserAccountByValidityNo(String validityNo) throws Exception {
       try {
            UserAccount userAccount=null;
            List list= getDao().findByNamedQuery("UserAccount.findByValidityNo", 
                    new String[]{"validityNo","isEmailValid"}, new Object[]{validityNo, Boolean.FALSE});
            if(list!=null && !list.isEmpty()){
             userAccount=(UserAccount) list.get(0);   
            }
            return userAccount;
        } catch (RuntimeException e) {
            throw new Exception("findEmpuserByUserCinAndEmail failed with the UserCin ");
        }
    }

    @Override
    public String generateCIN(String raName) {
        String raSymbol= adminAccountDao.getRaSymbolByName(raName);
        return UserHelper.generateCIN(dao.getLastUserId(), raSymbol);
    }

    @Override 
    public UserAccount findUserAccountByUserCinAndEmail(String userCIN, String emailAccount) throws Exception {
         
        try {
            UserAccount userAccount=null;
            List list= getDao().findByNamedQuery("UserAccount.findByUserCINAndEmail", 
                    new String[]{"userCIN","emailAccount"}, new Object[]{userCIN,emailAccount});
            if(list!=null && !list.isEmpty()){
             userAccount=(UserAccount) list.get(0);   
            }
            return userAccount;
        } catch (RuntimeException e) {
            throw new Exception("findEmpuserByUserCinAndEmail failed with the UserCin " + userCIN +" and Email "+emailAccount+": " + e.getMessage());
        }
    }

    public IAdminAccountDao getAdminAccountDao() {
        return adminAccountDao;
    }

    public void setAdminAccountDao(IAdminAccountDao adminAccountDao) {
        this.adminAccountDao = adminAccountDao;
    }

    @Override
    public List getUserAccountData(int page, int size) {
        return dao.getUserAccountData(page, size);
    }

    @Override
    public long getResultSize() {
        return dao.getResultSize();
    }

    @Override
    public UserAccount findUserAccountByMobileNo(String userCIN, String mobileNo) {
        UserAccount userAccount=null;
            List list= getDao().findByNamedQuery("UserAccount.findByCINAndMobileNo", 
                    new String[]{"userCIN","mobileNo","isPinValidate"}, new Object[]{userCIN, mobileNo, Boolean.FALSE});
            if(list!=null && !list.isEmpty()){
             userAccount=(UserAccount) list.get(0);   
            }
            return userAccount;
    }

    @Override
    public UserAccount findUserAccountByCIN(String userCIN) {
        UserAccount userAccount=null;
            List list= getDao().findByNamedQuery("UserAccount.findByUserCIN", 
                    new String[]{"userCIN"}, new Object[]{userCIN});
            if(list!=null && !list.isEmpty()){
             userAccount=(UserAccount) list.get(0);   
            }
            return userAccount;
    }
    
    
}