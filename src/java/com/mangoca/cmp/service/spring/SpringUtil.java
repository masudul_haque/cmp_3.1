/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.service.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 * @author Masudul Haque
 */
public class SpringUtil implements ApplicationContextAware {
    private static ApplicationContext applicationContext = null;

    public static Object getSpringBean(String beanName) {
        return applicationContext.getBean(beanName);
    }
    @Override
    public void setApplicationContext(ApplicationContext appContext)
        throws BeansException {
        applicationContext = appContext;
   }
}
