package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IEnrolleduserDao;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.service.IEnrolleduserService;
import java.util.Date;
import java.util.List;

/**
 * The service class for the Enrolleduser entity.
 */
public class EnrolleduserSpringService implements IEnrolleduserService {

    /**
     * The dao instance injected by Spring.
     */
    private IEnrolleduserDao dao;
    /**
     * The service Spring bean id, used in the applicationContext.xml file.
     */
    private static final String SERVICE_BEAN_ID = "EnrolleduserService";

    public EnrolleduserSpringService() {
        super();
    }

    /**
     * Returns the singleton <code>IEnrolleduserService</code> instance.
     */
    public static IEnrolleduserService getInstance() {
        return (IEnrolleduserService) SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
    }

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    @Override
    public Enrolleduser findEnrolleduserById(Long enrollId) throws Exception {
        try {
            return getDao().findEnrolleduserById(enrollId);
        } catch (RuntimeException e) {
            throw new Exception("findEnrolleduserById failed with the id " + enrollId + ": " + e.getMessage());
        }
    }

    /**
     * Return all persistent instances of the <code>Enrolleduser</code> entity.
     */
    @Override
    public List<Enrolleduser> findAllEnrolledusers() throws Exception {
        try {
            return getDao().findAllEnrolledusers();
        } catch (RuntimeException e) {
            throw new Exception("findAllEnrolledusers failed: " + e.getMessage());
        }
    }

    /**
     * Make the given instance managed and persistent.
     */
    @Override
    public void persistEnrolleduser(Enrolleduser enrolleduser) throws Exception {
        try {
            getDao().persistEnrolleduser(enrolleduser);
        } catch (RuntimeException e) {
            throw new Exception("persistEnrolleduser failed: " + e.getMessage());
        }
    }

    /**
     * Remove the given persistent instance.
     */
    @Override
    public void removeEnrolleduser(Enrolleduser enrolleduser) throws Exception {
        try {
            getDao().removeEnrolleduser(enrolleduser);
        } catch (RuntimeException e) {
            throw new Exception("removeEnrolleduser failed: " + e.getMessage());
        }
    }

    /**
     * Service method for named queries
     */
    /**
     * Called by Spring using the injection rules specified in 
     * the Spring beans file "applicationContext.xml".
     */
    public void setDao(IEnrolleduserDao dao) {
        this.dao = dao;
    }

    public IEnrolleduserDao getDao() {
        return this.dao;
    }

    @Override
    public Enrolleduser getEnrolluserByUserName(String userName) {
      //  return dao.getEnrolluserByUserName(userName);
        Enrolleduser enrolleduser = null;
        List list = getDao().findByNamedQuery("Enrolleduser.findByUserName", new String[]{"userName","status"},
                new Object[]{userName,Enrolleduser.ACTIVE});
        if (!list.isEmpty()) {
            enrolleduser = (Enrolleduser) list.get(0);
        }
        return enrolleduser;
    }

    @Override
    public Long getNoOfEnrolledUser() {
        return dao.getNoOfEnrolledUser();
    }

    @Override
    public List findUserByRaRole() {
        return dao.findUserByRaRole();
    }

    @Override
    public List findUserByAccRole() {
        return dao.findUserByAccRole();
    }

    @Override
    public List findUserByVaRole() {
        return dao.findUserByVaRole();
    }

    @Override
    public List findUserByTechRole() {
        return dao.findUserByTechRole();
    }

    @Override
    public List<Enrolleduser> findEnrolledusersByRaStatus(Short status) throws Exception {
        try {
            return getDao().findByNamedQuery("Enrolleduser.findByRaStatus", new String[]{"status"},
                    new Object[]{status});
        } catch (RuntimeException e) {
            throw new Exception("findAllEnrolledusers failed: " + e.getMessage());
        }
    }

    @Override
    public List<Enrolleduser> findEnrolledusersByAccStatus(Short status) throws Exception {
        try {
            return getDao().findByNamedQuery("Enrolleduser.findByAccStatus", new String[]{"status"},
                    new Object[]{status});
        } catch (RuntimeException e) {
            throw new Exception("findAllEnrolledusers failed: " + e.getMessage());
        }
    }

    @Override
    public List<Enrolleduser> findEnrolledusersByVaStatus(Short status) throws Exception {
        try {
            return getDao().findByNamedQuery("Enrolleduser.findByVaStatus", new String[]{"status"},
                    new Object[]{status});
        } catch (RuntimeException e) {
            throw new Exception("findAllEnrolledusers failed: " + e.getMessage());
        }
    }

    @Override
    public List<Enrolleduser> findEnrolledusersByTechStatus(Short status) throws Exception {
        try {
            return getDao().findByNamedQuery("Enrolleduser.findByTechStatus", new String[]{"status"},
                    new Object[]{status});
        } catch (RuntimeException e) {
            throw new Exception("findAllEnrolledusers failed: " + e.getMessage());
        }
    }

    /*    @Override
    public List<Enrolleduser> findRevocationEnrolledusers(String status) throws Exception {
    try {
    return getDao().findByNamedQuery("Enrolleduser.findRevocationByStatus", new String[]{"status"}, 
    new Object[]{status});
    } catch (RuntimeException e) {
    throw new Exception("findAllEnrolledusers failed: " + e.getMessage());
    } 
    }
    
    @Override
    public List<Enrolleduser> findRenewalEnrolledusers(String status) throws Exception {
    try {
    return getDao().findByNamedQuery("Enrolleduser.findRenewalByStatus", new String[]{"status"}, 
    new Object[]{status});
    } catch (RuntimeException e) {
    throw new Exception("findAllEnrolledusers failed: " + e.getMessage());
    } 
    }
    
    @Override
    public List<Enrolleduser> findSuspensionEnrolledusers(String status) throws Exception {
    try {
    return getDao().findByNamedQuery("Enrolleduser.findSuspensionByStatus", new String[]{"status"}, 
    new Object[]{status});
    } catch (RuntimeException e) {
    throw new Exception("findAllEnrolledusers failed: " + e.getMessage());
    } 
    }
     */
    @Override
    public Enrolleduser findEnrolluserByUserCIN(String userCIN) {
        Enrolleduser enrolleduser = null;
        List list = getDao().findByNamedQuery("Enrolleduser.findByUserCIN", new String[]{"userCIN","status"},
                new Object[]{userCIN,Enrolleduser.ACTIVE});
        if (!list.isEmpty()) {
            enrolleduser = (Enrolleduser) list.get(0);
        }
        return enrolleduser;
    }

    @Override
    public List<Enrolleduser> findEnrolledusersByAdvanceSearch(String userName, String emailAccount, String mobileNo,
                               String certClass, Date startDate, Date endDate) {
        return dao.findEnrolledusersByAdvanceSearch(userName, emailAccount, mobileNo,
                certClass, startDate, endDate);
    }

    @Override
    public List getEnrolleduserData(int page, int size) {
        return dao.getEnrolleduserData( page, size);
    }

    @Override
    public long getResultSize() {
        return dao.getResultSize();
    }
}