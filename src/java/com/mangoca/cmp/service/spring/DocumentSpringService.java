package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IDocumentDao;
import com.mangoca.cmp.model.Document;
import com.mangoca.cmp.service.IDocumentService;
import java.util.List;

/**
 * The service class for the Document entity.
 */
public class DocumentSpringService implements IDocumentService {

    
	/**
	 * The dao instance injected by Spring.
	 */
	private IDocumentDao dao;
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "DocumentService";
	
	public DocumentSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>IDocumentService</code> instance.
	 */
	public static IDocumentService getInstance() {
		return (IDocumentService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Document findDocumentById(Long documentId) throws Exception {
		try {
			return getDao().findDocumentById(documentId);
		} catch (RuntimeException e) {
			throw new Exception("findDocumentById failed with the id " + documentId + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>Document</code> entity.
	 */
    @Override
	public List<Document> findAllDocuments() throws Exception {
		try {
			return getDao().findAllDocuments();
		} catch (RuntimeException e) {
			throw new Exception("findAllDocuments failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistDocument(Document document) throws Exception {
		try {
			getDao().persistDocument(document);
		} catch (RuntimeException e) {
			throw new Exception("persistDocument failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeDocument(Document document) throws Exception {
		try {
			getDao().removeDocument(document);
		} catch (RuntimeException e) {
			throw new Exception("removeDocument failed: " + e.getMessage());
		}
	}

	/**
	 * Service method for named queries
	 */

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(IDocumentDao dao) {
		this.dao = dao;
	}
	public IDocumentDao getDao() {
		return this.dao;
	}

    @Override
    public long getResultSize() {
        return dao.getResultSize();
    }

    @Override
    public List getDocumentData(int page, int size) {
        return dao.getDocumentData( page, size);
    }
}