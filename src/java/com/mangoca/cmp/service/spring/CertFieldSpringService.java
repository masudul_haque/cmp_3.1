package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.ICertFieldDao;
import com.mangoca.cmp.model.Country;
import com.mangoca.cmp.model.Locality;
import com.mangoca.cmp.service.ICertFieldService;
import java.util.List;

/**
 * The service class for the entities: Country, Locality.
 */
public class CertFieldSpringService implements ICertFieldService {
	/**
	 * The dao instance injected by Spring.
	 */
	private ICertFieldDao dao;
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "CertFieldService";
	
	public CertFieldSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>ICertFieldService</code> instance.
	 */
	public static ICertFieldService getInstance() {
		return (ICertFieldService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
	}

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Country findCountryById(java.lang.Integer id) throws Exception {
		try {
			return getDao().findCountryById(id);
		} catch (RuntimeException e) {
			throw new Exception("findCountryById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>Country</code> entity.
	 */
    @Override
	public List<Country> findAllCountries() throws Exception {
		try {
			return getDao().findAllCountries();
		} catch (RuntimeException e) {
			throw new Exception("findAllCountries failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistCountry(Country country) throws Exception {
		try {
			getDao().persistCountry(country);
		} catch (RuntimeException e) {
			throw new Exception("persistCountry failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeCountry(Country country) throws Exception {
		try {
			getDao().removeCountry(country);
		} catch (RuntimeException e) {
			throw new Exception("removeCountry failed: " + e.getMessage());
		}
	}

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Locality findLocalityById(java.lang.Integer id) throws Exception {
		try {
			return getDao().findLocalityById(id);
		} catch (RuntimeException e) {
			throw new Exception("findLocalityById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>Locality</code> entity.
	 */
    @Override
	public List<Locality> findAllLocalities() throws Exception {
		try {
			return getDao().findAllLocalities();
		} catch (RuntimeException e) {
			throw new Exception("findAllLocalities failed: " + e.getMessage());
		}
	}
        
        
    @Override
	public List<String> findAllLocalityNames() throws Exception{
            try {
			return getDao().findAllLocalityNames();
		} catch (RuntimeException e) {
			throw new Exception("findAllLocalities failed: " + e.getMessage());
		}
        }
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistLocality(Locality locality) throws Exception {
		try {
			getDao().persistLocality(locality);
		} catch (RuntimeException e) {
			throw new Exception("persistLocality failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeLocality(Locality locality) throws Exception {
		try {
			getDao().removeLocality(locality);
		} catch (RuntimeException e) {
			throw new Exception("removeLocality failed: " + e.getMessage());
		}
	}

	/**
	 * Service method for named queries
	 */

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(ICertFieldDao dao) {
		this.dao = dao;
	}
	public ICertFieldDao getDao() {
		return this.dao;
	}
}