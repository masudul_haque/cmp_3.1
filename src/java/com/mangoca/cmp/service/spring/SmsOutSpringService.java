package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.ISmsOutDao;
import com.mangoca.cmp.model.SmsOut;
import com.mangoca.cmp.service.ISmsOutService;
import java.util.List;

/**
 * The service class for the SmsOut entity.
 */
public class SmsOutSpringService implements ISmsOutService {
	/**
	 * The dao instance injected by Spring.
	 */
	private ISmsOutDao dao;
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "SmsOutService";
	
	public SmsOutSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>ISmsOutService</code> instance.
	 */
	public static ISmsOutService getInstance() {
		return (ISmsOutService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public SmsOut findSmsOutById(int id) throws Exception {
		try {
			return getDao().findSmsOutById(id);
		} catch (RuntimeException e) {
			throw new Exception("findSmsOutById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>SmsOut</code> entity.
	 */
    @Override
	public List<SmsOut> findAllSmsOuts() throws Exception {
		try {
			return getDao().findAllSmsOuts();
		} catch (RuntimeException e) {
			throw new Exception("findAllSmsOuts failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistSmsOut(SmsOut smsOut) throws Exception {
		try {
			getDao().persistSmsOut(smsOut);
		} catch (RuntimeException e) {
			throw new Exception("persistSmsOut failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeSmsOut(SmsOut smsOut) throws Exception {
		try {
			getDao().removeSmsOut(smsOut);
		} catch (RuntimeException e) {
			throw new Exception("removeSmsOut failed: " + e.getMessage());
		}
	}

	/**
	 * Service method for named queries
	 */

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(ISmsOutDao dao) {
		this.dao = dao;
	}
	public ISmsOutDao getDao() {
		return this.dao;
	}

    @Override
    public List getSmsOutData(int page, int size) {
        return dao.getSmsOutData(page,size);
    }

    @Override
    public long getResultSize() {
        return dao.getResultSize();
    }
}