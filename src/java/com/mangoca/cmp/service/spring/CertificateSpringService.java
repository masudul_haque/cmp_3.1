package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.ICertificateDao;
import com.mangoca.cmp.model.Certificate;
import com.mangoca.cmp.service.ICertificateService;
import java.util.List;

/**
 * The service class for the Certificate entity.
 */
public class CertificateSpringService implements ICertificateService {
	/**
	 * The dao instance injected by Spring.
	 */
	private ICertificateDao dao;
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "CertificateService";
	
	public CertificateSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>ICertificateService</code> instance.
	 */
	public static ICertificateService getInstance() {
		return (ICertificateService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Certificate findCertificateById(java.lang.Long id) throws Exception {
		try {
			return getDao().findCertificateById(id);
		} catch (RuntimeException e) {
			throw new Exception("findCertificateById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>Certificate</code> entity.
	 */
    @Override
	public List<Certificate> findAllCertificates() throws Exception {
		try {
			return getDao().findAllCertificates();
		} catch (RuntimeException e) {
			throw new Exception("findAllCertificates failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistCertificate(Certificate certificate) throws Exception {
		try {
			getDao().persistCertificate(certificate);
		} catch (RuntimeException e) {
			throw new Exception("persistCertificate failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeCertificate(Certificate certificate) throws Exception {
		try {
			getDao().removeCertificate(certificate);
		} catch (RuntimeException e) {
			throw new Exception("removeCertificate failed: " + e.getMessage());
		}
	}

	

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(ICertificateDao dao) {
		this.dao = dao;
	}
	public ICertificateDao getDao() {
		return this.dao;
	}
}