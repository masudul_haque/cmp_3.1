package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IUserDao;
import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.IUserService;
import java.util.List;

/**
 * The service class for the AdminAccount entity.
 */
public class UserSpringService implements IUserService {
	/**
	 * The dao instance injected by Spring.
	 */
	private IUserDao dao;
        
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "UserService";
	
	public UserSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>IUserService</code> instance.
	 */
	public static IUserService getInstance() {
		return (IUserService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public User findUserById(Long id) throws Exception {
		try {
			return getDao().findUserById(id);
		} catch (RuntimeException e) {
			throw new Exception("findUserById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>AdminAccount</code> entity.
	 */
    @Override
	public List<User> findAllUsers() throws Exception {
		try {
			return getDao().findAllUsers();
		} catch (RuntimeException e) {
			throw new Exception("findAllUsers failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistUser(User user) throws Exception {
		try {
			getDao().persistUser(user);
		} catch (RuntimeException e) {
			throw new Exception("persistUser failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeUser(User user) throws Exception {
		try {
			getDao().removeUser(user);
		} catch (RuntimeException e) {
			throw new Exception("removeUser failed: " + e.getMessage());
		}
	}

	/**
	 * Service method for named queries
	 */

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(IUserDao dao) {
		this.dao = dao;
	}
	public IUserDao getDao() {
		return this.dao;
	}
	@Override
	public List findAllRaUser() {
		// TODO Auto-generated method stub
                String queryName="User.findByUserRole";
                
		return dao.findByNamedQuery(queryName, new String[]{"userRole"}, new String[]{Constants.ADMIN_RA});
	}
	@Override
	public User getUserByUserName(String userName) {
		return dao.getUserByUserName(userName);
	}
        
    @Override
        public List findUsersByUserRole(){
             return dao.findUsersByUserRole();
        }
}