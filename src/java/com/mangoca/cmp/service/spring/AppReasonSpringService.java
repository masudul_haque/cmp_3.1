package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IAppReasonDao;
import com.mangoca.cmp.model.RevokeReason;
import com.mangoca.cmp.model.SuspensionReason;
import com.mangoca.cmp.service.IAppReasonService;
import java.util.List;

/**
 * The service class for the entities: RevokeReason, SuspensionReason.
 */
public class AppReasonSpringService implements IAppReasonService {
	/**
	 * The dao instance injected by Spring.
	 */
	private IAppReasonDao dao;
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "AppReasonService";
	
	public AppReasonSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>IAppReasonService</code> instance.
	 */
	public static IAppReasonService getInstance() {
		return (IAppReasonService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
	}

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public RevokeReason findRevokeReasonById(int id) throws Exception {
		try {
			return getDao().findRevokeReasonById(id);
		} catch (RuntimeException e) {
			throw new Exception("findRevokeReasonById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>RevokeReason</code> entity.
	 */
    @Override
	public List<RevokeReason> findAllRevokeReasons() throws Exception {
		try {
			return getDao().findAllRevokeReasons();
		} catch (RuntimeException e) {
			throw new Exception("findAllRevokeReasons failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistRevokeReason(RevokeReason revokeReason) throws Exception {
		try {
			getDao().persistRevokeReason(revokeReason);
		} catch (RuntimeException e) {
			throw new Exception("persistRevokeReason failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeRevokeReason(RevokeReason revokeReason) throws Exception {
		try {
			getDao().removeRevokeReason(revokeReason);
		} catch (RuntimeException e) {
			throw new Exception("removeRevokeReason failed: " + e.getMessage());
		}
	}

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public SuspensionReason findSuspensionReasonById(int id) throws Exception {
		try {
			return getDao().findSuspensionReasonById(id);
		} catch (RuntimeException e) {
			throw new Exception("findSuspensionReasonById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>SuspensionReason</code> entity.
	 */
    @Override
	public List<SuspensionReason> findAllSuspensionReasons() throws Exception {
		try {
			return getDao().findAllSuspensionReasons();
		} catch (RuntimeException e) {
			throw new Exception("findAllSuspensionReasons failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistSuspensionReason(SuspensionReason suspensionReason) throws Exception {
		try {
			getDao().persistSuspensionReason(suspensionReason);
		} catch (RuntimeException e) {
			throw new Exception("persistSuspensionReason failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeSuspensionReason(SuspensionReason suspensionReason) throws Exception {
		try {
			getDao().removeSuspensionReason(suspensionReason);
		} catch (RuntimeException e) {
			throw new Exception("removeSuspensionReason failed: " + e.getMessage());
		}
	}

	/**
	 * Service method for named queries
	 */

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(IAppReasonDao dao) {
		this.dao = dao;
	}
	public IAppReasonDao getDao() {
		return this.dao;
	}
}