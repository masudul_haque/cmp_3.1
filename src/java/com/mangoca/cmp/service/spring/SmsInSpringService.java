package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.ISmsInDao;
import com.mangoca.cmp.model.SmsIn;
import com.mangoca.cmp.service.ISmsInService;
import java.util.List;

/**
 * The service class for the SmsIn entity.
 */
public class SmsInSpringService implements ISmsInService {

    /**
     * The dao instance injected by Spring.
     */
    private ISmsInDao dao;
    /**
     * The service Spring bean id, used in the applicationContext.xml file.
     */
    private static final String SERVICE_BEAN_ID = "SmsInService";

    public SmsInSpringService() {
        super();
    }

    /**
     * Returns the singleton <code>ISmsInService</code> instance.
     */
    public static ISmsInService getInstance() {
        return (ISmsInService) SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
    }

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    @Override
    public SmsIn findSmsInById(int id) throws Exception {
        try {
            return getDao().findSmsInById(id);
        } catch (RuntimeException e) {
            throw new Exception("findSmsInById failed with the id " + id + ": " + e.getMessage());
        }
    }

    /**
     * Return all persistent instances of the <code>SmsIn</code> entity.
     */
    @Override
    public List<SmsIn> findAllSmsIns() throws Exception {
        try {
            return getDao().findAllSmsIns();
        } catch (RuntimeException e) {
            throw new Exception("findAllSmsIns failed: " + e.getMessage());
        }
    }

    /**
     * Make the given instance managed and persistent.
     */
    @Override
    public void persistSmsIn(SmsIn smsIn) throws Exception {
        try {
            getDao().persistSmsIn(smsIn);
        } catch (RuntimeException e) {
            throw new Exception("persistSmsIn failed: " + e.getMessage());
        }
    }

    /**
     * Remove the given persistent instance.
     */
    @Override
    public void removeSmsIn(SmsIn smsIn) throws Exception {
        try {
            getDao().removeSmsIn(smsIn);
        } catch (RuntimeException e) {
            throw new Exception("removeSmsIn failed: " + e.getMessage());
        }
    }

    /**
     * Service method for named queries
     */
    /**
     * Called by Spring using the injection rules specified in 
     * the Spring beans file "applicationContext.xml".
     */
    public void setDao(ISmsInDao dao) {
        this.dao = dao;
    }

    public ISmsInDao getDao() {
        return this.dao;
    }

    @Override
    public List getSmsInData(int page, int size) {
        return dao.getSmsInData(page, size);
    }

    @Override
    public long getResultSize() {
        return dao.getResultSize();
    }
}