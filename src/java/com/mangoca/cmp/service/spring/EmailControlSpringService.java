package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IEmailControlDao;
import com.mangoca.cmp.model.EmailControl;
import com.mangoca.cmp.service.IEmailControlService;
import java.util.List;

/**
 * The service class for the EmailControl entity.
 */
public class EmailControlSpringService implements IEmailControlService {

    /**
     * The dao instance injected by Spring.
     */
    private IEmailControlDao dao;
    /**
     * The service Spring bean id, used in the applicationContext.xml file.
     */
    private static final String SERVICE_BEAN_ID = "EmailControlService";

    public EmailControlSpringService() {
        super();
    }

    /**
     * Returns the singleton <code>IEmailControlService</code> instance.
     */
    public static IEmailControlService getInstance() {
        return (IEmailControlService) SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
    }

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    @Override
    public EmailControl findEmailControlById(java.lang.Integer id) throws Exception {
        try {
            return getDao().findEmailControlById(id);
        } catch (RuntimeException e) {
            throw new Exception("findEmailControlById failed with the id " + id + ": " + e.getMessage());
        }
    }

    /**
     * Return all persistent instances of the <code>EmailControl</code> entity.
     */
    @Override
    public List<EmailControl> findAllEmailControls() throws Exception {
        try {
            return getDao().findAllEmailControls();
        } catch (RuntimeException e) {
            throw new Exception("findAllEmailControls failed: " + e.getMessage());
        }
    }

    /**
     * Make the given instance managed and persistent.
     */
    @Override
    public void persistEmailControl(EmailControl emailControl) throws Exception {
        try {
            getDao().persistEmailControl(emailControl);
        } catch (RuntimeException e) {
            throw new Exception("persistEmailControl failed: " + e.getMessage());
        }
    }

    /**
     * Remove the given persistent instance.
     */
    @Override
    public void removeEmailControl(EmailControl emailControl) throws Exception {
        try {
            getDao().removeEmailControl(emailControl);
        } catch (RuntimeException e) {
            throw new Exception("removeEmailControl failed: " + e.getMessage());
        }
    }

    /**
     * Service method for named queries
     */
    /**
     * Called by Spring using the injection rules specified in 
     * the Spring beans file "applicationContext.xml".
     */
    public void setDao(IEmailControlDao dao) {
        this.dao = dao;
    }

    public IEmailControlDao getDao() {
        return this.dao;
    }

    @Override
    public List getEmailControlData(int page, int size) {
        return dao.getEmailControlData(page, size);
    }

    @Override
    public long getResultSize() {
        return dao.getResultSize();
    }

    @Override
    public EmailControl findEmailControlByEmailType(Short emailType) throws Exception {
        try {
            EmailControl emailControl = null;
            List list = getDao().findByNamedQuery("EmailControl.findByEmailType",
                    new String[]{"emailType"}, new Object[]{emailType});
            if (list != null && !list.isEmpty()) {
                emailControl = (EmailControl) list.get(0);
            }
            return emailControl;
        } catch (RuntimeException e) {
            throw new Exception("findEmailControlByEmailType failed with the UserCin ");
        }
    }

    @Override
    public List<EmailControl> findAllClassesEmailControls() throws Exception {
        try {
            return getDao().findByNamedQuery("EmailControl.findAllClassesEmailControls");
        } catch (RuntimeException e) {
            throw new Exception("EmailControl.findAllClassesEmailControls failed: " + e.getMessage());
        }
    }
}