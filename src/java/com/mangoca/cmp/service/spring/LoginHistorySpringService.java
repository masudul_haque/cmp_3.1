package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.ILoginHistoryDao;
import com.mangoca.cmp.model.LoginHistoryLog;
import com.mangoca.cmp.service.ILoginHistoryService;
import java.util.List;

/**
 * The service class for the LoginHistoryLog entity.
 */
public class LoginHistorySpringService implements ILoginHistoryService {
	/**
	 * The dao instance injected by Spring.
	 */
	private ILoginHistoryDao dao;
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "LoginHistoryService";
	
	public LoginHistorySpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>ILoginHistoryService</code> instance.
	 */
	public static ILoginHistoryService getInstance() {
		return (ILoginHistoryService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public LoginHistoryLog findLoginHistoryLogById(java.lang.Integer id) throws Exception {
		try {
			return getDao().findLoginHistoryLogById(id);
		} catch (RuntimeException e) {
			throw new Exception("findLoginHistoryLogById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>LoginHistoryLog</code> entity.
	 */
    @Override
	public List<LoginHistoryLog> findAllLoginHistoryLogs() throws Exception {
		try {
			return getDao().findAllLoginHistoryLogs();
		} catch (RuntimeException e) {
			throw new Exception("findAllLoginHistoryLogs failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistLoginHistoryLog(LoginHistoryLog loginHistoryLog) throws Exception {
		try {
			getDao().persistLoginHistoryLog(loginHistoryLog);
		} catch (RuntimeException e) {
			throw new Exception("persistLoginHistoryLog failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeLoginHistoryLog(LoginHistoryLog loginHistoryLog) throws Exception {
		try {
			getDao().removeLoginHistoryLog(loginHistoryLog);
		} catch (RuntimeException e) {
			throw new Exception("removeLoginHistoryLog failed: " + e.getMessage());
		}
	}

	/**
	 * Service method for named queries
	 */

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(ILoginHistoryDao dao) {
		this.dao = dao;
	}
	public ILoginHistoryDao getDao() {
		return this.dao;
	}
}