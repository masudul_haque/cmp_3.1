package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IMangoCertDao;
import com.mangoca.cmp.model.Mangocert;
import com.mangoca.cmp.service.IMangoCertService;
import java.util.List;

/**
 * The service class for the Mangocert entity.
 */
public class MangoCertSpringService implements IMangoCertService {

    /**
     * The dao instance injected by Spring.
     */
    private IMangoCertDao dao;
    /**
     * The service Spring bean id, used in the applicationContext.xml file.
     */
    private static final String SERVICE_BEAN_ID = "MangoCertService";

    public MangoCertSpringService() {
        super();
    }

    /**
     * Returns the singleton <code>IMangoCertService</code> instance.
     */
    public static IMangoCertService getInstance() {
        return (IMangoCertService) SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
    }

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    @Override
    public Mangocert findMangocertById(int id) {
        return getDao().findMangocertById(id);
    }

    /**
     * Return all persistent instances of the <code>Mangocert</code> entity.
     */
    @Override
    public List<Mangocert> findAllMangocerts() throws Exception {
        try {
            return getDao().findAllMangocerts();
        } catch (RuntimeException e) {
            throw new Exception("findAllMangocerts failed: " + e.getMessage());
        }
    }

    /**
     * Make the given instance managed and persistent.
     */
    @Override
    public void persistMangocert(Mangocert mangocert) throws Exception {
        try {
            getDao().persistMangocert(mangocert);
        } catch (RuntimeException e) {
            throw new Exception("persistMangocert failed: " + e.getMessage());
        }
    }

    /**
     * Remove the given persistent instance.
     */
    @Override
    public void removeMangocert(Mangocert mangocert) throws Exception {
        try {
            getDao().removeMangocert(mangocert);
        } catch (RuntimeException e) {
            throw new Exception("removeMangocert failed: " + e.getMessage());
        }
    }

    /**
     * Service method for named queries
     */
    /**
     * Called by Spring using the injection rules specified in 
     * the Spring beans file "applicationContext.xml".
     */
    public void setDao(IMangoCertDao dao) {
        this.dao = dao;
    }

    public IMangoCertDao getDao() {
        return this.dao;
    }

    @Override
    public List getMangocertData(int page, int pageSize) {
        return dao.getMangocertData(page, pageSize);
    }

    @Override
    public long getResultSize() {
        return dao.getResultSize();
    }
}