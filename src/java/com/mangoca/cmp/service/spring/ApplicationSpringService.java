/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IApplicationDao;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.IApplicationService;
import java.util.List;

/**
 *
 * @author Masudul Haque
 */
public class ApplicationSpringService implements IApplicationService {

    private IApplicationDao dao;
    /**
     * The service Spring bean id, used in the applicationContext.xml file.
     */
    private static final String SERVICE_BEAN_ID = "ApplicationService";

    public ApplicationSpringService() {
        super();
    }

    /**
     * Returns the singleton <code>IBankService</code> instance.
     */
    public static IApplicationService getInstance() {
        return (IApplicationService) SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
    }

    public IApplicationDao getDao() {
        return dao;
    }

    public void setDao(IApplicationDao dao) {
        this.dao = dao;
    }

    

    @Override
    public Integer countRevocationByStatus(Short status) {
        return getDao().countByNamedQuery("Enrolleduser.countRevocationByStatus", "status",
                status);
    }

    @Override
    public Integer countRenewalByStatus(Short status) { 
        return getDao().countByNamedQuery("Enrolleduser.countRenewalByStatus", "status",
                status);
    }

    @Override
    public Integer countSuspensionByStatus(Short status) {
       return getDao().countByNamedQuery("Enrolleduser.countSuspensionByStatus", "status",
                status);
    }

    @Override
    public List<Enrolleduser> findRevocationEnrolledusers(Short status) {
        return dao.findRevocationEnrolledusers(status);
    }

    @Override
    public List<Enrolleduser> findRenewalEnrolledusers(Short status) {
        return dao.findRenewalEnrolledusers(status);
    }

    @Override
    public List<Enrolleduser> findSuspensionEnrolledusers(Short status) {
        return dao.findSuspensionEnrolledusers(status);
    }

    @Override
    public List<Object[]> countRevokeStatusesByRa() {
        return dao.countRevokeStatusesByRa();
    }
    @Override
    public List findRevocationByAdminStatus(int userRole,Short status){
        List result=null;
        if(userRole==User.ADMIN_RA){
            result= dao.findByNamedQuery("Revocation.findByRaStatus", new String[]{"status"}, new Object[]{status});
        }
        if(userRole==User.ADMIN_CC){
            result= dao.findByNamedQuery("Revocation.findByCcStatus", new String[]{"status"}, new Object[]{status});
        }
        if(userRole==User.ADMIN_TECH){
            result= dao.findByNamedQuery("Revocation.findByTechStatus", new String[]{"status"}, new Object[]{status});
        }
        return result;
    }
    @Override
    public List findSuspensionByAdminStatus(Integer userRole, Short status) {
         List result=null;
        if(userRole==User.ADMIN_RA){
            result= dao.findByNamedQuery("Suspension.findByRaStatus", new String[]{"status"}, new Object[]{status});
        }
        if(userRole==User.ADMIN_CC){
            result= dao.findByNamedQuery("Suspension.findByCcStatus", new String[]{"status"}, new Object[]{status});
        }
        if(userRole==User.ADMIN_TECH){
            result= dao.findByNamedQuery("Suspension.findByTechStatus", new String[]{"status"}, new Object[]{status});
        }
        return result;
    }
    
    @Override
    public List findRenewalByAdminStatus(Integer userRole, Short status) {
        List result=null;
        if(userRole==User.ADMIN_RA){
            result= dao.findByNamedQuery("Renewal.findByRaStatus", new String[]{"status"}, new Object[]{status});
        }
        if(userRole==User.ADMIN_CC){
            result= dao.findByNamedQuery("Renewal.findByCcStatus", new String[]{"status"}, new Object[]{status});
        }
        if(userRole==User.ADMIN_TECH){
            result= dao.findByNamedQuery("Renewal.findByTechStatus", new String[]{"status"}, new Object[]{status});
        }
        return result;
    }
    
    @Override
    public List<Object[]> countRevokeStatusesByCc() {
        return dao.countRevokeStatusesByCc();
    }

    @Override
    public List<Object[]> countRevokeStatusesByTech() {
        return dao.countRevokeStatusesByTech();
    }

    @Override
    public List<Object[]> countSuspendStatusesByRa() {
        return dao.countSuspendStatusesByRa();
    }

    @Override
    public List<Object[]> countSuspendStatusesByCc() {
        return dao.countSuspendStatusesByCc();
    }

    @Override
    public List<Object[]> countSuspendStatusesByTech() {
        return dao.countSuspendStatusesByTech();
    }

    @Override
    public List<Object[]> countRenewalStatusesByRa() {
        return dao.countRenewalStatusesByRa();
    }

    @Override
    public List<Object[]> countRenewalStatusesByCc() {
        return dao.countRenewalStatusesByCc();
    }

    @Override
    public List<Object[]> countRenewalStatusesByTech() {
        return dao.countRenewalStatusesByTech();
    }

    @Override
    public List<Object[]> countRenewalStatusesByAcc() {
        return dao.countRenewalStatusesByAcc();
    }

    

    
}
