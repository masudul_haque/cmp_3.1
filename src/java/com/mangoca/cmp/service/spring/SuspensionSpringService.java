package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.ISuspensionDao;
import com.mangoca.cmp.model.Suspension;
import com.mangoca.cmp.service.ISuspensionService;
import java.util.List;

/**
 * The service class for the Suspension entity.
 */
public class SuspensionSpringService implements ISuspensionService {
	/**
	 * The dao instance injected by Spring.
	 */
	private ISuspensionDao dao;
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "SuspensionService";
	
	public SuspensionSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>ISuspensionService</code> instance.
	 */
	public static ISuspensionService getInstance() {
		return (ISuspensionService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Suspension findSuspensionById(java.lang.Long id) throws Exception {
		try {
			return getDao().findSuspensionById(id);
		} catch (RuntimeException e) {
			throw new Exception("findSuspensionById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>Suspension</code> entity.
	 */
    @Override
	public List<Suspension> findAllSuspensions() throws Exception {
		try {
			return getDao().findAllSuspensions();
		} catch (RuntimeException e) {
			throw new Exception("findAllSuspensions failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistSuspension(Suspension suspension) throws Exception {
		try {
			getDao().persistSuspension(suspension);
		} catch (RuntimeException e) {
			throw new Exception("persistSuspension failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeSuspension(Suspension suspension) throws Exception {
		try {
			getDao().removeSuspension(suspension);
		} catch (RuntimeException e) {
			throw new Exception("removeSuspension failed: " + e.getMessage());
		}
	}

	

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(ISuspensionDao dao) {
		this.dao = dao;
	}
	public ISuspensionDao getDao() {
		return this.dao;
	}
}