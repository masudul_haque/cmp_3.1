package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IPaymentDao;
import com.mangoca.cmp.model.Payment;
import com.mangoca.cmp.service.IPaymentService;
import java.util.List;

/**
 * The service class for the Payment entity.
 */
public class PaymentSpringService implements IPaymentService {
	/**
	 * The dao instance injected by Spring.
	 */
	private IPaymentDao dao;
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "PaymentService";
	
	public PaymentSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>IPaymentService</code> instance.
	 */
	public static IPaymentService getInstance() {
		return (IPaymentService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Payment findPaymentById(Long id) throws Exception {
		try {
			return getDao().findPaymentById(id);
		} catch (RuntimeException e) {
			throw new Exception("findPaymentById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>Payment</code> entity.
	 */
    @Override
	public List<Payment> findAllPayments() throws Exception {
		try {
			return getDao().findAllPayments();
		} catch (RuntimeException e) {
			throw new Exception("findAllPayments failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistPayment(Payment payment) throws Exception {
		try {
			getDao().persistPayment(payment);
		} catch (RuntimeException e) {
			throw new Exception("persistPayment failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removePayment(Payment payment) throws Exception {
		try {
			getDao().removePayment(payment);
		} catch (RuntimeException e) {
			throw new Exception("removePayment failed: " + e.getMessage());
		}
	}

	/**
	 * Service method for named queries
	 */

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(IPaymentDao dao) {
		this.dao = dao;
	}
	public IPaymentDao getDao() {
		return this.dao;
	}

    @Override
    public List getPaymentData(int page, int size) {
        return dao.getPaymentData(page,size);
    }

    @Override
    public long getResultSize() {
        return dao.getResultSize();
    }
}