package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IAdminAccountDao;
import com.mangoca.cmp.model.AdminAccount;
import com.mangoca.cmp.model.User;
import com.mangoca.cmp.service.IAdminAccountService;
import java.util.List;

/**
 * The service class for the AdminAccount entity.
 */
public class AdminAccountSpringService implements IAdminAccountService {
	/**
	 * The dao instance injected by Spring.
	 */
	private IAdminAccountDao dao;
        
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "AdminAccountService";
	
	public AdminAccountSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>IUserService</code> instance.
	 */
	public static IAdminAccountService getInstance() {
		return (IAdminAccountService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public AdminAccount findAdminAccountById(Long id) throws Exception {
		try {
			return getDao().findAdminAccountById(id);
		} catch (RuntimeException e) {
			throw new Exception("findAdminAccountById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>AdminAccount</code> entity.
	 */
    @Override
	public List<AdminAccount> findAllAdminAccounts() throws Exception {
		try {
			return getDao().findAllAdminAccounts();
		} catch (RuntimeException e) {
			throw new Exception("findAllAdminAccounts failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistAdminAccount(AdminAccount user) throws Exception {
		try {
			getDao().persistAdminAccount(user);
		} catch (RuntimeException e) {
			throw new Exception("persistAdminAccount failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeAdminAccount(AdminAccount user) throws Exception {
		try {
			getDao().removeAdminAccount(user);
		} catch (RuntimeException e) {
			throw new Exception("removeAdminAccount failed: " + e.getMessage());
		}
	}

    public IAdminAccountDao getDao() {
        return dao;
    }

    public void setDao(IAdminAccountDao dao) {
        this.dao = dao;
    }

	/**
	 * Service method for named queries
	 */

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	
        
	@Override
	public List findAllRaAdminAccount() {
		// TODO Auto-generated method stub
                String queryName="AdminAccount.findByAdminAccountRole";
                
		return dao.findByNamedQuery(queryName, new String[]{"userRole"}, new Object[]{User.ADMIN_RA});
	}
	@Override
	public AdminAccount getAdminAccountByUserName(String userName) {
		return dao.getAdminAccountByUserName(userName);
	}

    @Override
    public List getAdminAccountData(int page, int size) { 
        return dao.getAdminAccountData(page, size);
    }

    @Override
    public long getResultSize() {
        return dao.getResultSize();
    }
      
}