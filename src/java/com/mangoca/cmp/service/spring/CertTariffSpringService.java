package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.ICertTariffDao;
import com.mangoca.cmp.model.CertType;
import com.mangoca.cmp.model.WebSSL;
import com.mangoca.cmp.service.ICertTariffService;
import java.util.List;

/**
 * The service class for the entities: CertType, WebSSL.
 */
public class CertTariffSpringService implements ICertTariffService {
	/**
	 * The dao instance injected by Spring.
	 */
	private ICertTariffDao dao;
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "CertTariffService";
	
	public CertTariffSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>ICertTariffService</code> instance.
	 */
	public static ICertTariffService getInstance() {
		return (ICertTariffService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
	}

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public CertType findCertTypeById(java.lang.Integer id) throws Exception {
		try {
			return getDao().findCertTypeById(id);
		} catch (RuntimeException e) {
			throw new Exception("findCertTypeById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>CertType</code> entity.
	 */
    @Override
	public List<CertType> findAllCertTypes() throws Exception {
		try {
			return getDao().findAllCertTypes();
		} catch (RuntimeException e) {
			throw new Exception("findAllCertTypes failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistCertType(CertType certType) throws Exception {
		try {
			getDao().persistCertType(certType);
		} catch (RuntimeException e) {
			throw new Exception("persistCertType failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeCertType(CertType certType) throws Exception {
		try {
			getDao().removeCertType(certType);
		} catch (RuntimeException e) {
			throw new Exception("removeCertType failed: " + e.getMessage());
		}
	}

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public WebSSL findWebSSLById(java.lang.Integer id) throws Exception {
		try {
			return getDao().findWebSSLById(id);
		} catch (RuntimeException e) {
			throw new Exception("findWebSSLById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>WebSSL</code> entity.
	 */
    @Override
	public List<WebSSL> findAllWebSSLs() throws Exception {
		try {
			return getDao().findAllWebSSLs();
		} catch (RuntimeException e) {
			throw new Exception("findAllWebSSLs failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistWebSSL(WebSSL webSSL) throws Exception {
		try {
			getDao().persistWebSSL(webSSL);
		} catch (RuntimeException e) {
			throw new Exception("persistWebSSL failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
        @Override
	public void removeWebSSL(WebSSL webSSL) throws Exception {
		try {
			getDao().removeWebSSL(webSSL);
		} catch (RuntimeException e) {
			throw new Exception("removeWebSSL failed: " + e.getMessage());
		}
	}

	/**
	 * Service method for named queries
	 */

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(ICertTariffDao dao) {
		this.dao = dao;
	}
	public ICertTariffDao getDao() {
		return this.dao;
	}

    @Override
    public List<String> findAllCertTypeNames() {
        return dao.findAllCertTypeNames();
    }
}