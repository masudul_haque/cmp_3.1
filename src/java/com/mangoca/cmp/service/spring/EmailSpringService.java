/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.service.IEmailService;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;

/**
 *
 * @author Masudul Haque
 */
public class EmailSpringService implements IEmailService {

    private static final String SERVICE_BEAN_ID = "EmailService";
    private JavaMailSender javaMailSender; 
    private MailSender mailSender;
    private SimpleMailMessage mailMessage;
    
    public EmailSpringService() {
        super();
    }

    public static IEmailService getInstance() {
        return (IEmailService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);// context.getBean(SERVICE_BEAN_ID);
    }

    public MailSender getMailSender() {
        return mailSender;
    }

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    public SimpleMailMessage getMailMessage() {
        return mailMessage;
    }

    public void setMailMessage(SimpleMailMessage mailMessage) {
        this.mailMessage = mailMessage;
    }

    

    public JavaMailSender getJavaMailSender() {
        return javaMailSender;
    }

    public void setJavaMailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }
    
    
    
    

    @Override
    public void sendEmail(String toAddress, String subject, String message) {
        SimpleMailMessage msg = new SimpleMailMessage(this.mailMessage);
        msg.setTo(toAddress);
        msg.setSubject(subject);
        msg.setText(message);
        try{
            this.mailSender.send(msg);
        }
        catch(MailException ex) {
            
            System.err.println(ex.getMessage());            
        }
    }
    
    @Override
    public void sendMimeEmail(final String toAddress, String subject, final String message) {
        
         MimeMessagePreparator preparator = new MimeMessagePreparator() {
        
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
        
                mimeMessage.setRecipient(Message.RecipientType.TO, 
                        new InternetAddress(toAddress));
                mimeMessage.setFrom(new InternetAddress("info@mangoca.com"));
                mimeMessage.setText(message);
            }
        };
        try {
            this.javaMailSender.send(preparator);
        }
        catch (MailException ex) {
            // simply log it and go on...
            System.err.println(ex.getMessage());            
        }
    }
    

    @Override
    public void sendEmail(String toAddress, String ccAddress, String subject, String message) {
        SimpleMailMessage msg = new SimpleMailMessage(this.mailMessage);
        msg.setTo(toAddress);
        msg.setCc(ccAddress);
        msg.setSubject(subject);
        msg.setText(message);
        try{
            this.mailSender.send(msg);
        }
        catch(MailException ex) {
            
            System.err.println(ex.getMessage());            
        }
    }

    @Override
    public void sendEmail(String[] toAddress, String[] ccAddress, String subject, String message) {
        SimpleMailMessage msg = new SimpleMailMessage(this.mailMessage);
        msg.setTo(toAddress);
        msg.setSubject(subject);
        msg.setText(message);
        try{
            this.mailSender.send(msg);
        }
        catch(MailException ex) {
            
            System.err.println(ex.getMessage());            
        }
    }
}
