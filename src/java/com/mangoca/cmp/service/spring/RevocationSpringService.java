package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IRevocationDao;
import com.mangoca.cmp.model.Revocation;
import com.mangoca.cmp.service.IRevocationService;
import java.util.List;

/**
 * The service class for the Revocation entity.
 */
public class RevocationSpringService implements IRevocationService {
	/**
	 * The dao instance injected by Spring.
	 */
	private IRevocationDao dao;
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "RevocationService";
	
	public RevocationSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>IRevocationService</code> instance.
	 */
	public static IRevocationService getInstance() {
		return (IRevocationService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Revocation findRevocationById(java.lang.Long id) throws Exception {
		try {
			return getDao().findRevocationById(id);
		} catch (RuntimeException e) {
			throw new Exception("findRevocationById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>Revocation</code> entity.
	 */
    @Override
	public List<Revocation> findAllRevocations() throws Exception {
		try {
			return getDao().findAllRevocations();
		} catch (RuntimeException e) {
			throw new Exception("findAllRevocations failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistRevocation(Revocation revocation) throws Exception {
		try {
			getDao().persistRevocation(revocation);
		} catch (RuntimeException e) {
			throw new Exception("persistRevocation failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeRevocation(Revocation revocation) throws Exception {
		try {
			getDao().removeRevocation(revocation);
		} catch (RuntimeException e) {
			throw new Exception("removeRevocation failed: " + e.getMessage());
		}
	}

	

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(IRevocationDao dao) {
		this.dao = dao;
	}
	public IRevocationDao getDao() {
		return this.dao;
	}
}