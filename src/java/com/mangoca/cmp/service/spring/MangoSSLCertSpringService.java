package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IMangoSSLCertDao;
import com.mangoca.cmp.model.MangoSSLCert;
import com.mangoca.cmp.service.IMangoSSLCertService;
import java.util.List;

/**
 * The service class for the MangoSSLCert entity.
 */
public class MangoSSLCertSpringService implements IMangoSSLCertService {

    /**
     * The dao instance injected by Spring.
     */
    private IMangoSSLCertDao dao;
    /**
     * The service Spring bean id, used in the applicationContext.xml file.
     */
    private static final String SERVICE_BEAN_ID = "MangoSSLCertService";

    public MangoSSLCertSpringService() {
        super();
    }

    /**
     * Returns the singleton <code>IMangoSSLCertService</code> instance.
     */
    public static IMangoSSLCertService getInstance() {
        return (IMangoSSLCertService) SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
    }

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    @Override
    public MangoSSLCert findMangoSSLCertById(int id){
            return getDao().findMangoSSLCertById(id);       
    }

    /**
     * Return all persistent instances of the <code>MangoSSLCert</code> entity.
     */
    @Override
    public List<MangoSSLCert> findAllMangoSSLCerts() throws Exception {
        try {
            return getDao().findAllMangoSSLCerts();
        } catch (RuntimeException e) {
            throw new Exception("findAllMangoSSLCerts failed: " + e.getMessage());
        }
    }

    /**
     * Make the given instance managed and persistent.
     */
    @Override
    public void persistMangoSSLCert(MangoSSLCert mangoSSLCert) throws Exception {
        try {
            getDao().persistMangoSSLCert(mangoSSLCert);
        } catch (RuntimeException e) {
            throw new Exception("persistMangoSSLCert failed: " + e.getMessage());
        }
    }

    /**
     * Remove the given persistent instance.
     */
    @Override
    public void removeMangoSSLCert(MangoSSLCert mangoSSLCert) throws Exception {
        try {
            getDao().removeMangoSSLCert(mangoSSLCert);
        } catch (RuntimeException e) {
            throw new Exception("removeMangoSSLCert failed: " + e.getMessage());
        }
    }

    /**
     * Service method for named queries
     */
    /**
     * Called by Spring using the injection rules specified in 
     * the Spring beans file "applicationContext.xml".
     */
    public void setDao(IMangoSSLCertDao dao) {
        this.dao = dao;
    }

    public IMangoSSLCertDao getDao() {
        return this.dao;
    }

    @Override
    public List getMangoSSLCertData(int page, int size){
        return dao.getMangoSSLCertData(page, size);
    }

    @Override
    public int getResultSize(){
        return (int)dao.getResultSize();
    }
}