package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.ITicketDao;
import com.mangoca.cmp.model.Ticket;
import com.mangoca.cmp.service.ITicketService;
import java.util.List;

/**
 * The service class for the Ticket entity.
 */
public class TicketSpringService implements ITicketService {

    /**
     * The dao instance injected by Spring.
     */
    private ITicketDao dao;
    /**
     * The service Spring bean id, used in the applicationContext.xml file.
     */
    private static final String SERVICE_BEAN_ID = "TicketService";

    public TicketSpringService() {
        super();
    }

    /**
     * Returns the singleton <code>ITicketService</code> instance.
     */
    public static ITicketService getInstance() {
        return (ITicketService) SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
    }

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    @Override
    public Ticket findTicketById(Long ticketId) throws Exception {
        try {
            return getDao().findTicketById(ticketId);
        } catch (RuntimeException e) {
            throw new Exception("findTicketById failed with the id " + ticketId + ": " + e.getMessage());
        }
    } 

    /**
     * Return all persistent instances of the <code>Ticket</code> entity.
     */
    @Override
    public List<Ticket> findAllTickets() throws Exception {
        try {
            return getDao().findAllTickets();
        } catch (RuntimeException e) {
            throw new Exception("findAllTickets failed: " + e.getMessage());
        }
    }

    /**
     * Make the given instance managed and persistent.
     */
    @Override
    public void persistTicket(Ticket clientticket) throws Exception {
        try {
            getDao().persistTicket(clientticket);
        } catch (RuntimeException e) {
            throw new Exception("persistTicket failed: " + e.getMessage());
        }
    }

    /**
     * Remove the given persistent instance.
     */
    @Override
    public void removeTicket(Ticket clientticket) throws Exception {
        try {
            getDao().removeTicket(clientticket);
        } catch (RuntimeException e) {
            throw new Exception("removeTicket failed: " + e.getMessage());
        }
    }

    /**
     * Service method for named queries
     */
    /**
     * Called by Spring using the injection rules specified in 
     * the Spring beans file "applicationContext.xml".
     */
    public void setDao(ITicketDao dao) {
        this.dao = dao;
    }

    public ITicketDao getDao() {
        return this.dao;
    }

    @Override
    public List<Ticket> findTicketsByUsername(String userName) throws Exception {
        try {
            return getDao().findByNamedQuery("Ticket.findByUserName", new String[]{"userName"}, new Object[]{userName});
        } catch (RuntimeException e) {
            throw new Exception("findTicketsByUsername failed: " + e.getMessage());
        }
    }

    @Override
    public List<Ticket> findTicketsByStatus(String status) throws Exception {
        try {
            return getDao().findByNamedQuery("Ticket.findByTicketStatus", new String[]{"status"}, new Object[]{status});
        } catch (RuntimeException e) {
            throw new Exception("findTicketsByTicketStatus failed: " + e.getMessage());
        }
    }
    
    
    @Override
    public List<Ticket> getTicketDataByUsername(String userName,int page, int pageSize)  {
        
            return getDao().getTicketDataByUserName(userName, page,pageSize);
    }

    @Override
    public List<Ticket> getTicketDataByStatus(Short status, int page, int pageSize){
        
            return getDao().getTicketDataByStatus(status,page,pageSize);
    }
    
    
    @Override
    public Integer countTicketByStatus(Short status){
        return getDao().countByNamedQuery("Ticket.countTicketByStatus", "status", status); 
    }


    @Override
    public List findTicketByStatus() {
        return getDao().findTicketByStatus();
    }

    @Override
    public int countTicketByUserName(String userName) {
        return (int)getDao().countTicketByUserName(userName);
    }
}