package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IRenewalDao;
import com.mangoca.cmp.model.Renewal;
import com.mangoca.cmp.service.IRenewalService;
import java.util.List;

/**
 * The service class for the Renewal entity.
 */
public class RenewalSpringService implements IRenewalService {
	/**
	 * The dao instance injected by Spring.
	 */
	private IRenewalDao dao;
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "RenewalService";
	
	public RenewalSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>IRenewalService</code> instance.
	 */
	public static IRenewalService getInstance() {
		return (IRenewalService)SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Renewal findRenewalById(java.lang.Long id) throws Exception {
		try {
			return getDao().findRenewalById(id);
		} catch (RuntimeException e) {
			throw new Exception("findRenewalById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>Renewal</code> entity.
	 */
    @Override
	public List<Renewal> findAllRenewals() throws Exception {
		try {
			return getDao().findAllRenewals();
		} catch (RuntimeException e) {
			throw new Exception("findAllRenewals failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistRenewal(Renewal renewal) throws Exception {
		try {
			getDao().persistRenewal(renewal);
		} catch (RuntimeException e) {
			throw new Exception("persistRenewal failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeRenewal(Renewal renewal) throws Exception {
		try {
			getDao().removeRenewal(renewal);
		} catch (RuntimeException e) {
			throw new Exception("removeRenewal failed: " + e.getMessage());
		}
	}

	

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(IRenewalDao dao) {
		this.dao = dao;
	}
	public IRenewalDao getDao() {
		return this.dao;
	}
}