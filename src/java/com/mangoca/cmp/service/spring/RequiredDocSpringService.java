package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IRequiredDocDao;
import com.mangoca.cmp.model.RequiredDoc;
import com.mangoca.cmp.service.IRequiredDocService;
import java.util.List;
import org.springframework.context.ApplicationContext;

/**
 * The service class for the RequiredDoc entity.
 */
public class RequiredDocSpringService implements IRequiredDocService {
	/**
	 * The dao instance injected by Spring.
	 */
	private IRequiredDocDao dao;
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "RequiredDocService";
	
	public RequiredDocSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>IRequiredDocService</code> instance.
	 */
	public static IRequiredDocService getInstance(ApplicationContext context) {
		return (IRequiredDocService)context.getBean(SERVICE_BEAN_ID);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public RequiredDoc findRequiredDocById(int id) throws Exception {
		try {
			return getDao().findRequiredDocById(id);
		} catch (RuntimeException e) {
			throw new Exception("findRequiredDocById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>RequiredDoc</code> entity.
	 */
    @Override
	public List<RequiredDoc> findAllRequiredDocs() throws Exception {
		try {
			return getDao().findAllRequiredDocs();
		} catch (RuntimeException e) {
			throw new Exception("findAllRequiredDocs failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistRequiredDoc(RequiredDoc requiredDoc) throws Exception {
		try {
			getDao().persistRequiredDoc(requiredDoc);
		} catch (RuntimeException e) {
			throw new Exception("persistRequiredDoc failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeRequiredDoc(RequiredDoc requiredDoc) throws Exception {
		try {
			getDao().removeRequiredDoc(requiredDoc);
		} catch (RuntimeException e) {
			throw new Exception("removeRequiredDoc failed: " + e.getMessage());
		}
	}

	/**
	 * Service method for named queries
	 */

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(IRequiredDocDao dao) {
		this.dao = dao;
	}
	public IRequiredDocDao getDao() {
		return this.dao;
	}
}