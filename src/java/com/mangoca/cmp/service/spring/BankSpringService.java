package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.IBankDao;
import com.mangoca.cmp.model.Bank;
import com.mangoca.cmp.service.IBankService;
import java.util.List;
import org.springframework.context.ApplicationContext;

/**
 * The service class for the Bank entity.
 */
public class BankSpringService implements IBankService {
	/**
	 * The dao instance injected by Spring.
	 */
	private IBankDao dao;
	/**
	 * The service Spring bean id, used in the applicationContext.xml file.
	 */
	private static final String SERVICE_BEAN_ID = "BankService";
	
	public BankSpringService() {
		super();
	}
	/**
	 * Returns the singleton <code>IBankService</code> instance.
	 */
	public static IBankService getInstance(ApplicationContext context) {
		return (IBankService)context.getBean(SERVICE_BEAN_ID);
	}
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
    @Override
	public Bank findBankById(int id) throws Exception {
		try {
			return getDao().findBankById(id);
		} catch (RuntimeException e) {
			throw new Exception("findBankById failed with the id " + id + ": " + e.getMessage());
		}
	}
	/**
	 * Return all persistent instances of the <code>Bank</code> entity.
	 */
    @Override
	public List<Bank> findAllBanks() throws Exception {
		try {
			return getDao().findAllBanks();
		} catch (RuntimeException e) {
			throw new Exception("findAllBanks failed: " + e.getMessage());
		}
	}
	/**
	 * Make the given instance managed and persistent.
	 */
    @Override
	public void persistBank(Bank bank) throws Exception {
		try {
			getDao().persistBank(bank);
		} catch (RuntimeException e) {
			throw new Exception("persistBank failed: " + e.getMessage());
		}
	}
	/**
	 * Remove the given persistent instance.
	 */
    @Override
	public void removeBank(Bank bank) throws Exception {
		try {
			getDao().removeBank(bank);
		} catch (RuntimeException e) {
			throw new Exception("removeBank failed: " + e.getMessage());
		}
	}

	/**
	 * Service method for named queries
	 */

	/**
	 * Called by Spring using the injection rules specified in 
	 * the Spring beans file "applicationContext.xml".
	 */
	public void setDao(IBankDao dao) {
		this.dao = dao;
	}
	public IBankDao getDao() {
		return this.dao;
	}
}