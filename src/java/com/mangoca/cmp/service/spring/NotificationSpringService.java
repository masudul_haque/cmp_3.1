package com.mangoca.cmp.service.spring;

import com.mangoca.cmp.dao.INotificationDao;
import com.mangoca.cmp.model.Notification;
import com.mangoca.cmp.service.INotificationService;
import java.util.List;

/**
 * The service class for the Notification entity.
 */
public class NotificationSpringService implements INotificationService {

    /**
     * The dao instance injected by Spring.
     */
    private INotificationDao dao;
    /**
     * The service Spring bean id, used in the applicationContext.xml file.
     */
    private static final String SERVICE_BEAN_ID = "NotificationService";

    public NotificationSpringService() {
        super();
    }

    /**
     * Returns the singleton <code>INotificationService</code> instance.
     */
    public static INotificationService getInstance() {
        return (INotificationService) SpringUtil.getSpringBean(SERVICE_BEAN_ID);//context.getBean(SERVICE_BEAN_ID);
    }

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    @Override
    public Notification findNotificationById(Long id) throws Exception {
        try {
            return getDao().findNotificationById(id);
        } catch (RuntimeException e) {
            throw new Exception("findNotificationById failed with the id " + id + ": " + e.getMessage());
        }
    }

    /**
     * Return all persistent instances of the <code>Notification</code> entity.
     */
    @Override
    public List<Notification> findAllNotifications() throws Exception {
        try {
            return getDao().findAllNotifications();
        } catch (RuntimeException e) {
            throw new Exception("findAllNotifications failed: " + e.getMessage());
        }
    }

    /**
     * Make the given instance managed and persistent.
     */
    @Override
    public void persistNotification(Notification notificatioin) throws Exception {
        try {
            getDao().persistNotification(notificatioin);
        } catch (RuntimeException e) {
            throw new Exception("persistNotification failed: " + e.getMessage());
        }
    }

    /**
     * Remove the given persistent instance.
     */
    @Override
    public void removeNotification(Notification notificatioin) throws Exception {
        try {
            getDao().removeNotification(notificatioin);
        } catch (RuntimeException e) {
            throw new Exception("removeNotification failed: " + e.getMessage());
        }
    }

    /**
     * Called by Spring using the injection rules specified in 
     * the Spring beans file "applicationContext.xml".
     */
    public void setDao(INotificationDao dao) {
        this.dao = dao;
    }

    public INotificationDao getDao() {
        return this.dao;
    }

    @Override
    public List getNotificationData(int page, int size) {
        return dao.getNotificationData(page, size);
    }

    @Override
    public int getResultSize() {
        return (int) dao.getResultSize();
    }

    @Override
    public List<Notification> findNotificationsByCIN(String userCIN) throws Exception {
        try {
            return getDao().findByNamedQuery("Notification.findByCINAndStatus",
                    new String[]{"userCIN", "status"}, new Object[]{userCIN, Notification.STATUS_SENT});
        } catch (RuntimeException e) {
            throw new Exception("findNotificationById failed with the id " + userCIN + ": " + e.getMessage());
        }
    }

    @Override
    public int countNotificationByCIN(String userCIN) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}