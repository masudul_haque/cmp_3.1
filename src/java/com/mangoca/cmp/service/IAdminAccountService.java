package com.mangoca.cmp.service;

import com.mangoca.cmp.model.AdminAccount;
import java.util.List;

/**
 * The service interface for the AdminAccount entity.
 */
public interface IAdminAccountService {

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public AdminAccount findAdminAccountById(Long id) throws Exception;

    /**
     * Return all persistent instances of the <code>AdminAccount</code> entity.
     */
    public List<AdminAccount> findAllAdminAccounts() throws Exception;

    /**
     * Make the given instance managed and persistent.
     */
    public void persistAdminAccount(AdminAccount user) throws Exception;

    /**
     * Remove the given persistent instance.
     */
    public void removeAdminAccount(AdminAccount user) throws Exception;

    /**
     * Service method for named queries
     */
    public List<AdminAccount> findAllRaAdminAccount();

    public AdminAccount getAdminAccountByUserName(String userName);

    public List getAdminAccountData(int page, int size);

    public long getResultSize();

}