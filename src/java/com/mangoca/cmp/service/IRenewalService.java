package com.mangoca.cmp.service;

import com.mangoca.cmp.model.Renewal;
import java.util.List;

/**
 * The service interface for the Renewal entity.
 */
public interface IRenewalService {
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public Renewal findRenewalById(java.lang.Long id) throws Exception;
	/**
	 * Return all persistent instances of the <code>Renewal</code> entity.
	 */
	public List<Renewal> findAllRenewals() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistRenewal(Renewal renewal) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeRenewal(Renewal renewal) throws Exception;
	/**
	 * Service method for named queries
	 */
	
}