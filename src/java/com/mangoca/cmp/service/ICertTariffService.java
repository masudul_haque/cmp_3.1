package com.mangoca.cmp.service;

import com.mangoca.cmp.model.CertType;
import com.mangoca.cmp.model.WebSSL;
import java.util.List;

/**
 * The service interface for the entities: CertType, WebSSL.
 */
public interface ICertTariffService {

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public CertType findCertTypeById(java.lang.Integer id) throws Exception;
	/**
	 * Return all persistent instances of the <code>CertType</code> entity.
	 */
	public List<CertType> findAllCertTypes() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistCertType(CertType certType) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeCertType(CertType certType) throws Exception;

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public WebSSL findWebSSLById(java.lang.Integer id) throws Exception;
	/**
	 * Return all persistent instances of the <code>WebSSL</code> entity.
	 */
	public List<WebSSL> findAllWebSSLs() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistWebSSL(WebSSL webSSL) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeWebSSL(WebSSL webSSL) throws Exception;

    public List<String> findAllCertTypeNames();
	/**
	 * Service method for named queries
	 */
}