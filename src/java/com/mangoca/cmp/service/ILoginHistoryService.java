package com.mangoca.cmp.service;

import com.mangoca.cmp.model.LoginHistoryLog;
import java.util.List;

/**
 * The service interface for the LoginHistoryLog entity.
 */
public interface ILoginHistoryService {
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public LoginHistoryLog findLoginHistoryLogById(java.lang.Integer id) throws Exception;
	/**
	 * Return all persistent instances of the <code>LoginHistoryLog</code> entity.
	 */
	public List<LoginHistoryLog> findAllLoginHistoryLogs() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistLoginHistoryLog(LoginHistoryLog loginHistoryLog) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeLoginHistoryLog(LoginHistoryLog loginHistoryLog) throws Exception;
	/**
	 * Service method for named queries
	 */
}