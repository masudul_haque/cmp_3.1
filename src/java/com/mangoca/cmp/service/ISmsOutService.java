package com.mangoca.cmp.service;

import com.mangoca.cmp.model.SmsOut;
import java.util.List;

/**
 * The service interface for the SmsOut entity.
 */
public interface ISmsOutService {
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public SmsOut findSmsOutById(int id) throws Exception;
	/**
	 * Return all persistent instances of the <code>SmsOut</code> entity.
	 */
	public List<SmsOut> findAllSmsOuts() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistSmsOut(SmsOut smsOut) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeSmsOut(SmsOut smsOut) throws Exception;

    public List getSmsOutData(int page, int size);

    public long getResultSize();
	/**
	 * Service method for named queries
	 */
}