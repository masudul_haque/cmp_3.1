package com.mangoca.cmp.service;

import com.mangoca.cmp.model.Document;
import java.util.List;

/**
 * The service interface for the Document entity.
 */
public interface IDocumentService {

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public Document findDocumentById(Long documentId) throws Exception;

    /**
     * Return all persistent instances of the <code>Document</code> entity.
     */
    public List<Document> findAllDocuments() throws Exception;

    /**
     * Make the given instance managed and persistent.
     */
    public void persistDocument(Document document) throws Exception;

    /**
     * Remove the given persistent instance.
     */
    public void removeDocument(Document document) throws Exception;

    /**
     * Service method for named queries
     */
    public List getDocumentData(int page, int size);

    public long getResultSize();
}