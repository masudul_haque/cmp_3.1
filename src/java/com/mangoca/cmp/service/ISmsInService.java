package com.mangoca.cmp.service;

import com.mangoca.cmp.model.SmsIn;
import java.util.List;

/**
 * The service interface for the SmsIn entity.
 */
public interface ISmsInService {

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public SmsIn findSmsInById(int id) throws Exception;

    /**
     * Return all persistent instances of the <code>SmsIn</code> entity.
     */
    public List<SmsIn> findAllSmsIns() throws Exception;

    /**
     * Make the given instance managed and persistent.
     */
    public void persistSmsIn(SmsIn smsIn) throws Exception;

    /**
     * Remove the given persistent instance.
     */
    public void removeSmsIn(SmsIn smsIn) throws Exception;

    public List getSmsInData(int page, int size);

    public long getResultSize();
    /**
     * Service method for named queries
     */
}