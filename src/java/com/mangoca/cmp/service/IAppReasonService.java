package com.mangoca.cmp.service;

import com.mangoca.cmp.model.RevokeReason;
import com.mangoca.cmp.model.SuspensionReason;
import java.util.List;

/**
 * The service interface for the entities: RevokeReason, SuspensionReason.
 */
public interface IAppReasonService {

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public RevokeReason findRevokeReasonById(int id) throws Exception;
	/**
	 * Return all persistent instances of the <code>RevokeReason</code> entity.
	 */
	public List<RevokeReason> findAllRevokeReasons() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistRevokeReason(RevokeReason revokeReason) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeRevokeReason(RevokeReason revokeReason) throws Exception;

	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public SuspensionReason findSuspensionReasonById(int id) throws Exception;
	/**
	 * Return all persistent instances of the <code>SuspensionReason</code> entity.
	 */
	public List<SuspensionReason> findAllSuspensionReasons() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistSuspensionReason(SuspensionReason suspensionReason) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeSuspensionReason(SuspensionReason suspensionReason) throws Exception;
	/**
	 * Service method for named queries
	 */
}