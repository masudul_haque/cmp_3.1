package com.mangoca.cmp.service;

import com.mangoca.cmp.model.RequiredDoc;
import java.util.List;

/**
 * The service interface for the RequiredDoc entity.
 */
public interface IRequiredDocService {
	/**
	 * Find an entity by its id (primary key).
	 * @return The found entity instance or null if the entity does not exist.
	 */
	public RequiredDoc findRequiredDocById(int id) throws Exception;
	/**
	 * Return all persistent instances of the <code>RequiredDoc</code> entity.
	 */
	public List<RequiredDoc> findAllRequiredDocs() throws Exception;
	/**
	 * Make the given instance managed and persistent.
	 */
	public void persistRequiredDoc(RequiredDoc requiredDoc) throws Exception;
	/**
	 * Remove the given persistent instance.
	 */
	public void removeRequiredDoc(RequiredDoc requiredDoc) throws Exception;
	/**
	 * Service method for named queries
	 */
}