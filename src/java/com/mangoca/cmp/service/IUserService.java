package com.mangoca.cmp.service;

import com.mangoca.cmp.model.User;
import java.util.List;

/**
 * The service interface for the AdminAccount entity.
 */
public interface IUserService {

    /**
     * Find an entity by its id (primary key).
     * @return The found entity instance or null if the entity does not exist.
     */
    public User findUserById(Long id) throws Exception;

    /**
     * Return all persistent instances of the <code>AdminAccount</code> entity.
     */
    public List<User> findAllUsers() throws Exception;

    /**
     * Make the given instance managed and persistent.
     */
    public void persistUser(User user) throws Exception;

    /**
     * Remove the given persistent instance.
     */
    public void removeUser(User user) throws Exception;

    /**
     * Service method for named queries
     */
    public List<User> findAllRaUser();

    public User getUserByUserName(String userName);

    public List findUsersByUserRole();
}