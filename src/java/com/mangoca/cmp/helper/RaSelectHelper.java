/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.helper;

import com.mangoca.cmp.model.AdminAccount;
import com.mangoca.cmp.service.IAdminAccountService;
import com.mangoca.cmp.service.spring.AdminAccountSpringService;
import java.util.List;
import javax.servlet.http.HttpSession;
/**
 *
 * @author Masudul Haque
 */
public class RaSelectHelper {
    public static void loadRaAdmins(HttpSession session){
        IAdminAccountService userService = AdminAccountSpringService.getInstance();
        List<AdminAccount> raUsers = userService.findAllRaAdminAccount();

       // HttpSession session = request.getSession();
        session.setAttribute("raUsers", raUsers);
    }
}
