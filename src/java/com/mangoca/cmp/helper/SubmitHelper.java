/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.helper;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.model.Accaction;
import com.mangoca.cmp.model.Certificate;
import com.mangoca.cmp.model.Document;
import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.model.MangoSSLCert;
import com.mangoca.cmp.model.Mangocert;
import com.mangoca.cmp.model.Raaction;
import com.mangoca.cmp.model.Techaction;
import com.mangoca.cmp.model.UserSSLCert;
import com.mangoca.cmp.model.Userbasic;
import com.mangoca.cmp.model.Vaaction;
import com.mangoca.cmp.service.IMangoCertService;
import com.mangoca.cmp.service.IMangoSSLCertService;
import com.mangoca.cmp.service.spring.MangoCertSpringService;
import com.mangoca.cmp.service.spring.MangoSSLCertSpringService;
import com.mangoca.cmp.web.form.DocumentForm;
import com.mangoca.cmp.web.form.EnrolleduserForm;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.upload.FormFile;
import org.jasypt.hibernate3.encryptor.HibernatePBEStringEncryptor;

/**
 *
 * @author Masudul Haque
 */
public class SubmitHelper {
    
    public static Enrolleduser afterSubmit(Enrolleduser enrolleduser, EnrolleduserForm enrollForm)
            throws  Exception {
        Userbasic userbasic = enrolleduser.getUserbasic();
        if (userbasic == null) {
            userbasic = new Userbasic();
        }
        
        Certificate certificate = enrolleduser.getCertificate();
        if (certificate == null) {
            certificate = new Certificate();
            certificate.setCertStatus(Certificate.CERT_NEW);
        }
        Raaction raaction = enrolleduser.getRaaction();
        if (raaction == null) {
            raaction = new Raaction();
            raaction.setStatus(Raaction.STATUS_NEW);
            enrolleduser.setRaaction(raaction);
        }
        Accaction accaction = enrolleduser.getAccaction();
        if (accaction == null) {
            accaction = new Accaction();
            accaction.setStatus(Accaction.STATUS_NEW);
            enrolleduser.setAccaction(accaction);
        }
        Vaaction vaaction = enrolleduser.getVaaction();
        if (vaaction == null) {
            vaaction = new Vaaction();
            vaaction.setStatus(Vaaction.STATUS_NEW);
            enrolleduser.setVaaction(vaaction);
        }
        Techaction techaction = enrolleduser.getTechaction();
        if (techaction == null) {
            techaction = new Techaction();
            techaction.setStatus(Techaction.STATUS_NEW);
            enrolleduser.setTechaction(techaction);
        }
        
        
        BeanUtils.copyProperties(enrolleduser, enrollForm);
        BeanUtils.copyProperties(certificate, enrollForm);
        
        if (enrollForm.getCertId() != 0) {
            IMangoCertService mangoCertService = MangoCertSpringService.getInstance();
            Mangocert mangocert = mangoCertService.findMangocertById(enrollForm.getCertId());
            enrolleduser.setCertClass(mangocert.getCertClass());
            certificate.setCertClass(mangocert.getCertClass());
            
            
            //HibernatePBEStringEncryptor encryptor = new HibernatePBEStringEncryptor();
          //  encryptor.setAlgorithm("SHA-1");//digester.setAlgorithm("SHA-1");
            //encryptor.setPassword(Constants.ENCRYPT_PASSWORD);
            String encryptSerialNo = EncryptionHelper.encrypt(enrollForm.getSerialNo());
            certificate.setSerialNo(encryptSerialNo);
            if (Mangocert.CERT_SERVER.equals(mangocert.getCertType())) {
                UserSSLCert userSSLCert = new UserSSLCert();
                Integer mangoSSL = enrollForm.getSslId();
                IMangoSSLCertService certService = MangoSSLCertSpringService.getInstance();
                MangoSSLCert mangoSSLCert = certService.findMangoSSLCertById(mangoSSL);
                if (mangoSSLCert != null) {
                    BeanUtils.copyProperties(userSSLCert, mangoSSLCert);
                }
                BeanUtils.copyProperties(userSSLCert, enrollForm);
                userSSLCert.setAdminEmail(enrollForm.getCertEmail() + "@" + enrollForm.getCertEmailDomain());
                userSSLCert.setUpdateDate(new Date());
                enrolleduser.setUserSSLCert(userSSLCert);                
            }            
        }
        
        BeanUtils.copyProperties(userbasic, enrollForm);
        
        String dobStr = enrollForm.getBirthDate() + enrollForm.getBirthMonth() + enrollForm.getBirthYear();
        Date dateOfBirth = MyConverter.strToDate(dobStr, "ddMMMyyyy");
        userbasic.setDateOfBirth(dateOfBirth);
        
        enrolleduser.setUserbasic(userbasic);
        enrolleduser.setCertificate(certificate);
        
        return enrolleduser;
    }
    
    public static Enrolleduser afterUpload(Enrolleduser enrolleduser, DocumentForm documentForm,
            HttpServletRequest request) throws IOException, IllegalAccessException, InvocationTargetException {
        
        String documentNames[] = {"National ID", "Passport", "Driving Licence", "TIN", "General"};
        
        HttpSession session = request.getSession();
        String userName = (String) session.getAttribute("userName");
        FileOutputStream outputStream = null;
        List<Document> documents = enrolleduser.getDocuments();
        if (documents == null) {
            documents = new ArrayList<Document>(5);
        }
        String folderName = enrolleduser.getUserCIN();
        String pathPrefix = "";
        if (request.getServerName().equals("localhost")) {
            pathPrefix = Constants.LOCAL_HOST_LOCATION;
        } else {
            pathPrefix = Constants.SERVER_LOCATION;
        }
        
        File file = new File(pathPrefix);
        if (!file.exists()) {
            file.mkdir();
        }
        for (int i = 0; i < documentNames.length; i++) {
            FormFile formFile = documentForm.getFile(i);
            if (formFile != null && formFile.getFileSize() != 0) {
                String path = pathPrefix + "/" + documentNames[i] + ".jpg";
                String locationStr = AdminHelper.serverAddress(request) + folderName + "/" + documentNames[i] + ".jpg";
                //location.append(documentNames[i]).append(".jpg");
                try {
                    //Provide the real path of document
                    outputStream = new FileOutputStream(new File(path));
                    outputStream.write(formFile.getFileData());
                } finally {
                    if (outputStream != null) {
                        outputStream.close();
                    }
                }
                Document document = new Document();
                BeanUtils.copyProperties(document, enrolleduser);
                document.setDocumentName(documentNames[i]);
                document.setDocumentLink(locationStr);
                document.setUploadDate(new Date());
                document.setUploadedBy(userName);
                document.setDocumentStatus(Constants.DOC_NEW);
                document.setDocumentType(Constants.DOC_UPLOADED_FOR_VALIDATION);
                documents.add(document);
            }
        }
        Raaction raaction = enrolleduser.getRaaction();
        raaction.setStatus(documentForm.getStatus());
        raaction.setComment(documentForm.getComment());
        raaction.setUpdatedBy(userName);
        raaction.setUpdateDate(new Date());
        enrolleduser.setRaaction(raaction);
        enrolleduser.setDocuments(documents);
        return enrolleduser;
    }
}
