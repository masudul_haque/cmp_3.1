/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.helper;

import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Masudul Haque
 */
public class EncryptionHelper {

    public static final String DIGEST_ALG = "SHA-1";
    public static final String SECRET_KEY_ALG = "DESede";
    public static final String ENCRYPTION_ALG = "DESede/CBC/PKCS5Padding";
    public static final String ENC_FORMAT = "utf-8";

    public static String encrypt(String message) throws Exception {
        MessageDigest md = MessageDigest.getInstance(DIGEST_ALG);
        byte[] digestOfPassword = md.digest("".getBytes(ENC_FORMAT));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        for (int j = 0, k = 16; j < 8;) {
            keyBytes[k++] = keyBytes[j++];
        }

        SecretKey key = new SecretKeySpec(keyBytes, SECRET_KEY_ALG);
        IvParameterSpec iv = new IvParameterSpec(new byte[8]);
        Cipher cipher = Cipher.getInstance(ENCRYPTION_ALG);
        cipher.init(Cipher.ENCRYPT_MODE, key, iv);

        byte[] plainTextBytes = message.getBytes(ENC_FORMAT);
        //  cipher.
        byte[] cipherText = cipher.doFinal(plainTextBytes);

        BASE64Encoder be = new BASE64Encoder();
        return be.encode(cipherText);
    }

    public static String decrypt(String message) throws Exception {
        MessageDigest md = MessageDigest.getInstance(DIGEST_ALG);
        byte[] digestOfPassword = md.digest("".getBytes(ENC_FORMAT));
        byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
        for (int j = 0, k = 16; j < 8;) {
            keyBytes[k++] = keyBytes[j++];
        }

        SecretKey key = new SecretKeySpec(keyBytes, SECRET_KEY_ALG);
        IvParameterSpec iv = new IvParameterSpec(new byte[8]);
        Cipher decipher = Cipher.getInstance(ENCRYPTION_ALG);
        decipher.init(Cipher.DECRYPT_MODE, key, iv);

        BASE64Decoder de = new BASE64Decoder();
        byte[] output = de.decodeBuffer(message);
        byte[] plainText = decipher.doFinal(output);

        return new String(plainText, ENC_FORMAT);
    }
}
