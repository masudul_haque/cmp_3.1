/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.helper;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Masudul Haque
 */
public class AdminHelper {
    
    public static String getPictureLink(HttpServletRequest request){
        return "";
        
    }
    public static String serverAddress(HttpServletRequest request){
        StringBuffer location= new StringBuffer();
        if(request.getServerName().equals("localhost")){
            
        location.append("http://localhost:8084").append(request.getContextPath()).append("/file/");
        }
        else{            
        location.append("https://").append(request.getServerName()).append(":")
                .append(request.getServerPort()).append(request.getContextPath()).append("/file/");
        }
        return location.toString();
    }
    
    public static String serverAttachAddress(HttpServletRequest request){
          StringBuffer location= new StringBuffer();
        if(request.getServerName().equals("localhost")){
            
        location.append("http://localhost:8084").append(request.getContextPath()).append("/attachment/");
        }
        else{            
        location.append("https://").append(request.getServerName()).append(":")
                .append(request.getServerPort()).append(request.getContextPath()).append("/attachment/");
        }
        return location.toString();
    }
   
}
