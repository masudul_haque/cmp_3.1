/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.helper;

import com.mangoca.cmp.model.Enrolleduser;
import com.mangoca.cmp.service.IEnrolleduserService;
import com.mangoca.cmp.service.spring.EnrolleduserSpringService;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Masudul Haque
 */
public class EnrolleduserHelper {
    
    public static void setEnrolleduser(HttpServletRequest request) throws Exception{
        String enrollId= request.getParameter("enrollId");
        Long id=MyConverter.strToLong(enrollId);
        IEnrolleduserService enrolleduserService= EnrolleduserSpringService.getInstance();        
        Enrolleduser enrolleduser= enrolleduserService.findEnrolleduserById(id);       
        request.setAttribute("enrolleduser", enrolleduser);
        
    }
}
