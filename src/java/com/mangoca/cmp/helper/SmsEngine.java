/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.helper;

import com.mangoca.cmp.database.Constants;
import com.mangoca.cmp.model.SmsOut;
import com.mangoca.cmp.service.ISmsOutService;
import com.mangoca.cmp.service.spring.SmsOutSpringService;

/**
 *
 * @author Masudul Haque
 */
public class SmsEngine {

    public static boolean sendSms(String smsBody, String reciverNo) throws Exception {
        boolean isSend=false;
        if(smsBody!=null && reciverNo!=null){
        ISmsOutService smsOutService = SmsOutSpringService.getInstance();
        SmsOut smsOut = new SmsOut();
        smsOut.setMsg(smsBody);
        smsOut.setReceiver(reciverNo);
        smsOut.setSender(Constants.SENDER_NO);
        smsOut.setStatus(SmsOut.STATUS_SEND);
        smsOutService.persistSmsOut(smsOut);
        isSend=true;
        }
      return isSend;  
    }
}
