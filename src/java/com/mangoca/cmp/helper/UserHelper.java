/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.helper;

import com.mangoca.cmp.database.Constants;
import java.util.Calendar;
import java.util.Random;

/**
 *
 * @author Masudul Haque
 */
public class UserHelper {

    private static final Random generator = new Random();

    public static String generateCIN(Long id, String raSymbol) {

        StringBuffer trackId = new StringBuffer();
        Calendar calendar = Calendar.getInstance();
        String yearStr = "" + (calendar.get(Calendar.YEAR));
        String yearFraction = yearStr.substring(2, 4);
        trackId.append(yearFraction).append(raSymbol).append(getIdStr(id + 1)).append(Constants.RA_SYMBOL);

        return trackId.toString();
    }
    
    public static String generateRenewalCIN(String userCIN){
        int length=userCIN.length();
        String remainCIN= userCIN.substring(0, length-1);
        String value = userCIN.substring(length-1, length);
        int charValue = value.charAt(0);
        String next = String.valueOf((char) (charValue + 1));
        return remainCIN+next;
    }

    /**
     * If the id is greater than 6 digit then id will start from 0.
     * @param id
     * @return
     */
    public static String getIdStr(Long id) {
        if (id < 1000000) {
            String idStr = "" + id;
            return fractionIdStr(idStr);
        } else {
            long fracId = id % 1000000;
            //TODO modulus operation is slow, so need better operation.
            return fractionIdStr("" + fracId);
        }
    }

    /**
     * If id is less than 6 digit than remaining field will be zero.
     * @param idStr
     * @return
     */
    public static String fractionIdStr(String idStr) {
        //TODO Better algorithm need to implement.
        if (idStr.length() == 1) {
            return "00000" + idStr;
        } else if (idStr.length() == 2) {
            return "0000" + idStr;
        } else if (idStr.length() == 3) {
            return "000" + idStr;
        } else if (idStr.length() == 4) {
            return "00" + idStr;
        } else if (idStr.length() == 5) {
            return "0" + idStr;
        } else if (idStr.length() == 6) {
            return idStr;
        } else {
            return "000000";
        }
    }

    public static int generatePin() {
        return 100000 + generator.nextInt(900000); 
    }
}
