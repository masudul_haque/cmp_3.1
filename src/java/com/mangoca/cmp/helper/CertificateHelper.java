/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.helper;

import com.mangoca.cmp.model.Certificate;
import com.mangoca.cmp.model.Mangocert;
import com.mangoca.cmp.service.ICertTariffService;
import com.mangoca.cmp.service.IMangoCertService;
import com.mangoca.cmp.service.IMangoSSLCertService;
import com.mangoca.cmp.service.spring.CertTariffSpringService;
import com.mangoca.cmp.service.spring.MangoCertSpringService;
import com.mangoca.cmp.service.spring.MangoSSLCertSpringService;
import com.mangoca.cmp.web.form.EnrolleduserForm;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author Masudul Haque
 */
public class CertificateHelper {
    public static void loadCertiticate(HttpServletRequest request) throws Exception{
        IMangoCertService service = MangoCertSpringService.getInstance();
        List mangoCerts = service.findAllMangocerts();
        request.setAttribute("mangoCerts", mangoCerts);
        IMangoSSLCertService sSLCertService = MangoSSLCertSpringService.getInstance();
        List sslCerts = sSLCertService.findAllMangoSSLCerts();
        request.setAttribute("sslCerts", sslCerts);
    }

    public static void loadCertTypes(HttpServletRequest request) {
        ICertTariffService service= CertTariffSpringService.getInstance();
        List<String> certTypes= service.findAllCertTypeNames();
        request.setAttribute("certTypes", certTypes);
    }
    
    public static void saveCertInfo(Certificate certificate, EnrolleduserForm enrollForm) throws IllegalAccessException, InvocationTargetException{
        
        BeanUtils.copyProperties(certificate, enrollForm);
        if (enrollForm.getCertId() != 0) {
            IMangoCertService mangoCertService = MangoCertSpringService.getInstance();
            Mangocert mangocert = mangoCertService.findMangocertById(enrollForm.getCertId());
            certificate.setCertClass(mangocert.getCertClass());                 
        }
       
    }
}
