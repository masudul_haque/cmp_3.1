/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 *
 * @author Masudul Haque
 */
public class MyConverter {

    static Logger log = Logger.getLogger(MyConverter.class.getName());

    public static short strToShort(String str) {

        short res = 0;
        if (str != null && !str.equals("")) {
            try {
                res = Short.parseShort(str);
            } catch (NumberFormatException nfex) {
                log.error("string to int exception for" + nfex);
            }
        }
        return res;
    }

    public static int strToInt(String str) {

        int res = 0;
        if (str != null && !str.equals("")) {
            try {
                res = Integer.parseInt(str);
            } catch (NumberFormatException nfex) {
                log.error("string to int exception for" + nfex);
            }
        }
        return res;
    }

    public static long strToLong(String str) {
        long res = 0;
        
        if (str != null && !str.equals("")) {
        try {
            res = Long.parseLong(str);
        } catch (NumberFormatException nfex) {
            log.error("string to long exception for" + nfex);
        }
        }
        return res;
    }

    public static Date strToDate(String dateStr, String pattern) {
        Date res = null;
        DateFormat format = new SimpleDateFormat(pattern);
        try {
            res = format.parse(dateStr);
            log.info("Converstion occured.");
        } catch (ParseException nfex) {
            log.error("string to date exception for" + nfex);
        }
        return res;
    }
}
