/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.helper;

import com.mangoca.cmp.service.ICertFieldService;
import com.mangoca.cmp.service.spring.CertFieldSpringService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Masudul Haque
 */
public class CertFieldHelper {
    public static void loadLocalities(HttpServletRequest request) throws Exception{
        ICertFieldService service=CertFieldSpringService.getInstance();
        List<String> localities= service.findAllLocalityNames();
        request.setAttribute("localities", localities);        
    }
}
