package com.mangoca.cmp.helper;

import com.mangoca.cmp.database.MailConstant;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailEngine {

    public static boolean isMailSendToUserAccount(final String mailSubject,
            final String mailTo, final String mailText) {

        Boolean isMailSend = false;
        String host = MailConstant.MAIL_HOST;
        String from = MailConstant.MAIL_MESSAGE_FROM;
        String subject = mailSubject;
        String messageText = mailText;
        boolean sessionDebug = false;
        // Create some properties and get the default Session.
        Properties props = System.getProperties();
        props.put("mail.host", host);
        props.put("mail.transport.protocol", "smtp");
        Session mailSession = Session.getDefaultInstance(props,
                null);

        mailSession.setDebug(sessionDebug);

        // Instantiate a new MimeMessage and fill it with the 
        // required information.
        try {
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(mailTo)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setText(messageText);

            Transport.send(msg);
            isMailSend = true;
        } catch (Exception exp) {
        }

        return isMailSend;
    }

    public static boolean isMailSendWithAttchement(final String mailSubject,
            final String mailTo, final String mailText, final String fileName, final String fileLocation) {

        Boolean isMailSend = false;
        String host = MailConstant.MAIL_HOST;
        String from = MailConstant.MAIL_MESSAGE_FROM;
        String subject = mailSubject;
        String messageText = mailText;
        boolean sessionDebug = false;
        // Create some properties and get the default Session.
        Properties props = System.getProperties();
        props.put("mail.host", host);
        props.put("mail.transport.protocol", "smtp");
        Session mailSession = Session.getDefaultInstance(props,
                null);

        mailSession.setDebug(sessionDebug);

        // Instantiate a new MimeMessage and fill it with the 
        // required information.
        try {
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(mailTo)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            
            
            MimeBodyPart messagePart = new MimeBodyPart();
            messagePart.setText(messageText);
 


            BodyPart attachmentPart = new MimeBodyPart();
            Multipart multipart = new MimeMultipart();

            DataSource source = new FileDataSource(fileLocation);
            attachmentPart.setDataHandler(new DataHandler(source));
            attachmentPart.setFileName(fileName);
          //  messageBodyPart.setText(messageText);
            
            multipart.addBodyPart(messagePart);
            multipart.addBodyPart(attachmentPart);
            
            
            msg.setContent(multipart);
            Transport.send(msg);
            isMailSend = true;
        } catch (Exception exp) {
        }

        return isMailSend;
    }
}
