/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.helper;

import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Masudul Haque
 */
public class DataHelper {
    
    public static  int getPage(HttpServletRequest request) {
        int page = 0;
        Enumeration paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String name = (String) paramNames.nextElement();
            if (name != null && name.startsWith("d-") && name.endsWith("-p")) {
                String pageValue = request.getParameter(name);
                if (pageValue != null) {
                    page = Integer.parseInt(pageValue) - 1;
                }
            }
        }
        return page;
    }

}
