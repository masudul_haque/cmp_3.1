/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mangoca.cmp.helper;

import com.mangoca.cmp.model.EmailControl;
import com.mangoca.cmp.model.UserAccount;
import com.mangoca.cmp.service.IEmailControlService;
import com.mangoca.cmp.service.spring.EmailControlSpringService;
import java.text.MessageFormat;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Masudul Haque
 */
public class MailHelper {
    
    public static String forgotPassMailBody(UserAccount userAccount, String msgBody,String password) throws Exception{
       
       // int length=password.length();
      //  String lastTwo=password.substring(length-3, length-1);
        String val=MessageFormat.format(msgBody, userAccount.getUserName(), password);
        
        return val;
    }
    
    public static String activationMailBody(UserAccount userAccount, String msgBody,String password) throws Exception{
       /* StringBuffer buffer= new StringBuffer();
        int length=password.length();
        String lastTwo=password.substring(length-3, length-1);
        buffer.append("Dear ").append(userAccount.getUserName()).append("\n\n")
               .append("Thank you for choosing Mango CA as your digital signature certificate provider. Please preserve the following information for future reference-")
               .append("\nYour Portal Username Login:").append(userAccount.getUserName())
               .append("\nYour Portal Password: ****"+lastTwo)
               .append("\nYour mobile number:").append(userAccount.getMobileNo())
               .append("\nYour Customer Identification Number (CIN):").append(userAccount.getUserCIN())
               .append("\n\nClick to following link to activate your email account.")
               .append("\nhttps://portal.mangoca.com/cmp/guestAction.do?method=mailActivation&validity=")
               .append(userAccount.getValidityNo())
               .append("\n\nThank you once again.")
               .append(footer());
                return buffer.toString();*/
        
        int length=password.length();
        String lastTwo=password.substring(length-3, length-1);
        
        
        String val=MessageFormat.format(msgBody, userAccount.getUserName(),
                        lastTwo, userAccount.getMobileNo(), userAccount.getUserCIN(), userAccount.getValidityNo());
        
        return val;
    }
    
    public static String certificateIssueMailBody( UserAccount userAccount, String msgBody, String password) throws Exception{
        /*
        StringBuffer buffer= new StringBuffer();
        
        int length=password.length();
        String lastTwo=password.substring(length-3, length-1);
        buffer.append("Dear ").append(userAccount.getUserName())
        .append("\n\nCongratulations.")
        .append("\n\nYou have successfully complied to all steps and requirements to receive a Mango Digital Signature Certificate. You can download your certificate from https://secure.mangoca.com")
        .append("Instructions to install/download your digital certificate: (Applicable to Class-1,2 & 3 Individual & Enterprise only)")
        .append("\n\nIf you enroll for a Soft Digital Signature Certificate:")
        .append("\n\n1. Install “Root CA” as following-")
        .append("\na. Click “Root CA” link")
        .append("\nb. A pop-up window appears. Select “Open”")
        .append("\nc. Click “Install Certificate” button on the Certificate window. Click “Next” and on the following screen choose “Place all certificates in the following store” and then select browse and from the browsed window select “Trusted Root Certification Authority”")
        .append("\nd. Click “Next” and “Finish”")
        .append("\ne. Your Root is installed automatically.")
        .append("\n\n2. Please use Internet Explorer Version-6 onwards if you are using a Microsoft Windows computer. If you are a Linux  user you can use Mozila-Firefox Browser Version 4 onwards. ")
        .append("\n\n3. Select “Create Browser Certificate” menu and login with the information provided below.")
        .append("\n\n4. For Microsoft Internet Explorer user- “Microsoft Enhanced Cryptographic 1.0” from Cryptographic Service Provider list")
        .append("\n\n5. Select “Key Size” as “2048” bit")
        .append("\n\n6. Then continue OK to generate certificate or download it.")
        .append("\n\nIf you enroll for a Hard Token Signature Certificate:")
        .append("\n1. Install “Root CA” as following-")
        .append("\na. Clock “Root CA” link")
        .append("\nb. A pop-up window appears. Select “Open”")
        .append("\nc. Click “Install Certificate” button on the Certificate window. Click “Next” and on the following screen choose “Place all certificates in the following store” and then select browse and from the browsed window select “Trusted Root Certification Authority”")
        .append("\nd. Click “Next” and “Finish”")
        .append("\ne. Your Root is installed automatically.")
        .append("\n\n2. Make sure before logging in and generating your certificate, you have installed your Hard Token Driver on your computer.")
        .append("\n\n3.	Please use Internet Explorer Version-6 onwards if you are using a Microsoft Windows computer. At present we do not support other Operating System.")
        .append("\n\n4.	Select “Create Browser Certificate” menu and login with the information provided below.")
        .append("\n\n5.	If you use Feitian Hard Token, Select (Based on your model)-")        
        .append("\na.	Entersafe ePass 1000auto CSP 1.0")        
        .append("\nb.	Entersafe ePass 3003 CSP 1.0")        
        .append("\nc.	Entersafe ePass 2003 CSP 1.0")        
        .append("\n\n6.	If you use Aladdin eToken Pro, Select")        
        .append("\na.	eToken base cryptographic provider")        
        .append("\n\n7.	Select “Key Size” as “2048” bit")        
        .append("\n\n8.	Then continue OK to generate certificate or download it.")        
        .append("\n\nFor Class-3 Server & Device (SSL certificates) certificates, please check our knowledge base located http://kb.mangoca.com")
        .append("\n\nYour Username: ").append(userAccount.getUserName())
        .append("\nYour One time Challenge Pass-phrase to use your certificate: ****").append(lastTwo)
        .append("\n\n*** Caution:")
        .append("\nYou can download/generate your certificate only once for a single application. You will not be able to download/generate your certificates if the certificate is already generated.")
        .append(footer());
                
        return buffer.toString();   
         * 
         */
        int length=password.length();
        String lastTwo=password.substring(length-3, length-1);
       
        String val=MessageFormat.format(msgBody, userAccount.getUserName(),
                        lastTwo, userAccount.getUserCIN());
        
        return val;
    }
    
    public static String footer(){
        StringBuffer buffer= new StringBuffer();
        buffer.append("\nFor technical support please contact us.")
        .append("\n\nBest Regards,")
        .append("\n\nMango Certifying Authority")
        .append("\n82, Mohakhali C/A (12th floor), Dhaka 1212, Bangladesh")
        .append("\nContact: (+880 2) 8814507, 9895712, 9892219, 9896913")
        .append("\nEmail: support@mangoca.com ");
        return buffer.toString();
    }

    public static String forgotUsernameMailBody(UserAccount userAccount, String msgBody) {
           String val=MessageFormat.format(msgBody, userAccount.getUserName());
        return val;
    }

    public static String certAttachMailBody(UserAccount userAccount, String msgBody) {
         String val=MessageFormat.format(msgBody, userAccount.getUserName());
        return val;
    }
    public static void loadEmailAttachments(HttpServletRequest request) throws Exception{
        IEmailControlService service= EmailControlSpringService.getInstance();
        List<EmailControl> emailControls= service.findAllClassesEmailControls();
        
        request.setAttribute("emailControls", emailControls);
    }
    
}
