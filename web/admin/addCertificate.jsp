<%-- 
    Document   : addCertificate
    Created on : Mar 14, 2012, 5:09:26 PM
    Author     : Masudul Haque
--%>


<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<script>  
    function copy_username()
    {   
        var str = $("#cp1").html();
        window.clipboardData.setData('Text',str);
        
    }
    function copy_email()
    {   
        var str = $("#cp_email").html();
        alert(str);
        window.clipboardData.setData('Text',str);
        
    }
    function copy_email_dom()
    {   
        var str = $("#cp_email_dom").html();
        window.clipboardData.setData('Text',str);
        
    }
    function copy_serial_no()
    {   
        var str = $("#cp_sn").html();
        window.clipboardData.setData('Text',str);
        
    }
    function copy_cn()
    {   
        var str = $("#cp_cn").html();
        window.clipboardData.setData('Text',str);
        
    }function copy_o()
    {   
        var str = $("#cp_o").html();
        window.clipboardData.setData('Text',str);
        
    }function copy_ou()
    {   
        var str = $("#cp_ou").html();
        window.clipboardData.setData('Text',str);
        
    }function copy_l()
    {   
        var str = $("#cp_l").html();
        window.clipboardData.setData('Text',str);
        
    }function copy_pc()
    {   
        var str = $("#cp_pc").html();
        window.clipboardData.setData('Text',str);
        
    }
    function copy_password ()
    {        
        var str = $("#cop11").val();
        window.clipboardData.setData('Text',str);       
    }    
</script>


<html:form action="CertificateAction" styleClass="form">   
    <c:choose>
        <c:when test="${!empty enrolleduser}">
            <html:hidden property="userCIN" value="${enrolleduser.userCIN}"/>
            <%@include file="../WEB-INF/jspf/admin/userSummery.jspf" %>
        </c:when>
        <c:otherwise>
            <%@include file="../WEB-INF/jspf/common/assignTo.jspf" %>
        </c:otherwise>
    </c:choose>

    <%@include file="../WEB-INF/jspf/admin/techCertInfo.jspf" %>
</table>    
<c:set var="sslCert" value="${enrolleduser.userSSLCert}"/>
<fieldset id="ssl">
    <legend>SSL Information</legend>
    <table  width="100%">
        <%@include file="../WEB-INF/jspf/admin/editSSLCertInfo.jspf" %>
    </table>
</fieldset>

<fieldset id="ssl">
    <legend>Technical Action</legend>
    <table  width="100%">
        <%@include file="../WEB-INF/jspf/admin/commonAction.jspf" %>
    </table>
</fieldset>
<table width="100%">    
    <tr>
        <td style="width: 33%">&nbsp;</td>
        <td>
            <html:submit value="Submit" style="width: 100px"/>
        </td>

    </tr>
</html:form>
