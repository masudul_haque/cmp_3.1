<%-- 
    Document   : adminSearch
    Created on : Nov 23, 2011, 9:00:02 PM
    Author     : hp
--%>
<%@page import="com.mangoca.cmp.database.DatabaseConstant"%>
<%@page import="com.mangoca.cmp.model.Enrolleduser"%>
<%@page import="java.util.List"%>
<%
   String userRole=(String)session.getAttribute("userRole");
   List<Enrolleduser> searchResult=(List<Enrolleduser>)request.getAttribute("searchOutput");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
        </table>    
         <table cellpadding="5" cellspacing="3" style="width: 100%">
            
                <tr>
                        <td id="tab-report-head" style="height: 35px">ID#</td>
                        <td id="tab-report-head" style="height: 35px">CIN</td>
                        <td id="tab-report-head" style="height: 35px">Name</td>
                        <td id="tab-report-head" style="height: 35px">Certificate Class</td>
                        <td id="tab-report-head" style="height: 35px"><%=userRole%> Status</td>
                        <td id="tab-report-head" style="height: 35px">Action</td>
                </tr> 
                
        <logic:iterate name="searchOutput" id="contact" indexId="cnt">
            <bean:define id="enrollId" name="contact" property="enrollId" scope="page" toScope="page"/>
            <tr>
                <td><bean:write name="contact" property="enrollId"/></td>
                <td><bean:write name="contact" property="trackId"/></td>
                <td><bean:write name="contact" property="fullName"/></td>
                <td><bean:write name="contact" property="certificateClass"/></td>
                <%
                  int complete=0; 
                  if(userRole.equals(DatabaseConstant.STATUS_RA_ADMIN)){
                      
                 %>
                <td><bean:write name="contact" property="raStatus"/></td>
                <td>
                 <logic:notEqual name="contact" property="raStatus" value="Completed">   
                 <html:link page="/adminAction.do?method=goSearchAction" paramName="enrollId" paramId="enrollId">Action</html:link>
                 </logic:notEqual>
                <%
                 }
                 else if(userRole.equals(DatabaseConstant.STATUS_ACCOUNT_ADMIN)){
                 %>
                <td><bean:write name="contact" property="accountStatus"/></td>
                <td>
                 <logic:notEqual name="contact" property="accountStatus" value="Completed">   
                 <html:link page="/adminAction.do?method=goSearchAction" paramName="enrollId" paramId="enrollId">Action</html:link>
                 </logic:notEqual> 
                <%
                 }
                 else if(userRole.equals(DatabaseConstant.STATUS_VALIDATION_ADMIN)){
                 %>
                <td><bean:write name="contact" property="vaStatus"/></td>
                <td>
                 <logic:notEqual name="contact" property="vaStatus" value="Completed">   
                 <html:link page="/adminAction.do?method=goSearchAction" paramName="enrollId" paramId="enrollId">Action</html:link>
                 </logic:notEqual> 
                <%
                 }
                 else if(userRole.equals(DatabaseConstant.STATUS_TECHNICAL)){
                 %>
                <td><bean:write name="contact" property="technicalStatus"/></td>
                <td>
                 <logic:notEqual name="contact" property="technicalStatus" value="Completed">   
                 <html:link page="/adminAction.do?method=goSearchAction" paramName="enrollId" paramId="enrollId">Action</html:link>
                 </logic:notEqual><%
                 }
                %> 
               <html:link page="/adminAction.do?method=goDetailsAction" paramName="enrollId" paramId="enrollId">Details</html:link>
              </td> 
            
              <%--  <td><a href="admin/adminAction.jsp?method=goSearchResult&abc=<%=result%>" >Action</a></td>--%>
            </tr>      
           
        </logic:iterate>
                
         </table>
    </body>
</html>
