<%-- 
    Document   : addDoc
    Created on : Mar 11, 2012, 1:37:37 PM
    Author     : Masudul Haque
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<script language="javascript" type="text/javascript">
    function confirmSubmit() {
       // parent.overview.refresh = true;
        document.forms[0].action="documentAction.do?method=addDocsSubmit"; 
        document.forms[0].submit();
        return confirm('Are you sure to save?'); 
    }	 
</script>
 
<html:form action="documentAction" method="post" enctype="multipart/form-data">
    <%@include file="../WEB-INF/jspf/common/assignTo.jspf" %>
    <%@include file="../WEB-INF/jspf/admin/raGenUpload.jspf"%>
    <%@include file="../WEB-INF/jspf/admin/commonAction.jspf" %>
    <tr>
        <td>&nbsp;</td>
        <td>
            <html:submit value="Submit" onclick="confirmSubmit()" style="width: 100px"/>
        </td></tr>
</html:form>
