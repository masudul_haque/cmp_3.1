<%-- 
    Document   : adminDashboard
    Created on : Nov 23, 2011, 12:54:35 PM
    Author     : hp
--%>

<%@page import="com.mangoca.cmp.model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>


<c:set var="admin" value="<%=User.ADMIN_ADMIN%>"/>
<c:set var="ra" value="<%=User.ADMIN_RA%>"/>
<c:set var="acc" value="<%=User.ADMIN_ACC%>"/>
<c:set var="va" value="<%=User.ADMIN_VA%>"/>
<c:set var="tech" value="<%=User.ADMIN_TECH%>"/>
<c:set var="cc" value="<%=User.ADMIN_CC%>"/>

<c:if test="${userRole==ra || userRole==tech || userRole==acc || userRole==va}">
    <%@include file="../WEB-INF/jspf/admin/adminReqest.jspf"%> 
</c:if>

<c:if test="${userRole==ra || userRole==tech || userRole==cc}">

    <%@include file="../WEB-INF/jspf/admin/revokeReqests.jspf"%> 
    <%@include file="../WEB-INF/jspf/admin/suspendReqests.jspf"%> 
    <%@include file="../WEB-INF/jspf/admin/renewalReqests.jspf"%> 
</c:if>


<%--@include file="../WEB-INF/jspf/admin/adminApplication.jspf"--%> 
<%--@include file="../WEB-INF/jspf/admin/defaultSearch.jspf" --%>
