<%-- 
    Document   : editPayment
    Created on : Mar 11, 2012, 1:37:53 PM
    Author     : Masudul Haque
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<html:form action="PaymentAction" styleClass="form">
    <input type="hidden" name="paymentId" value="${payment.id}"/>
    <html:hidden property="userCIN" value="${payment.userCIN}"/>
    <c:choose>
        <c:when test="${!empty enrolleduser}">
            <%@include file="../WEB-INF/jspf/admin/userSummery.jspf" %>
        </c:when>
        <c:otherwise>
            <%@include file="../WEB-INF/jspf/common/assignTo.jspf" %>
        </c:otherwise>
    </c:choose>
    <%@include file="../WEB-INF/jspf/admin/editPayment.jspf" %>   
    <tr>
        <td id="item"><bean:message key="label.comment"/></td>
        <td><html:textarea property="comment" rows="5" cols="20" value="${payment.comment}"/></td> 
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <html:submit value="Save" style="width: 80px"/>
        </td>
    </tr>
</html:form>


