<%-- 
    Document   : accPayment
    Created on : Mar 6, 2012, 10:14:42 PM
    Author     : Masudul Haque
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

    <c:set var="enrolleduser" value="${sessionScope.enrolleduser}"/>
    <%@include file="../WEB-INF/jspf/admin/userSummery.jspf" %>
    <html:form action="paymentSubmit">
        <%@include file="../WEB-INF/jspf/admin/addPayment.jspf"%>
        <%@include file="../WEB-INF/jspf/admin/commonAction.jspf" %>
        
        <tr>
            <td>&nbsp;</td>
            <td colspan="3" height="5">
                <html:submit value="Submit" style="width: 80px"/>
        </td></tr>
    </html:form>
