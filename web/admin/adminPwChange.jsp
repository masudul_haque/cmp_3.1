<%-- 
    Document   : changePassword
    Created on : Mar 14, 2012, 9:17:41 PM
    Author     : Masudul Haque
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>


<html:form action="adminPwChange">
<%@include file="../WEB-INF/jspf/common/pwChange.jspf" %>
</html:form>

