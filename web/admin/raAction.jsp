<%-- 
    Document   : raUpload
    Created on : Nov 27, 2011, 5:41:30 PM
    Author     : hp
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<c:set var="enrolleduser" value="${sessionScope.enrolleduser}"/>
<%@include file="../WEB-INF/jspf/admin/userSummery.jspf" %>
<html:form action="raActionSubmit" method="post" enctype="multipart/form-data">
    <%@include file="../WEB-INF/jspf/admin/raGenUpload.jspf" %>
    <%@include file="../WEB-INF/jspf/admin/commonAction.jspf" %>
    <tr>
        <td>&nbsp;</td>   
        <td height="5" >
            <html:submit value="Submit" style="width: 80px"/>
        </td>
    </tr> 
</html:form>
