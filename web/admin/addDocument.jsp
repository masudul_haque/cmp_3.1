<%-- 
    Document   : addDocuments
    Created on : Mar 14, 2012, 5:07:47 PM
    Author     : Masudul Haque
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<script language="javascript" type="text/javascript">
    function confirmSubmit() {
        document.forms[0].action="DocumentAction.do?method=save";   
        document.forms[0].submit();
        return confirm('Are you sure to save?'); 
    }	 
</script> 
<html:form action="DocumentAction" method="post" enctype="multipart/form-data">
    <c:choose>
        <c:when test="${!empty enrolleduser}">            
            <html:hidden property="userCIN" value="${enrolleduser.userCIN}"/>
            <%@include file="../WEB-INF/jspf/admin/userSummery.jspf" %> 
        </c:when>
        <c:otherwise>
            <%@include file="../WEB-INF/jspf/common/assignTo.jspf" %>
        </c:otherwise>
    </c:choose>

    <%@include file="../WEB-INF/jspf/admin/raGenUpload.jspf"%>
    <%@include file="../WEB-INF/jspf/admin/commonAction.jspf" %>
    <tr>
        <td>&nbsp;</td>
        <td>
            <html:submit value="Submit" onclick="confirmSubmit()" style="width: 100px"/>
        </td></tr>
</html:form>