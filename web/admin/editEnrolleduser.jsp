<%-- 
    Document   : editEnrolledUser
    Created on : Mar 14, 2012, 11:42:16 PM
    Author     : Masudul Haque
--%>


<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<c:choose>
    <c:when test="${!empty enrolleduser}">
        <script language="javascript" type="text/javascript">
    function confirmSubmit() {
       // parent.overview.refresh = true;
        document.forms[0].action="EnrolleduserAction.do?method=save"; 
        document.forms[0].submit();
        return confirm('Are you sure to save?'); 
    }	 
</script>

        <html:form action="EnrolleduserAction">
            <%--
            <input type="hidden" name="enrollId" value="${enrolleduser.enrollId}"/>
            --%>
            
            <c:set var="certificate" value="${enrolleduser.certificate}"/>
            <c:set var="userbasic" value="${enrolleduser.userbasic}"/>
            <%@include file="../WEB-INF/jspf/admin/editCertClass.jspf" %>
            <%@include file="../WEB-INF/jspf/admin/editEnrollInfo.jspf" %>
            <%@include file="../WEB-INF/jspf/admin/editCertInfo.jspf" %>
            <%@include file="../WEB-INF/jspf/admin/editBasicInfo.jspf" %>   
            
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <html:submit value="Save" style="width: 80px" onclick="confirmSubmit()"/>
                </td>
            </tr>
        </html:form>
    </c:when>
    <c:otherwise>
        <tr><td colspan="2">No Enrolled User</td></tr>
    </c:otherwise>
</c:choose>


