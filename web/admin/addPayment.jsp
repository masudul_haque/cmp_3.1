<%-- 
    Document   : addPayment
    Created on : Mar 11, 2012, 1:37:23 PM
    Author     : Masudul Haque
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>


<html:form action="PaymentAction" styleClass="form">   
    <c:choose>
        <c:when test="${!empty enrolleduser}">
            <html:hidden property="userCIN" value="${enrolleduser.userCIN}"/>
            <%@include file="../WEB-INF/jspf/admin/userSummery.jspf" %>
        </c:when>
        <c:otherwise>
            <%@include file="../WEB-INF/jspf/common/assignTo.jspf" %>
        </c:otherwise>
    </c:choose>

    <%@include file="../WEB-INF/jspf/admin/addPayment.jspf"%>
    <%@include file="../WEB-INF/jspf/admin/commonAction.jspf" %>
    <tr>
        <td>&nbsp;</td>
        <td>
            <html:submit value="Submit" onclick="confirmSubmit()" style="width: 100px"/>
        </td></tr>
</html:form>