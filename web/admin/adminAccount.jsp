<%-- 
    Document   : adminAccount
    Created on : Mar 6, 2012, 4:11:41 PM
    Author     : Masudul Haque
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:choose>
    <c:when test="${!empty user}">
        <%@include file="../WEB-INF/jspf/admin/adminAccountInfo.jspf" %>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="button" style="width: 130px" value="Change Password" 
                       onclick="location.href='adminAction.do?method=pwChange'"/>
            </td>
        </tr>
    </c:when>
    <c:otherwise>
        <tr>
            <td colspan="2">No information is available.</td>
        </tr>
    </c:otherwise>
</c:choose>
