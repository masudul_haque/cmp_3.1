<%-- 
    Document   : vaAction
    Created on : Mar 8, 2012, 11:22:18 AM
    Author     : Masudul Haque
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<html:form action="ValidationAction" styleClass="form">
    <c:choose>
        <c:when test="${!empty enrolleduser}">            
            <html:hidden property="userCIN" value="${enrolleduser.userCIN}"/>
            <%@include file="../WEB-INF/jspf/admin/userSummery.jspf" %> 
            <%@include file="../WEB-INF/jspf/user/userDocuments.jspf" %>
        </c:when>
        <c:otherwise>
            <%@include file="../WEB-INF/jspf/common/assignTo.jspf" %>
        </c:otherwise>
    </c:choose>
    <%@include file="../WEB-INF/jspf/admin/commonAction.jspf" %>
    <tr>
        <td>&nbsp;</td>   
        <td height="5" >
            <html:submit value="Submit" style="width: 80px"/>
        </td>
    </tr>
</html:form>
