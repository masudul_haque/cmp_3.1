<%-- 
    Document   : adminAction
    Created on : Nov 25, 2011, 12:27:57 PM
    Author     : hp
--%>

<%@page import="com.mangoca.cmp.database.Constants"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<c:set var="ra" value="<%=Constants.ADMIN_RA%>"/>
<c:set var="acc" value="<%=Constants.ADMIN_ACC%>"/>
<c:set var="va" value="<%=Constants.ADMIN_VA%>"/>
<c:set var="tech" value="<%=Constants.ADMIN_TECH%>"/>
    <c:set var="enrolleduser" value="${sessionScope.enrolleduser}"/>
    <%@include file="../WEB-INF/jspf/user/enrollInfo.jspf" %>
    <html:form action="actionSubmit">   
        <%@include file="../WEB-INF/jspf/admin/commonAction.jspf" %>
        <tr>
            <td colspan="2" height="5" align="center">
                <html:submit value="Submit" style="width: 100px"/>
            </td>
        </tr> 
    </html:form>
    <%--
    <c:choose>
        <c:when test="${userRole==ra}">
            <html:form action="actionSubmit" method="post" enctype="multipart/form-data">
                <%@include file="../WEB-INF/jspf/admin/raAction.jspf" %>    
                <%@include file="../WEB-INF/jspf/admin/commonAction.jspf" %>
                <tr>
                    <td colspan="2" height="5" align="center">
                        <html:submit value="Submit" style="width: 100px"/>
                    </td>
                </tr> 
            </html:form>
        </c:when>
        <c:when test="${userRole==acc}">
        </c:when>
        <c:when test="${userRole==va}">
        </c:when>
        <c:when test="${userRole==tech}">
        </c:when>
        <c:otherwise>
        </c:otherwise>
    </c:choose>
    --%>
   