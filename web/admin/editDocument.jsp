
<%-- 
    Document   : editDoc
    Created on : Mar 11, 2012, 1:38:09 PM
    Author     : Masudul Haque
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<c:choose>
    <c:when test="${!empty document}">
        <tr>
            <td colspan="2">
                <html:image src="${document.documentLink}" alt="No image"/>
            </td>
        </tr>
        <script language="javascript" type="text/javascript">
    function confirmSubmit() {
       // parent.overview.refresh = true;
        document.forms[0].action="DocumentAction.do?method=save"; 
        document.forms[0].submit();
        return confirm('Are you sure to save?'); 
    }	 
</script>

        <html:form action="editDocSubmit" method="post" enctype="multipart/form-data">
            <html:hidden property="documentId" value="${document.documentId}"/>
            <%@include file="../WEB-INF/jspf/admin/editDoc.jspf" %>   
            <tr>
                <td id="item"><bean:message key="label.raComment"/></td>
                <td><html:textarea property="raComment" rows="5" cols="20" value="${document.raComment}"/></td> 
            </tr>
            <tr>
                <td id="item">Change</td>
                <td><html:file property="formFile"/></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <html:submit value="Update" style="width: 80px"/>
                </td>
            </tr>
        </html:form>
    </c:when>
    <c:otherwise>
        <tr><td colspan="2">No Document</td></tr>
    </c:otherwise>
</c:choose>

