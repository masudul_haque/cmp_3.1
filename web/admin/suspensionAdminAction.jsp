<%-- 
    Document   : suspensionAdminAction
    Created on : Mar 8, 2012, 11:40:11 AM
    Author     : Masudul Haque
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<html:form action="suspensionAdminSubmit">
    <c:if test="${!empty enrolleduser}">            
        <html:hidden property="userCIN" value="${enrolleduser.userCIN}"/>
        <%@include file="../WEB-INF/jspf/user/enrollInfo.jspf" %>
    </c:if>
    <%@include file="../WEB-INF/jspf/admin/commonAction.jspf" %>
    <tr>
        <td colspan="2" height="5" align="center">
            <html:submit value="Submit" style="width: 100px"/>
        </td>
    </tr> 
</html:form>
