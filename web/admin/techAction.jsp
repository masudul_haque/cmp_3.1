<%-- 
    Document   : techAction
    Created on : Mar 8, 2012, 11:22:33 AM
    Author     : Masudul Haque
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<%@include file="../WEB-INF/jspf/admin/userSummery.jspf" %>
<html:form action="techActionSubmit">
    <c:set var="cert" value="${enrolleduser.certificate}"/>
    <%@include file="../WEB-INF/jspf/user/certInfo.jspf" %>
    <%@include file="../WEB-INF/jspf/admin/commonAction.jspf" %>
    <tr>
        <td>&nbsp;</td>   
        <td height="5" >
            <html:submit value="Submit" style="width: 80px"/>
        </td>
    </tr> 
</html:form>