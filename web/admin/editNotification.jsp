
<%-- 
    Document   : editDoc
    Created on : Mar 11, 2012, 1:38:09 PM
    Author     : Masudul Haque
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<html:form action="NotificationAction" styleClass="form"> 

    <c:choose>
        <c:when test="${!empty notification}">
            <html:hidden property="userCIN" value="${notification.userCIN}"/>
            <tr>
                <td id="item"><bean:message key="label.userCIN"/></td>
                <td><c:out value="${notification.userCIN}"/></td>
                <td style="color: red"></td>
            </tr>
        </c:when>
        <c:otherwise>            
            <%@include file="../WEB-INF/jspf/common/assignTo.jspf" %>
        </c:otherwise>
    </c:choose>

    <%@include file="../WEB-INF/jspf/admin/editNotification.jspf" %>   

    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <html:submit value="Save" style="width: 80px"/>
        </td>
    </tr>
</html:form>