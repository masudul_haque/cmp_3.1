<%-- 
    Document   : forgot
    Created on : Mar 13, 2012, 7:52:59 AM
    Author     : Masudul Haque
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.mangoca.cmp.database.Constants"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

        <html:form action="forgotSubmit">
                    <tr>
                        <td colspan="2"><html:errors/></td>
                    </tr> 
                    <tr>
                        <td style="width: 33%" id="item"><bean:message key="label.forgotOption"/></td>
                        <td>
                            <html:radio property="forgotOption" style="width: 15px" value="<%=Constants.FORGOT_USERNAME%>" />&nbsp;<bean:message key="label.forgotUsername"/>&nbsp;
                            <html:radio property="forgotOption" style="width: 15px" value="<%=Constants.FORGOT_PASSWORD%>"/>&nbsp;<bean:message key="label.forgotPassword"/>
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="width: 33%" id="item"><bean:message key="label.userCIN"/></td>
                        <td style="width: 25px"><html:text property="userCIN"/></td>
                    <tr>
                    <tr>
                        <td style="width: 33%" id="item"><bean:message key="label.emailAccount"/></td>
                        <td style="width: 25px"><html:text property="emailAccount"/></td>
                    </tr>
                    <c:if test="${!empty submitFailed}">
                       <tr>
                        <td colspan="2" style="color: red">&nbsp;${submitFailed}</td>
                    </tr> 
                    </c:if>
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr> 
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <html:submit style="width: 100px" value="Submit"/></td>
                    </tr>
                    
                    
           </html:form>