<%-- 
    Document   : ticketAction
    Created on : Mar 7, 2012, 9:06:20 AM
    Author     : Masudul Haque
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<c:choose>
    <c:when test="${!empty enrolleduser && !empty ticket}">

        <%@include file="../WEB-INF/jspf/user/enrollInfo.jspf" %>
        <%@include file="../WEB-INF/jspf/common/newTicketInfo.jspf" %>
        <script language="javascript" type="text/javascript">
            function confirmSubmit() {
                // parent.overview.refresh = true;
                document.forms[0].action="TicketAction.do?method=reply"; 
                document.forms[0].submit();
                return confirm('Are you sure to submit?'); 
            }	 
        </script>
        <html:form action="TicketAction">
            <%@include file="../WEB-INF/jspf/cc/addCcAction.jspf" %>
            <tr><td colspan="2" align="center">
                    <html:submit value="Submit" style="width: 100px" onclick="confirmSubmit()"/>
                </td></tr>
            </html:form>
        </c:when>
        <c:otherwise>
        <tr>
            <td>No Data</td>
        </tr>
    </c:otherwise>
</c:choose>
