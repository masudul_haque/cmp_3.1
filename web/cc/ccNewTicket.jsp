<%-- 
    Document   : ccNewTicket
    Created on : Mar 10, 2012, 9:29:26 PM
    Author     : Masudul Haque
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<html:form action="TicketAction">
    <%@include file="../WEB-INF/jspf/common/assignTo.jspf" %>
    <%@include file="../WEB-INF/jspf/common/addNewTicket.jspf" %>
    <tr><td colspan="2" align="center">
    <html:submit value="Submit" style="width: 100px"/>
    </td></tr>
</html:form>