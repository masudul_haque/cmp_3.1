<%-- 
    Document   : login
    Created on : Nov 22, 2011, 5:22:40 PM
    Author     : hp
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<html:form action="loginAction">
    <tr>
        <td colspan="2"><html:errors/></td>
    </tr> 
    <tr>
        <td style="width: 25px" id="item"><bean:message key="label.userName"/></td>
        <td style="width: 25px"><html:text property="userName" /></td>
    </tr>
    <tr>
        <td style="width: 25px" id="item"><bean:message key="label.password"/></td>
        <td style="width: 25px"><html:password property="password"/></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr> 

    <tr>
        <td>&nbsp;</td>
        <td>
            <html:submit style="width: 100px" value="Login"/></td>
    </tr>
</html:form>