
<%@page import="com.mangoca.cmp.model.Certificate"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<c:set var="certActive" value="<%=Certificate.CERT_ACTIVE%>"/>
<c:set var="revocation" value="${enrolleduser.revocation}"/>
<c:set var="certRevoked" value="<%=Certificate.CERT_REVOKED%>"/>
<c:set var="applyRevoke" value="<%=Certificate.CERT_APPLY_REVOKE%>"/>
<tr>
    <td colspan="3">
        <p>
            <bean:message key="revoke.user.msg.header"/>
        </p></td>
</tr>

<c:set var="certStatus" value="${enrolleduser.certificate.certStatus}"/> 
<c:choose>
    <%@include file="../WEB-INF/jspf/user/certNewRevoke.jspf" %>
    <%@include file="../WEB-INF/jspf/user/certSuspendTest.jspf" %>
    <c:when test="${certStatus==certActive}">
        <%@include file="../WEB-INF/jspf/user/enrollInfo.jspf" %>
        <html:form action="RevocationAction" styleClass="form">
            <tr>
                <td colspan="2" align="center" height="3">&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 33%" id="item"><b>Revocation Reasons:</b>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <%@include file="../WEB-INF/jspf/user/requestReasons.jspf" %>
                </td>
            </tr>
            <tr>
                <td style="width: 33%" id="item">Comment</td>
                <td><html:textarea property="revokComment"  rows="15" cols="30"/></td>
            </tr>
            <tr>
                <td style="height: 5">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <html:submit style="width: 100px" value="Submit" onclick="confirmSubmit()"/>
                </td>
            </tr>
        </html:form>
    </c:when>

    <c:when test="${certStatus==certRevoked}">
        <tr>
            <td colspan="2" style="height: 15"><bean:message key="revoke.user.msg.revoked"/></td>
        </tr>
        <%@include file="../WEB-INF/jspf/user/revokeInfo.jspf" %>
    </c:when>
    <c:when test="${certStatus==applyRevoke}">
        <tr>
            <td colspan="2" style="text-align: left;width: 100%"><br/>
                <bean:message key="revoke.user.msg.apply.revoke"/></td>
        </tr>
        <%@include file="../WEB-INF/jspf/user/revokeInfo.jspf" %>
    </c:when>
    <c:otherwise>
        <%@include file="../WEB-INF/jspf/user/applyReq.jspf" %>
    </c:otherwise>
</c:choose>

