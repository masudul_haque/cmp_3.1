
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<table>
    <tr>
        <td width="100%" >
            <div class="instruction">
                <p><bean:message key="instruction.add.pin"/></p>
            </div> 
        </td>
    </tr>
</table>           
<script>
    function confirmSubmit(){                        
        document.forms[0].action="PinAction.do?method=save"; 
        document.forms[0].submit();
    }
</script>              
<c:choose>
    <c:when test="${!empty userAccount}">
        <table> 
            <html:form action="PinAction">

                <html:hidden property="userCIN" value="${userAccount.userCIN}"/>
                <html:hidden property="mobileNo" value="${userAccount.mobileNo}"/>
                <%@include file="../WEB-INF/jspf/user/addPinInfo.jspf" %> 
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td></td> 
                    <td>
                        <html:submit value="Submit" onclick="confirmSubmit()" style="width: 100px"/>
                    </td>
                </tr>
            </html:form>
        </table>
    </c:when>
    <c:otherwise>
        User does not exist.
    </c:otherwise>
</c:choose>