<%-- 
    Document   : documentList
    Created on : Mar 13, 2012, 12:48:16 PM
    Author     : Masudul Haque
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>


<c:choose>
    <c:when test="${!empty enrolleduser}">
        <%@include file="../WEB-INF/jspf/user/document.jspf" %>
    </c:when>
    <c:otherwise>
        <%@include file="../WEB-INF/jspf/user/applyReq.jspf" %>
    </c:otherwise>
</c:choose>