<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<c:set var="enroll" value="${sessionScope.enrolleduser}"/>
    <c:choose>
        <c:when test="${!empty enroll}">
            <%@include file="../WEB-INF/jspf/user/statusDetails.jspf" %>
        </c:when>
        <c:otherwise>
            <%@include file="../WEB-INF/jspf/user/applyReq.jspf" %>
        </c:otherwise>
    </c:choose>
