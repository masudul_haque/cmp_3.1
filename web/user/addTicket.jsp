<%@page import="com.mangoca.cmp.model.User"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<c:set var="cc" value="<%=User.ADMIN_CC%>"/>
<html:form action="TicketAction" styleClass="form" >
    <c:choose>
        <c:when test="${cc==userRole}">

            <%@include file="../WEB-INF/jspf/common/assignTo.jspf" %>
        </c:when>
        <c:otherwise>
            <c:if test="${!empty enrolleduser}">            
                <%@include file="../WEB-INF/jspf/user/enrollInfo.jspf" %> 
            </c:if>
        </c:otherwise>
    </c:choose>
    
    <%@include file="../WEB-INF/jspf/common/addNewTicket.jspf" %>
    <tr>
        <td colspan="2">&nbsp;</td> 
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <html:submit value="Submit" style="width: 100px" />
        </td>
    </tr>
</html:form>

