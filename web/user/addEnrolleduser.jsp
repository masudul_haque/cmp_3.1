<%-- 
    Document   : newEnrollment
    Created on : Dec 11, 2011, 8:46:10 PM
    Author     : hp
--%>

<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:choose>
    <c:when test="${empty enrolleduser}">

        <script language="JavaScript">
            function validate()
            {
                try
                {
                    with( document.EnrolleduserForm )
                    {
                        if( certId.value == "" )
                        {
                            throw "Please select your certificate class.";
                        }  
                        if( certEmail.value == "" || certEmailDomain.value== "" )
                        {
                            throw "Please provide certificate email.";
                        }  
                        if( verifyEmail.value == "" || verifyEmailDomain.value== "" )
                        {
                            throw "Please confirm the certificate email.";
                        }
                        if(certEmail.value != verifyEmail.value || certEmailDomain.value != verifyEmailDomain.value){
                            throw "Certificate email addresses are mis-matched.";
                        }
                        if( certPassword.value == "") 
                        {
                            throw "Please provide certificate password.";
                        }
                        if( verifyPassword.value == "")
                        {
                            throw "Please re-type certificate password.";
                        }
                        if(certPassword.value !=verifyPassword.value){
                            throw "Certificate passwords are mis-matched.";
                        }
                        if( commonName.value == "")
                        {
                            throw "Please provide common name.";
                        }
                        if( organization.value == "")
                        {
                            throw "Please provide organization.";
                        }
                        if( orgUnit.value == "")
                        {
                            throw "Please provide organization unit.";
                        }
                        if( postalCode.value == "" || 
                            isNaN( postalCode.value ) || 
                            postalCode.value.length != 4 )
                        {
                            throw "Please provide postal code in the format ####";
                        }	
                    }
                }
                catch( error )
                {
                    alert( error );
                    return( false );
                }
        
                document.forms[0].action="EnrolleduserAction.do?method=save"; 
                document.forms[0].submit();
            }
        </script>
        <html:form  action="EnrolleduserAction" onsubmit="return validate()">
        </table>
        <fieldset>
            <legend>Certificate Information</legend>
            <table>
                <%@include file="../WEB-INF/jspf/user/enrollHeader.jspf" %>
                <%@include file="../WEB-INF/jspf/user/certClass.jspf" %>
                <%@include file="../WEB-INF/jspf/user/addEnrollInfo.jspf" %>
                <%@include file="../WEB-INF/jspf/user/addCertInfo.jspf" %>
            </table>
        </fieldset>
        <fieldset id="ssl">
            <legend>SSL Information</legend>
            <table  width="100%">
                <%@include file="../WEB-INF/jspf/user/addSSLCertInfo.jspf" %>
            </table>
        </fieldset>
        <fieldset>
            <legend>Other Information</legend>
            <table>
                <%@include file="../WEB-INF/jspf/user/addBasicInfo.jspf" %>
            </table>
        </fieldset>
        <table width="100%">
            <tr>
                <td colspan="3">&nbsp;</td> 
            </tr>
            <tr>
                <td style="width: 33%">&nbsp;</td>  
                <td>
                    <html:submit value="submit" style="width: 100px"/>
                </td>
                <td>&nbsp;</td> 
            </tr>
        </html:form>     
    </c:when>
    <c:otherwise>
        <script>
            setTimeout(function() {
                window.location.href = "userMenuAction.do?method=userHome";
            }, 50000);
        </script>
        <%@include file="../WEB-INF/jspf/user/enrollInfo.jspf" %> 
        <c:set var="userbasic" value="${enrolleduser.userbasic}"/>
        <c:set var="cert" value="${enrolleduser.certificate}"/>
        <%@include file="../WEB-INF/jspf/user/basicInfo.jspf" %>  
        <%@include file="../WEB-INF/jspf/user/certInfo.jspf" %>
    </c:otherwise>
</c:choose>
