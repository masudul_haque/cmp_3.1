<%@page import="com.mangoca.cmp.model.Certificate"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib  uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<c:set var="applyRenew" value="<%=Certificate.CERT_APPLY_RENEW%>"/>
<c:set var="certActive" value="<%=Certificate.CERT_ACTIVE%>"/>
<c:set var="renewal" value="${enrolleduser.renewal}"/>
 <tr>
        <td colspan="2">
            <p>
                <bean:message key="renew.user.msg.header"/>
            </p>
        </td>
    </tr>
    <c:set var="certStatus" value="${enrolleduser.certificate.certStatus}"/>
    <c:choose>
        <%@include file="../WEB-INF/jspf/user/certNewRenew.jspf" %>
        <%@include file="../WEB-INF/jspf/user/certRevokeTest.jspf" %>
        <%@include file="../WEB-INF/jspf/user/certSuspendTest.jspf" %>
        <c:when test="${certStatus==certActive && empty renewal}">
            <script language="javascript" type="text/javascript">
            function confirmSubmit() {
                // parent.overview.refresh = true;
                document.forms[0].action="RenewalAction.do?method=save"; 
                document.forms[0].submit();
                return confirm('Are you sure to submit?'); 
            }	 
        </script>
            <html:form action="RenewalAction">   

                <tr>
                    <td colspan="2" align="center" height="3">&nbsp;</td>
                </tr>
                
                <%@include file="../WEB-INF/jspf/user/renewalHeader.jspf" %>
                <%@include file="../WEB-INF/jspf/user/certClass.jspf" %>
                <%@include file="../WEB-INF/jspf/user/addEnrollInfo.jspf" %>
                <%@include file="../WEB-INF/jspf/user/addCertInfo.jspf" %>


                <tr>
                    <td style="width: 33%" id="item">Comment</td>
                    <td><html:textarea property="renewComment"  rows="15" cols="30"/></td>
                </tr>
                <tr>
                    <td style="height: 5">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <html:submit style="width: 100px" value="Submit" onclick="confirmSubmit()"/>
                    </td>
                </tr>
            </html:form>
        </c:when> 
            <c:when test="${certStatus==certActive && !empty renewal}">
            <tr>
                <td colspan="2"><h3>
                        <p>You already apply for renewal with following information.</p>
                    </h3>
                </td>
            </tr>
            <%@include file="../WEB-INF/jspf/user/renewalInfo.jspf" %>
        </c:when>
        <c:otherwise>
            <%@include file="../WEB-INF/jspf/user/applyReq.jspf" %>
        </c:otherwise>
    </c:choose>