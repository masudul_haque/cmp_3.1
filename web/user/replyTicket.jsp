<%-- 
    Document   : replyTicket
    Created on : Mar 11, 2012, 7:47:32 AM
    Author     : Masudul Haque
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<c:choose>
    <c:when test="${!empty enrolleduser && !empty clientticket}">

        <%@include file="../WEB-INF/jspf/user/enrollInfo.jspf" %>
        <%@include file="../WEB-INF/jspf/common/newTicketInfo.jspf" %>

        <html:form action="userReplySubmit">

            <tr>
                <td style="width: 33%" id="item">Message</td>
                <td><html:textarea property="message" rows="15" cols="25"/></td>
                <td style="width: 30%">&nbsp;</td>
            </tr> 
            <tr>
                <td colspan="2">&nbsp;</td> 
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <html:submit value="Submit" style="width: 100px"/>
                </td>
            </tr>
            </html:form>
        </c:when>
        <c:otherwise>
        <tr>
            <td>No Data</td>
        </tr>
    </c:otherwise>
</c:choose>

