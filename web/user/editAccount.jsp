<%-- 
    Document   : editAccount
    Created on : Mar 6, 2012, 4:44:19 PM
    Author     : Masudul Haque
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib  uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
    <html:form action="accEditSubmit"> 
        <tr>
            <td id="item"><bean:message key="label.fullName"/></td>
            <td><html:text property="fullName" value="${userAccount.fullName}"/></td>
            <td style="color: red"><html:errors property="fullName"/></td>
        </tr>
        <%@include file="../WEB-INF/jspf/common/editCommonAccount.jspf" %>
                        <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><html:submit style="width:100px" property="Submit"/></td>
                    <td>&nbsp;</td>
                </tr>
       </html:form>