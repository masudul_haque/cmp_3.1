<%-- 
    Document   : ticketList
    Created on : Mar 5, 2012, 10:28:08 PM
    Author     : Masudul Haque
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib  uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%--
<script>
    function newTicket(){
        location.href="userAction.do?method=newTicket";
    }
</script>
--%>
<c:set var="enroll" value="${sessionScope.enrolleduser}"/>

    <tr><td colspan="2" align="right" style="height: 30">
            <input type="button" value="New Ticket"  onclick="location.href='TicketAction.do?method=add'"  style="width: 200px"/>
        </td></tr>
    <tr><td  colspan="2" style="height: 30">&nbsp;</td></tr>
    <c:choose>
        <c:when test="${!empty enrolleduser}">
            <tr><td colspan="2">
                    <c:choose>
                        <c:when test="${!empty sessionScope.TicketList.tickets}">
                            <display:table class="display" id="data" name="sessionScope.TicketList.tickets" requestURI="/TicketData.do" pagesize="15" >
                                <%@include file="../WEB-INF/jspf/admin/ticketDataBody.jspf" %>
                            </display:table>
                        </c:when>
                        <c:otherwise>
                            No data to display. 
                        </c:otherwise>
                    </c:choose>
                </td></tr>
            </c:when>
            <c:otherwise>
                <%@include file="../WEB-INF/jspf/user/applyReq.jspf" %>
            </c:otherwise>
        </c:choose>
