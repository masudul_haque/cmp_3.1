<%@page import="com.mangoca.cmp.model.Certificate"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib  uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>

<c:set var="applySuspend" value="<%=Certificate.CERT_APPLY_SUSPEND%>"/>
<c:set var="certActive" value="<%=Certificate.CERT_ACTIVE%>"/>
<c:set var="certSuspended" value="<%=Certificate.CERT_SUSPENDED%>"/>
<tr>
    <td colspan="2">
        <p>
            <bean:message key="suspension.user.msg.header"/>
        </p></td>
</tr>
<c:set var="certStatus" value="${enrolleduser.certificate.certStatus}"/>
<c:choose>
    <%@include file="../WEB-INF/jspf/user/certNewSuspend.jspf"%>
    <%@include file="../WEB-INF/jspf/user/certRevokeTest.jspf" %>
    <c:when test="${certStatus==certActive}">
        <%@include file="../WEB-INF/jspf/user/enrollInfo.jspf" %>
        <script language="javascript" type="text/javascript">
            function confirmSubmit() {
                // parent.overview.refresh = true;
                document.forms[0].action="SuspensionAction.do?method=save"; 
                document.forms[0].submit();
                return confirm('Are you sure to submit?'); 
            }	 
        </script>
        <html:form action="SuspensionAction">
            <tr>
                <td colspan="2" align="center" height="3">&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 33%" id="item"><b>Suspension Reasons:</b>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <%@include file="../WEB-INF/jspf/user/requestReasons.jspf" %>
                </td>
            </tr>


            <tr>
                <td style="width: 33%" id="item">Comment</td>
                <td><html:textarea property="suspendComment"  rows="15" cols="30"/></td>
            </tr>
            <tr>
                <td style="height: 5">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <html:submit style="width: 100px" value="Submit" onclick="confirmSubmit()"/>
                </td>
            </tr>
        </html:form>
    </c:when>
    <c:when test="${certStatus==certSuspended}">

        <%@include file="../WEB-INF/jspf/user/enrollInfo.jspf" %>
        <tr>
            <td colspan="2" style="height: 15"><bean:message key="suspension.user.msg.suspended"/></td>
        </tr>
        <%@include file="../WEB-INF/jspf/user/suspensionInfo.jspf" %>
    </c:when>
    <c:when test="${certStatus==applySuspend}">

        <%@include file="../WEB-INF/jspf/user/enrollInfo.jspf" %>
        <tr>
            <td colspan="2" id="item" style="text-align: left;width: 100%"><br/>
                <bean:message key="suspension.user.msg.apply.suspension"/></td>
        </tr>
        <%@include file="../WEB-INF/jspf/user/suspensionInfo.jspf" %>
    </c:when>
    <c:otherwise>
        <%@include file="../WEB-INF/jspf/user/applyReq.jspf" %>
    </c:otherwise>
</c:choose>