<%-- 
    Document   : registration
    Created on : Feb 3, 2012, 8:25:24 PM
    Author     : Masudul Haque
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
  String http="https://";
  if(request.getServerName().equals("localhost")){
      http="http://";
  }
   StringBuffer stringBuffer= new StringBuffer();
   stringBuffer.append(http).append(request.getServerName());
   stringBuffer.append(":"+request.getServerPort()).append(request.getContextPath());
   stringBuffer.append("/servlet/captcha");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib  uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>

        <script>
            function addUserAccount(){
                document.forms[0].action="UserAccountAction.do?method=save"; 
                document.forms[0].submit();
            }
        </script>
        <html:form action="UserAccountAction">
                <tr>
                    <td id="item"><bean:message key="label.fullName"/></td>
                    <td><html:text property="fullName"/></td>
                    <td style="color: red"><html:errors property="fullName"/></td>
                </tr>
                <%@include file="../WEB-INF/jspf/common/addCommonAccount.jspf" %>
                <tr>
                    <td id="item"><bean:message key="label.userType"/></td>
                    <td><html:select  property="userType">
                            <html:option value="Individual">Individual</html:option>
                            <html:option value="Company">Company</html:option>
                            <html:option value="Government">Government</html:option>
                        </html:select></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td id="item"><bean:message key="label.raSelect"/></td>
                    <td><html:select property="raSelect">
                            <c:forEach items="${raUsers}" var="user">
                                <html:option value="${user.raName}"/>
                            </c:forEach>
                        </html:select></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td id="item">&nbsp;</td>
                    <td>
                    &nbsp;&nbsp;<img src="<%=stringBuffer%>" border="0"
								alt="Captcha image" width="130" /></td>
                </tr>
                <tr>
                    <td  id="item"><bean:message key="label.captcha"/></td>
                    <td><html:text property="captchaText"/></td>
                     <td style="color: red"><%
                     String captchaFailed=(String)request.getAttribute("captchaFailed");
                     if(captchaFailed!=null){
                         out.print(captchaFailed);
                     }
                     %>
                    <html:errors property="captchaText"/></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><html:submit style="width:100px" onclick="addUserAccount()"/></td>
                    <td>&nbsp;</td>
                </tr>
       </html:form>

