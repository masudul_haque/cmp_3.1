<%-- 
    Document   : userDashboard
    Created on : Nov 25, 2011, 10:11:56 AM
    Author     : hp
--%>

<%@page import="com.mangoca.cmp.model.UserAccount"%>
<%@page import="com.mangoca.cmp.model.Techaction"%>
<%@page import="com.mangoca.cmp.model.Vaaction"%>
<%@page import="com.mangoca.cmp.model.Accaction"%>
<%@page import="com.mangoca.cmp.model.Raaction"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<c:set var="ra" value="<%=Raaction.STATUS_COMPLETE%>"/>
<c:set var="acc" value="<%=Accaction.STATUS_COMPLETE%>"/>
<c:set var="va" value="<%=Vaaction.STATUS_COMPLETE%>"/>
<c:set var="tech" value="<%=Techaction.STATUS_COMPLETE%>"/>
<c:set var="mailed" value="<%=UserAccount.MAILED%>"/>
<c:set var="downloaded" value="<%=UserAccount.DOWNLOADED%>"/>
<script>
    $(document).ready(function(){
        $("#formTable").hide();
        $("#link").click(function (){
            $("#formTable").slideToggle("slow");
        });
    });
</script>

<c:if test="${!empty notifySize}">
    <tr><td>
            <div class="notificationMsg">
                <div class="notification"></div>
                <p>You have <b><html:link page="/NotificationData.do">${notifySize}</html:link></b> notification.</p>
                </div>              
            </td> 
        </tr>
</c:if>

<tr>
    <td>
        <ul id="col2">
            <li><strong>Step-1:</strong> Once your account is activated. Go to <html:link page="/EnrolleduserAction.do?method=add">Apply</html:link> 
                    section and complete all necessary parts of the online application.
                <c:if test="${!empty sessionScope.enrolleduser}">
                    <div class="message">
                        <div class="success"></div>
                        <p>You have completed this step.</p>
                    </div>
                </c:if>
            </li>

            <li><strong>Step-2:</strong>
                <c:choose>
                    <c:when test="${!empty sessionScope.enrolleduser && ra !=enrolleduser.raaction.status && empty userAccount.docStatus}">
                        <br /><br />
                        Click to got your desired certificate documents. <a href="#" id="link">Click</a>
                        <div id="formTable">
                            <table border="0" width="600px">
                                <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>Class</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${emailControls}" var="email" varStatus="status">
                                        <tr>
                                            <td>${status.count}</td>
                                            <td><bean:message key="value.cert.class.${status.count}"/></td>
                                            <td>
                                                Receive Documents By 
                                                <input type="button" value="Email" onclick="location.href='userAction.do?method=sendAttachment&certClass=${status.count}'"
                                                       style="width: 80px"/> or
                                                <input type="button" value="Download" onclick="location.href='userAction.do?method=download&certClass=${status.count}'"
                                                       style="width: 80px"/>                                  
                                            </td>
                                        </tr>
                                    </c:forEach> 
                                </tbody>
                            </table>
                            <div>
                                
                            </div>
                        </div>

                    </c:when>
                    <c:when test="${mailed == userAccount.docStatus}"> 
                            Your document was emailed to your address.
                        <div>
                            <bean:message key="message.get.after.common"/>
                        </div>
                    </c:when>
                    <c:when test="${downloaded == userAccount.docStatus}">
                            Thank you for downloading the supporting document.                    
                        <div>
                            <bean:message key="message.get.after.common"/>
                        </div>
                    </c:when>    
                    <c:otherwise>
                            <bean:message key="message.get.after.common"/>
                    </c:otherwise>
                </c:choose>
                <c:if test="${ra==enrolleduser.raaction.status}">
                    <div class="message">
                        <div class="success"></div>
                        <p>You have completed this step.</p>
                    </div>
                </c:if> 
            </li>

            <li><strong>Step-3:</strong> Make <strong>Cash Deposit</strong> to the following Bank details 
                at any branch in the country-<br><br><strong>Prime Bank Limited<br>
                </strong>Account Name: 
                Mango Teleservices Limited<br><br>Alternatively you can make cash to 
                Mango RA office in Dhaka at the following address:<br><br><strong>Mango 
                    Teleservices Limited<br></strong>82, Mohakhali C/A, 12th floor, Dhaka, 1212, 
                Bangladesh
                <c:if test="${acc==enrolleduser.accaction.status}">
                    <div class="message">
                        <div class="success"></div>
                        <p>You have completed this step.</p>
                    </div>
                </c:if>  
            </li>

            <li><strong>Step-4:</strong> Your application will be verified and processed by Validation Authority(VA) Team. 
                Physical presence may or may not be required, Mango CA would decide on case to case basis. 
                <c:if test="${va==enrolleduser.vaaction.status}">
                    <div class="message">
                        <div class="success"></div>
                        <p>You have completed this step.</p>
                    </div>
                </c:if>  
            </li>

            <li><strong>Step-5:</strong> Depending on the Certificate type (soft/dongle), registration 
                process completes usually within 5 to 7 working days. MANGO CA holds the right to reject the certificate request if it 
                finds the application is not meeting the criteria. You can check your certificate on <html:link page="/userMenuAction.do?method=userStatus">Status</html:link> section.
                <c:if test="${tech==enrolleduser.techaction.status}">
                    <div class="message">
                        <div class="success"></div>
                        <p>You have completed this step.</p>
                    </div>
                </c:if>     
            </li>
        </ul>

    </td>
</tr>