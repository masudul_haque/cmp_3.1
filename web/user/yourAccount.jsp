<%-- 
    Document   : yourAccount
    Created on : Mar 5, 2012, 8:12:49 PM
    Author     : Masudul Haque
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:choose>
    <c:when test="${!empty userAccount}">
        <%@include file="../WEB-INF/jspf/user/accountInfo.jspf" %>
        
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
             <%--   <input type="button" style="width: 80px" value="Edit" 
                       onclick="location.href='userAction.do?method=editAccount'"/>&nbsp;--%>
                <input type="button" style="width: 130px" value="Change Password" 
                       onclick="location.href='PasswordAction.do?method=add'"/>
            </td>
        </tr>
    </c:when> 
    <c:otherwise>
        <tr>
            <td colspan="2">No information is available.</td>
        </tr>
    </c:otherwise>
</c:choose>
