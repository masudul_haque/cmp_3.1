<%-- 
    Document   : certificate
    Created on : Mar 13, 2012, 1:12:08 PM
    Author     : Masudul Haque
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>


<c:choose>
    <c:when test="${!empty enrolleduser}">

        <c:set var="cert" value="${enrolleduser.certificate}"/>
        <%@include file="../WEB-INF/jspf/user/certificate.jspf" %>
    </c:when>
    <c:otherwise>
        <%@include file="../WEB-INF/jspf/user/applyReq.jspf" %>
    </c:otherwise>
</c:choose>
