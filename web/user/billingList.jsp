<%-- 
    Document   : billingList
    Created on : Mar 5, 2012, 7:47:30 PM
    Author     : Masudul Haque
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>


    <c:choose>
        <c:when test="${!empty enrolleduser}">
            <%@include file="../WEB-INF/jspf/user/billing.jspf" %>
        </c:when>
        <c:otherwise>
            <%@include file="../WEB-INF/jspf/user/applyReq.jspf" %>
        </c:otherwise>
    </c:choose>