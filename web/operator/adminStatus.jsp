<%-- 
    Document   : adminStatus
    Created on : Mar 14, 2012, 1:28:12 PM
    Author     : Masudul Haque
--%>

<%@page import="com.mangoca.cmp.model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<c:set var="admin" value="<%=User.ADMIN_ADMIN%>"/>
<c:set var="ra" value="<%=User.ADMIN_RA%>"/>
<c:set var="acc" value="<%=User.ADMIN_ACC%>"/>
<c:set var="va" value="<%=User.ADMIN_VA%>"/>
<c:set var="tech" value="<%=User.ADMIN_TECH%>"/>
<c:set var="cc" value="<%=User.ADMIN_CC%>"/>

 <c:if test="${targetAdminRole ==ra || targetAdminRole==tech || targetAdminRole==acc || targetAdminRole==va}">
    <%@include file="../WEB-INF/jspf/admin/adminReqest.jspf"%> 
</c:if>

<c:if test="${targetAdminRole==ra || targetAdminRole==tech || targetAdminRole==cc}">

    <%@include file="../WEB-INF/jspf/admin/revokeReqests.jspf"%> 
    <%@include file="../WEB-INF/jspf/admin/suspendReqests.jspf"%> 
    <%@include file="../WEB-INF/jspf/admin/renewalReqests.jspf"%> 
</c:if>