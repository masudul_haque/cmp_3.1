<%-- 
    Document   : addAdmin
    Created on : Mar 8, 2012, 1:10:55 PM
    Author     : Masudul Haque
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<script>
    function confirmSubmit(){
        document.forms[0].action="AdminAccountAction.do?method=save"; 
        document.forms[0].submit();
        return confirm('Are you sure to submit?'); 
    }
</script>
 <html:form action="AdminAccountAction">
        <%@include file="../WEB-INF/jspf/operator/addAdminBody.jspf" %> 
        <tr>
        <td colspan="2" height="5">&nbsp;</td>
    </tr> 
        <tr>
            <td align="center" colspan="2">
                <html:submit style="width: 100px" onclick="confirmSubmit()"/>
            </td>
    </tr> 
    </html:form>
