<%-- 
    Document   : adminEdit
    Created on : Mar 8, 2012, 1:09:43 PM
    Author     : Masudul Haque
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<script>
    function confirmSubmit(){
        document.forms[0].action="AdminAccountAction.do?method=save"; 
        document.forms[0].submit();
        return confirm('Are you sure to submit?'); 
    }
</script>
<c:set var="admin" value="${requestScope.otherAdmin}"/>
<c:set var="userAccount" value="${requestScope.otherAdmin}"/>
    <html:form action="AdminAccountAction">
        <input type="hidden" name="id" value="${admin.id}"/>
        <%@include file="../WEB-INF/jspf/operator/editAdminBody.jspf" %> 
        <tr>
        <td colspan="2" height="5">&nbsp;</td>
    </tr> 
        <tr>
            <td align="center" colspan="2">
                <html:submit style="width: 100px" onclick="confirmSubmit()"/>
            </td>
    </tr> 
    </html:form>
