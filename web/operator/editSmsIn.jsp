<%-- 
    Document   : mangocertEdit
    Created on : Mar 17, 2012, 8:47:47 PM
    Author     : Masudul Haque
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>


<html:form action="SmsInAction" styleClass="form">
   <input type="hidden" name="id" value="${smsIn.id}"/>  
    <%@include file="../WEB-INF/jspf/operator/editSmsInBody.jspf" %> 
    <tr>
        <td colspan="2" height="5">&nbsp;</td>
    </tr> 
    <tr>
        <td>&nbsp;</td>
        <td>
            <html:submit style="width: 100px"/>
        </td>
    </tr> 
</html:form>
