<%-- 
    Document   : empuserEdit
    Created on : Mar 8, 2012, 1:10:05 PM
    Author     : Masudul Haque
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<html:form action="AdminUserAction" styleClass="form">
        <input type="hidden" name="id" value="${userAccount.id}"/>
        <%@include file="../WEB-INF/jspf/operator/editEmpuserBody.jspf" %> 
        <tr>
        <td colspan="2" height="5">&nbsp;</td>
    </tr> 
        <tr>
            <td>&nbsp;</td>
            <td>
                <html:submit value="Submit" style="width: 100px"/>
            </td>
    </tr> 
    </html:form>
