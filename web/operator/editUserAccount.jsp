<%-- 
    Document   : empuserEdit
    Created on : Mar 8, 2012, 1:10:05 PM
    Author     : Masudul Haque
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<script>
    function confirmSubmit(){
        document.forms[0].action="UserAccountAction.do?method=save"; 
        document.forms[0].submit();
        return confirm('Are you sure to submit?'); 
    }
</script>

    <html:form action="UserAccountAction">
        <input type="hidden" name="id" value="${userAccount.id}"/>
        <%@include file="../WEB-INF/jspf/operator/editEmpuserBody.jspf" %> 
        <tr>
        <td colspan="2" height="5">&nbsp;</td>
    </tr> 
        <tr>
            <td>&nbsp;</td>
            <td>
                <html:submit style="width: 100px" onclick="confirmSubmit()"/>
            </td>
    </tr> 
    </html:form>
