<%-- 
    Document   : mangocertEdit
    Created on : Mar 17, 2012, 8:47:47 PM
    Author     : Masudul Haque
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<script language="javascript" type="text/javascript">
    function confirmSubmit() {
        // parent.overview.refresh = true;
        document.forms[0].action="MangocertAction.do?method=save"; 
        document.forms[0].submit();
        return confirm('Are you sure to save?'); 
    }	 
</script>

<html:form action="MangocertAction">
   <input type="hidden" name="id" value="${mangocert.id}"/>  
    <%@include file="../WEB-INF/jspf/operator/editMangocertBody.jspf" %> 
    <tr>
        <td colspan="2" height="5">&nbsp;</td>
    </tr> 
    <tr>
        <td>&nbsp;</td>
        <td>
            <html:submit style="width: 100px" onclick="confirmSubmit()"/>
        </td>
    </tr> 
</html:form>
