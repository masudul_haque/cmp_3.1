<%-- 
    Document   : adminDetails
    Created on : Mar 8, 2012, 1:09:54 PM
    Author     : Masudul Haque
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:choose>
    <c:when test="${!empty otherAdmin}">
        <c:set var="user" value="${otherAdmin}"/> 
        <%@include file="../WEB-INF/jspf/admin/adminAccountInfo.jspf" %>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
        </tr>
    </c:when>
    <c:when test="${!empty payment}">
        <c:set var="docPay" value="${payment}"/>
        <%@include file="../WEB-INF/jspf/common/docPaySummery.jspf" %>
        <%@include file="../WEB-INF/jspf/common/paymentInfo.jspf"%> 
    </c:when>

    <c:when test="${!empty document}">
        <c:set var="docPay" value="${document}"/>
        <%@include file="../WEB-INF/jspf/common/docPaySummery.jspf" %>
        <%@include file="../WEB-INF/jspf/common/docInfo.jspf"%> 
        <tr>
            <td>&nbsp;</td>
            <td style="width: 70%">
                <html:image  src="${document.documentLink}" alt="No image"/>
            </td>
        </tr>
        <c:if test="${userRole==ra}">
            <tr>
                <td>&nbsp;</td>
                <td>
                    <html:button property="" value="Edit" style="width:80px" 
                                 onclick="location.href='DocumentAction.do?method=edit&documentId=${document.documentId}'"/>
                </td>
            </tr>  
        </c:if>
    </c:when>
    <c:when test="${!empty enrolleduser}">
            <%@include file="../WEB-INF/jspf/user/enrollInfo.jspf"%>
            
            <c:set var="userbasic" value="${enrolleduser.userbasic}"/>
            <%@include file="../WEB-INF/jspf/user/basicInfo.jspf"%>
            
            <c:set var="admin" value="${enrolleduser}"/>
            <%@include file="../WEB-INF/jspf/common/commonAdminInfo.jspf"%> 
        </c:when>
            
    <c:when test="${!empty mangocert}">
        <%@include file="../WEB-INF/jspf/operator/mangocertInfo.jspf" %>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="button" style="width: 80px" value="Edit" 
                       onclick="location.href='MangocertAction.do?method=edit&id=${mangocert.id}'"/>
            </td>
        </tr>
    </c:when>
    <c:otherwise>
        <tr>
            <td colspan="2">No information is available.</td>
        </tr>
    </c:otherwise>
</c:choose>
 