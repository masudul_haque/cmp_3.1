<%-- 
    Document   : userData
    Created on : Mar 8, 2012, 11:36:15 AM
    Author     : Masudul Haque
--%>
<%@page import="com.mangoca.cmp.model.AdminAccount"%>
<%@page import="com.mangoca.cmp.model.Mangocert"%> 
<%@page import="com.mangoca.cmp.database.Constants"%>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<c:set var="admin_account" value="<%=Constants.ADMIN_ACCOUNT%>"/>
<c:set var="enrolled_user" value="<%=Constants.ENROLLED_USER%>"/>
<c:set var="mango_cert" value="<%=Constants.MANGO_CERT%>"/>
<c:set var="user_account" value="<%=Constants.USER_ACCOUNT%>"/>
<c:set var="document" value="<%=Constants.DOCUMENT%>"/>
<c:set var="payment" value="<%=Constants.PAYMENT%>"/>
<c:set var="revocation" value="<%=Constants.REVOCATION%>"/>
<c:set var="suspension" value="<%=Constants.SUSPENSION%>"/>
<c:set var="renewal" value="<%=Constants.RENEWAL%>"/>
<c:set var="ticket" value="<%=Constants.TICKET%>"/>
<tr><td>

        <c:choose>
            <c:when test="${page==admin_account}">

                <display:table class="display" id="data" name="collection"  
                requestURI="/AdminAccountData.do"  pagesize="<%=AdminAccount.PAGE_SIZE%>"  partialList="true" size="resultSize">
                    <%@include file="../WEB-INF/jspf/operator/adminDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:when test="${page==user_account}">
                <display:table class="display" id="data" name="sessionScope.UserAccountList.userAccounts" 
                               requestURI="/UserAccountData.do" pagesize="15">
                    <%@include file="../WEB-INF/jspf/operator/empuserDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:when test="${page==enrolled_user && !empty collection}">
                <display:table class="display" id="data" name="collection" 
                               requestURI="/EnrolleduserData.do" pagesize="10" partialList="" size="resultSize">
                    <%@include file="../WEB-INF/jspf/admin/enrolledUserDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:when test="${page==document && !empty collection}">
                <display:table class="display" id="data" name="collection" 
                               requestURI="/DocumentData.do" pagesize="10" partialList="true" size="resultSize">
                    <%@include file="../WEB-INF/jspf/admin/documentDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:when test="${page==mango_cert && !empty collection}">
                <display:table class="display" id="data" name="collection" 
                requestURI="/MangocertData.do" pagesize="<%=Mangocert.PAGE_SIZE%>"  partialList="true" size="resultSize">
                    <%@include file="../WEB-INF/jspf/operator/mangocertDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:when test="${page==payment && !empty collection}">
                <display:table class="display" id="data" name="collection" 
                               requestURI="/PaymentData.do" pagesize="10" partialList="true" size="resultSize">
                    <%@include file="../WEB-INF/jspf/admin/paymentDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:when test="${page==renewal && !empty collection}">
                <display:table class="display" id="data" name="collection" 
                               requestURI="/RenewalData.do" pagesize="10" partialList="true" size="resultSize">
                    <%@include file="../WEB-INF/jspf/admin/renewalDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:when test="${page==revocation && !empty collection}">
                <display:table class="display" id="data" name="collection" 
                               requestURI="/RevocationData.do" pagesize="10" partialList="true" size="resultSize">
                    <%@include file="../WEB-INF/jspf/admin/revokeDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:when test="${page==suspension && !empty collection}">
                <display:table class="display" id="data" name="collection" 
                               requestURI="/SuspensionData.do"  pagesize="10" partialList="true" size="resultSize">
                    <%@include file="../WEB-INF/jspf/admin/suspensionDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:when test="${page==ticket && !empty collection}">
                <display:table class="display" id="data" name="collection" 
                               requestURI="/TicketData.do" pagesize="15" partialList="true" size="resultSize" >
                    <%@include file="../WEB-INF/jspf/admin/ticketDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:otherwise>
                No data to display. 
            </c:otherwise>
        </c:choose>
    </td></tr>
