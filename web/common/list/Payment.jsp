<%-- 
    Document   : paymentData
    Created on : Mar 8, 2012, 11:34:56 AM
    Author     : Masudul Haque
--%>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
    <tr><td  height="20px"> 
            <c:choose>
                <c:when test="${!empty collection}">
                     <display:table class="display" id="data" name="collection" 
                               requestURI="/PaymentData.do" pagesize="10" partialList="true" size="resultSize">
                         <%@include file="../../WEB-INF/jspf/admin/paymentDataBody.jspf" %>
                </display:table>
           </c:when>
                <c:otherwise>
                    No data to display. 
                </c:otherwise>
            </c:choose>
        </td>
    </tr>