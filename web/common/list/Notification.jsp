<%-- 
    Document   : userData
    Created on : Mar 8, 2012, 11:36:15 AM
    Author     : Masudul Haque
--%>
<%@page import="com.mangoca.cmp.model.Notification"%>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<tr><td   height="20px">
        <c:choose>
            <c:when test="${!empty collection}">
                <display:table class="display" id="data" name="collection" requestURI="/NotificationData.do"
                pagesize="<%=Notification.PAGE_SIZE%>" partialList="true" size="resultSize">
                    <%@include file="../../WEB-INF/jspf/admin/notificationDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:when test="${!empty sessionScope.NotificationList.notifications}">
                 <display:table class="display" id="data" name="sessionScope.NotificationList.notifications" 
                requestURI="/NotificationData.do" pagesize="<%=Notification.PAGE_SIZE%>">
                    <%@include file="../../WEB-INF/jspf/admin/notificationDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:otherwise>
                No data to display. 
            </c:otherwise>
        </c:choose>
    </td></tr>
