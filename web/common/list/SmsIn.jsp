<%-- 
    Document   : userData
    Created on : Mar 8, 2012, 11:36:15 AM
    Author     : Masudul Haque
--%>
<%@page import="com.mangoca.cmp.model.SmsIn"%>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<tr><td> 
        <c:choose>
            <c:when test="${!empty collection}">
                      <display:table class="display" id="data" name="collection" 
                requestURI="/SmsInData.do" pagesize="<%=SmsIn.PAGE_SIZE%>"  partialList="true" size="resultSize">
                    <%@include file="../../WEB-INF/jspf/operator/smsInDataBody.jspf" %>
                </display:table>
          
            </c:when>
            <c:otherwise>
                No data to display.
            </c:otherwise>
        </c:choose>
</td></tr>
