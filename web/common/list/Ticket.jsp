<%-- 
    Document   : ccTicketData
    Created on : Mar 6, 2012, 11:26:42 PM
    Author     : Masudul Haque
--%>

<%@page import="com.mangoca.cmp.model.Ticket"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<tr><td colspan="2">
        <c:choose>
            <c:when test="${!empty collection}">
                 <display:table class="display" id="data" name="collection" 
                               requestURI="/TicketData.do" pagesize="<%=Ticket.PAGE_SIZE%>" partialList="true" size="resultSize" >
                     <%@include file="../../WEB-INF/jspf/admin/ticketDataBody.jspf" %>
                </display:table>
           </c:when>
            <c:otherwise>
                No data to display. 
            </c:otherwise>
        </c:choose>
    </td></tr>
