<%-- 
    Document   : enrolledUserData
    Created on : Feb 9, 2012, 11:55:57 AM
    Author     : Masudul Haque
--%>

<%@page import="com.mangoca.cmp.model.Enrolleduser"%>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<tr><td  height="20px"> 
        <c:choose>

            <c:when test="${!empty sessionScope.EnrolleduserList.enrolledusers && empty collection}">
                <display:table class="display" id="data" name="sessionScope.EnrolleduserList.enrolledusers" 
                requestURI="/EnrolleduserData.do" pagesize="<%=Enrolleduser.PAGE_SIZE%>">
                    <%@include file="../../WEB-INF/jspf/admin/enrolledUserDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:when test="${!empty collection}">
                <display:table class="display" id="data" name="collection" requestURI="/EnrolleduserData.do"
                pagesize="<%=Enrolleduser.PAGE_SIZE%>" partialList="" size="resultSize">
                    <%@include file="../../WEB-INF/jspf/admin/enrolledUserDataBody.jspf" %>
                </display:table>
            </c:when>
            <c:otherwise>
                No data to display. 
            </c:otherwise>
        </c:choose>


    </td>
</tr>
