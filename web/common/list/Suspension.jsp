<%-- 
    Document   : revokeData
    Created on : Mar 7, 2012, 10:53:31 AM
    Author     : Masudul Haque
--%>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
    <tr><td  height="20px"> 
            <c:choose>
                 <c:when test="${!empty collection}">
                   <display:table class="display" id="data" name="collection" 
                               requestURI="/SuspensionData.do"  pagesize="10" partialList="true" size="resultSize">
                       <%@include file="../../WEB-INF/jspf/admin/suspensionDataBody.jspf" %>
                </display:table>
             </c:when>
                <c:otherwise>
                    No data to display. 
                </c:otherwise>
            </c:choose>
        </td>
    </tr>