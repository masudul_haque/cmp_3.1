<%-- 
    Document   : commonMenu
    Created on : Mar 10, 2012, 11:04:14 AM
    Author     : Masudul Haque
--%> 

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.mangoca.cmp.model.User"%>
<c:set var="admin" value="<%=User.ADMIN_ADMIN%>"/>
<c:set var="cc" value="<%=User.ADMIN_CC%>"/>
<c:choose>
    <c:when test="${!empty userAccount}">
        <%@include file="userMenu.jsp" %>
    </c:when>
    <c:when test="${!empty userRole}">
        <%@include file="adminRoleMenu.jsp" %>
    </c:when>
    <c:otherwise>
        <%@include file="guestMenu.jsp" %>
    </c:otherwise>
</c:choose>


