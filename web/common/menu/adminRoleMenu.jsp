<%@page import="com.mangoca.cmp.model.User"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<c:set var="cc" value="<%=User.ADMIN_CC%>"/>
<c:set var="operator" value="<%=User.ADMIN_ADMIN%>"/>
<c:choose>
    <c:when test="${userRole==cc}">        
        <%@include file="ccMenu.jsp" %>
    </c:when>
    <c:when test="${userRole==operator}">
        <%@include file="operatorMenu.jsp" %>
    </c:when>
    <c:otherwise>
        <%@include file="adminMenu.jsp" %>
    </c:otherwise>
</c:choose>
