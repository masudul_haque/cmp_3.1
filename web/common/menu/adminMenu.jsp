	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.mangoca.cmp.model.User"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<c:set var="acc" value="<%=User.ADMIN_ACC%>"/>
<table cellpadding="0" cellspacing="0" style="width: 980px">
    <tr>
        <td style="width: 250px" valign="top">

            <ul id="left_menu_seperator"><li><a href="#top"><bean:message key="user.role.${userRole}"/></a></li></ul>

            <ul id="left_menu">
                <li ><span lang="en-ca">
                        <html:link page="/adminMenuAction.do?method=adminHome">Home</html:link></span></li>
                <li><span lang="en-ca">
                        <html:link page="/EnrolleduserAction.do?method=list">Users</html:link></span></li>
                <li><span lang="en-ca">        
                        <html:link page="/DocumentAction.do?method=list">Documents</html:link></span></li>
                <c:if test="${userRole==acc}">       
                <li><span lang="en-ca">        
                        <html:link page="/PaymentAction.do?method=list">Payments</html:link></span></li>
                </c:if>
                <li><span lang="en-ca">        
                        <html:link page="/NotificationAction.do?method=list">Notifications</html:link></span></li>
                </ul>
            <%@include file="commonMenu.jspf" %> 
        </td>
        <td style="width: 32px" valign="top">

            <img alt="" height="32" src="images/spacer.gif" width="32" align="top" /></td>
        <td valign="top">