
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>		
<table cellpadding="0" cellspacing="0" style="width: 980px">
    <tr>
        <td style="width: 250px" valign="top">
              
            <ul id="left_menu_seperator"><li><a href="#top">CC</a></li></ul>

            <ul id="left_menu">
                <li ><span lang="en-ca">
                        <html:link page="/adminMenuAction.do?method=adminHome">Home</html:link></span></li> 
            </ul>
            <ul id="left_menu_seperator"><li><a href="#top">Ticket</a></li></ul>

            <ul id="left_menu">
                <li ><span lang="en-ca">
                        <html:link page="/TicketAction.do?method=ccOpen">Open</html:link></span></li>
                <li ><span lang="en-ca">
                        <html:link page="/TicketAction.do?method=ccReOpen">Re-open</html:link></span></li>
                <li ><span lang="en-ca">
                        <html:link page="/TicketAction.do?method=ccClosed">Closed</html:link></span></li>
             </ul>
            <ul id="left_menu_seperator"><li><a href="#top">List</a></li></ul>

            <ul id="left_menu">           
                <li><span lang="en-ca">
                        <html:link page="/EnrolleduserAction.do?method=list">Users</html:link></span></li>
                <li><span lang="en-ca">        
                        <html:link page="/DocumentAction.do?method=list">Documents</html:link></span></li>
            </ul>
            <%@include file="commonMenu.jspf" %>
        </td>
        <td style="width: 32px" valign="top">

            <img alt="" height="32" src="images/spacer.gif" width="32" align="top" /></td>
        <td valign="top">