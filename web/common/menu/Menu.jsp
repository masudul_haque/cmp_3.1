<%@page import="com.mangoca.cmp.model.User"%>  

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="active" value="<%=User.USER_ACTIVE%>"/>
<c:set var="enrolled" value="<%=User.USER_ENROLL%>"/>

<c:set var="admin" value="<%=User.ADMIN_ADMIN%>"/>

<c:set var="cc" value="<%=User.ADMIN_CC%>"/>


<c:set var="ra" value="<%=User.ADMIN_RA%>"/>
<c:set var="acc" value="<%=User.ADMIN_ACC%>"/>
<c:set var="va" value="<%=User.ADMIN_VA%>"/>
<c:set var="tech" value="<%=User.ADMIN_TECH%>"/>
<c:choose>
    <c:when test="${!empty userRole}">
        <c:choose>
            <c:when test="${userRole==active || userRole==enrolled}">
                <%@include file="userMenu.jsp" %>
            </c:when>
            <c:when test="${userRole==admin}">
                <%@include file="operatorMenu.jsp" %>
            </c:when>
            <c:when test="${userRole==cc}">
                <%@include file="ccMenu.jsp" %>
            </c:when>
            <c:when test="${userRole==ra || userRole==acc || userRole==va || userRole==tech}">
                <%@include file="adminMenu.jsp" %>
            </c:when>
            <c:otherwise>
                <%@include file="guestMenu.jsp" %>
            </c:otherwise>
        </c:choose>
    </c:when>
    <c:otherwise>
        <c:choose>
                <%@include file="guestMenu.jsp" %>
        </c:choose>

    </c:otherwise>
</c:choose>
