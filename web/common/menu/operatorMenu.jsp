
<%@page import="com.mangoca.cmp.model.User"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<c:set var="admin" value="<%=User.ADMIN_ADMIN%>"/>
<c:set var="ra" value="<%=User.ADMIN_RA%>"/>
<c:set var="acc" value="<%=User.ADMIN_ACC%>"/>
<c:set var="va" value="<%=User.ADMIN_VA%>"/>
<c:set var="tech" value="<%=User.ADMIN_TECH%>"/>
<c:set var="cc" value="<%=User.ADMIN_CC%>"/>
<table cellpadding="0" cellspacing="0" style="width: 980px">
    <tr>
        <td style="width: 250px" valign="top">

            <ul id="left_menu_seperator" id="deb1"><li><a href="#top">Super Admin</a></li></ul>

            <ul id="left_menu" id="deb11">
                <li ><span lang="en-ca">
                        <html:link page="/AdminAccountAction.do?method=list">Authority</html:link></span></li>
                <li ><span lang="en-ca">
                        <html:link page="/MangocertAction.do?method=list">Products</html:link></span></li>
                <li ><span lang="en-ca">
                        <html:link page="/MangoSSLCertAction.do?method=list">SSL Products</html:link></span></li>
                <li ><span lang="en-ca">
                        <html:link page="/EmailControlAction.do?method=list">Email</html:link></span></li>
                <li ><span lang="en-ca">
                        <html:link page="/AdminUserAction.do?method=list">All Users</html:link></span></li>
                
            <ul id="left_menu_seperator" id="deb1"><li><a href="#top">List</a></li></ul>
                <li><span lang="en-ca">
                        <html:link page="/EnrolleduserAction.do?method=list">Users</html:link></span></li>
                <li><span lang="en-ca">        
                        <html:link page="/DocumentAction.do?method=list">Documents</html:link></span></li>
                <li><span lang="en-ca">        
                        <html:link page="/PaymentAction.do?method=list">Payments</html:link></span></li>
                <li><span lang="en-ca">        
                        <html:link page="/NotificationAction.do?method=list">Notifications</html:link></span></li>
                <li><span lang="en-ca">        
                        <html:link page="/SmsInAction.do?method=list">SMS Inbox</html:link></span></li>
                <li><span lang="en-ca">        
                        <html:link page="/SmsOutAction.do?method=list">SMS Outbox</html:link></span></li>
            </ul>
            <ul id="left_menu_seperator" id="deb2"><li><a href="#top">Status</a></li></ul>
 
            <ul id="left_menu" id="deb21">
                <li ><span lang="en-ca">
                        <html:link page="/operatorMenuAction.do?method=adminStatus&admin=${ra}">
                            <bean:message key="status.user.role.${ra}"/></html:link></span></li>
                <li ><span lang="en-ca">
                        <html:link page="/operatorMenuAction.do?method=adminStatus&admin=${acc}"> 
                            <bean:message key="status.user.role.${acc}"/></html:link></span></li>
                <li ><span lang="en-ca">
                        <html:link page="/operatorMenuAction.do?method=adminStatus&admin=${va}">
                            <bean:message key="status.user.role.${va}"/></html:link></span></li>   
                <li ><span lang="en-ca">
                        <html:link page="/operatorMenuAction.do?method=adminStatus&admin=${tech}">
                            <bean:message key="status.user.role.${tech}"/></html:link></span></li>  
                <li ><span lang="en-ca">
                        <html:link page="/operatorMenuAction.do?method=ccStatus">
                            <bean:message key="status.user.role.${cc}"/></html:link></span></li>
            </ul>
            <%@include file="commonMenu.jspf" %> 
        </td>
        <td style="width: 32px" valign="top">

            <img alt="" height="32" src="images/spacer.gif" width="32" align="top" /></td>
        <td valign="top">