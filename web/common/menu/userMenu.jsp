<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%> 
<table cellpadding="0" cellspacing="0" style="width: 980px">
    <tr>
        <td style="width: 250px" valign="top">

            <ul id="left_menu_seperator"><li><a href="#top">User</a></li></ul>

            <ul id="left_menu">
                <li ><span lang="en-ca">
                        <html:link page="/userMenuAction.do?method=userHome">Home</html:link>
                        </span></li>
                    <li><span lang="en-ca">
                        <html:link page="/EnrolleduserAction.do?method=add">Apply</html:link>
                        </span></li> 
                    <li><span lang="en-ca">
                        <html:link page="/userMenuAction.do?method=userStatus">Status</html:link>
                        </span></li>  
                </ul>
                <ul id="left_menu_seperator"><li><a href="#top">Application</a></li></ul>

                <ul id="left_menu">  
                    <li><span lang="en-ca">
                        <html:link page="/RevocationAction.do?method=add">Revoke</html:link>
                        </span></li>
                    <li><span lang="en-ca">
                        <html:link page="/SuspensionAction.do?method=add">Suspend</html:link> 
                        </span></li>
                    <li><span lang="en-ca">
                        <html:link page="/RenewalAction.do?method=add">Renewal</html:link>
                        </span></li> 
                </ul>
                <ul id="left_menu_seperator"><li><a href="#top">Misc</a></li></ul>

                <ul id="left_menu">  
                    <li><span lang="en-ca">
                        <html:link page="/userMenuAction.do?method=billing">Billing</html:link>
                        </span></li>
                    <li><span lang="en-ca">
                        <html:link page="/TicketAction.do?method=userList">Ticket</html:link>
                        </span></li>
                    <li><span lang="en-ca">
                        <html:link page="/userMenuAction.do?method=setting">Setting</html:link>
                        </span></li>
                    <li><span lang="en-ca"><html:link page="/guestAction.do?method=login"><strong>Logout</strong></html:link> 
                    </span></li>
            </ul>




        </td>
        <td style="width: 32px" valign="top">

            <img alt="" height="32" src="images/spacer.gif" width="32" align="top" /></td>
        <td valign="top">