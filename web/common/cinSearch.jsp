<%@page import="com.mangoca.cmp.model.User"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : cinSearch
    Created on : Mar 12, 2012, 7:13:32 PM
    Author     : Masudul Haque
--%>

<c:set var="userAction" value="${requestScope.userAction}"/>
<c:choose>
    <c:when test="${!empty userAction}">
        <%@include file="userAction.jsp" %>
    </c:when>
    <c:otherwise> 
        <script>
            function cinSearch(){
                var userCIN=document.getElementById("userCIN");
                location.href='adminAction.do?method=cinSearch&userCIN='+userCIN.value;
            }
        </script>

        <input type="text" align="right" id="userCIN" style="width: 180px" name="userCIN"/>
        <input type="button" align="right" style="width: 60px" onclick="cinSearch();" value="Search"/>
        <input type="button" align="right" style="width: 110px" 
               onclick="location.href='adminAction.do?method=advanceSearch'" value="Advance Search"/>

        <c:set var="ra" value="<%=User.ADMIN_RA%>"/>
        <c:set var="acc" value="<%=User.ADMIN_ACC%>"/>
        <c:set var="tech" value="<%=User.ADMIN_TECH%>"/>
        <c:set var="admin" value="<%=User.ADMIN_ADMIN%>"/>
        <c:set var="cc" value="<%=User.ADMIN_CC%>"/>
        <c:choose>
            <c:when test="${userRole==ra}">
                <input type="button" value="Add Documents" onclick="location.href='DocumentAction.do?method=add'"
                       style="width: 120px"/>
            </c:when>
            <c:when test="${userRole==acc}">
                <input type="button" value="Add Payment" onclick="location.href='PaymentAction.do?method=add'"
                       style="width: 100px"/>
            </c:when>
            <c:when test="${userRole==tech}">
                <input type="button" value="Add Certificate" onclick="location.href='CertificateAction.do?method=add'"
                       style="width: 120px"/>
            </c:when>
            <c:when test="${userRole==cc}">
                <input type="button" value="Add Ticket" onclick="location.href='TicketAction.do?method=add'"
                       style="width: 100px"/>
            </c:when>
            <c:when test="${userRole==admin}">
                <input type="button" value="Add Admin" onclick="location.href='AdminAccountAction.do?method=add'"
                       style="width: 100px"/>
                <input type="button" value="Add Product" onclick="location.href='MangocertAction.do?method=add'"
                       style="width: 100px"/>
            </c:when>
            <c:otherwise>
            </c:otherwise>
        </c:choose>

    </c:otherwise>
</c:choose>

