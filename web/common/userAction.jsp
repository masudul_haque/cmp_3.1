<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : userAction
    Created on : Mar 15, 2012, 9:49:01 AM
    Author     : Masudul Haque
--%>
<%@page import="com.mangoca.cmp.model.User"%>
<c:set var="admin" value="<%=User.ADMIN_ADMIN%>"/>
<c:set var="acc" value="<%=User.ADMIN_ACC%>"/>
<c:set var="cc" value="<%=User.ADMIN_CC%>"/>
<input type="button" align="right" style="width: 120px" 
       onclick="location.href='adminAction.do?method=goEdit'" value="Edit Enrollment"/>
<input type="button" align="right" style="width: 110px" 
       onclick="location.href='documentAction.do?method=userDocs'" value="Documents"/>
<c:choose>
    <c:when test="${userRole==acc}">
        <input type="button" align="right" style="width: 110px" 
               onclick="location.href='paymentAction.do?method=userPayments'" value="Payments"/>
    </c:when>
    <c:when test="${userRole==cc}">
        <input type="button" align="right" style="width: 110px" 
               onclick="location.href='ccAction.do?method=userTickets'" value="Tickets"/>
    </c:when>
    <c:when test="${userRole==admin}">
        <input type="button" align="right" style="width: 110px" 
               onclick="location.href='paymentAction.do?method=userPayments'" value="Payments"/>
        <input type="button" align="right" style="width: 110px" 
               onclick="location.href='ccAction.do?method=userTickets'" value="Tickets"/>
    </c:when>
</c:choose>

