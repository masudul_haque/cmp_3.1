<%-- 
    Document   : revokeDetails
    Created on : Mar 9, 2012, 11:24:45 AM
    Author     : Masudul Haque
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

    <c:set var="revocation" value="${enrolleduser.revocation}"/>
    <c:set var="admin" value="${revocation}"/>
    <%@include file="../../WEB-INF/jspf/user/enrollInfo.jspf" %>
    <%@include file="../../WEB-INF/jspf/user/revokeInfo.jspf" %>
    <%@include file="../../WEB-INF/jspf/common/commonAppInfo.jspf" %>
