<%-- 
    Document   : empuserDetails
    Created on : Mar 8, 2012, 1:10:21 PM
    Author     : Masudul Haque
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:choose>
    <c:when test="${!empty userAccount}">
        <c:set var="user" value="${userAccount}"/> 
        <%@include file="../../WEB-INF/jspf/user/accountInfo.jspf" %>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="button" style="width: 80px" value="Edit" 
                       onclick="location.href='UserAccountAction.do?method=edit&id=${user.id}'"/>
            </td>
        </tr>
    </c:when>
    <c:otherwise>
        <tr>
            <td colspan="2">No information is available.</td>
        </tr>
    </c:otherwise>
</c:choose>
