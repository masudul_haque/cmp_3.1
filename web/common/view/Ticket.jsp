<%-- 
    Document   : vewPayment
    Created on : Mar 11, 2012, 1:36:31 PM
    Author     : Masudul Haque
--%>

<%@page import="com.mangoca.cmp.model.Ticket"%>
<%@page import="com.mangoca.cmp.model.User"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<c:set var="user_enroll" value="<%=User.USER_ENROLL%>"/>
<c:set var="ticket_closed" value="<%=Ticket.STATUS_CLOSED%>"/>
<c:choose>
    <c:when test="${!empty ticket}">
        <%@include file="../../WEB-INF/jspf/common/newTicketInfo.jspf" %>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <c:if test="${userRole==user_enroll && ticket.status==ticket_closed}">
          <tr>
            <td>&nbsp;</td>
            <td>
                <input type="button" style="width: 80px" value="Reply" 
                       onclick="location.href='TicketAction.do?method=reply&id=${ticket.ticketId}'"/>
            </td>
        </tr>  
        </c:if>
        
    </c:when>
    <c:otherwise>
        <tr>
            <td colspan="2">No information is available.</td>
        </tr>
    </c:otherwise>
</c:choose>
