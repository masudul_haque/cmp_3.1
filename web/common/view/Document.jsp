<%-- 
    Document   : viewDoc
    Created on : Mar 11, 2012, 1:36:03 PM
    Author     : Masudul Haque
--%>


<%@page import="com.mangoca.cmp.model.User"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<c:set var="ra" value="<%=User.ADMIN_RA%>"/>
    <c:choose>
        <c:when test="${!empty document}">
            <c:set var="docPay" value="${document}"/>
            <%@include file="../../WEB-INF/jspf/common/docPaySummery.jspf" %>
            
            <%@include file="../../WEB-INF/jspf/common/docInfo.jspf"%> 
            <tr>
                <td>&nbsp;</td>
                <td style="width: 70%">
                <html:image  src="${document.documentLink}" alt="No image"/>
            </td>
            </tr>
            <c:if test="${userRole==ra}">
              <tr>
                  <td>&nbsp;</td>
            <td>
                <html:button property="" value="Edit" style="width:80px" 
                         onclick="location.href='DocumentAction.do?method=edit&documentId=${document.documentId}'"/>
            </td>
            </tr>  
            </c:if>
            
        </c:when>
        <c:otherwise>
            <tr>
                <td colspan="2" height="3">No user is selected.</td>
            </tr>
        </c:otherwise>
    </c:choose>
