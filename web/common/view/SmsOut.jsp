<%-- 
    Document   : vewPayment
    Created on : Mar 11, 2012, 1:36:31 PM
    Author     : Masudul Haque
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

    
<c:choose>
    <c:when test="${!empty smsOut}">
        <%@include file="../WEB-INF/jspf/operator/smsOutInfo.jspf" %>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input type="button" style="width: 80px" value="Edit" 
                       onclick="location.href='SmsOutAction.do?method=edit&id=${smsOut.id}'"/>
            </td>
        </tr>
    </c:when>
    <c:otherwise>
        <tr>
            <td colspan="2">No information is available.</td>
        </tr>
    </c:otherwise>
</c:choose>
