<%-- 
    Document   : vewPayment
    Created on : Mar 11, 2012, 1:36:31 PM
    Author     : Masudul Haque
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

    <c:choose>
        <c:when test="${!empty payment}">
            <c:set var="docPay" value="${payment}"/>
            <%@include file="../../WEB-INF/jspf/common/docPaySummery.jspf" %>
            
            <%@include file="../../WEB-INF/jspf/common/paymentInfo.jspf"%> 
        </c:when>
        <c:otherwise>
            <tr>
                <td colspan="2" height="3">No user is selected.</td>
            </tr>
        </c:otherwise>
    </c:choose>
