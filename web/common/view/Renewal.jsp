<%-- 
    Document   : renewalDetails
    Created on : Mar 9, 2012, 11:25:04 AM
    Author     : Masudul Haque
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

    <c:set var="renewal" value="${enrolleduser.renewal}"/>
    <c:set var="admin" value="${renewal}"/>
    <%@include file="../../WEB-INF/jspf/user/enrollInfo.jspf" %>
    <%@include file="../../WEB-INF/jspf/user/renewalInfo.jspf" %>
    <%@include file="../../WEB-INF/jspf/common/commonAppInfo.jspf" %>
