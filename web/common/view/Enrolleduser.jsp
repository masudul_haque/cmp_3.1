<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : enrollUserDetails
    Created on : Feb 13, 2012, 2:44:52 PM
    Author     : mango
--%>
<%@page import="com.mangoca.cmp.model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

    

    <c:choose>
        <c:when test="${!empty enrolleduser}">
            <%@include file="../../WEB-INF/jspf/user/enrollInfo.jspf"%>
            
            <c:set var="userbasic" value="${enrolleduser.userbasic}"/>
            <%@include file="../../WEB-INF/jspf/user/basicInfo.jspf"%>
            
            <c:set var="admin" value="${enrolleduser}"/>
            <%@include file="../../WEB-INF/jspf/common/commonAdminInfo.jspf"%> 
        </c:when>
        <c:otherwise>
            <tr>
                <td colspan="2" height="3">No user is selected.</td>
            </tr>
        </c:otherwise>
    </c:choose>