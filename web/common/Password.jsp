<%-- 
    Document   : Password
    Created on : May 22, 2012, 5:24:04 PM
    Author     : Masudul Haque
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>


<html:form action="PasswordAction" styleClass="form">
    <%@include file="../WEB-INF/jspf/common/pwChange.jspf" %>
    
    <c:if test="${!empty failure}">
        <tr>
            <td colspan="2" style="color: red">
                ${failure}
            </td>
        </tr> 
    </c:if>
    <tr>
    <td></td> 
    <td>
        <html:submit value="Change" style="width: 100px"/>
    </td>
</tr>
</html:form>
