<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : mainLayout
    Created on : Mar 9, 2012, 10:58:09 PM
    Author     : Masudul Haque
--%>
<%--
<c:if test="${!login}">
    <c:if test="${empty userRole}">
        <%response.sendRedirect("index.jsp");%>
    </c:if> 
</c:if>--%>

<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <script src="script/jquery-1.5.1.min.js"></script>
    <script src="script/jquery-ui-1.8.14.custom.min.js"></script>
    <script src="script/submit.js"></script>
    <script src="script/jquery.wysiwyg.js"></script>
    <script src="script/simpla.jquery.configuration.js"></script>
    <script src="script/wysiwyg.default.control.js"></script>


    <title><tiles:getAsString name="title"/></title>
    <link href="style/style.css" rel="stylesheet" type="text/css" />
    <link href="style/jquery.wysiwyg.css" rel="stylesheet" type="text/css" />
</head>

<body style="margin: 10px 0;">
    <table align="center" cellpadding="0" cellspacing="0" style="width: 980px">
        <tr><td style="width: 240px">
                <img alt="mango ca logo" height="66" src="images/logo.jpg" width="240" /></td><td id="toper">Mango Teleservices Limited is an 
                accredited Certifying Authority (CA) of 
                Bangladesh<br /><br />
                <a href="http://www.mangoca.com">Home</a>&nbsp; 
                <a href="http://www.mangoca.com/contact.aspx">Contact<strong> Us</strong></a>&nbsp; 
                <a href="#top">Call: <strong>02.</strong>8814507</a>&nbsp; 
                <a href="mailto:info@mangoca.com">info@mangoca.com</a>
                <br />
            </td></tr>
        <tr><td style="width: 240px">
                <img alt="" height="18" src="images/spacer.gif" width="18" /></td><td>&nbsp;</td></tr>
    </table>

    <table align="center" cellpadding="0" cellspacing="0" style="width: 980px">
        <tr>
            <td id="menu_title" class="auto-style7">Customer Management Portal</td>
        </tr>
        <tr><td align="right" height="32">
                <c:if test="${!empty user}">
                    Welcome: <strong>${user.userName}</strong>
                </c:if>
                <c:if test="${!empty userAccount}">
                    CIN: <strong>${userAccount.userCIN}</strong>
                </c:if>                
                <tiles:insert attribute="search"/></td></tr>
        <tr>
            <td valign="top">
                <!--- End of Header-->		
                <tiles:insert attribute="menu"/>

                <c:if test="${!empty success || !empty failure || !empty instruction}">
                    <table width="100%" >

                        <c:if test="${!empty success}">
                            <tr>
                                <td width="100%" >
                                    <div class="successMessage">
                                        <div class="success"></div>
                                        <p><bean:message key="${success}"/></p>
                                    </div>  
                                </td>
                            </tr>   
                        </c:if>
                        <c:if test="${!empty failure}">
                            <tr>
                                <td width="100%" >
                                    <div class="errorMessage">
                                        <div class="error"></div>
                                        <p><bean:message key="${failure}"/></p>
                                    </div> 
                                </td>
                            </tr>
                        </c:if>
                        <c:if test="${!empty instruction}">
                            <tr>
                                <td width="100%" >
                                    <div class="instruction">
                                        <p><bean:message key="${instruction}"/></p>
                                    </div> 
                                </td>
                            </tr>
                        </c:if>

                    </table>
                </c:if>
                <table cellpadding="0" cellspacing="3" style="width: 100%" >

                    <tr>
                        <td>
                            <h2><tiles:getAsString name="title"/>
                            </h2>
                        </td>
                    </tr>
                    <tiles:insert attribute="body"/>   
                </table>       
            </td>
        </tr>
    </table>

</td>
</tr>
</table>
<table align="center" cellpadding="0" cellspacing="0" style="width: 980px">
    <tr >
        <td>
            <img alt="" height="16" src="images/spacer.gif" width="16" /></td>
    </tr>
    <tr >
        <td class="style1">
            <img alt="" height="18" src="images/spacer.gif" width="18" /></td>
    </tr>
    <tr >
        <td >
            <img alt="" height="9" src="images/spacer.gif" width="9" /></td>
    </tr>
</table>

<table align="center" cellpadding="0" cellspacing="0" style="width: 980px">
    <tr>
        <td id="footer">Mango teleservices limited is An 
            accredited Certifying Authority in 
            bangladesh under Controller of certifying authority, bangladesh<br />
            copyright 2011-12 by <a href="http://www.mango.com.bd">Mango teleservices 
                limited</a>&nbsp; All Rights Reserved</td>
    </tr>
</table>

</body>

</html>
